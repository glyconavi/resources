<?php

// * Input_File: "20150717result-GlyTouCan.sdf"
//Structure: 1. ID=G35898DT
//InChI=1S/C62H104N4O44/c1-14-31(76)41(86)45(90)58(96-14)109-52-43(88)34(79)21(8-68)100-61(52)105-48-23(10-70)102-56(28(39(48)84)64-17(4)73)94-12-25-36(81)50(30(54(93)98-25)66-19(6)75)107-60-47(92)51(37(82)26(104-60)13-95-55-27(63-16(3)72)38(83)33(78)20(7-67)99-55)108-57-29(65-18(5)74)40(85)49(24(11-71)103-57)106-62-53(44(89)35(80)22(9-69)101-62)110-59-46(91)42(87)32(77)15(2)97-59/h14-15,20-62,67-71,76-93H,7-13H2,1-6H3,(H,63,72)(H,64,73)(H,65,74)(H,66,75)/t14-,15-,20+,21+,22+,23+,24+,25+,26+,27+,28+,29+,30+,31+,32+,33+,34-,35-,36-,37-,38+,39+,40+,41+,42+,43-,44-,45-,46-,47+,48+,49+,50+,51-,52+,53+,54-,55+,56+,57-,58-,59-,60-,61-,62-/m0/s1
//InChIKey=ACXRYLKERSTZIM-XHUXCUBWSA-N


// $ php 20151105-06-SPARQLthon38_IUPAC-InChI_2_rdf.php ./txt/

include("para.php");
/*
$SPARQLthon = "SPARQLthon40";
*/

include("db.php");
/*
$DB_name         = "glytoucan.org";
$data_entry      = "<http://glytoucan.org/Structures/Glycans/";
$data_entry_type = "glycan:saccharide";
$match           = "/(G[0-9]{5}[a-zA-Z]{2})/";
$publisher       = "<http://glytoucan.org>";
$datasetName     = "<http://glycoinfo.org/structure/inchi/>";
$datasetversion  = "0.2";
$license         = "<http://creativecommons.org/licenses/by/2.1/>";
$landingPage     = "<http://www.wurcs.org/inchi/glytoucan/>";
$picBoolean = true;
$pic1 = "<https://www.glytoucan.org/glyspace/service/glycans/";
$pic2 = "/image?style=extended&format=png&notation=cfg>";

$contributedBy   = "<http://jglobal.jst.go.jp/detail.php?JGLOBAL_ID=200901097596928716>";
$family_name     = "YAMADA";
$givenname       = "ISSAKU";
$mbox            = "<mailto:issaku@noguchi.or.jp>";
*/

include("inchi.php");
/*
$InChI_type      = "IUPAC_InChI";
$software        = $InChI_type."-1.0.4";
$ext             = '.txt';
$data_dir        = "txt";
$CHEMINF_000200  = "cheminf:CHEMINF_000200";
$SIO_000300      = "sio:SIO_000300";
$CHEMINF_000396  = "cheminf:CHEMINF_000396";
$CHEMINF_000399  = "cheminf:CHEMINF_000399";
*/


$dirpath         = dirname(__FILE__);
$res_dir         = opendir( $dirpath.DIRECTORY_SEPARATOR.$data_dir);
date_default_timezone_set('Asia/Tokyo');
$fileHeader      = date("Y-m-d_H-i-s");

$today = date("F j, Y, g:i a");


$wfp = fopen($dirpath.DIRECTORY_SEPARATOR.$fileHeader."_".$DB_name."_id-".$InChI_type.".ttl", "a");









$prefix = "@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n";
$prefix .= "@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n";
$prefix .= "@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n";
$prefix .= "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n";
//$prefix .= "@prefix wurcs: <http://www.glycoinfo.org/glyco/owl/wurcs#> .\n";

if($prefix_glycan){
    $prefix .= "@prefix glycan: <http://purl.jp/bio/12/glyco/glycan#> .\n";
}
//$prefix .= "@prefix glytoucan: <http://www.glytoucan.org/glyco/owl/glytoucan#> .\n";
$prefix .= "@prefix dcterms: <http://purl.org/dc/terms/> .\n";
$prefix .= "@prefix void:  <http://rdfs.org/ns/void#> .\n";
$prefix .= "@prefix pav:   <http://purl.org/pav/> .\n";
$prefix .= "@prefix dcat:  <http://www.w3.org/ns/dcat#> .\n";
$prefix .= "@prefix foaf: <http://xmlns.com/foaf/0.1/> .\n";

//$prefix .= "@prefix inchi: <http://glyconavi.org/inchi/> .\n";
$prefix .= $inchi_prefix." .\n";

$prefix .= "@prefix sio: <http://semanticscience.org/resource/> .\n";
$prefix .= "@prefix cheminf: <http://semanticscience.org/resource/> .\n";


$prefix .= "\n\n";
fwrite($wfp, $prefix);


if(strlen($DB_name) > 0 ) {
    $dataset = "<".$datasetName.">
            a                           void:Dataset ;
            dcterms:description        \"".$DB_name.": ".$software." and InChIKey\"@en ;
            dcterms:license             ".$license." ;
            dcterms:publisher           ".$publisher." ;
            dcterms:title              \"".$DB_name.": ".$software." and InChIKey ".$today."\"@en ;
            pav:contributedBy           ".$contributedBy." ;
            pav:version                \"".$datasetversion."\";
            dcterms:hasVersion         <".$datasetName.$datasetversion."> ;
            dcat:landingPage           ".$landingPage ." .\n";

    if (strlen($contributedBy) > 0 ){

            $dataset .= $contributedBy."\n";
            $dataset .= "\ta\tfoaf:Person ;\n";
            if (strlen($mbox) > 0 ) {
                $dataset .= "\tfoaf:mbox         ".$mbox." ;\n";
            }
            $dataset .= "\tfoaf:family_name \"".$family_name."\" ;\n";
            $dataset .= "\tfoaf:givenname   \"".$givenname."\" .\n";
    }

    $dataset .= "\n\n";  
    fwrite($wfp, $dataset);
}



fwrite($wfp, "<".$datasetName.">\n\towl:versionInfo \"".$DB_name." ".$software."@ ".$SPARQLthon."\"^^xsd:string .\n");


fwrite($wfp, ""."\n");

$id = "";

while( $file_name = readdir( $res_dir ) ){
    if (strstr($file_name, $ext)) {


        echo $file_name."\n";

        $fp = fopen($dirpath.DIRECTORY_SEPARATOR.$data_dir.DIRECTORY_SEPARATOR.$file_name,"r");

        while($datas = fgets($fp)){

            // 改行で分割
            $array = explode("\n", $datas); // とりあえず行に分割
            $array = array_map('trim', $array); // 各要素をtrim()にかける
            $array = array_filter($array, 'strlen'); // 文字数が0のやつを取り除く
            $array = array_values($array); // これはキーを連番に振りなおしてるだけ

            // Structure: 1. ID=C00000001
            // InChI=1S/C19H24O6/c1-9-7-17-8-18(9,24)5-3-10(17)19-6-4-11(20)16(2,15(23)25-19)13(19)12(17)14(21)22/h10-13,20,24H,1,3-8H2,2H3,(H,21,22)/t10-,11+,12-,13-,16-,17+,18+,19-/m1/s1
            // InChIKey=JLJLRLWOEMWYQK-OBDJNFEBSA-N

            foreach ($array as $data) {

                preg_match($match, $data, $result);
                if (count($result)) {
//                if(ereg("/(G[0-9]{5}[a-zA-Z]{2})/", $data, $result)){
//                if (preg_match("/(G[0-9]{5}[a-zA-Z]{2})/", $result)) {
                    $id = $result[0];

                    echo $id."\n";

                    fwrite($wfp, $data_entry.$id.">"."\n");
                    fwrite($wfp, "\ta ".$data_entry_type." ;\n");
                    fwrite($wfp, "\t".$CHEMINF_000200."\n");
                    fwrite($wfp, "\t".$data_entry.$id."_".$InChI_type.">, ".$data_entry.$id."_".$InChI_type."Key> ;\n");
                    
                    if ($picBoolean == true) {
                        fwrite($wfp, "\t"."foaf:depiction ".$pic1.$id.$pic2." ;\n");
                    }
                    fwrite($wfp, "\t"."dcterms:identifier \"".$id."\" .\n");
                    fwrite($wfp, "\n\n");
                }

                if (strstr($data, 'InChI=')) {
                    $inchi = trim($data);
                    
                    // 2015-09-15
                    // parse of InChI Code


                    fwrite($wfp, $data_entry.$id."_".$InChI_type.">\n");
                    fwrite($wfp, "\t".$SIO_000300." \"".$inchi."\" ;\n");
//                    fwrite($wfp, "\t"."rdf:type ".$CHEMINF_000396." .\n");

                    $inchiArray = explode("InChI=1S/", $inchi); // とりあえず行に分割


//                    $Main_layer   = "";
                    $Formula                      = "";
                    $Connections                  = "";
                    $H_atoms                      = "";
//                    $Charge_layer = "";
                    $Charge                       = "";
                    $Protons                      = "";
//                    $Stereo_layer = "";
                    $Stereo_dbond                 = "";
                    $Stereo_sp3                   = "";
                    $Stereo_sp3_inverted          = "";
                    $Stereo_type                  = "";
//                    $Isotopic_layer = "";
                    $Isotopic_atoms               = "";
                    $Isotopic_exchangeable_H      = "";
                    $Isotopic_stereo_dbond        = "";
                    $Isotopic_stereo_sp3          = "";
                    $Isotopic_stereo_sp3_inverted = "";
                    $Isotopic_stereo_type         = "";


                    foreach ($inchiArray as $inchidata) {

                        $IsotopicArray = explode("/i", $inchidata); // Isotopic layerを分割

                        $isoCount = 0;
                        foreach ($IsotopicArray as $isodata) {
                            if ($isoCount == 0) {
                                // for Main, charge, streo layer
                                $MainChargeStereo = explode("/", $isodata); // とりあえず行に分割

                                $formuraFlag = 0;
                                foreach ($MainChargeStereo as $mainValue) {
                                    if($formuraFlag == 0 ) {
                                        $Formula = $mainValue;
                                    }
                                    else {
                                        if($mainValue[0] == "c") {
                                            $Connections = $mainValue;
                                        }
                                        if($mainValue[0] == "h") {
                                            $H_atoms = $mainValue;
                                        }
                                        if($mainValue[0] == "q") {
                                            $Charge = $mainValue;
                                        }
                                        if($mainValue[0] == "p") {
                                            $Protons = $mainValue;
                                        }
                                        if($mainValue[0] == "b") {
                                            $Stereo_dbond = $mainValue;
                                        } 
                                        if($mainValue[0] == "t") {
                                            $Stereo_sp3 = $mainValue;
                                        } 
                                        if($mainValue[0] == "m") {
                                            $Stereo_sp3_inverted = $mainValue;
                                        } 
                                        if($mainValue[0] == "s") {
                                            $Stereo_type = $mainValue;
                                        }
                                    }
                                    $formuraFlag++;
                                }
                            }
                            else {
                                // for isotopic layer
                                $isoSub = explode("/", $isodata); // とりあえず行に分割
                            }
                            $isoCount++;
                        }
                    }

                    $Main_layer   = $Formula;
                    if ($Connections != "" ){
                        if (strlen($Main_layer) != 0 ) { $Main_layer .= "/"; }
                        $Main_layer .= $Connections;
                    }
                    if ($H_atoms != "" ){
                        if (strlen($Main_layer) != 0 ) { $Main_layer .= "/"; }
                        $Main_layer .= $H_atoms;
                    }

                    $Charge_layer = "";
                    if ($Charge != "" ){
                        if (strlen($Charge_layer) != 0 ) { $Charge_layer .= "/"; }
                        $Charge_layer .= $Charge;
                    }
                    if ($Protons != "" ){
                        if (strlen($Charge_layer) != 0 ) { $Charge_layer .= "/"; }
                        $Charge_layer .= $Protons;
                    }

                    $Stereo_layer = "";
                    if ($Stereo_dbond != "" ){
                        if (strlen($Stereo_layer) != 0 ) { $Stereo_layer .= "/"; }
                        $Stereo_layer .= $Stereo_dbond;
                    }
                    if ($Stereo_sp3 != "" ){
                        if (strlen($Stereo_layer) != 0 ) { $Stereo_layer .= "/"; }
                        $Stereo_layer .= $Stereo_sp3;
                    }
                    if ($Stereo_sp3_inverted != "" ){
                        if (strlen($Stereo_layer) != 0 ) { $Stereo_layer .= "/"; }
                        $Stereo_layer .= $Stereo_sp3_inverted;
                    }
                    if ($Stereo_type != "" ){
                        if (strlen($Stereo_layer) != 0 ) { $Stereo_layer .= "/"; }
                        $Stereo_layer .= $Stereo_type;
                    }

                    $Isotopic_layer = "";
                    if ($Isotopic_atoms != "" ){
                        if (strlen($Isotopic_layer) != 0 ) { $Isotopic_layer .= "/"; }
                        $Isotopic_layer .= $Isotopic_atoms;
                    }
                    if ($Isotopic_exchangeable_H != "" ){
                        if (strlen($Isotopic_layer) != 0 ) { $Isotopic_layer .= "/"; }
                        $Isotopic_layer .= $Isotopic_exchangeable_H;
                    }
                    if ($Isotopic_stereo_dbond != "" ){
                        if (strlen($Isotopic_layer) != 0 ) { $Isotopic_layer .= "/"; }
                        $Isotopic_layer .= $Isotopic_stereo_dbond;
                    }
                    if ($Isotopic_stereo_sp3 != "" ){
                        if (strlen($Isotopic_layer) != 0 ) { $Isotopic_layer .= "/"; }
                        $Isotopic_layer .= $Isotopic_stereo_sp3;
                    }
                    if ($Isotopic_stereo_sp3_inverted != "" ){
                        if (strlen($Isotopic_layer) != 0 ) { $Isotopic_layer .= "/"; }
                        $Isotopic_layer .= $Isotopic_stereo_sp3_inverted;
                    }
                    if ($Isotopic_stereo_type != "" ){
                        if (strlen($Isotopic_layer) != 0 ) { $Isotopic_layer .= "/"; }
                        $Isotopic_layer .= $Isotopic_stereo_type;
                    }

                    // Version of InChI code
                    fwrite($wfp, "\t".$inchi_Version." \"1S\" ;\n");
                    
                    if (strlen($Main_layer) > 0) {
                        fwrite($wfp, "\tsio:SIO_000028 ".$data_entry.$id."_".$InChI_type."_Main_layer> ;\n");
                    }
                    if (strlen($Charge_layer) > 0) {
                        fwrite($wfp, "\tsio:SIO_000028 ".$data_entry.$id."_".$InChI_type."_Charge_layer> ;\n");
                    }
                    if (strlen($Stereo_layer) > 0) {
                        fwrite($wfp, "\tsio:SIO_000028 ".$data_entry.$id."_".$InChI_type."_Stereo_layer> ;\n");
                    }
                    if (strlen($Isotopic_layer) > 0) {
                        fwrite($wfp, "\tsio:SIO_000028 ".$data_entry.$id."_".$InChI_type."_Isotopic_layer> ;\n");
                    }

                    fwrite($wfp, "\trdfs:seeAlso <http://info.identifiers.org/inchi/".$inchi."> ;\n");
                    fwrite($wfp, "\trdf:type ".$CHEMINF_000396." .\n");




                    // Main_layer
                    if (strlen($Main_layer) > 0) {
                    
                        fwrite($wfp, $data_entry.$id."_".$InChI_type."_Main_layer>\n");
                    
                        if (strlen($Formula) > 0) {
                            fwrite($wfp, "\tsio:SIO_000028 [ sio:SIO_000300 \"".$Formula."\" ; rdf:type cheminf:CHEMINF_000042 ] ;\n");
                        }
                        if (strlen($Connections) > 0) {
                            fwrite($wfp, "\tsio:SIO_000028 [ sio:SIO_000300 \"".$Connections."\" ; rdf:type ".$inchi_Connections." ] ;\n");
                        }
                        if (strlen($H_atoms) > 0) {
                            fwrite($wfp, "\tsio:SIO_000028 [ sio:SIO_000300 \"".$H_atoms."\" ; rdf:type ".$inchi_H_atoms." ] ;\n");
                        }
                        
                        fwrite($wfp, "\tsio:SIO_000028 [ sio:SIO_000300 \"".$Main_layer."\" ; rdf:type ".$inchi_Main_layer." ] .\n");
                    }

                    // Charge_layer
                    if (strlen($Charge_layer) > 0) {

                        fwrite($wfp, $data_entry.$id."_".$InChI_type."_Charge_layer>\n");
                    
                        if (strlen($Charge) > 0) {
                            fwrite($wfp, "\tsio:SIO_000028 [ sio:SIO_000300 \"".$Charge."\" ; rdf:type ".$inchi_Charge." ] ;\n");
                        }
                        if (strlen($Protons) > 0) {
                            fwrite($wfp, "\tsio:SIO_000028 [ sio:SIO_000300 \"".$Protons."\" ; rdf:type ".$inchi_Protons." ] ;\n");
                        }

                        fwrite($wfp, "\tsio:SIO_000028 [ sio:SIO_000300 \"".$Charge_layer."\" ; rdf:type ".$inchi_Charge_layer." ] .\n");
                    }

                    // Stereo_layer
                    if (strlen($Stereo_layer) > 0) {

                        fwrite($wfp, $data_entry.$id."_".$InChI_type."_Stereo_layer>\n");
                    
                        if (strlen($Stereo_dbond) > 0) {
                            fwrite($wfp, "\tsio:SIO_000028 [ sio:SIO_000300 \"".$Stereo_dbond."\" ; rdf:type ".$inchi_Stereo_dbond." ] ;\n");
                        }
                        if (strlen($Stereo_sp3) > 0) {
                            fwrite($wfp, "\tsio:SIO_000028 [ sio:SIO_000300 \"".$Stereo_sp3."\" ; rdf:type ".$inchi_Stereo_sp3." ] ;\n");
                        }
                        if (strlen($Stereo_sp3_inverted) > 0) {
                            fwrite($wfp, "\tsio:SIO_000028 [ sio:SIO_000300 \"".$Stereo_sp3_inverted."\" ; rdf:type ".$inchi_Stereo_sp3_inverted." ] ;\n");
                        }
                        if (strlen($Stereo_type) > 0) {
                            fwrite($wfp, "\tsio:SIO_000028 [ sio:SIO_000300 \"".$Stereo_type."\" ; rdf:type ".$inchi_Stereo_type." ] ;\n");
                        }

                        fwrite($wfp, "\tsio:SIO_000028 [ sio:SIO_000300 \"".$Stereo_layer."\" ; rdf:type ".$inchi_Stereo_layer." ] .\n");
                    }

                    // Isotopic_layer
                    if (strlen($Isotopic_layer) > 0) {
                        fwrite($wfp, $data_entry.$id."_".$InChI_type."_Isotopic_layer>\n");
                    
                        if (strlen($Isotopic_atoms) > 0) {
                            fwrite($wfp, "\tsio:SIO_000028 [ sio:SIO_000300 \"".$Isotopic_atoms."\" ; rdf:type ".$inchi_Isotopic_atoms." ] ;\n");
                        }
                        if (strlen($Isotopic_exchangeable_H) > 0) {
                            fwrite($wfp, "\tsio:SIO_000028 [ sio:SIO_000300 \"".$Isotopic_exchangeable_H."\" ; rdf:type ".$inchi_Isotopic_exchangeable_H." ] ;\n");
                        }
                        if (strlen($Isotopic_stereo_dbond) > 0) {
                            fwrite($wfp, "\tsio:SIO_000028 [ sio:SIO_000300 \"".$Isotopic_stereo_dbond."\" ; rdf:type ".$inchi_Isotopic_stereo_dbond." ] ;\n");
                        }
                        if (strlen($Isotopic_stereo_sp3) > 0) {
                            fwrite($wfp, "\tsio:SIO_000028 [ sio:SIO_000300 \"".$Isotopic_stereo_sp3."\" ; rdf:type ".$inchi_Isotopic_stereo_sp3." ] ;\n");
                        }
                        if (strlen($Isotopic_stereo_sp3_inverted) > 0) {
                            fwrite($wfp, "\tsio:SIO_000028 [ sio:SIO_000300 \"".$Isotopic_stereo_sp3_inverted."\" ; rdf:type ".$inchi_Isotopic_stereo_sp3_inverted." ] ;\n");
                        }
                        if (strlen($Isotopic_stereo_type) > 0) {
                            fwrite($wfp, "\tsio:SIO_000028 [ sio:SIO_000300 \"".$Isotopic_stereo_type."\" ; rdf:type ".$inchi_Isotopic_stereo_type." ] ;\n");
                        }

                        fwrite($wfp, "\tsio:SIO_000028 [ sio:SIO_000300 \"".$Isotopic_layer."\" ; rdf:type ".$inchi_Isotopic_layer." ] .\n");
                    }

                    fwrite($wfp, "\n");
                }

                if (strstr($data, 'InChIKey=')) {
                    $key = str_replace("InChIKey=", "", trim($data));

                    fwrite($wfp, $data_entry.$id."_".$InChI_type."Key>\n");
                    fwrite($wfp, "\t".$SIO_000300." \"".$key."\";\n");



                    // 2015-10-08 SPARQLthon37
                    // parse of InChIKey Code
                    // http://www.inchi-trust.org/download/104/InChI_UserGuide.pdf page 42.

                    // Key_1st   14-character hash  [1]
                    // Key_2nd    8-character hash  [2]
                    // Key_flag   1-character       [3]
                    // Key_ver    1-character       [4]
                    // Key_last   1-character       [5]

                    $keymatch = "/^([A-Z]{14})-([A-Z]{8})([A-Z])([A-Z])-([A-N])/";
                    preg_match($keymatch, $key, $keyresult);
                    if (count($keyresult)) {
                        $Skeleton  = $keyresult[1];
                        $Stereo_isotopic_substitution_information  = $keyresult[2];
                        $Kind_of_InChIKey = $keyresult[3];
                        $InChI_version  = $keyresult[4];
                        $Number_of_protons = $keyresult[5];

                        if (strlen($Skeleton) > 0) {
                            fwrite($wfp, "\tsio:SIO_000028 [ sio:SIO_000300 \"".$Skeleton."\" ; rdf:type ".$inchi_Skeleton." ] ;\n");
                        }
                        if (strlen($Stereo_isotopic_substitution_information) > 0) {
                            fwrite($wfp, "\tsio:SIO_000028 [ sio:SIO_000300 \"".$Stereo_isotopic_substitution_information."\" ; rdf:type ".$inchi_Stereo_isotopic_substitution_information." ] ;\n");
                        }
                        if (strlen($Kind_of_InChIKey) > 0) {
                            fwrite($wfp, "\tsio:SIO_000028 [ sio:SIO_000300 \"".$Kind_of_InChIKey."\" ; rdf:type ".$inchi_Kind_of_InChIKey." ] ;\n");
                        }
                        if (strlen($InChI_version) > 0) {
                            fwrite($wfp, "\tsio:SIO_000028 [ sio:SIO_000300 \"".$InChI_version."\" ; rdf:type ".$inchi_InChI_version." ] ;\n");
                        }
                        if (strlen($Number_of_protons) > 0) {
                            fwrite($wfp, "\tsio:SIO_000028 [ sio:SIO_000300 \"".$Number_of_protons."\" ; rdf:type ".$inchi_Number_of_protons." ] ;\n");
                        }
                    }

                    fwrite($wfp, "\trdfs:seeAlso <http://info.identifiers.org/inchikey/".$key."> ;\n");
                    fwrite($wfp, "\trdf:type ".$CHEMINF_000399." .\n");

                    fwrite($wfp, "\n");
                }
            }
        }
    }
}
closedir( $res_dir );
fclose( $fp );
fclose( $wfp );
?>