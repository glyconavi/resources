<?php

include("para.php");


if ($data == "glytoucan") {
	// GlyTouCan
	$DB_name         = "glytoucan.org";
	$data_entry      = "<http://glytoucan.org/Structures/Glycans/";
	$data_entry_type = "glycan:saccharide";
	$match           = "/(G[0-9]{5}[a-zA-Z]{2})/";
	$publisher       = "<http://glytoucan.org>";
	$datasetName     = "http://glytoucan.org/structure/inchi/";
	$datasetversion  = "0.2";
	$license         = "<http://creativecommons.org/licenses/by/2.1/>";
	$landingPage     = "<http://www.wurcs.org/inchi/glytoucan/>";
	$picBoolean = true;
	$pic1 = "<https://www.glytoucan.org/glyspace/service/glycans/";
	$pic2 = "/image?style=extended&format=png&notation=cfg>";

	$contributedBy   = "<http://jglobal.jst.go.jp/detail.php?JGLOBAL_ID=200901097596928716>";
	$family_name     = "YAMADA";
	$givenname       = "ISSAKU";
	$mbox            = "<mailto:issaku@noguchi.or.jp>";

	$prefix_glycan = true;
}
if ($data == "KNApSAcK") {
	// KNApSAcK
	$DB_name         = "KNApSAcK";
	$data_entry      = "<http://kanaya.naist.jp/knapsack_jsp/information.jsp?word=";
	// molecule [SIO:011125]
	// description	A molecule is the mereological maximal sum of a collection of covalently bonded atoms.
	$data_entry_type = "sio:SIO_011125";
	$match           = "/(C[0-9]{8})/";
	$publisher       = "<http://kanaya.naist.jp>";
	$datasetName     = "http://kanaya.naist.jp/KNApSAcK/rdf/inchi/";
	$datasetversion  = "1.1";
	$license         = "<http://creativecommons.org/licenses/by-sa/2.1/jp/>";
	$landingPage     = "<http://kanaya.naist.jp/KNApSAcK_Family/>";
	$picBoolean = true;
	// http://kanaya.naist.jp/sun/MSdata/SD/C00000006.gif
	$pic1 = "<http://kanaya.naist.jp/sun/MSdata/SD/";
	$pic2 = ".gif>";

	$contributedBy   = "<http://rns.nii.ac.jp/d/nr/1000090224584>";
	$family_name     = "Kanaya";
	$givenname       = "Shigehiko";
	$mbox            = "";

}

?>