# WURCSを利用したKNApSAcKとの連携の検討

* KNApSAcKに含まれる化合物は糖鎖を含むが、アグリコンを持つ糖鎖を含む。そのため、これらを区別してRDFとして記述したい。

* そのため、以下の２種のWURCSを生成し、RDF化について検討する。


## RDF sample

* GlycoConjugate Ontology (GlycoCoO) 

   * https://bioportal.bioontology.org/ontologies/GLYCOCOO
   * https://github.com/glycoinfo/GlycoCoO

```sparql
@prefix rdf:	<http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix glycan: <http://purl.jp/bio/12/glyco/glycan#> .
@prefix gtc:   <http://glytoucan.org/Structures/Glycans/> .
@prefix foaf:  <http://xmlns.com/foaf/0.1/> .

<KNApSAcK-ReferenceEntity>  gco:has_saccharide_part  <ReferencedSaccharide> .
<ReferencedSaccharide>  glycan:has_glycan  <http://rdf.glycoinfo.org/glycan/G17266GE> .

<http://rdf.glycoinfo.org/glycan/G17266GE>
        a glycan:Saccharide ;
        foaf:primaryTopic   gtc:G17266GE .
        glycan:has_glycosequence <http://rdf.glycoinfo.org/glycan/G17266GE/wurcs/2.0> .

<http://rdf.glycoinfo.org/glycan/G17266GE/wurcs/2.0>
    rdf:type	glycan:glycosequence ;
    glycan:has_sequence	"WURCS=2.0/4,7,6/[a2122h-1b_1-5_2*NCC/3=O][a1221m-1a_1-5][a1122h-1b_1-5][a1122h-1a_1-5]/1-2-1-2-3-4-2/a3-b1_a4-c1_a6-g1_c3-d1_c4-e1_e3-f1"^^xsd:string ;
    glycan:in_carbohydrate_format	glycan:carbohydrate_format_wurcs .
```

## 手順

1. 金谷先生から提供いただいたKNApSAcKのMolfileをSDF(KNApSAcK-v20150318_MOL.sdf.gz)とした。

2. MolWURCS (version 0.9.4: https://gitlab.com/glycoinfo/molwurcs) で、SDFファイルから糖鎖を抽出してWURCSに変換


```
gzcat "KNApSAcK-v20150318_MOL.sdf.gz" | java -jar MolWURCS_0.9.4.jar --title-property-id "ID" --in sdf --out wurcs --double-check 1> 20221222_Togothon123.txt 2> 20221222_Togothon123.err.txt
```

3. 2と同様にSDFファイルから糖鎖を抽出してWURCSに変換するが、この際に `--with-aglycone` オプションを追加することで、標準のWURCSではないアグリコン（糖鎖以外）を含む文字列が得られる。

```
gzcat "KNApSAcK-v20150318_MOL.sdf.gz" | java -jar MolWURCS_0.9.4.jar --title-property-id "ID" --in sdf --out wurcs --double-check --with-aglycone 1> 20221222_Togothon123-with-agly.txt 2> 20221222_Togothon123-with-agly.err.txt
```

4. WURCS, IDのリスト (20221222_Togothon123.txt, 20221222_Togothon123-with-agly.txt) から、WURCSRDFを生成

* without aglycone

```
java -jar WURCSRDF-1.2.7-jar-with-dependencies.jar 20221222_Togothon123.txt
```

* result

```
-rw-r--r--  1 yamada  staff   193B 12 22 13:23 20221222RDFConversionResult.log
-rw-r--r--  1 yamada  staff   543K 12 22 13:23 20221222WURCS-MS-RDF-0.2.ttl.gz
-rw-r--r--  1 yamada  staff   1.1M 12 22 13:23 20221222WURCS-RDF-0.5.1.ttl.gz
-rw-r--r--  1 yamada  staff   968K 12 22 13:23 20221222WURCS-RDF-0.5.2.ttl.gz
-rw-r--r--  1 yamada  staff   640K 12 22 13:23 20221222WURCS-SEQ-RDF-0.3.ttl.gz
```



* with aglycone

```
java -jar WURCSRDF-1.2.7-jar-with-dependencies.jar 20221222_Togothon123-with-agly.txt 
```

* result

```
-rw-r--r--  1 yamada  staff   203B 12 22 13:29 20221222RDFConversionResult.log
-rw-r--r--  1 yamada  staff   5.2M 12 22 13:29 20221222WURCS-MS-RDF-0.2.ttl.gz
-rw-r--r--  1 yamada  staff   1.6M 12 22 13:29 20221222WURCS-RDF-0.5.1.ttl.gz
-rw-r--r--  1 yamada  staff   1.5M 12 22 13:29 20221222WURCS-RDF-0.5.2.ttl.gz
-rw-r--r--  1 yamada  staff   972K 12 22 13:29 20221222WURCS-SEQ-RDF-0.3.ttl.gz
```

5. 生成したRDFを virtuosoにロード

* SPARQL Endpoint: https://sparql.glyconavi.org/

* GRAPH: http://Togothon123/KNApSAcK/WURCSRDF/MS

* GRAPH: http://Togothon123/KNApSAcK/WURCSRDF

* ロードするRDFリスト

```
# without
-rw-r--r--  1 yamada  staff   543K 12 22 13:23 20221222WURCS-MS-RDF-0.2.ttl.gz
-rw-r--r--  1 yamada  staff   1.1M 12 22 13:23 20221222WURCS-RDF-0.5.1.ttl.gz
-rw-r--r--  1 yamada  staff   640K 12 22 13:23 20221222WURCS-SEQ-RDF-0.3.ttl.gz
# with
-rw-r--r--  1 yamada  staff   5.2M 12 22 13:29 20221222WURCS-MS-RDF-0.2.ttl.gz
-rw-r--r--  1 yamada  staff   1.6M 12 22 13:29 20221222WURCS-RDF-0.5.1.ttl.gz
-rw-r--r--  1 yamada  staff   972K 12 22 13:29 20221222WURCS-SEQ-RDF-0.3.ttl.gz
```

* 結果

| GRAPH | triples |
| ------ | ------ |
|    http://Togothon123/KNApSAcK/WURCSRDF    |   504871     |
|     http://Togothon123/KNApSAcK/WURCSRDF/MS   |    381638    |








