<?php

setlocale(LC_ALL,'ja_JP.UTF-8');
$csvfile = "Motif_Motif-list-edwardsLab.csv";

$row = 1;
// ファイルが存在しているかチェックする
if (($handle = fopen($csvfile, "r")) !== FALSE) {


$ttl = "@base <http://glycoinfo.org/motif/> .".PHP_EOL;
$ttl .= "@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .".PHP_EOL;
$ttl .= "@prefix gn: <http://glyconavi.org/owl#> .".PHP_EOL;
$ttl .= "@prefix dc: <http://purl.org/dc/terms/> .".PHP_EOL;
$ttl .= "@prefix wc: <http://wurcs.org/> .".PHP_EOL;


while (($row = fgetcsv($handle))) {
        
        $gtc = "";
        $reducingend = "";
        $aglycon = "";
        $preferred_name = "";
        $name = "";
        $wurcs = "";

        if (count($row) > 3) {
            if(strpos($row[0],"accession") === false){
                //  "accession"[0] , "name"[1] , "redend"[2] , "aglycon"[3] ,
                $gtc = trim($row[0]);
                $reducingend = trim($row[2]);
                $aglycon = trim($row[3]);
                $preferred_name = trim($row[1]);
                $name = trim($row[1]);
                $wurcs = "";

                echo $gtc."\n";

                try {
                    $url = "https://sparqlist.glyconavi.org/api/GTC2WURCS_text?id=".$gtc;

                    // cURLセッションを初期化
                    $ch = curl_init();

                    // オプションを設定
                    curl_setopt($ch, CURLOPT_URL, $url); // 取得するURLを指定
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // 実行結果を文字列で返す
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // サーバー証明書の検証を行わない

                    // URLの情報を取得
                    $response =  curl_exec($ch);

                    // 取得結果を表示
                    //echo $value.":\t".$response."\n";
                    
                    if(preg_match('/^WURCS=2.0/', $response)) {
                        $wurcs = trim($response);
                    }
                    else {
                        echo $gtc."\n";
                        echo $response."\n";
                    }
                    // $result = json_decode($response, true);
                    // セッションを終了
                    curl_close($ch);
                    //break;
                } catch (Exception $e) {
                    echo $e->getMessage(), "\n";
                }
                $md5 = "";
                $sha256 = "";
                $md5_res = "<http://wurcs.org/md5/";
                $sha256_res = "<http://wurcs.org/sha256/";

                $ttl .= "<".$gtc.">".PHP_EOL;
                $ttl .= "  a gn:Motif ;".PHP_EOL;
                $ttl .= "  rdfs:label \"".$name."\" ;".PHP_EOL;
                $ttl .= "  dc:identifier \"".$gtc."\" ;".PHP_EOL;
                $ttl .= "  gn:has_reducingend \"".$reducingend."\" ;".PHP_EOL;
                $ttl .= "  gn:has_aglycon \"".$aglycon."\" ;".PHP_EOL;
                $ttl .= "  gn:has_preferred_name \"".$preferred_name."\" ;".PHP_EOL;
                if ($wurcs !== ""){
                    $ttl .= "  gn:has_wurcs \"".$wurcs."\" ;".PHP_EOL;
                
                    // MD5
                    $md5 = hash_hmac('md5', $wurcs, 'secret', false);
                    $ttl .= "  gn:has_wurcs_md5 \"".$md5."\" ;".PHP_EOL;
                    $ttl .= "  gn:has_wurcs_md5_resource ".$md5_res.$md5."> ;".PHP_EOL;
                    // SHA-256
                    $sha256 = hash_hmac('sha256', $wurcs, 'secret', false);
                    $ttl .= "  gn:has_wurcs_sha256 \"".$sha256."\" ;".PHP_EOL;
                    $ttl .= "  gn:has_wurcs_sha256_resource ".$sha256_res.$sha256."> ;".PHP_EOL;


                }
                $ttl .= "  rdfs:seeAlso wc:".urlencode($wurcs)." .".PHP_EOL;

                // MD5
                $ttl .= $md5_res.$md5."> gn:has_md5 \"".$md5."\" .".PHP_EOL;
                $ttl .= $md5_res.$md5."> a wc:Hash_md5 .".PHP_EOL;
                $ttl .= $md5_res.$md5."> gn:has_wurcs \"".$wurcs."\" .".PHP_EOL;
                // SHA_256
                $ttl .= $sha256_res.$sha256."> gn:has_sha256 \"".$md5."\" .".PHP_EOL;
                $ttl .= $sha256_res.$sha256."> a wc:Hash_sha256 .".PHP_EOL;
                $ttl .= $sha256_res.$sha256."> gn:has_wurcs \"".$wurcs."\" .".PHP_EOL;


            }
        }
    }

    //echo $ttl;

    //$file = $csvfile.".ttl";
    $file = "EdwardsLab-Motif-with-GTC.ttl";
    $result = file_put_contents($file, $ttl, FILE_APPEND | LOCK_EX);
    
    if ( $result === 0 ) {
        echo "書き込み失敗\n";
    } else {
        echo "書き込み成功：" . $result . " Byteの文字列の書込みをおこないました\n";
    }

    fclose($handle);
}
?>
