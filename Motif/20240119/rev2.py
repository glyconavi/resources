import sys
from rdflib import Graph, Namespace, RDF, Literal

def add_dc_identifier(input_file_path, output_file_path):
    # RDFグラフを作成し、入力データを読み込む
    g = Graph()
    with open(input_file_path, 'r') as input_file:
        g.parse(file=input_file, format='turtle')

    # プレフィックスを定義
    gn = Namespace("http://glyconavi.org/owl#")
    dc = Namespace("http://purl.org/dc/terms/")

    # 各モチーフにdc:identifierを追加
    for motif_uri in g.subjects(RDF.type, gn.Motif):
        motif_id = motif_uri.split('#')[-1]  # URIの末尾部分を取得
        g.add((motif_uri, dc.identifier, Literal(motif_id)))

    # RDFグラフを指定したファイルに保存 (text mode)
    with open(output_file_path, 'w') as output_file:
        output_file.write(g.serialize(format='turtle'))

    print(f'RDFグラフが {output_file_path} に保存されました.')

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python script.py <input_file_path> <output_file_path>")
    else:
        input_file_path = sys.argv[1]
        output_file_path = sys.argv[2]
        add_dc_identifier(input_file_path, output_file_path)

