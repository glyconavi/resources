import sys
import json
from rdflib import Graph, Namespace, RDF, Literal, XSD

def convert_json_to_rdf(json_file_path, output_file_path):
    # ファイルからJSONデータを読み込む
    with open(json_file_path, 'r') as file:
        json_data = file.read()

    # JSONをPythonオブジェクトに変換
    data = json.loads(json_data)

    # RDFグラフを作成
    g = Graph()

    # プレフィックスを定義
    ex = Namespace("http://glyconavi.org/motif/")

    # ヘッダーの変換
    for var in data['head']['vars']:
        g.add((ex[var], RDF.type, RDF.Property))

    # リザルトの変換
    for binding in data['results']['bindings']:
        result_node = ex[binding['accession']['value']]
        g.add((result_node, RDF.type, ex['result']))

        for var, value_data in binding.items():
            if var == 'accession':
                continue
            elif value_data['type'] == 'literal':
                g.add((result_node, ex[var], Literal(value_data['value'])))
            elif value_data['type'] == 'literal' and 'datatype' in value_data:
                datatype = Namespace(value_data['datatype'])
                g.add((result_node, ex[var], Literal(value_data['value'], datatype=datatype)))

    # RDFグラフをTurtle形式でファイルに保存
    with open(output_file_path, 'w') as output:
        output.write(g.serialize(format='turtle'))

    print(f'RDFグラフが {output_file_path} に保存されました.')

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python script.py <json_file_path> <output_file_path>")
    else:
        json_file_path = sys.argv[1]
        output_file_path = sys.argv[2]
        convert_json_to_rdf(json_file_path, output_file_path)

