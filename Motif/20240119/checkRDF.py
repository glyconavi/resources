from rdflib import Graph
import sys

def validate_rdf(file_path):
    g = Graph()
    try:
        g.parse(file_path, format="turtle")
        print("Validation successful.")
    except Exception as e:
        print(f"Validation error: {e}")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python script.py <rdf_file_path>")
    else:
        rdf_file_path = sys.argv[1]
        validate_rdf(rdf_file_path)

