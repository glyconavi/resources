<?php

setlocale(LC_ALL,'ja_JP.UTF-8');
$file_path = 'EdwardsLab-Motif-with-GTC.json';
//$file = new SplFileObject($file_path, 'r');

$ttl = "@base <http://glytoucan.org/Structures/Glycans/> .".PHP_EOL;
$ttl .= "@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .".PHP_EOL;
$ttl .= "@prefix gn: <http://glyconavi.org/owl#> .".PHP_EOL;
$ttl .= "@prefix dc: <http://purl.org/dc/terms/> .".PHP_EOL;

$json = file_get_contents($file_path);
$json = mb_convert_encoding($json, 'UTF8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');
$arr = json_decode($json,true);

if ($arr === NULL) {
        return;
}else{

    $json_count = count($arr["results"]["bindings"]);
    for($i=$json_count-1;$i>=0;$i--){

        if ($arr["results"]["bindings"][$i]["accession"] != null){
            $gtc = $arr["results"]["bindings"][$i]["accession"]["value"];    
        }
        if ($arr["results"]["bindings"][$i]["name"] != null){
            $name = $arr["results"]["bindings"][$i]["name"]["value"];    
        }
        if ($arr["results"]["bindings"][$i]["redend"] != null){
            $redend = $arr["results"]["bindings"][$i]["redend"]["value"];    
        }
        if ($arr["results"]["bindings"][$i]["aglycon"] != null){
            $aglycon = $arr["results"]["bindings"][$i]["aglycon"]["value"];    
        }

        $ttl .= "<".$gtc.">".PHP_EOL;
        $ttl .= "  a gn:Motif ;".PHP_EOL;
        $ttl .= "  rdfs:label \"".$name."\" ;".PHP_EOL;
        $ttl .= "  dc:identifier \"".$gtc."\" ;".PHP_EOL;
        $ttl .= "  gn:has_reducingend \"".$redend."\" ;".PHP_EOL;
        $ttl .= "  gn:has_aglycon \"".$aglycon."\" .".PHP_EOL;



    }
}


echo $ttl;

$file = "EdwardsLab-Motif-with-GTC.ttl";
$result = file_put_contents($file, $ttl, FILE_APPEND | LOCK_EX);
 
if ( $result === 0 ) {
    echo "書き込み失敗\n";
} else {
    echo "書き込み成功：" . $result . " Byteの文字列の書込みをおこないました\n";
}

?>

