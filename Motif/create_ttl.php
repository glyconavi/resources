<?php

setlocale(LC_ALL,'ja_JP.UTF-8');
$file_path = 'EdMS-edit_Sulf.tsv';
$file = new SplFileObject($file_path, 'r');

$ttl = "@base <http://glycoinfo.org/motif/> .".PHP_EOL;
$ttl .= "@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .".PHP_EOL;
$ttl .= "@prefix gn: <http://glyconavi.org/owl#> .".PHP_EOL;
$ttl .= "@prefix dc: <http://purl.org/dc/terms/> .".PHP_EOL;
$ttl .= "@prefix wc: <http://wurcs.org/> .".PHP_EOL;


while ($file->valid()) {
    
        $row = $file->fgetcsv("\t", '"');
        
        $gtc = "";
        $reducingend = "";
        $aglycon = "";
        $preferred_name = "";
        $name = "";
        $wurcs = "";

        if (count($row) == 6) {
            if(strpos($row[0],"accession") === false){
                $gtc = $row[0];
                $reducingend = $row[1];
                $aglycon = $row[2];
                $preferred_name = $row[3];
                $name = $row[4];
                $wurcs = $row[5];

                $ttl .= "<".$gtc.">".PHP_EOL;
                $ttl .= "  a gn:Motif ;".PHP_EOL;
                $ttl .= "  rdfs:label \"".$name."\" ;".PHP_EOL;
                $ttl .= "  dc:identifier \"".$gtc."\" ;".PHP_EOL;
                $ttl .= "  gn:has_reducingend \"".$reducingend."\" ;".PHP_EOL;
                $ttl .= "  gn:has_aglycon \"".$aglycon."\" ;".PHP_EOL;
                $ttl .= "  gn:has_preferred_name \"".$preferred_name."\" ;".PHP_EOL;
                $ttl .= "  gn:has_wurcs \"".$wurcs."\" ;".PHP_EOL;
                $ttl .= "  rdfs:seeAlso wc:".urlencode($wurcs)." .".PHP_EOL;
            }
        }
    
    $file->next();
}

//echo $ttl;

$file = "Ed-Motif.ttl";
$result = file_put_contents($file, $ttl, FILE_APPEND | LOCK_EX);
 
if ( $result === 0 ) {
    echo "書き込み失敗\n";
} else {
    echo "書き込み成功：" . $result . " Byteの文字列の書込みをおこないました\n";
}

?>

