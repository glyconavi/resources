# Data source: https://edwardslab.bmcb.georgetown.edu/glycomotif/Main_Page

  * SPARQL Endpoint: http://edwardslab.bmcb.georgetown.edu/yasgui/#
  * https://edwardslab.bmcb.georgetown.edu/glycomotif/index.php?title=Special:Search&limit=500&offset=0&profile=default&search=Sulfo-isogloboside
  

## EdwardsLab Motifs

```
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX glycomotif: <http://glycandata.glygen.org/glycomotif#>

SELECT DISTINCT ?accession ?name ?redend ?aglycon 
WHERE {
  ?motif rdf:type glycomotif:Motif .
  ?motif glycomotif:accession ?accession .
  ?motif glycomotif:incollection ?collection . 
  ?collection rdf:type glycomotif:Collection .
  ?motif glycomotif:name ?name .
  FILTER REGEX(?accession, "^G[0-9]{5}[A-Z]{2}")
  OPTIONAL { ?motif glycomotif:aglycon ?aglycon }
  OPTIONAL { ?motif glycomotif:reducingend ?redend }
}
```


## Edwards motif subset (EdMS)

```
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX glycomotif: <http://glycandata.glygen.org/glycomotif#>

SELECT DISTINCT ?accession ?reducingend ?aglycon ?preferred_name ?name ?wurcs
WHERE {
  ?motif rdf:type glycomotif:Motif .
  ?motif <http://glycandata.glygen.org/glycomotif#glytoucan> ?accession .
  ?motif <http://glycandata.glygen.org/glycomotif#wurcs> ?wurcs .
  ?motif <http://glycandata.glygen.org/glycomotif#reducingend> ?reducingend .
  OPTIONAL { ?motif <http://glycandata.glygen.org/glycomotif#aglycon> ?aglycon . }
  ?motif <http://glycandata.glygen.org/glycomotif#preferred_name> ?preferred_name .
  ?motif <http://glycandata.glygen.org/glycomotif#name> ?name .
}
```



## GM

```SAPRQL
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX glycomotif: <http://glycandata.glygen.org/glycomotif#>

SELECT ?accession ?name ?redend ?aglycon 
WHERE {
  ?motif rdf:type glycomotif:Motif .
  ?motif glycomotif:accession ?accession .
  ?motif glycomotif:incollection ?collection . 
  ?collection rdf:type glycomotif:Collection .
  ?collection glycomotif:id "GM" 
  OPTIONAL { ?motif glycomotif:name ?name }
  OPTIONAL { ?motif glycomotif:aglycon ?aglycon }
  OPTIONAL { ?motif glycomotif:reducingend ?redend }
}
```

## GlytouCan

```SAPRQL
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX glycomotif: <http://glycandata.glygen.org/glycomotif#>

SELECT ?accession ?name ?redend ?aglycon 
WHERE {
  ?motif rdf:type glycomotif:Motif .
  ?motif glycomotif:accession ?accession .
  ?motif glycomotif:incollection ?collection . 
  ?collection rdf:type glycomotif:Collection .
  ?collection glycomotif:id "GTC" 
  OPTIONAL { ?motif glycomotif:name ?name }
  OPTIONAL { ?motif glycomotif:aglycon ?aglycon }
  OPTIONAL { ?motif glycomotif:reducingend ?redend }
}
```

# 2021-02-20

* BLAH7 2021-01-18-22 (https://blah7.linkedannotation.org/)

*Motif_Motif-list-edwardsLab.rev_mod.txt* は、BLAH7 において、PubDictionaries(http://pubdictionaries.org/), PubAnnotation(https://pubannotation.org/) を利用して、 *EdwardsLab Motifs* のデータを整理したデータです。


# 2021-08-23

* 還元末端を持つ還元末端を持つMotifリストの取得

  * EP: https://sparql.glyconavi.org/sparql

SPARQL
```
SELECT DISTINCT (str(?id) as ?gtc) str(?w) AS ?WURCS
FROM <http://rdf.glycoinfo.org/edwardslab-motif-with-gtc>
WHERE {
#?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?o .
?s <http://purl.org/dc/terms/identifier> ?id .
?s <http://glyconavi.org/owl#has_reducingend> "true" .
?s <http://glyconavi.org/owl#has_wurcs> ?w .
}
```

* 取得tsvデータ

  * 2021-08-23_motif-redEnd-true.tsv


