#!/bin/bash

# 入力ファイル
INPUT_FILE="components.cif"

# 出力ファイル
OUTPUT_FILE="chem_comp_types.txt"

# _chem_comp.type の一覧を取得し、重複を削除
awk '/^_chem_comp.type/ {print $2}' "$INPUT_FILE" | sort -u > "$OUTPUT_FILE"

echo "_chem_comp.type の一覧を $OUTPUT_FILE に保存しました。"

