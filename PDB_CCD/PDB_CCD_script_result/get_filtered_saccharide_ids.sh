#!/bin/bash

# 入力ファイル
INPUT_FILE="components.cif"

# 出力ファイル
OUTPUT_FILE="filtered_saccharide_ids.txt"

# 抽出する _chem_comp.type のパターン（大文字小文字を区別しない）
PATTERNS="D-saccharide|L-saccharide|D-SACCHARIDE|D-saccharide|L-SACCHARIDE|L-saccharide|SACCHARIDE|saccharide"

# _chem_comp.id を取得する
awk -v patterns="$PATTERNS" '
BEGIN {id=""; type=""}
/^_chem_comp.id/ {id=$2}
/^_chem_comp.type/ {
    type=$2
    if (type ~ patterns && id != "") {
        print id
    }
}' "$INPUT_FILE" | sort -u > "$OUTPUT_FILE"

echo "指定の _chem_comp.type を含む _chem_comp.id を $OUTPUT_FILE に保存しました。"

