#!/bin/bash

# 入力ファイル
INPUT_FILE="components.cif"

# 出力ファイル
OUTPUT_FILE="saccharide_ids.txt"

# _chem_comp.id を取得する
awk '
BEGIN {id=""; type=""}
/^_chem_comp.id/ {id=$2}
/^_chem_comp.type/ {
    type=$2
    if (tolower(type) ~ /saccharide/ && id != "") {
        print id
    }
}' "$INPUT_FILE" > "$OUTPUT_FILE"

echo "Saccharide を含む _chem_comp.id を $OUTPUT_FILE に保存しました。"

