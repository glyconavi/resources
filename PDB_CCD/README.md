# PDB Chemical Component Dictionary 内の糖のリスト取得

## wwPDBのサイトより、components.cif　（mmCIF形式）をダウンロード

* https://www.wwpdb.org/data/ccd

  * 必要に応じて解凍を行う。



## _chem_comp.type の一覧を重複無しのリストで取得するスクリプト

```bash
#!/bin/bash

# 入力ファイル
INPUT_FILE="components.cif"

# 出力ファイル
OUTPUT_FILE="chem_comp_types.txt"

# _chem_comp.type の一覧を取得し、重複を削除
awk '/^_chem_comp.type/ {print $2}' "$INPUT_FILE" | sort -u > "$OUTPUT_FILE"

echo "_chem_comp.type の一覧を $OUTPUT_FILE に保存しました。"
```

`chmod +x get_chem_comp_types.sh`

`./get_chem_comp_types.sh`

### result

* chem_comp_types.txt


## _chem_comp.type に以下を含む、_chem_comp.id　のリストを取得するbashスクリプト
- D-saccharide
- L-saccharide
- D-SACCHARIDE
- D-saccharide
- L-SACCHARIDE
- L-saccharide
- SACCHARIDE
- saccharide

```bash
#!/bin/bash

# 入力ファイル
INPUT_FILE="components.cif"

# 出力ファイル
OUTPUT_FILE="filtered_saccharide_ids.txt"

# 抽出する _chem_comp.type のパターン（大文字小文字を区別しない）
PATTERNS="D-saccharide|L-saccharide|D-SACCHARIDE|D-saccharide|L-SACCHARIDE|L-saccharide|SACCHARIDE|saccharide"

# _chem_comp.id を取得する
awk -v patterns="$PATTERNS" '
BEGIN {id=""; type=""}
/^_chem_comp.id/ {id=$2}
/^_chem_comp.type/ {
    type=$2
    if (type ~ patterns && id != "") {
        print id
    }
}' "$INPUT_FILE" | sort -u > "$OUTPUT_FILE"

echo "指定の _chem_comp.type を含む _chem_comp.id を $OUTPUT_FILE に保存しました。"
```

`chmod +x get_filtered_saccharide_ids.sh`


`./get_filtered_saccharide_ids.sh`


### result

* filtered_saccharide_ids.txt
