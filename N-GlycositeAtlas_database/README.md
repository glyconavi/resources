# N-GlycositeAtlas

* http://nglycositeatlas.biomarkercenter.org/glycosites/download/


# ref.

* https://pubmed.ncbi.nlm.nih.gov/31516400/

* Sun S, Hu Y, Ao M, Shah P, Chen J, Yang W, Jia X, Tian Y, Thomas S, Zhang H. N-GlycositeAtlas: a database resource for mass spectrometry-based human N-linked glycoprotein and glycosylation site mapping. Clin Proteomics. 2019 Sep 7;16:35. doi: 10.1186/s12014-019-9254-0. PMID: 31516400; PMCID: PMC6731604.
