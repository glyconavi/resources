<?php
setlocale(LC_ALL,'ja_JP.UTF-8');
if (count($argv) >1){
    $csvfile = $argv[1];
    $outfile =  $csvfile."_csv2.ttl";

$row = 1;
// ファイルが存在しているかチェックする
if (($handle = fopen($csvfile, "r")) !== FALSE) {

$ttl = "@base <http://glycoinfo.org/motif/> .".PHP_EOL;
$ttl .= "@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .".PHP_EOL;
$ttl .= "@prefix gn: <http://glyconavi.org/owl#> .".PHP_EOL;
$ttl .= "@prefix dc: <http://purl.org/dc/terms/> .".PHP_EOL;

$ttl .= "<http://glycoinfo.org/motif/> gn:source \"".$csvfile."\" .".PHP_EOL;

while (($row = fgetcsv($handle))) {
        $gtc = "";
        $name = "";
        if (count($row) > 1) {
                $gtc = trim($row[1]);
                $name = trim($row[0]);
                echo $gtc."\n";
                $ttl .= "<".$gtc.">".PHP_EOL;
                $ttl .= "  a gn:Motif ;".PHP_EOL;
                $ttl .= "  rdfs:label \"".$name."\" ;".PHP_EOL;
                $ttl .= "  dc:identifier \"".$gtc."\" .".PHP_EOL;
        }
    }
    $result = file_put_contents($outfile, $ttl, FILE_APPEND | LOCK_EX);
    
    if ( $result === 0 ) {
        echo "書き込み失敗\n";
    } else {
        echo "書き込み成功：" . $result . " Byteの文字列の書込みをおこないました\n";
    }
    fclose($handle);
}

}
else {
    echo 'file name not found';
}
?>
