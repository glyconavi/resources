# archetypeを生成するAPI

* https://doc.glytoucan.org/en/alpha/api/api-document

* POST

```
curl -X POST -H "Content-Type: application/json" -d '{"input": "WURCS=2.0/4,5,4/[a2122h-1x_1-5_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-2-3-4-4/a4-b1_b4-c1_c3-d1_c6-e1"}' "https://alpha.glytoucan.org/api/archetype/1.2.13/generator" | jq
```

```
{
  "output": "WURCS=2.0/4,5,4/[u2122h_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-2-3-4-4/a4-b1_b4-c1_c3-d1_c6-e1",
  "input": "WURCS=2.0/4,5,4/[a2122h-1x_1-5_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-2-3-4-4/a4-b1_b4-c1_c3-d1_c6-e1",
  "canGenerateArchetype": true,
  "isArchetype": false
}
```

## has_archetypeの述語で取れる主語と目的語です。

```
SELECT * WHERE { 
GRAPH <http://rdf.glytoucan.org/archetype/pre-release> {
?id <http://purl.jp/bio/12/glyco/glycan#has_archetype> ?archetype .
}
}
```



