MCAW profile

1. https://glyconavi.org/GlycoAbun/Sample/index.php?id=GS_3&graph=disease

2. SPARQLで、ratioとGlyToucanID, (WURCS) を取得

* https://sparqlist.glyconavi.org/DiseaseAbunEntry

3. MCAW Tool用にデータを変換

```
6.4	G78059CC	WURCS=2.0/5,10,9/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5][a1221m-1a_1-5]/1-1-2-3-1-4-3-1-4-5/a4-b1_a6-j1_b4-c1_c3-d1_c6-g1_d2-e1_e4-f1_g2-h1_h4-i1
6.9	G36191CD	WURCS=2.0/4,9,8/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5]/1-1-2-3-1-4-3-1-4/a4-b1_b4-c1_c3-d1_c6-g1_d2-e1_e4-f1_g2-h1_h4-i1
...
```

↓

```
G78059CC	WURCS=2.0/5,10,9/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5][a1221m-1a_1-5]/1-1-2-3-1-4-3-1-4-5/a4-b1_a6-j1_b4-c1_c3-d1_c6-g1_d2-e1_e4-f1_g2-h1_h4-i1	
G36191CD	WURCS=2.0/4,9,8/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5]/1-1-2-3-1-4-3-1-4/a4-b1_b4-c1_c3-d1_c6-g1_d2-e1_e4-f1_g2-h1_h4-i1	
G52428MJ	WURCS=2.0/5,10,9/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5][a1221m-1a_1-5]/1-1-2-3-1-4-1-4-3-5/a4-b1_a6-j1_b4-c1_c3-d1_c6-i1_d2-e1_d4-g1_e4-f1_g4-h1
...	
```

↓

4. MCAW toolでProfile imageを取得

https://rings.glycoinfo.org/mcaw/new

https://rings.glycoinfo.org/mcaw/384



Profile image

5. Profile imageをDBに追加するために、RDFデータを作成

6. SPARQList作成

7. 可視化ツールの改良

8. 


# Example
* input data

```
G78059CC	WURCS=2.0/5,10,9/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5][a1221m-1a_1-5]/1-1-2-3-1-4-3-1-4-5/a4-b1_a6-j1_b4-c1_c3-d1_c6-g1_d2-e1_e4-f1_g2-h1_h4-i1
G78059CC	WURCS=2.0/5,10,9/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5][a1221m-1a_1-5]/1-1-2-3-1-4-3-1-4-5/a4-b1_a6-j1_b4-c1_c3-d1_c6-g1_d2-e1_e4-f1_g2-h1_h4-i1
G78059CC	WURCS=2.0/5,10,9/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5][a1221m-1a_1-5]/1-1-2-3-1-4-3-1-4-5/a4-b1_a6-j1_b4-c1_c3-d1_c6-g1_d2-e1_e4-f1_g2-h1_h4-i1
G78059CC	WURCS=2.0/5,10,9/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5][a1221m-1a_1-5]/1-1-2-3-1-4-3-1-4-5/a4-b1_a6-j1_b4-c1_c3-d1_c6-g1_d2-e1_e4-f1_g2-h1_h4-i1
G78059CC	WURCS=2.0/5,10,9/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5][a1221m-1a_1-5]/1-1-2-3-1-4-3-1-4-5/a4-b1_a6-j1_b4-c1_c3-d1_c6-g1_d2-e1_e4-f1_g2-h1_h4-i1
G78059CC	WURCS=2.0/5,10,9/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5][a1221m-1a_1-5]/1-1-2-3-1-4-3-1-4-5/a4-b1_a6-j1_b4-c1_c3-d1_c6-g1_d2-e1_e4-f1_g2-h1_h4-i1
G78059CC	WURCS=2.0/5,10,9/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5][a1221m-1a_1-5]/1-1-2-3-1-4-3-1-4-5/a4-b1_a6-j1_b4-c1_c3-d1_c6-g1_d2-e1_e4-f1_g2-h1_h4-i1
G78059CC	WURCS=2.0/5,10,9/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5][a1221m-1a_1-5]/1-1-2-3-1-4-3-1-4-5/a4-b1_a6-j1_b4-c1_c3-d1_c6-g1_d2-e1_e4-f1_g2-h1_h4-i1
G78059CC	WURCS=2.0/5,10,9/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5][a1221m-1a_1-5]/1-1-2-3-1-4-3-1-4-5/a4-b1_a6-j1_b4-c1_c3-d1_c6-g1_d2-e1_e4-f1_g2-h1_h4-i1
G36191CD	WURCS=2.0/4,9,8/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5]/1-1-2-3-1-4-3-1-4/a4-b1_b4-c1_c3-d1_c6-g1_d2-e1_e4-f1_g2-h1_h4-i1
G36191CD	WURCS=2.0/4,9,8/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5]/1-1-2-3-1-4-3-1-4/a4-b1_b4-c1_c3-d1_c6-g1_d2-e1_e4-f1_g2-h1_h4-i1
G36191CD	WURCS=2.0/4,9,8/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5]/1-1-2-3-1-4-3-1-4/a4-b1_b4-c1_c3-d1_c6-g1_d2-e1_e4-f1_g2-h1_h4-i1
G36191CD	WURCS=2.0/4,9,8/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5]/1-1-2-3-1-4-3-1-4/a4-b1_b4-c1_c3-d1_c6-g1_d2-e1_e4-f1_g2-h1_h4-i1
G36191CD	WURCS=2.0/4,9,8/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5]/1-1-2-3-1-4-3-1-4/a4-b1_b4-c1_c3-d1_c6-g1_d2-e1_e4-f1_g2-h1_h4-i1
G36191CD	WURCS=2.0/4,9,8/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5]/1-1-2-3-1-4-3-1-4/a4-b1_b4-c1_c3-d1_c6-g1_d2-e1_e4-f1_g2-h1_h4-i1
G36191CD	WURCS=2.0/4,9,8/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5]/1-1-2-3-1-4-3-1-4/a4-b1_b4-c1_c3-d1_c6-g1_d2-e1_e4-f1_g2-h1_h4-i1
G36191CD	WURCS=2.0/4,9,8/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5]/1-1-2-3-1-4-3-1-4/a4-b1_b4-c1_c3-d1_c6-g1_d2-e1_e4-f1_g2-h1_h4-i1
```

* output image

<img src="https://gitlab.com/glyconavi/resources/-/raw/master/MCAW/sample.png" />

# 参考

https://glycosmos.org/glycoproteins/show/uniprot/Q2F1K8#mcaw






https://docs.google.com/spreadsheets/d/1u6lfG3cKdY8SJSY-tHTemw4srvVFTmiyTnlRa5yI6m4/edit#gid=1393404800



