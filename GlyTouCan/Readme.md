# GlyTouCan Dataset

## GlyTouCan-IUPAC

* file: GTC-IUPAC_YYYYMMDD.tsv

* batch: GTC2IUPAC

```
GlyTouCan ID <tab> IUPAC <tab> IUPAC_Condensed <tab> IUPAC_Extended


```

## SPARQL Query
```
PREFIX glycan: <http://purl.jp/bio/12/glyco/glycan#>
PREFIX glytoucan: <http://www.glytoucan.org/glyco/owl/glytoucan#>
SELECT DISTINCT str(?id) str(?iupac) str(?iupac_con) str(?iupac_ex) 
WHERE {

	GRAPH <http://rdf.glytoucan.org/core> {
		?Saccharide glytoucan:has_primary_id ?id .
	}
	OPTIONAL {
		GRAPH <http://rdf.glytoucan.org/sequence/iupac> {
		  ?Saccharide glycan:has_glycosequence ?GlycoSequence.
		  ?GlycoSequence glycan:in_carbohydrate_format glycan:carbohydrate_format_iupac .
		  ?GlycoSequence glycan:has_sequence ?iupac.
	  }
	}
	OPTIONAL {
		GRAPH <http://rdf.glytoucan.org/sequence/iupac_extended> {
		  ?Saccharide glycan:has_glycosequence ?GlycoSequence_ex.
		  ?GlycoSequence_ex glycan:in_carbohydrate_format glycan:carbohydrate_format_iupac_extended .
		  ?GlycoSequence_ex glycan:has_sequence ?iupac_ex.
	  }
	}
	OPTIONAL {
		GRAPH <http://rdf.glytoucan.org/sequence/iupac_condensed> {
		  ?Saccharide glycan:has_glycosequence ?GlycoSequence_con .
		  ?GlycoSequence_con glycan:in_carbohydrate_format glycan:carbohydrate_format_iupac_condensed .
		  ?GlycoSequence_con glycan:has_sequence ?iupac_con.
	  }
	}
}
# LIMIT 10
```



## GlyTouCan-WURCS

* file: GTC-WURCS_YYYYMMDD.tsv

* batch: GTC-WURCS


```
WURCS <tab> GlyTouCan ID
"WURCS=2.0/2,7,6/[u2122h][a2122h-1a_1-5]/1-2-2-2-2-2-2/a4-b1_b4-c1_c6-d1_d4-e1_e6-f1_f4-g1"	"G96141FI"
...
```

## SPARQL Query
```
PREFIX glycan: <http://purl.jp/bio/12/glyco/glycan#>
PREFIX glytoucan: <http://www.glytoucan.org/glyco/owl/glytoucan#>
SELECT DISTINCT str(?wurcs) str(?id)
FROM <http://rdf.glytoucan.org/core>
FROM <http://rdf.glytoucan.org/sequence/wurcs>
WHERE {
  ?Saccharide glytoucan:has_primary_id ?id .
  ?Saccharide glycan:has_glycosequence ?GlycoSequence .
  ?GlycoSequence glycan:has_sequence ?wurcs.
  ?GlycoSequence glycan:in_carbohydrate_format glycan:carbohydrate_format_wurcs.
}
# LIMIT 10
```


