# GlyTouCanのWURCSをアーカイブ以外全て取得し、GlycanFormatConverter-cli で IUPAC に変換する。

1. GlyTouCanからのデータ取得

  * SPARQL endpointから取得、詳細は`LocalGlyTouCanID`のファイルを参照

```
./LocalGlyTouCanID.sh
```

* GlyTouCanからWURCSを取得した日: 6月 26 16:59

```
-rw-rw-r--. 1 glyconavi glyconavi 55967423  6月 26 17:07 glytoucan.tsv                            
``` 

1. GlycanFormatConverterのgitからのセットアップ(@192.168.0.110)

* 方法は、https://gitlab.com/glycoinfo/GlycanFormatConverter-cli　を参照

```
$ java -jar ../git/gitlab/glycoinfo/GlycanFormatConverter-cli/target/GlycanFormatConverter-cli.jar -h
usage: java -jar GlycanFormatConverter-cli.jar --import <FORMAT> --export
            <FORMAT> --sequence <Sequence>

Options:
 -e,--export
 <FORMAT=[IUPAC-Short|IUPAC-Condensed|IUPAC-Extended|GlycoCT|WURCS|GlycanW
 eb|JSON]>   export format.
 -h,--help
 Show usage help
 -i,--import
 <FORMAT=[IUPAC-Condensed|IUPAC-Extended|GlycoCT|KCF|LinearCode|WURCS|JSON
 ]>          import format.
 -seq,--sequence <SEQUENCE>
 Import glycan text format.
 -v,--version
 Print version information.

Version: 2.10.3
```

1. WURCS to IUPAC-Extended

```
./convert_wurcs2iupac.sh
```

1. WURCS to IUPAC-Short

```
./convert_wurcs2iupac-short.sh
```

* 結果と実行ファイルの一覧

```
-rwxrwxr-x. 1 glyconavi glyconavi     1666  6月 26 16:59 LocalGlyTouCanID                         
-rwxrwxr-x. 1 glyconavi glyconavi     1202  6月 27 13:17 convert_wurcs2iupac-short.sh             
-rwxrwxr-x. 1 glyconavi glyconavi     1205  6月 26 17:33 convert_wurcs2iupac.sh                   
-rw-rw-r--. 1 glyconavi glyconavi 55967423  6月 26 17:07 glytoucan.tsv                            
-rw-rw-r--. 1 glyconavi glyconavi       28  6月 27 08:25 log_20240626_173426.txt                  
-rw-rw-r--. 1 glyconavi glyconavi       28  6月 28 04:00 log_20240627_131721.txt                  
-rw-rw-r--. 1 glyconavi glyconavi 28931901  6月 27 08:25 output_20240626_173426.txt               
-rw-rw-r--. 1 glyconavi glyconavi 12808786  6月 28 04:00 output_20240627_131721.txt  
```


