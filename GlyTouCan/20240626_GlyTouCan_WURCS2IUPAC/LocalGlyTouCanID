#!/usr/bin/env bash

EP=https://ts.glytoucan.org/sparql
LIMIT=500

function query() {
  local offset=${1:-0}
  shift

  local sparql=$(cat <<EOS | sed -e 's/^\s\+//' | tr '\n' ' '
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcterms: <http://purl.org/dc/terms/>
PREFIX glycan: <http://purl.jp/bio/12/glyco/glycan#>
PREFIX glytoucan:  <http://www.glytoucan.org/glyco/owl/glytoucan#>

SELECT DISTINCT ?wurcs ?id 
WHERE {
  GRAPH <http://rdf.glytoucan.org/core> {
    ?Saccharide a <http://purl.jp/bio/12/glyco/glycan#Saccharide> .
    ?Saccharide glytoucan:has_primary_id ?id .
  }
  GRAPH <http://rdf.glytoucan.org/sequence/wurcs> {
      ?Saccharide glycan:has_glycosequence ?GSeq .
      ?GSeq a <http://purl.jp/bio/12/glyco/glycan#Glycosequence>.
      ?GSeq glycan:has_sequence ?wurcs.
      ?GSeq glycan:in_carbohydrate_format <http://purl.jp/bio/12/glyco/glycan#carbohydrate_format_wurcs> .
  }
  FILTER NOT EXISTS {
   GRAPH <http://rdf.glytoucan.org/archive> {
     ?Saccharide rdf:type ?ArchiveSaccharide .
   }
 }
}
LIMIT ${LIMIT}
OFFSET ${offset}
EOS
)

  curl -XPOST -H 'Accept: text/tab-separated-values' --data-urlencode "query=${sparql}" ${EP} 2>/dev/null | sed '1d'

  return $?
}

function query_recursive() {
  local offset=${1:-0}
  shift

  result=$(query ${offset})

  local status=$?
  if [ ${status} -ne 0 ]; then
    printf "\ncurl returned exit code %s\n" ${status} >&2
    exit ${status}
  fi

  if [ -n "${result}" ]; then
    printf "\rObtained %s entries..." $((offset + $(echo -e "${result}" | wc -l))) >&2
    echo -e "${result}"
    query_recursive $((offset + LIMIT))
  else
    printf "\n" >&2
  fi
}

query_recursive
