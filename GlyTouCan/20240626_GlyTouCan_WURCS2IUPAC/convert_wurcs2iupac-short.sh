#!/bin/bash

# 実行開始日時を取得
start_date=$(date +%Y%m%d_%H%M%S)

# 出力ファイル名とログファイル名を設定
output_file="output_${start_date}.txt"
log_file="log_${start_date}.txt"

# TSVファイルのパスを設定（この部分を適宜変更してください）
tsv_file="glytoucan.tsv"

# Javaアプリのjarファイルのパス（この部分を適宜変更してください）
java_app="../git/gitlab/glycoinfo/GlycanFormatConverter-cli/target/GlycanFormatConverter-cli.jar"

# ファイルが存在しない場合、エラーメッセージを出力してスクリプトを終了
if [ ! -f "$tsv_file" ]; then
    echo "Error: TSV file does not exist." >> "$log_file"
    exit 1
fi

# TSVファイルを読み込み、各行に対して処理
while IFS=$'\t' read -r column1 column2
do
    # Javaアプリを実行し、結果を一時的な変数に保存
    result=$(java -jar "$java_app" -i WURCS -e IUPAC-Short -seq "$column1" 2>> "$log_file")

    # 結果と2つ目の列のデータを出力ファイルに書き出す
    echo -e "Result: $result\nInput: $column2" >> "$output_file"
done < "$tsv_file"

echo "Script execution completed." >> "$log_file"

