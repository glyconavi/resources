# SAPRQL Query 

* SPARQL Endpoint: https://sparql.glyconavi.org/sparql

```
select DISTINCT str (?wurcs)
FROM <http://glyconavi.org/tcarp>
WHERE {
?branch_entry <http://purl.jp/bio/12/glyco/glycan#has_glycan> ?glycan .
?glycan <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://glyconavi.org/ontology/pdb#WURCSStandard> ;
<http://purl.jp/bio/12/glyco/glycan#has_glycosequence> ?glycosequence .
?glycosequence <http://purl.jp/bio/12/glyco/glycan#in_carbohydrate_format> <http://purl.jp/bio/12/glyco/glycan#carbohydrate_format_wurcs> ;
<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://purl.jp/bio/12/glyco/glycan#Glycosequence> ;
<http://purl.jp/bio/12/glyco/glycan#has_sequence> ?wurcs .

MINUS { ?glycan <http://glyconavi.org/ontology/pdb#has_glytoucan_id> ?gtc . }
}
```


```
# @endpoint https://sparql.glyconavi.org/sparql
PREFIX glycan: <http://purl.jp/bio/12/glyco/glycan#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX navi: <http://glyconavi.org/ontology/pdb#>
PREFIX dct: <http://purl.org/dc/terms/>
SELECT DISTINCT ?model_wurcs ?branch_wurcs ?gtc ?id
FROM <http://glyconavi.org/tcarp>
WHERE {
  ?node_0 rdf:type navi:TCarpEntry .
  ?node_0 navi:has_PDB_model_glycan ?node_2 .
  ?node_0 navi:has_branch_entry ?node_4 .
  ?node_0 dct:identifier ?id .
  ?node_2 navi:has_model ?node_19 .
  ?node_4 rdf:type navi:BranchEntry .
  ?node_4 glycan:has_glycan ?node_28 .
  ?node_28 rdf:type navi:WURCSStandard .
  ?node_28 glycan:has_glycosequence ?node_31 .
  ?node_31 rdf:type glycan:Glycosequence .
  ?node_31 glycan:has_sequence ?branch_wurcs .
  ?node_31 glycan:in_carbohydrate_format ?node_35 .
  ?node_19 rdf:type navi:PDBModel .
  ?node_19 glycan:has_glycan ?node_41 .
  ?node_41 rdf:type navi:WURCSStandard .
  OPTIONAL { 
      ?node_41 navi:has_glytoucan_id ?gtc . 
      VALUES (?gtc) { ("") }
  }
  ?node_41 glycan:has_glycosequence ?node_45 .
  ?node_45 rdf:type glycan:Glycosequence .
  ?node_45 glycan:has_sequence ?model_wurcs .

}
LIMIT 100
```
