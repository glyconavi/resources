#!/usr/bin/env bash

EP=https://sparql.glyconavi.org/sparql
LIMIT=500

function query() {
  local offset=${1:-0}
  shift

  local sparql=$(cat <<EOS | sed -e 's/^\s\+//' | tr '\n' ' '
SELECT DISTINCT ?gtc ?id
FROM <http://glyconavi.org/tcarp>
WHERE {
?pdb_entry <http://purl.org/dc/terms/identifier> ?id .
?pdb_entry <http://glyconavi.org/ontology/pdb#has_branch_entry> ?branch_entry .
?branch_entry <http://purl.jp/bio/12/glyco/glycan#has_glycan> ?glycan .
?branch_entry <http://glyconavi.org/ontology/pdb#has_entity_id> ?entity .
?glycan <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://glyconavi.org/ontology/pdb#WURCSStandard> .
?glycan <http://purl.jp/bio/12/glyco/glycan#has_glycosequence> ?glycosequence .
?glycosequence <http://purl.jp/bio/12/glyco/glycan#has_sequence> ?wurcs .
?glycosequence <http://purl.jp/bio/12/glyco/glycan#in_carbohydrate_format> <http://purl.jp/bio/12/glyco/glycan#carbohydrate_format_wurcs> .
?glycan <http://glyconavi.org/ontology/pdb#has_glytoucan_id> ?gtc .
}
LIMIT ${LIMIT}
OFFSET ${offset}
EOS
)

  curl -XPOST -H 'Accept: text/tab-separated-values' --data-urlencode "query=${sparql}" ${EP} 2>/dev/null | sed '1d'

  return $?
}

function query_recursive() {
  local offset=${1:-0}
  shift

  result=$(query ${offset})

  local status=$?
  if [ ${status} -ne 0 ]; then
    printf "\ncurl returned exit code %s\n" ${status} >&2
    exit ${status}
  fi

  if [ -n "${result}" ]; then
    printf "\rObtained %s entries..." $((offset + $(echo -e "${result}" | wc -l))) >&2
    echo -e "${result}"
    query_recursive $((offset + LIMIT))
  else
    printf "\n" >&2
  fi
}

query_recursive
