# TCarp Dataset Resources

## TCarpRDF-20200604-turtle.tar.gz  201.3 MB

* All PDB results analyzed by TCarpRDF (PDB2Glycan) software.

* TCarpRDF (PDB2Glycan) output data in TTL format (13765 files)

  * 2020-06-03, bug_fix branch: fix_typo, 951308332d24bb2e59af5cd9c1c3298c2a637fcc
  


## pdb.struct_conf_type._struct_conf_type.id.tsv

* Data source: http://mmcif.wwpdb.org/dictionaries/mmcif_pdbx_v50.dic/Items/_struct_conf_type.id.html

  * Item name: _struct_conf_type.id
  * Category name: struct_conf_type
 
  * Item Description

```
The descriptor that categorizes the type of the conformation
of the backbone of the polymer (whether protein or nucleic acid).
Explicit values for the torsion angles that define each
conformation are not given here, but it is expected that the
author would provide such information in either the
_struct_conf_type.criteria or _struct_conf_type.reference data
items, or both.
```
               