<?php

setlocale(LC_ALL,'ja_JP.UTF-8');
$file_path = 'pdb.struct_conf_type._struct_conf_type.id.tsv';
$file = new SplFileObject($file_path, 'r');

$ttl = "@base <http://glycoinfo.org/pdb/struct_conf_type/> .".PHP_EOL;
$ttl .= "@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .".PHP_EOL;
$ttl .= "@prefix dc: <http://purl.org/dc/terms/> .".PHP_EOL;
$ttl .= "@prefix pdbo: <https://rdf.wwpdb.org/schema/pdbx-v50.owl#> .".PHP_EOL;

while ($file->valid()) {
    
        $row = $file->fgetcsv("\t", '"');
        
        $Value = "";
        $Detail = "";

        if (count($row) == 2) {
            if(strpos($row[0],"Value") === false){
                $Value = $row[0];
                $Detail = $row[1];

                $ttl .= "<".urlencode($Value).">".PHP_EOL;
                $ttl .= "  a pdbo:struct_conf_type ;".PHP_EOL;
                $ttl .= "  rdfs:label \"".$Value."\" ;".PHP_EOL;
                $ttl .= "  dc:identifier \"".$Value."\" ;".PHP_EOL;
                $ttl .= "  dc:description \"".$Detail."\" .".PHP_EOL;
            }
        }
    
    $file->next();
}

//echo $ttl;

$file = "pdb-struct-conf-type.ttl";
$result = file_put_contents($file, $ttl, FILE_APPEND | LOCK_EX);
 
if ( $result === 0 ) {
    echo "書き込み失敗\n";
} else {
    echo "書き込み成功：" . $result . " Byteの文字列の書込みをおこないました\n";
}

?>

