<?php
header('Content-Type: text/plain;charset=UTF-8');
ini_set('auto_detect_line_endings', 1);
ini_set("memory_limit", "5000M");

$dirpath = dirname(__FILE__);
$inputfile = "";

if (count($argv) > 0){
  $inputfile = $argv[1];
}

date_default_timezone_set('Asia/Tokyo');
$fileHeader = date(".Y-m-d_H-i-s");

$wfp = fopen($inputfile.$fileHeader.".add-footer-pubchem.sdf", "a");

$sdf = file_get_contents($inputfile);

$sdf = str_replace(array("INPUT_WURCS"), "PUBCHEM_SUBSTANCE_SYNONYM", $sdf);
$sdf = str_replace(array("\r\n", "\r", "\n"), "\n", $sdf);



$mols = explode("\n$$$$\n", $sdf);
$size = 1;
foreach ($mols as $mol) {
    


    $line = explode("\n", $mol);

    $count = 0;
    foreach ($line as $ln) {
        //fwrite($wfp, $ln."\n");

        if ($count == 0){
            $GTC = trim($ln);
        }
        $count++;
    }



    if (strlen($GTC) > 0){
        echo $size."\t".$GTC."\n";
        $size++;
        /*
        echo $mol;
        echo "> <PUBCHEM_EXT_SUBSTANCE_URL>\n";
        echo "https://glytoucan.org/".$GTC."\n";
        echo "\n";
        echo "> <PUBCHEM_XREF_EXT_ID>\n";
        echo $GTC."\n";
        echo "\n";
        echo "> <PUBCHEM_EXT_DATASOURCE_URL>\n";
        echo "https://glytoucan.org/\n";
        echo "\n";
        echo "$$$$\n";
        */

        fwrite($wfp, $mol."\n");
        fwrite($wfp, "> <PUBCHEM_EXT_DATASOURCE_URL>\n");
        fwrite($wfp, "https://glytoucan.org/\n");
        fwrite($wfp, "\n");

        fwrite($wfp, "> <PUBCHEM_XREF_EXT_ID>\n");
        fwrite($wfp, $GTC."\n");
        fwrite($wfp, "\n");

        fwrite($wfp, "> <PUBCHEM_EXT_SUBSTANCE_URL>\n");
        fwrite($wfp, "https://glytoucan.org/Structures/Glycans/".$GTC."\n");
        fwrite($wfp, "\n");

        fwrite($wfp, "$$$$\n");
    }
}
   
fclose($wfp);
?>