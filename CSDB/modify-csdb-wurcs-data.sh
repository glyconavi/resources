#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 input_file_path output_file_path"
    exit 1
fi

input_path=$1
output_path=$2

# TSVファイルの処理
awk -F'\t' '{print $2"\t"$3}' "$input_path" > "$output_path"

echo "Processing complete. Output written to $output_path"

