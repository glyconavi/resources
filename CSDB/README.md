# Submission of CSDB data to GlyTouCan

1. get CSDB GlycoCT data (Kinoshita) ... 2022-10-19 DONE, /CSDB_GlycoCT_data.tar.gz

2. get GlycoCT > GlycanFormatConverter > WURCS (Yamada) ... 2022-10-19 DONE, /CSDB_GlycoCT_data.tar.gz

3. submit WURCS to GlyTouCan (Yamada) with an account at csdbglyconavi@gmail.com

4. get GlyTouCan ID and WURCS by SPARQL (SPARQL Query is supported by Mr. Fujita) (Yamada)

```sparql
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcterms: <http://purl.org/dc/terms/>
PREFIX glycan: <http://purl.jp/bio/12/glyco/glycan#>
PREFIX glytoucan:  <http://www.glytoucan.org/glyco/owl/glytoucan#>

SELECT distinct ?id ?WurcsSeq{
?user_submission_id <http://repository.sparqlite.com/terms#contributor>  <http://repository.sparqlite.com/user#{user id}>.
?structure_hash <http://repository.sparqlite.com/terms#submission> ?user_submission_id.
?structure_hash <http://glycosmos.org/batch/AccessionNumber/wurcs2GTCID/0.0.3-SNAPSHOT> ?glycan.
?glycan <http://www.glytoucan.org/glyco/owl/glytoucan#has_primary_id>  ?id.

  GRAPH <http://rdf.glytoucan.org/sequence/wurcs> {
    optional {
      ?glycan glycan:has_glycosequence ?GSeq .
      ?GSeq glycan:has_sequence ?WurcsSeq.
    }
  }

  FILTER NOT EXISTS {
   GRAPH <http://rdf.glytoucan.org/archive> {
     ?glycan rdf:type ?ArchiveSaccharide
   }
}
}
```

5. compare lists of GlyTouCan ID-WURCS, CSDB ID-WURCS and make a list of CSDB IDs and GlyTouCan IDs with matching WURCS (Yamada)

## result

* Save the list as a file for each database

  * 2024-01-28_bcsdb-gtc.tsv
  * 2024-01-28_fcsdb-gtc.tsv
  * 2024-01-28_pcsdb-gtc.tsv

```
% cat 2024-01-28_bcsdb-gtc.tsv | wc -l
   13444
% cat 2024-01-28_fcsdb-gtc.tsv | wc -l
    4191
% cat 2024-01-28_pcsdb-gtc.tsv | wc -l
    1665
```
