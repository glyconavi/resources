RES
1r:r1
REP
REP1:6o(3+1)2d=-1--1
RES
2b:a-dgal-HEX-1:5
3b:a-dglc-HEX-x:x
4b:b-dglc-HEX-1:5
5s:phosphate
6b:b-dgal-HEX-1:5
7b:o-dgro-TRI-0:0|1:aldi
LIN
1:2o(4+1)3d
2:3o(6+1)4d
3:3o(4+1)5n
4:4o(4+1)6d
5:5n(1+1)7o
