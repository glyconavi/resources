RES
1r:r1
REP
REP1:9o(3+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3s:n-acetyl
4b:b-dgal-HEX-1:5
5s:phosphate
6b:b-dglc-HEX-1:5
7s:n-acetyl
8b:o-dgro-TRI-0:0|1:aldi
9b:b-dglc-HEX-1:5|6:d
10s:n-acetyl
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4o(3+1)5n
4:4o(2+1)6d
5:6d(2+1)7n
6:5n(1+3)8o
7:8o(1+1)9d
8:9d(4+1)10n
