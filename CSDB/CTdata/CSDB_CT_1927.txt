RES
1r:r1
REP
REP1:7o(3+1)2n=-1--1
RES
2s:phosphate
3b:o-dman-HEX-0:0|1:aldi
4b:b-dgal-HEX-1:4
5b:b-dglc-HEX-1:5
6b:b-dgal-HEX-1:4
7b:b-dgal-HEX-1:5
LIN
1:2n(1+6)3o
2:3o(1+1)4d
3:4o(6+1)5d
4:5o(3+1)6d
5:6o(3+1)7d
