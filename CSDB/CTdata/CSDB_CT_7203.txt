RES
1b:b-dgal-HEX-1:5
2s:methyl
3b:b-dglc-HEX-1:5
4s:n-acetyl
5b:b-dgal-HEX-1:5
6b:b-dglc-HEX-1:5
7b:b-dgal-HEX-1:5
8b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d
9s:amino
10n:n1
11b:b-dglc-HEX-1:5
12b:b-dglc-HEX-1:5
13s:amino
14b:x-dgal-HEX-1:5
15b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d
16s:amino
17n:n2
LIN
1:1o(1+1)2n
2:1o(3+1)3d
3:3d(2+1)4n
4:3o(4+1)5d
5:3o(6+1)6d
6:6o(4+1)7d
7:5o(3+2)8d
8:8d(5+1)9n
9:8o(5+1)10n
10:7o(3+1)11d
11:11o(6+1)12d
12:11d(2+1)13n
13:11o(4+1)14d
14:14o(3+2)15d
15:15d(5+1)16n
16:15o(5+1)17n
NON
NON1
Parent:8
Linkage:o(5+1)n
SmallMolecule:SMILES [3CH3][2CH2][1C](=O)O
Description:propanoic acid
NON2
Parent:15
Linkage:o(5+1)n
SmallMolecule:SMILES [3CH3][2CH2][1C](=O)O
Description:propanoic acid
