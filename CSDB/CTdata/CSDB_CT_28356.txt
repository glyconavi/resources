RES
1r:r1
REP
REP1:2o(4+1)2d=-1--1
RES
2b:a-lgal-HEX-1:5|6:a
3s:n-acetyl
4s:n-acetyl
LIN
1:2d(2+1)3n
2:2d(3+1)4n
