RES
1b:a-dglc-HEX-1:5|6:a
2b:b-dglc-HEX-1:5|6:a
3b:a-dglc-HEX-1:5
4s:n-acetyl
5b:b-dglc-HEX-1:5
6b:a-dman-HEX-1:5
7b:a-dgal-HEX-1:5
LIN
1:1o(4+1)2d
2:2o(4+1)3d
3:3d(2+1)4n
4:3o(4+1)5d
5:5o(3+1)6d
6:6o(3+1)7d
