RES
1r:r1
REP
REP1:8o(3+1)2d=-1--1
RES
2b:a-dgal-HEX-1:5|6:a
3s:n-acetyl
4b:a-lman-HEX-1:5|6:d
5b:a-dglc-HEX-1:5
6s:n-acetyl
7s:n-acetyl
8b:a-lglc-HEX-1:5|6:d
9s:n-acetyl
LIN
1:2d(2+1)3n
2:2o(4+1)4d
3:2o(3+1)5d
4:4d(2+1)6n
5:5d(2+1)7n
6:5o(3+1)8d
7:8d(2+1)9n
