RES
1b:a-dgal-HEX-1:5
2s:n-acetyl
3b:a-dgal-HEX-1:5|6:a
4s:n-acetyl
5n:n1
6n:n2
LIN
1:1d(2+1)2n
2:1o(4+1)3d
3:3d(2+1)4n
4:3o(6+2)5n
5:5n(1+2)6n
NON
NON1
Parent:3
Linkage:o(6+2)n
SmallMolecule:SMILES [5C](=O)(O)[4CH2][3CH2][2C@H](N)[1C](=O)O
Description:(L)-glutamic acid
NON2
Parent:5
Linkage:n(1+2)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine
