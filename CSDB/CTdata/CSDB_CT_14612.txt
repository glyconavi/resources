RES
1b:b-dglc-HEX-1:5
2s:acetyl
3n:n1
4b:a-lman-HEX-1:5|6:d
5s:acetyl
6s:acetyl
7s:acetyl
LIN
1:1o(6+1)2n
2:1o(1+1)3n
3:1o(2+1)4d
4:4o(2+1)5n
5:4o(3+1)6n
6:4o(4+1)7n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES [6CH3][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:hexanoic acid
