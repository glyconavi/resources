RES
1n:n1
2n:n2
3s:phosphate
4n:n3
5b:a-dglc-HEX-1:5
6s:amino
7n:n4
8b:a-dman-HEX-1:5
9b:a-dman-HEX-1:5
10b:a-dman-HEX-1:5
11b:b-dgal-HEX-1:4
12b:a-dman-HEX-1:5
13b:b-dgal-HEX-1:4
LIN
1:1n(2+1)2n
2:1n(1+1)3n
3:3n(1+1)4n
4:4n(6+1)5d
5:5d(2+1)6n
6:5o(6+1)7n
7:5o(4+1)8d
8:8o(6+1)9d
9:9o(2+1)10d
10:10o(3+1)11d
11:10o(2+1)12d
12:12o(3+1)13d
NON
NON1
HistoricalEntity:any sphinganine
NON2
Parent:1
Linkage:n(2+1)n
SmallMolecule:SMILES [24CH3][23CH2][22CH2][21CH2][20CH2][19CH2][18CH2][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:lignoceric acid
NON3
Parent:3
Linkage:n(1+1)n
SmallMolecule:SMILES [6C@H](O)1[5C@H](O)[4C@@H](O)[3C@H](O)[2C@H](O)[1C@H](O)1
Description:myo-inositol
NON4
Parent:5
Linkage:o(6+1)n
SmallMolecule:SMILES [3CH2](N)[2CH2][1P](=O)(O)O
Description:2-amino-ethylphosphonic acid
