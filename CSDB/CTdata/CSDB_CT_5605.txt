RES
1b:x-dman-OCT-x:x|1:a|2:keto|3:d
2b:a-dman-OCT-2:6|1:a|2:keto|3:d
3b:b-lara-PEN-1:5
4b:a-lgro-dman-HEP-1:5
5b:b-dglc-HEX-1:5
6s:amino
7b:a-lgro-dman-HEP-1:5
8s:phospho-ethanolamine
9b:a-dgal-HEX-1:5|6:a
10b:a-lgro-dman-HEP-1:5
11b:b-dgal-HEX-1:5|6:a
12b:a-dglc-HEX-1:5
13s:amino
14n:n1
15b:b-dglc-HEX-1:5
16s:n-acetyl
17b:b-dgal-HEX-1:5|6:a
18s:phospho-ethanolamine
19n:n2
LIN
1:1o(4+2)2d
2:1o(8+1)3d
3:1o(5+1)4d
4:4o(4+1)5d
5:3d(4+1)6n
6:4o(3+1)7d
7:7o(6+1)8n
8:7o(3+1)9d
9:7o(7+1)10d
10:10o(7+1)11d
11:9o(4+1)12d
12:12d(2+1)13n
13:12o(2+1)14n
14:12o(6+1)15d
15:15d(2+1)16n
16:15o(3+1)17d
17:17o(4+1)18n
18:17o(6+1)19n
NON
NON1
Parent:12
Linkage:o(2+1)n
SmallMolecule:SMILES [2CH2](N)[1C](=O)O
Description:glycine
NON2
Parent:17
Linkage:o(6+1)n
SmallMolecule:SMILES [6CH2](N)[5CH2][4CH2][3CH2][2C@H](N)[1C](=O)O
Description:(L)-lysine
