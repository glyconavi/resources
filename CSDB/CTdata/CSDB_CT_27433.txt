RES
1r:r1
REP
REP1:7o(2+1)2d=-1--1
RES
2b:a-dgal-HEX-1:5
3b:b-dglc-HEX-1:5
4s:n-acetyl
5b:b-dglc-HEX-1:5
6b:a-lgal-HEX-1:5|6:d
7b:a-dman-HEX-1:5|6:d
8s:n-acetyl
LIN
1:2o(4+1)3d
2:2d(2+1)4n
3:2o(3+1)5d
4:5o(4+1)6d
5:6o(3+1)7d
6:7d(4+1)8n
