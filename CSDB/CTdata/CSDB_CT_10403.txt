RES
1r:r1
REP
REP1:6n(2+1)2n=-1--1
RES
2s:phosphate
3b:o-drib-PEN-0:0|1:aldi
4b:a-lman-HEX-1:5|6:d
5b:a-dglc-HEX-1:5
6a:a1
LIN
1:2n(1+5)3o
2:3o(4+1)4d
3:4o(3+1)5d
4:5o(3+1)6n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:7
RES
7b:a-dglc-HEX-1:5
ALTSUBGRAPH2
LEAD-IN RES:8
RES
8b:a-dgal-HEX-1:5
