RES
1b:a-dglc-HEX-1:5
2s:amino
3n:n1
4n:n2
5s:phosphate
6b:b-dglc-HEX-1:5
7s:phosphate
8s:amino
9n:n3
10b:b-lara-PEN-1:5
11n:n4
12b:a-dman-OCT-2:6|1:a|2:keto|3:d
13s:amino
14n:n5
15b:a-dgro-dtal-OCT-2:6|1:a|2:keto
16b:a-lgro-dman-HEP-1:5
17b:b-dglc-HEX-1:5
18b:b-lara-PEN-1:5
19b:a-lgro-dman-HEP-1:5
20b:a-lgro-dman-HEP-1:5
21s:amino
22b:a-dgal-HEX-1:5
23b:a-dglc-HEX-1:5
24b:a-lgro-dman-HEP-1:5
25b:b-dglc-HEX-1:5|6:d
26b:a-lman-HEX-1:5|6:d
27s:n-acetyl
LIN
1:1d(2+1)2n
2:1o(2+1)3n
3:1o(3+1)4n
4:1o(1+1)5n
5:1o(6+1)6d
6:6o(4+1)7n
7:6d(2+1)8n
8:6o(3+1)9n
9:5n(1+1)10o
10:6o(2+1)11n
11:6o(6+2)12d
12:10d(4+1)13n
13:11n(3+1)14n
14:12o(4+2)15d
15:12o(5+1)16d
16:16o(4+1)17d
17:15o(8+1)18d
18:16o(3+1)19d
19:19o(7+1)20d
20:18d(4+1)21n
21:19o(3+1)22d
22:22o(6+1)23d
23:22o(2+1)24d
24:24o(7+1)25d
25:25o(3+1)26d
26:25d(2+1)27n
NON
NON1
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-hexadecanoic acid
NON2
Parent:1
Linkage:o(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-tetradecanoic acid
NON3
Parent:6
Linkage:o(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-tetradecanoic acid
NON4
Parent:6
Linkage:o(2+1)n
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-hexadecanoic acid
NON5
Parent:11
Linkage:n(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:myristic acid
