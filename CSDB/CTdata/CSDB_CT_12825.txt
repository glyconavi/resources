RES
	glyco:in_carbohydrate_format glyco:smiles .
<http://csdb.glycoscience.ru/integration/make_rdf.php?db=database&mode=structure&id_list=13390#GLYCAN_WURCS_0_0>
	a glyco:glycosequence ;
	glyco:has_sequence "WURCS=2.0/5,6,4/[a2122h-1x_1-5_2*NCC/3=O][a1221m-1a_1-5][a2122h-1b_1-5_2*NCC/3=O][a2122h-1b_1-5_2*NCCCCCCCCCCCCCCCCCC/3=O][]/1-2-3-3-4-5/a4-c1_a6-b1_c4-d1_d4-e1"^^xsd:string ;
