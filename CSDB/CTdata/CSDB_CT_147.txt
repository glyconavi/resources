RES
1b:b-dglc-HEX-1:5
2s:n-acetyl
3n:n1
LIN
1:1d(2+1)2n
2:1o(1+2)3n
NON
NON1
Parent:1
Linkage:o(1+2)n
SmallMolecule:SMILES OC{2}C{3}(C(C(C(CCO)O)O)O)O
Description:6-deoxy-heptitol
