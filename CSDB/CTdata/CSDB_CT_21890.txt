RES
1r:r1
REP
REP1:7n(4+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5|6:d
3s:acetyl
4s:n-acetyl
5s:amino
6n:n1
7a:a1
8s:acetyl
9n:n2
LIN
1:2o(3+1)3n
2:2d(2+1)4n
3:2d(4+1)5n
4:2o(4+5)6n
5:6n(2+2)7n
6:7n(5+1)8n
7:7n(7+1)9n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:10
LEAD-OUT RES:10+9|10+8
RES
10b:a-lgro-lman-NON-2:6|1:a|2:keto|3:d|9:d
11s:amino
12s:amino
13s:amino
LIN
1:10d(8+1)11n
2:10d(5+1)12n
3:10d(7+1)13n
ALTSUBGRAPH2
LEAD-IN RES:14
LEAD-OUT RES:14+9|14+8
RES
14n:n3
NON
NON1
Parent:2
Linkage:o(4+5)n
SmallMolecule:SMILES [5C](=O)(O)[4CH2][3CH2][2C@H](O)[1C](=O)(O)
Description:(S)-2-hydroxyglutaric acid
NON2
Parent:7
Linkage:n(7+1)n
SmallMolecule:SMILES [4CH3][3CH2][2C@H](O)[1C](=O)O
Description:(S)-2-hydroxybutanoic acid
NON3
SmallMolecule:SMILES O{4}[C@@H](C1){5}[C@@H]([C@H]({7}[C@H]([C@@H](C)N)N)O{2}[C@]1(C(O)=O)O)N
Description:8-amino-8-epi-pseudaminic acid
