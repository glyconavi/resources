RES
1b:a-dglc-HEX-1:5
2a:a1
3s:phosphate
4s:amino
5n:n1
6b:b-dglc-HEX-1:5
7s:phosphate
8s:amino
9n:n2
10n:n3
11b:a-dman-OCT-2:6|1:a|2:keto|3:d
12n:n4
13b:a-dman-OCT-2:6|1:a|2:keto|3:d
14b:a-dglc-HEX-1:5
15b:x-HEX-1:5
16b:x-HEX-1:5
17s:phosphate
18b:a-dglc-HEX-1:5|6:a
19b:a-dglc-HEX-1:5
20b:x-HEX-1:5
LIN
1:1o(2+1)2n
2:1o(1+1)3n
3:1d(2+1)4n
4:1o(3+1)5n
5:1o(6+1)6d
6:6o(4+1)7n
7:6d(2+1)8n
8:6o(3+1)9n
9:6o(2+1)10n
10:6o(6+2)11d
11:10n(3+1)12n
12:11o(4+2)13d
13:11o(5+1)14d
14:14o(-1+1)15d
15:14o(-1+1)16d
16:14o(-1+1)17n
17:13o(4+1)18d
18:18o(4+1)19d
19:18o(-1+1)20d
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:21
RES
21n:n5
ALTSUBGRAPH2
LEAD-IN RES:22
RES
22n:n6
NON
NON1
Parent:1
Linkage:o(3+1)n
SmallMolecule:SMILES [10CH3][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-decanoic acid
NON2
Parent:6
Linkage:o(3+1)n
SmallMolecule:SMILES [10CH3][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-decanoic acid
NON3
Parent:6
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON4
Parent:10
Linkage:n(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH]=[7CH][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:tetradec-7-enoic acid
NON5
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C](=O)[2CH2][1C](=O)O
Description:3-oxo-tetradecanoic acid
NON6
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
