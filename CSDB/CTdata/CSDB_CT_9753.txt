RES
1r:r1
REP
REP1:6o(2+1)2d=-1--1
RES
2b:a-dglc-HEX-1:5|6:a
3s:ethanolamine
4b:a-dgro-lglc-HEP-1:5
5b:b-dgal-HEX-1:4
6b:b-drib-PEN-1:4
7s:n-acetyl
8s:methyl
9n:n1
10n:n2
LIN
1:2d(6+2)3n
2:2o(3+1)4d
3:2o(4+1)5d
4:5o(5+1)6d
5:5d(2+1)7n
6:4o(6+1)8n
7:4o(4+1)9n
8:5o(3+1)10n
NON
NON1
Parent:4
Linkage:o(4+1)n
SmallMolecule:SMILES {1}OP(OC)(N)=O
Description:O-methyl phosphamide (OHPO(NH2)OMe)
NON2
Parent:5
Linkage:o(3+1)n
SmallMolecule:SMILES {1}OP(OC)(N)=O
Description:O-methyl phosphamide (OHPO(NH2)OMe)
