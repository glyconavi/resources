RES
1b:a-dglc-HEX-1:5
2s:phosphate
3s:amino
4n:n1
5b:b-dglc-HEX-1:5
6s:amino
7n:n2
8s:phosphate
9b:a-dman-OCT-2:6|1:a|2:keto|3:d
10b:a-dman-OCT-2:6|1:a|2:keto|3:d
11b:b-lara-PEN-1:5
12b:b-lara-PEN-1:5
13b:a-lgro-dman-HEP-1:5
14b:b-dglc-HEX-1:5
15s:amino
16s:amino
17b:a-lgro-dman-HEP-1:5
18s:phospho-ethanolamine
19b:a-dgal-HEX-1:5|6:a
20b:a-lgro-dman-HEP-1:5
21b:a-dgal-HEX-1:5
22b:b-dgal-HEX-1:5|6:a
23a:a1
24s:amino
LIN
1:1o(1+1)2n
2:1d(2+1)3n
3:1o(2+1)4n
4:1o(6+1)5d
5:5d(2+1)6n
6:5o(2+1)7n
7:5o(4+1)8n
8:5o(6+2)9d
9:9o(4+2)10d
10:8n(1+1)11o
11:9o(8+1)12d
12:9o(5+1)13d
13:13o(4+1)14d
14:11d(4+1)15n
15:12d(4+1)16n
16:13o(3+1)17d
17:17o(6+1)18n
18:17o(3+1)19d
19:17o(7+1)20d
20:19o(4+1)21d
21:20o(7+1)22d
22:22o(6+1)23n
23:21d(2+1)24n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:25
RES
25n:n3
ALTSUBGRAPH2
LEAD-IN RES:26
RES
26n:n4
NON
NON1
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-tetradecanoic acid
NON2
Parent:5
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-tetradecanoic acid
NON3
SmallMolecule:SMILES N{1}CCCNCCCCN
Description:spermidine
NON4
SmallMolecule:SMILES N{1}CCCCN
Description:putrescine
