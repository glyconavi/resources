RES
1n:n1
2b:a-lgro-dgal-NON-2:6|1:a|2:keto|3:d|9:d
3s:n-acetyl
4s:n-acetyl
LIN
1:1n(1+2)2d
2:2d(5+1)3n
3:2d(7+1)4n
NON
NON1
SmallMolecule:SMILES [2CH2](O)[1CH2](O)
Description:ethylene glycol
