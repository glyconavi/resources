RES
1r:r1
REP
REP1:8o(3+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3s:n-acetyl
4b:a-dgal-HEX-1:5
5s:n-acetyl
6b:b-dgal-HEX-1:5
7s:n-acetyl
8b:b-dglc-HEX-1:5
9n:n1
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4d(2+1)5n
4:4o(4+1)6d
5:6d(2+1)7n
6:6o(3+1)8d
7:8o(4+2)9n
NON
NON1
Parent:8
Linkage:o(4+2)n
SmallMolecule:SMILES O=C1O[C@H](C)C{2}[C@@H]1O
Description:(2S,4R)-2,4-dihydroxypentanoic acid 1,4-lactone
