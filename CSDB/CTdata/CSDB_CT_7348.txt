RES
1b:o-xgro-TRI-0:0|1:aldi
2a:a1
3n:n1
4s:phosphate
5n:n2
6b:a-dglc-HEX-1:5
7s:amino
8b:a-dman-HEX-1:5
9b:b-dgal-HEX-1:5
10b:a-dman-HEX-1:5
11b:a-dman-HEX-1:5
12s:n-acetyl
LIN
1:1o(2+1)2n
2:1o(1+1)3n
3:1o(3+1)4n
4:4n(1+1)5n
5:5n(6+1)6d
6:6d(2+1)7n
7:6o(4+1)8d
8:8o(4+1)9d
9:8o(6+1)10d
10:10o(2+1)11d
11:9d(2+1)12n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:13
RES
13n:n3
ALTSUBGRAPH2
LEAD-IN RES:14
RES
14n:n4
NON
NON1
Parent:1
Linkage:o(1+1)n
HistoricalEntity:superclass: lipid residue
NON2
Parent:4
Linkage:n(1+1)n
SmallMolecule:SMILES [6C@H](O)1[5C@H](O)[4C@@H](O)[3C@H](O)[2C@H](O)[1C@H](O)1
Description:myo-inositol
NON3
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:palmitic acid
NON4
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2]/[10CH]=[9CH]\[8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:oleic acid
