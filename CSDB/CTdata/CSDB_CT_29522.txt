RES
1b:b-dgal-HEX-1:5
2s:n-acetyl
3s:phosphate
4b:a-dgal-HEX-1:5
5s:n-acetyl
6n:n1
7s:phosphate
8n:n2
LIN
1:1d(2+1)2n
2:1o(6+1)3n
3:1o(3+1)4d
4:4d(2+1)5n
5:3n(1+1)6n
6:4o(6+1)7n
7:7n(1+1)8n
NON
NON1
Parent:3
Linkage:n(1+1)n
SmallMolecule:SMILES C[N+](C)(C)[2CH2][1CH2]O
Description:choline
NON2
Parent:7
Linkage:n(1+1)n
SmallMolecule:SMILES C[N+](C)(C)[2CH2][1CH2]O
Description:choline
