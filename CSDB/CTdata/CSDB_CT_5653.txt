RES
1b:a-dglc-HEX-1:5
2s:phosphate
3s:amino
4n:n1
5b:b-dglc-HEX-1:5
6s:phosphate
7s:amino
8n:n2
9b:a-dman-OCT-2:6|1:a|2:keto|3:d
10b:a-dman-OCT-2:6|1:a|2:keto|3:d
11b:b-lara-PEN-1:5
12b:a-lgro-dman-HEP-1:5
13b:b-dglc-HEX-1:5
14s:amino
15b:a-lgro-dman-HEP-1:5
16s:phospho-ethanolamine
17b:a-lgro-dman-HEP-1:5
18b:a-dgal-HEX-1:5|6:a
19b:a-dgal-HEX-1:5
20b:b-dgal-HEX-1:5|6:a
21a:a1
22s:amino
LIN
1:1o(1+1)2n
2:1d(2+1)3n
3:1o(2+1)4n
4:1o(6+1)5d
5:5o(4+1)6n
6:5d(2+1)7n
7:5o(2+1)8n
8:5o(6+2)9d
9:9o(4+2)10d
10:9o(8+1)11d
11:9o(5+1)12d
12:12o(4+1)13d
13:11d(4+1)14n
14:12o(3+1)15d
15:15o(6+1)16n
16:15o(7+1)17d
17:15o(3+1)18d
18:18o(4+1)19d
19:17o(7+1)20d
20:20o(6+1)21n
21:19d(2+1)22n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:23
RES
23n:n3
ALTSUBGRAPH2
LEAD-IN RES:24
RES
24n:n4
NON
NON1
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-tetradecanoic acid
NON2
Parent:5
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-tetradecanoic acid
NON3
SmallMolecule:SMILES N{1}CCCNCCCCN
Description:spermidine
NON4
SmallMolecule:SMILES N{1}CCCCN
Description:putrescine
