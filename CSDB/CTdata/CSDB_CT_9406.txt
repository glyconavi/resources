RES
1n:n1
2b:a-dgal-HEX-1:5
3s:phosphate
4b:b-dgal-HEX-1:5
5s:n-acetyl
6s:phospho-ethanolamine
7n:n2
LIN
1:1n(1+1)2d
2:2o(6+1)3n
3:2o(3+1)4d
4:4d(2+1)5n
5:4o(6+1)6n
6:3n(1+1)7n
NON
NON1
SmallMolecule:SMILES [2CH2](O)[1CH2](O)
Description:ethylene glycol
NON2
Parent:3
Linkage:n(1+1)n
SmallMolecule:SMILES [2CH2](O)[1CH2](O)
Description:ethylene glycol
