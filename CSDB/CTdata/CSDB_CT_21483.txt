RES
1b:o-xgro-TRI-0:0
2b:b-dglc-HEX-1:5
3s:n-acetyl
4s:(s)-carboxyethyl
5b:b-dglc-HEX-1:5
6b:a-lman-HEX-1:5|6:d
7s:n-acetyl
LIN
1:1o(2+1)2d
2:2d(2+1)3n
3:2o(4+2)4n
4:2o(3+1)5d
5:5o(3+1)6d
6:5d(2+1)7n
