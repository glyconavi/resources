RES
1r:r1
REP
REP1:13o(4+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3s:n-acetyl
4s:(r)-carboxyethyl
5n:n1
6b:b-dglc-HEX-1:5
7s:n-acetyl
8n:n2
9b:b-dglc-HEX-1:5
10s:(r)-carboxyethyl
11s:anhydro
12s:amino
13b:b-dglc-HEX-1:5
14n:n3
15s:n-acetyl
16n:n4
LIN
1:2d(2+1)3n
2:2o(3+1)4n
3:2o(8+2)5n
4:2o(4+1)6d
5:6d(2+1)7n
6:5n(1+2)8n
7:6o(4+1)9d
8:9o(3+1)10n
9:9d(2+1)11n
10:9o(8+1)11n
11:9d(2+1)12n
12:9o(4+1)13d
13:8n(-1+-1)14n
14:13d(2+1)15n
15:14n(-1+-1)16n
NON
NON1
Parent:2
Linkage:o(8+2)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine
NON2
Parent:5
Linkage:n(1+2)n
SmallMolecule:SMILES [5C](=O)(O)[4CH2][3CH2][2C@@H](N)[1C](=O)O
Description:(D)-glutamic acid
NON3
Parent:8
Linkage:n(-1+-1)n
SmallMolecule:SMILES [7C](=O)(O)[6C@H](N)[5CH2][4CH2][3CH2][2C@H](N)[1C](=O)O
Description:meso-diaminopimelic acid
NON4
Parent:14
Linkage:n(-1+-1)n
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine
