RES
1b:a-lman-HEX-1:5|6:d
2a:a1
LIN
1:1o(1+3)2n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:3
RES
3n:n1
ALTSUBGRAPH2
LEAD-IN RES:4
RES
4n:n2
ALTSUBGRAPH3
LEAD-IN RES:5
RES
5n:n3
NON
NON1
SmallMolecule:SMILES [8CH3][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-octanoic acid
NON2
SmallMolecule:SMILES [10CH3][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-decanoic acid
NON3
HistoricalEntity:superclass: lipid residue
