RES
1b:o-dgro-TRI-0:0|1:aldi
2b:b-dgal-HEX-1:5
3n:n1
4n:n2
LIN
1:1o(3+1)2d
2:1o(1+1)3n
3:1o(2+1)4n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES [18CH3][17CH2][16CH]=[15CH][14CH2][13CH]=[12CH][11CH2][10CH]=[9CH][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:(9,12,15)-linolenic acid
NON2
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [16CH3][15CH2]/[14CH]=[13CH]\[12CH2]/[11CH]=[10CH]\[9CH2]/[8CH]=[7CH]\[6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:cis-7,10,13-hexadecatrienoic acid
