RES
1b:b-dgal-HEX-1:5
2b:a-lman-HEX-1:5|6:d
3b:a-lman-HEX-1:5|6:d
4b:a-lman-HEX-1:5|6:d
5b:a-lman-HEX-1:5|6:d
LIN
1:1o(3+1)2d
2:2o(3+1)3d
3:3o(3+1)4d
4:4o(3+1)5d
