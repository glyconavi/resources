RES
1r:r1
REP
REP1:6o(8+1)2d=-1--1
RES
2b:a-dglc-HEX-1:5|6:d
3s:n-acetyl
4b:a-lgal-HEX-1:5|6:d
5s:n-acetyl
6b:a-lgro-dgal-NON-2:6|1:a|2:keto|3:d|9:d
7s:n-acetyl
8s:n-acetyl
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4d(2+1)5n
4:4o(3+2)6d
5:6d(5+1)7n
6:6d(7+1)8n
