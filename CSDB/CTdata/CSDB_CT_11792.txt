RES
1r:r1
REP
REP1:9o(2+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3s:n-acetyl
4b:a-dgal-HEX-1:5
5s:n-acetyl
6b:a-dglc-HEX-1:5
7s:n-acetyl
8b:a-dglc-HEX-1:5
9b:b-dglc-HEX-1:5
10s:n-acetyl
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4d(2+1)5n
4:4o(4+1)6d
5:6d(2+1)7n
6:6o(6+1)8d
7:8o(6+1)9d
8:8d(2+1)10n
