RES
1b:a-dglc-HEX-1:5|6:d
2s:n-acetyl
3s:n-acetyl
4b:b-dglc-HEX-1:5|6:a
5s:amino
6n:n1
LIN
1:1d(2+1)2n
2:1d(4+1)3n
3:1o(3+1)4d
4:4d(2+1)5n
5:4o(2+1)6n
NON
NON1
Parent:4
Linkage:o(2+1)n
SmallMolecule:SMILES [2CH3][1C](=N)O
Description:acetimidic acid
