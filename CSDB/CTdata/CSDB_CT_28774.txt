RES
1r:r1
REP
REP1:3o(6+2)2d=-1--1
RES
2b:b-dman-OCT-2:6|1:a|2:keto|3:d
3b:a-dglc-HEX-1:5
4s:n-acetyl
LIN
1:2o(5+1)3d
2:3d(2+1)4n
