RES
1b:a-dman-OCT-2:6|1:a|2:keto|3:d
2a:a1
3b:a-lgro-dman-HEP-1:5
4b:b-dglc-HEX-1:5
5s:phospho-ethanolamine
6b:a-lgro-dman-HEP-1:5
7b:b-dglc-HEX-1:5
8b:a-lgro-dman-HEP-1:5
9a:a2
10s:n-acetyl
LIN
1:1o(4+2)2n
2:1o(5+1)3d
3:3o(4+1)4d
4:2n(7+1)5n
5:3o(3+1)6d
6:6o(3+1)7d
7:6o(7+1)8d
8:8o(7+1)9n
9:7d(2+1)10n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:11
LEAD-OUT RES:11+5
RES
11b:a-dgro-dtal-OCT-2:6|1:a|2:keto
ALTSUBGRAPH2
LEAD-IN RES:12
LEAD-OUT RES:12+5
RES
12b:a-dman-OCT-2:6|1:a|2:keto|3:d
ALT2
ALTSUBGRAPH1
LEAD-IN RES:13
RES
13b:b-dgal-HEX-1:5
ALTSUBGRAPH2
LEAD-IN RES:14
RES
14b:a-dgro-dman-HEP-1:5
