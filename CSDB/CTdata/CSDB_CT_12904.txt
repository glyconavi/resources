RES
1b:o-xgro-TRI-0:0|1:aldi
2n:n1
3n:n2
4b:a-dglc-HEX-1:5
5b:a-dglc-HEX-1:5
6s:phosphate
7b:o-xgro-TRI-0:0|1:aldi
8n:n3
9s:phosphate
10b:o-xgro-TRI-0:0|1:aldi
11n:n4
12s:phosphate
13b:o-xgro-TRI-0:0|1:aldi
14n:n5
LIN
1:1o(1+1)2n
2:1o(2+1)3n
3:1o(3+1)4d
4:4o(2+1)5d
5:5o(6+1)6n
6:6n(1+1)7o
7:7o(2+1)8n
8:7o(3+1)9n
9:9n(1+1)10o
10:10o(2+1)11n
11:10o(3+1)12n
12:12n(1+1)13o
13:13o(2+1)14n
NON
NON1
Parent:1
Linkage:o(1+1)n
HistoricalEntity:superclass: lipid residue
NON2
Parent:1
Linkage:o(2+1)n
HistoricalEntity:superclass: lipid residue
NON3
Parent:7
Linkage:o(2+1)n
SmallMolecule:SMILES [3CH3][2CH](N)[1C](=O)O
Description:alanine
NON4
Parent:10
Linkage:o(2+1)n
SmallMolecule:SMILES [3CH3][2CH](N)[1C](=O)O
Description:alanine
NON5
Parent:13
Linkage:o(2+1)n
SmallMolecule:SMILES [3CH3][2CH](N)[1C](=O)O
Description:alanine
