RES
1b:o-lman-HEX-0:0|1:aldi|6:d
2b:b-dgal-HEX-1:5
3b:a-lman-HEX-1:5|6:d
4b:a-lman-HEX-1:5|6:d
5b:b-dglc-HEX-1:5|6:a
6b:a-lman-HEX-1:5|6:d
7b:a-lman-HEX-1:5|6:d
8b:b-dgal-HEX-1:5
LIN
1:1o(2+1)2d
2:2o(3+1)3d
3:3o(2+1)4d
4:4o(2+1)5d
5:5o(4+1)6d
6:6o(2+1)7d
7:7o(2+1)8d
