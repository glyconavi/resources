RES
1b:x-dman-OCT-x:x|1:a|2:keto|3:d
2b:a-dman-OCT-2:6|1:a|2:keto|3:d
3b:a-lgro-dman-HEP-1:5
4b:b-dglc-HEX-1:5
5b:a-lgro-dman-HEP-1:5
6b:a-lgro-dman-HEP-1:5
7b:b-dglc-HEX-1:5
8b:a-dgro-dman-HEP-1:5
9s:n-acetyl
10b:a-dgal-HEX-1:5
11b:b-dgro-dman-HEP-1:5
12b:a-dxyl-HEX-1:5|3:d|6:d
LIN
1:1o(4+2)2d
2:1o(5+1)3d
3:3o(4+1)4d
4:3o(3+1)5d
5:5o(7+1)6d
6:5o(3+1)7d
7:6o(7+1)8d
8:7d(2+1)9n
9:7o(3+1)10d
10:10o(4+1)11d
11:11o(3+1)12d
