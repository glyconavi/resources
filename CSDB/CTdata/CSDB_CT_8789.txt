RES
1b:a-dman-HEX-1:5|6:d
2s:methyl
3s:amino
4n:n1
5b:a-dman-HEX-1:5|6:d
6s:amino
7n:n2
8b:a-dman-HEX-1:5|6:d
9s:amino
10n:n3
11b:a-dman-HEX-1:5|6:d
12s:amino
13n:n4
14b:a-dman-HEX-1:5|6:d
15s:amino
16n:n5
LIN
1:1o(1+1)2n
2:1d(4+1)3n
3:1o(4+1)4n
4:1o(2+1)5d
5:5d(4+1)6n
6:5o(4+1)7n
7:5o(2+1)8d
8:8d(4+1)9n
9:8o(4+1)10n
10:8o(2+1)11d
11:11d(4+1)12n
12:11o(4+1)13n
13:11o(2+1)14d
14:14d(4+1)15n
15:14o(4+1)16n
NON
NON1
Parent:1
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH2](O)[3CH2][2C@H](O)[1C](=O)O
Description:(S)-2,4-dihydroxybutanoic acid
NON2
Parent:5
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH2](O)[3CH2][2C@H](O)[1C](=O)O
Description:(S)-2,4-dihydroxybutanoic acid
NON3
Parent:8
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH2](O)[3CH2][2C@H](O)[1C](=O)O
Description:(S)-2,4-dihydroxybutanoic acid
NON4
Parent:11
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH2](O)[3CH2][2C@H](O)[1C](=O)O
Description:(S)-2,4-dihydroxybutanoic acid
NON5
Parent:14
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH2](O)[3CH2][2C@H](O)[1C](=O)O
Description:(S)-2,4-dihydroxybutanoic acid
