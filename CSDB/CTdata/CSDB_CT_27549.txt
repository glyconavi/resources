RES
1r:r1
REP
REP1:6o(4+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3s:n-acetyl
4b:b-dgal-HEX-1:5
5b:a-dman-HEX-1:5
6b:b-dglc-HEX-1:5|6:a
7b:a-lman-HEX-1:5|6:d
8s:acetyl
9s:acetyl
10s:acetyl
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4o(4+1)5d
4:5o(2+1)6d
5:5o(3+1)7d
6:7o(2+1)8n
7:7o(3+1)9n
8:7o(4+1)10n
