RES
1r:r1
REP
REP1:7o(3+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3b:x-dgal-HEX-1:5
4b:b-dglc-HEX-1:5
5b:b-dglc-HEX-1:5|6:a
6b:x-dgal-HEX-1:5
7b:b-dglc-HEX-1:5
8s:pyruvate
LIN
1:2o(6+1)3d
2:3o(4+1)4d
3:3o(6+1)5d
4:3o(3+1)6d
5:6o(3+1)7d
6:7o(6+2)8n
7:7o(4+2)8n
