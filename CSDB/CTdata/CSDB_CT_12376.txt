RES
1b:a-dglc-HEX-1:5
2s:phosphate
3n:n1
4b:b-dglc-HEX-1:5
5s:phosphate
6n:n2
7n:n3
8n:n4
9n:n5
LIN
1:1o(1+1)2n
2:1o(2+1)3n
3:1o(6+1)4d
4:4o(4+1)5n
5:4o(3+1)6n
6:3n(3+1)7n
7:4o(2+1)8n
8:8n(3+1)9n
NON
NON1
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-dodecanoic acid
NON2
Parent:4
Linkage:o(3+1)n
SmallMolecule:SMILES [10CH3][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-decanoic acid
NON3
Parent:3
Linkage:n(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:lauric acid
NON4
Parent:4
Linkage:o(2+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-dodecanoic acid
NON5
Parent:8
Linkage:n(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:lauric acid
