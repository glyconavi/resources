RES
1n:n1
2s:phosphate
3b:a-dman-HEX-1:5
4b:a-dman-HEX-1:5
5n:n2
6b:a-dman-HEX-1:5
7b:o-xgro-TRI-0:0|1:aldi
8b:a-dman-HEX-1:5
9n:n3
10n:n4
11b:a-dman-HEX-1:5
12b:a-dman-HEX-1:5
13b:a-dman-HEX-1:5
14b:a-dman-HEX-1:5
15b:a-dman-HEX-1:5
16b:a-dman-HEX-1:5
17b:a-dman-HEX-1:5
LIN
1:1n(1+1)2n
2:1n(2+1)3d
3:1n(6+1)4d
4:3o(6+1)5n
5:4o(6+1)6d
6:2n(1+1)7o
7:6o(2+1)8d
8:7o(2+1)9n
9:7o(3+1)10n
10:6o(6+1)11d
11:11o(2+1)12d
12:11o(6+1)13d
13:13o(2+1)14d
14:13o(6+1)15d
15:15o(2+1)16d
16:15o(6+1)17d
NON
NON1
SmallMolecule:SMILES [6C@H](O)1[5C@H](O)[4C@@H](O)[3C@H](O)[2C@H](O)[1C@H](O)1
Description:myo-inositol
NON2
Parent:3
Linkage:o(6+1)n
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:stearic acid
NON3
Parent:7
Linkage:o(2+1)n
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:stearic acid
NON4
Parent:7
Linkage:o(3+1)n
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:stearic acid
