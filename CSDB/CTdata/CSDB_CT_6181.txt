RES
1b:o-xgro-TRI-0:0|1:aldi
2a:a1
3n:n1
4s:phosphate
5n:n2
6b:a-dglc-HEX-1:5
7s:amino
8s:phosphate
9b:a-dman-HEX-1:5
10n:n3
11b:a-dman-HEX-1:5
12b:a-dman-HEX-1:5
13b:a-dman-HEX-1:5
14s:phospho-ethanolamine
LIN
1:1o(2+1)2n
2:1o(1+1)3n
3:1o(3+1)4n
4:4n(1+1)5n
5:5n(6+1)6d
6:6d(2+1)7n
7:6o(6+1)8n
8:6o(4+1)9d
9:8n(1+1)10n
10:9o(6+1)11d
11:11o(2+1)12d
12:12o(2+1)13d
13:12o(6+1)14n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:15
RES
15n:n4
ALTSUBGRAPH2
LEAD-IN RES:16
RES
16n:n5
NON
NON1
Parent:1
Linkage:o(1+1)n
HistoricalEntity:superclass: alcohol residue
NON2
Parent:4
Linkage:n(1+1)n
SmallMolecule:SMILES [6C@H](O)1[5C@H](O)[4C@@H](O)[3C@H](O)[2C@H](O)[1C@H](O)1
Description:myo-inositol
NON3
Parent:8
Linkage:n(1+1)n
SmallMolecule:SMILES [3CH2](N)[2CH2][1P](=O)(O)O
Description:2-amino-ethylphosphonic acid
NON4
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2]/[10CH]=[9CH]\[8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:oleic acid
NON5
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2]/[13CH]=[12CH]\[11CH2]/[10CH]=[9CH]\[8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:linoleic acid
