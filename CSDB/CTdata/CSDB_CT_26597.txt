RES
1b:a-dman-OCT-2:6|1:a|2:keto|3:d
2a:a1
3b:a-lgro-dman-HEP-1:5
4b:b-dglc-HEX-1:5
5s:phospho-ethanolamine
6b:a-lgro-dman-HEP-1:5
7b:a-lgro-dman-HEP-1:5
8b:b-dgal-HEX-1:5
LIN
1:1o(4+2)2n
2:1o(5+1)3d
3:3o(4+1)4d
4:2n(7+1)5n
5:3o(3+1)6d
6:6o(7+1)7d
7:7o(7+1)8d
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:9
LEAD-OUT RES:9+5
RES
9b:a-dgro-dtal-OCT-2:6|1:a|2:keto
ALTSUBGRAPH2
LEAD-IN RES:10
LEAD-OUT RES:10+5
RES
10b:a-dman-OCT-2:6|1:a|2:keto|3:d
