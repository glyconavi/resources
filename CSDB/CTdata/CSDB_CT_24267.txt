RES
1b:b-dgal-HEX-1:5
2b:a-lman-HEX-1:5|6:d
3b:a-lman-HEX-1:5|6:d
4b:a-lman-HEX-1:5|6:d
5b:b-dman-HEX-1:5
6b:b-dman-HEX-1:5
7b:a-dglc-HEX-1:5
8s:n-acetyl
9s:n-acetyl
10b:b-dgal-HEX-1:5
11s:phosphate
12n:n1
13b:b-dman-HEX-1:5
14b:a-dglc-HEX-1:5
15s:n-acetyl
16b:b-dgal-HEX-1:5
17b:b-dman-HEX-1:5
18b:a-dglc-HEX-1:5
19b:b-dgal-HEX-1:5
20s:n-acetyl
LIN
1:1o(3+1)2d
2:2o(3+1)3d
3:3o(3+1)4d
4:4o(4+1)5d
5:4o(3+1)6d
6:6o(6+1)7d
7:6d(2+1)8n
8:5d(2+1)9n
9:6o(4+1)10d
10:5o(4+1)11n
11:11n(1+2)12n
12:10o(3+1)13d
13:13o(6+1)14d
14:13d(2+1)15n
15:13o(4+1)16d
16:16o(3+1)17d
17:17o(6+1)18d
18:17o(4+1)19d
19:17d(2+1)20n
NON
NON1
Parent:11
Linkage:n(1+2)n
SmallMolecule:SMILES [3CH2](O)[2CH](O)[1C](=O)O
Description:glyceric acid
