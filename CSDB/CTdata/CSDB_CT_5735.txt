RES
1b:a-lman-HEX-1:5|6:d
2s:methyl
3n:n1
4b:a-lman-HEX-1:5|6:d
5s:methyl
6s:methyl
7b:b-dglc-HEX-1:5
8s:methyl
9s:methyl
LIN
1:1o(3+1)2n
2:1o(1+1)3n
3:1o(2+1)4d
4:4o(2+1)5n
5:4o(3+1)6n
6:4o(4+1)7d
7:7o(3+1)8n
8:7o(6+1)9n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES [3CH3][2CH2][1CH2]O
Description:propanol
