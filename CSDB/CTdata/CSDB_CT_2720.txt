RES
1r:r1
REP
REP1:8o(3+1)2n=-1--1
RES
2s:phosphate
3b:o-xgro-TRI-0:0|1:aldi
4b:b-dglc-HEX-1:5|6:d
5s:n-acetyl
6b:a-dglc-HEX-1:5
7s:n-acetyl
8b:b-dgal-HEX-1:5
9b:a-dglc-HEX-1:5
10b:b-dglc-HEX-1:5
11s:n-acetyl
LIN
1:2n(1+3)3o
2:3o(1+1)4d
3:4d(4+1)5n
4:4o(3+1)6d
5:6d(2+1)7n
6:6o(3+1)8d
7:8o(4+1)9d
8:8o(2+1)10d
9:10d(2+1)11n
