RES
1b:a-dman-HEX-1:5|6:d
2s:amino
3n:n1
4b:a-dman-HEX-1:5|6:d
5s:amino
6b:a-dman-HEX-1:5|6:d
7s:amino
8b:a-dman-HEX-1:5|6:d
9s:amino
LIN
1:1d(4+1)2n
2:1o(1+2)3n
3:1o(2+1)4d
4:4d(4+1)5n
5:4o(3+1)6d
6:6d(4+1)7n
7:6o(2+1)8d
8:8d(4+1)9n
NON
NON1
Parent:1
Linkage:o(1+2)n
HistoricalEntity:N-deformylated polysaccharide (ID 29630)
