RES
1r:r1
REP
REP1:8o(4+1)2d=-1--1
RES
2b:a-dgal-HEX-1:5
3s:phosphate
4b:b-dgal-HEX-1:5
5s:n-acetyl
6n:n1
7b:a-dgal-HEX-1:5
8b:b-dgal-HEX-1:5
9s:n-acetyl
LIN
1:2o(6+1)3n
2:2o(4+1)4d
3:4d(2+1)5n
4:3n(1+1)6n
5:4o(3+1)7d
6:7o(3+1)8d
7:8d(2+1)9n
NON
NON1
Parent:3
Linkage:n(1+1)n
SmallMolecule:SMILES [5CH3][4C@@H](N[2CH2][1CH2]O)[3C](=O)O
Description:(R)-2-(1-carboxyethylamino)ethanol [N-(2-hydroxyethyl)alanine]
