RES
1b:a-dman-HEX-1:5
2s:methyl
3s:amino
4n:n1
LIN
1:1o(1+1)2n
2:1d(4+1)3n
3:1o(4+1)4n
NON
NON1
Parent:1
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH2](O)[3CH2][2C@H](O)[1C](=O)O
Description:(S)-2,4-dihydroxybutanoic acid
