RES
1n:n1
2n:n2
3n:n3
4n:n4
LIN
1:1n(1+1)2n
2:1n(7+1)3n
3:3n(7+1)4n
NON
NON1
SmallMolecule:SMILES O{1}[C@@H]1[C@@H]([C@H]([C@]2([C@@H](C{7}[C@H]([C@]([C@H]2O)(C)O)O)O1)O)O)O
Description:4,9-cyclo-6-deoxy-8-C-methyl-D-xylo-D-galacto-nonose (bradyrhizose)
NON2
Parent:1
Linkage:n(1+1)n
SmallMolecule:SMILES [3CH3][2CH2][1CH2]O
Description:propanol
NON3
Parent:1
Linkage:n(7+1)n
SmallMolecule:SMILES O{1}[C@@H]1[C@@H]([C@H]([C@]2([C@@H](C{7}[C@H]([C@]([C@H]2O)(C)O)O)O1)O)O)O
Description:4,9-cyclo-6-deoxy-8-C-methyl-D-xylo-D-galacto-nonose (bradyrhizose)
NON4
Parent:3
Linkage:n(7+1)n
SmallMolecule:SMILES O{1}[C@@H]1[C@@H]([C@H]([C@]2([C@@H](C{7}[C@H]([C@]([C@H]2O)(C)O)O)O1)O)O)O
Description:4,9-cyclo-6-deoxy-8-C-methyl-D-xylo-D-galacto-nonose (bradyrhizose)
