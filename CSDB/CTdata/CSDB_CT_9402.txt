RES
1b:o-dgro-TRI-0:0|1:aldi
2b:a-dgal-HEX-1:5
3s:phosphate
4b:b-dgal-HEX-1:5
5s:phospho-ethanolamine
6s:n-acetyl
7n:n1
8b:b-dgal-HEX-1:5
9s:phospho-ethanolamine
LIN
1:1o(1+1)2d
2:2o(6+1)3n
3:2o(3+1)4d
4:4o(6+1)5n
5:4d(2+1)6n
6:3n(1+1)7n
7:4o(3+1)8d
8:8o(6+1)9n
NON
NON1
Parent:3
Linkage:n(1+1)n
SmallMolecule:SMILES [2CH2](O)[1CH2](O)
Description:ethylene glycol
