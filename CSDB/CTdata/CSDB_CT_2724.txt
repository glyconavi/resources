RES
1n:n1
2b:b-dglc-HEX-1:5|6:d
3s:n-acetyl
LIN
1:1n(1+1)2d
2:2d(4+1)3n
NON
NON1
SmallMolecule:SMILES [2CH2](O)[1CH2](O)
Description:ethylene glycol
