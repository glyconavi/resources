RES
1b:x-dgal-HEX-x:x
2b:a-ltre-HEX-1:5|4,5:en
3s:n-acetyl
LIN
1:1o(3+1)2d
2:1d(2+1)3n
