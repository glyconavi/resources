RES
1n:n1
2s:acetyl
3b:b-dglc-HEX-1:5|6:a
4s:amino
5s:n-formyl
6s:amino
7n:n2
8b:b-dgro-lglc-NON-2:6|1:a|2:keto|3:d|9:d
9s:amino
10s:n-acetyl
11n:n3
LIN
1:1n(2+1)2n
2:1n(3+1)3d
3:3d(6+1)4n
4:3d(3+1)5n
5:3d(2+1)6n
6:3o(2+1)7n
7:3o(4+2)8d
8:8d(5+1)9n
9:8d(7+1)10n
10:8o(5+1)11n
NON
NON1
SmallMolecule:SMILES CC(C{3}C(O)C{2}(N)C(O)[2H])(O)[2H]
Description:CDHOH-CH(NH2)-CHOH-CH2-CDOH-CH3
NON2
Parent:3
Linkage:o(2+1)n
SmallMolecule:SMILES [3CH3][2CH2][1C](=O)O
Description:propanoic acid
NON3
Parent:8
Linkage:o(5+1)n
SmallMolecule:SMILES [2CH3][1C](=N)O
Description:acetimidic acid
