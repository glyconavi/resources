RES
1r:r1
REP
REP1:7o(3+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3s:n-acetyl
4s:phosphate
5b:o-dgro-TRI-0:0|1:aldi
6b:a-dgal-HEX-1:5
7b:b-dglc-HEX-1:5
8s:n-acetyl
9s:(r)-carboxyethyl
LIN
1:2d(2+1)3n
2:2o(3+1)4n
3:4n(1+1)5o
4:5o(3+1)6d
5:6o(3+1)7d
6:7d(2+1)8n
7:7o(4+2)9n
