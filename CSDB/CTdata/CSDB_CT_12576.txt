RES
1b:o-xgro-TRI-0:0|1:aldi
2n:n1
LIN
1:1o(1+1)2n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES [3CH3][2CH](N)[1C](=O)O
Description:alanine
