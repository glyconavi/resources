RES
1r:r1
REP
REP1:8o(3+1)2d=-1--1
RES
2b:a-lman-HEX-1:5|6:d
3b:b-dxyl-HEX-1:5|4:keto|6:d
4s:n-acetyl
5b:b-dgal-HEX-1:5
6b:a-dgal-HEX-1:5
7s:phosphate
8b:o-xglc-HEX-0:0|1:aldi
LIN
1:2o(3+1)3d
2:3d(2+1)4n
3:3o(3+1)5d
4:5o(2+1)6d
5:5o(3+1)7n
6:7n(1+6)8o
