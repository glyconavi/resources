RES
1b:a-dglc-HEX-1:5
2s:amino
3n:n1
4b:b-dglc-HEX-1:5
5s:amino
6n:n2
7s:phosphate
8n:n3
9a:a1
10b:b-lara-PEN-1:5
11s:amino
LIN
1:1d(2+1)2n
2:1o(2+1)3n
3:1o(6+1)4d
4:4d(2+1)5n
5:4o(3+1)6n
6:4o(4+1)7n
7:4o(2+1)8n
8:8n(3+1)9n
9:7n(1+1)10o
10:10d(4+1)11n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:12
RES
12n:n4
ALTSUBGRAPH2
LEAD-IN RES:13
RES
13n:n5
NON
NON1
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON2
Parent:4
Linkage:o(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON3
Parent:4
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON4
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:myristic acid
NON5
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:lauric acid
