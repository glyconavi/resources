RES
1r:r1
REP
REP1:2o(8+2)2d=-1--1
RES
2b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d
3s:amino
4n:n1
LIN
1:2d(5+1)3n
2:2o(5+1)4n
NON
NON1
Parent:2
Linkage:o(5+1)n
SmallMolecule:SMILES [3CH3][2CH2][1C](=O)O
Description:propanoic acid
