RES
1r:r1
REP
REP1:8o(3+1)2d=-1--1
RES
2b:a-dglc-HEX-1:5|6:d
3s:n-acetyl
4s:acetyl
5b:a-dglc-HEX-1:5
6s:amino
7n:n1
8b:b-dglc-HEX-1:5|6:d
9s:amino
10s:amino
11n:n2
12n:n3
13s:acetyl
14s:acetyl
LIN
1:2d(2+1)3n
2:2o(4+1)4n
3:2o(3+1)5d
4:5d(2+1)6n
5:5o(2+1)7n
6:5o(3+1)8d
7:8d(2+1)9n
8:8d(4+1)10n
9:8o(2+1)11n
10:8o(4+1)12n
11:11n(2+1)13n
12:12n(2+1)14n
NON
NON1
Parent:5
Linkage:o(2+1)n
SmallMolecule:SMILES [2CH3][1C](=N)O
Description:acetimidic acid
NON2
Parent:8
Linkage:o(2+1)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine
NON3
Parent:8
Linkage:o(4+1)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine
