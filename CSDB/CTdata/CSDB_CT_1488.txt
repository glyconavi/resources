RES
1r:r1
REP
REP1:5o(3+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3b:b-lman-HEX-1:5|6:d
4b:a-dglc-HEX-1:5
5b:a-lman-HEX-1:5|6:d
6s:phosphate
7b:o-xgro-TRI-0:0|1:aldi
8s:phosphate
9b:o-xgro-TRI-0:0|1:aldi
LIN
1:2o(4+1)3d
2:3o(3+1)4d
3:4o(3+1)5d
4:4o(6+1)6n
5:6n(1+1)7o
6:5o(4+1)8n
7:8n(1+1)9o
