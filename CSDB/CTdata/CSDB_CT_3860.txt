RES
1b:x-dman-OCT-x:x|1:a|2:keto|3:d
2b:b-dgal-HEX-1:5
3b:a-dgal-HEX-1:5
4b:b-dgal-HEX-1:5
LIN
1:1o(5+1)2d
2:2o(3+1)3d
3:3o(4+1)4d
