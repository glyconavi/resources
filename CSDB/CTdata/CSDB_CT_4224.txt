RES
1r:r1
REP
REP1:5o(4+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3b:a-lgal-HEX-1:5|6:d
4a:a1
5b:b-dglc-HEX-1:5|6:a
6s:(s)-pyruvate
LIN
1:2o(3+1)3d
2:3o(-1+-1)4n
3:3o(4+1)5d
4:5o(3+2)6n
5:5o(2+2)6n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:7
RES
7s:acetyl
ALTSUBGRAPH2
LEAD-IN RES:8
RES
8s:acetyl
