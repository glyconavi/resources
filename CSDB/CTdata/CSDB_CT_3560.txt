RES
1b:o-dgro-TRI-0:0|1:aldi
2n:n1
3n:n2
4b:b-dglc-HEX-1:5
5b:b-dglc-HEX-1:5
6s:phosphate
7b:o-dgro-TRI-0:0|1:aldi
8a:a1
9s:phosphate
10b:o-dgro-TRI-0:0|1:aldi
11a:a2
12s:phosphate
13b:o-dgro-TRI-0:0|1:aldi
14a:a3
15s:phosphate
16b:o-dgro-TRI-0:0|1:aldi
17a:a4
18s:phosphate
19b:o-dgro-TRI-0:0|1:aldi
20a:a5
21s:phosphate
22b:o-dgro-TRI-0:0|1:aldi
23a:a6
LIN
1:1o(2+1)2n
2:1o(3+1)3n
3:1o(1+1)4d
4:4o(6+1)5d
5:5o(6+1)6n
6:6n(1+3)7o
7:7o(2+1)8n
8:7o(1+1)9n
9:9n(1+3)10o
10:10o(2+1)11n
11:10o(1+1)12n
12:12n(1+3)13o
13:13o(2+1)14n
14:13o(1+1)15n
15:15n(1+3)16o
16:16o(2+1)17n
17:16o(1+1)18n
18:18n(1+3)19o
19:19o(2+1)20n
20:19o(1+1)21n
21:21n(1+3)22o
22:22o(2+1)23n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:24
RES
24n:n3
ALTSUBGRAPH2
LEAD-IN RES:25
RES
25b:a-dglc-HEX-1:5
26s:n-acetyl
LIN
1:25d(2+1)26n
ALT2
ALTSUBGRAPH1
LEAD-IN RES:27
RES
27n:n4
ALTSUBGRAPH2
LEAD-IN RES:28
RES
28b:a-dglc-HEX-1:5
29s:n-acetyl
LIN
1:28d(2+1)29n
ALT3
ALTSUBGRAPH1
LEAD-IN RES:30
RES
30n:n5
ALTSUBGRAPH2
LEAD-IN RES:31
RES
31b:a-dglc-HEX-1:5
32s:n-acetyl
LIN
1:31d(2+1)32n
ALT4
ALTSUBGRAPH1
LEAD-IN RES:33
RES
33n:n6
ALTSUBGRAPH2
LEAD-IN RES:34
RES
34b:a-dglc-HEX-1:5
35s:n-acetyl
LIN
1:34d(2+1)35n
ALT5
ALTSUBGRAPH1
LEAD-IN RES:36
RES
36n:n7
ALTSUBGRAPH2
LEAD-IN RES:37
RES
37b:a-dglc-HEX-1:5
38s:n-acetyl
LIN
1:37d(2+1)38n
ALT6
ALTSUBGRAPH1
LEAD-IN RES:39
RES
39n:n8
ALTSUBGRAPH2
LEAD-IN RES:40
RES
40b:a-dglc-HEX-1:5
41s:n-acetyl
LIN
1:40d(2+1)41n
NON
NON1
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:myristic acid
NON2
Parent:1
Linkage:o(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:myristic acid
NON3
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine
NON4
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine
NON5
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine
NON6
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine
NON7
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine
NON8
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine
