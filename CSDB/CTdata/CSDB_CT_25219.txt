RES
1b:a-dglc-HEX-1:5
2s:phosphate
3s:amino
4n:n1
5b:b-dglc-HEX-1:5
6s:phosphate
7s:amino
8n:n2
9b:a-dman-OCT-x:x|1:a|2:keto|3:d
10b:a-dman-OCT-2:6|1:a|2:keto|3:d
11b:a-lgro-dman-HEP-1:5
12b:b-dglc-HEX-1:5
13b:a-lgro-dman-HEP-1:5
14s:phospho-ethanolamine
15b:a-dglc-HEX-1:5
16s:n-acetyl
LIN
1:1o(1+1)2n
2:1d(2+1)3n
3:1o(2+1)4n
4:1o(6+1)5d
5:5o(4+1)6n
6:5d(2+1)7n
7:5o(2+1)8n
8:5o(6+2)9d
9:9o(4+2)10d
10:9o(5+1)11d
11:11o(4+1)12d
12:11o(3+1)13d
13:13o(3+1)14n
14:13o(2+1)15d
15:15d(2+1)16n
NON
NON1
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON2
Parent:5
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
