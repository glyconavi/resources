RES
1b:a-dglc-HEX-1:5|6:a
2n:n1
3b:a-dglc-HEX-1:5
4n:n2
LIN
1:1o(1+4)2n
2:1o(4+1)3d
3:3o(3+1)4n
NON
NON1
Parent:1
Linkage:o(1+4)n
SmallMolecule:SMILES [6C@H](O)1[5C@H](O)[4C@@H](O)[3C@H](O)[2C@H](O)[1C@H](O)1
Description:myo-inositol
NON2
Parent:3
Linkage:o(3+1)n
HistoricalEntity:superclass: any monosaccharide
