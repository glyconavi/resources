RES
1r:r1
REP
REP1:6o(6+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3s:n-acetyl
4b:b-dglc-HEX-1:5|6:a
5n:n1
6b:b-dgal-HEX-1:5
7s:n-acetyl
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4o(6+2)5n
4:4o(4+1)6d
5:6d(2+1)7n
NON
NON1
Parent:4
Linkage:o(6+2)n
SmallMolecule:SMILES [5C](=O)(O)[4CH2][3CH2][2C@H](N)[1C](=O)O
Description:(L)-glutamic acid
