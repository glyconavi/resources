RES
1b:a-dman-OCT-2:6|1:a|2:keto|3:d
2b:b-dglc-HEX-1:5|6:d
3s:n-acetyl
4b:b-dman-HEX-1:5
5b:a-lgal-HEX-1:5|6:d
6s:methyl
7b:a-lgal-HEX-1:5|6:d
8s:methyl
9b:a-ltal-HEX-1:5|6:d
10b:a-dglc-HEX-1:5|6:a
11s:methyl
12b:a-lgal-HEX-1:5|6:d
13s:methyl
14b:a-dglc-HEX-1:5|6:a
15b:a-ltal-HEX-1:5|6:d
16s:methyl
17b:a-lgal-HEX-1:5|6:d
18s:methyl
19b:a-ltal-HEX-1:5|6:d
20b:a-dglc-HEX-1:5|6:a
21s:methyl
22b:a-lgal-HEX-1:5|6:d
23s:methyl
24b:a-ltal-HEX-1:5|6:d
25b:a-dglc-HEX-1:5|6:a
26s:methyl
27b:a-lgal-HEX-1:5|6:d
28s:methyl
29b:a-ltal-HEX-1:5|6:d
30b:a-dglc-HEX-1:5|6:a
31s:methyl
32b:a-lgal-HEX-1:5|6:d
33s:methyl
34s:methyl
35s:methyl
LIN
1:1o(4+1)2d
2:2d(2+1)3n
3:2o(3+1)4d
4:4o(3+1)5d
5:5o(2+1)6n
6:5o(3+1)7d
7:7o(2+1)8n
8:7o(3+1)9d
9:7o(4+1)10d
10:9o(3+1)11n
11:10o(4+1)12d
12:12o(2+1)13n
13:12o(4+1)14d
14:12o(3+1)15d
15:15o(3+1)16n
16:14o(4+1)17d
17:17o(2+1)18n
18:17o(3+1)19d
19:17o(4+1)20d
20:19o(3+1)21n
21:20o(4+1)22d
22:22o(2+1)23n
23:22o(3+1)24d
24:22o(4+1)25d
25:24o(3+1)26n
26:25o(4+1)27d
27:27o(2+1)28n
28:27o(3+1)29d
29:27o(4+1)30d
30:29o(3+1)31n
31:30o(4+1)32d
32:32o(2+1)33n
33:32o(4+1)34n
34:32o(3+1)35n
