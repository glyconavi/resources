RES
1b:b-dgro-dgal-NON-2:6|1:a|2:keto|3:d
2s:pyruvate
3s:amino
4n:n1
LIN
1:1o(9+2)2n
2:1o(7+2)2n
3:1d(5+1)3n
4:1o(5+1)4n
NON
NON1
Parent:1
Linkage:o(5+1)n
SmallMolecule:SMILES [3CH2](O)[2CH](O)[1C](=O)O
Description:glyceric acid
