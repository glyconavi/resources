RES
1r:r1
REP
REP1:5o(3+1)2d=-1--1
RES
2b:a-lgal-HEX-1:5|6:d
3a:a1
4b:b-dglc-HEX-1:5|6:a
5b:b-dglc-HEX-1:5
6s:(s)-pyruvate
LIN
1:2o(-1+-1)3n
2:2o(4+1)4d
3:4o(4+1)5d
4:4o(3+2)6n
5:4o(2+2)6n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:7
RES
7s:acetyl
ALTSUBGRAPH2
LEAD-IN RES:8
RES
8s:acetyl
