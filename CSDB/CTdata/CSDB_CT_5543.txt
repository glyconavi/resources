RES
1r:r1
REP
REP1:4o(4+1)2d=-1--1
RES
2b:b-dgal-HEX-1:5
3s:n-acetyl
4b:b-dglc-HEX-1:5|6:a
5b:b-dara-HEX-2:5|1:aldi|2:keto
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4o(3+2)5d
