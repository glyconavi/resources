RES
1b:a-dglc-HEX-1:5
2b:a-dglc-HEX-1:5
3s:n-acetyl
4n:n1
LIN
1:1o(4+1)2d
2:1d(2+1)3n
3:1o(1+1)4n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES [3CH2]=[2CH][1CH2]O
Description:allyl alcohol
