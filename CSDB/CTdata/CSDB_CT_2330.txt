RES
1b:x-xgal-HEX-x:x
2b:x-xgal-HEX-x:x|6:a
3n:n1
LIN
1:1o(-1+1)2d
2:2o(-1+2)3n
NON
NON1
Parent:2
Linkage:o(-1+2)n
SmallMolecule:SMILES [3CH3][2CH](N)[1C](=O)O
Description:alanine
