RES
1b:a-dman-HEX-1:5|6:d
2s:amino
3n:n1
4b:a-dman-HEX-1:5|6:d
5s:amino
6n:n2
7b:a-dman-HEX-1:5|6:d
8s:amino
9n:n3
10b:a-dman-HEX-1:5|6:d
11s:amino
12n:n4
13b:a-dman-HEX-1:5|6:d
14s:amino
15n:n5
16b:a-dman-HEX-1:5|6:d
17s:methyl
18s:amino
19n:n6
LIN
1:1d(4+1)2n
2:1o(4+1)3n
3:1o(2+1)4d
4:4d(4+1)5n
5:4o(4+1)6n
6:4o(2+1)7d
7:7d(4+1)8n
8:7o(4+1)9n
9:7o(2+1)10d
10:10d(4+1)11n
11:10o(4+1)12n
12:10o(2+1)13d
13:13d(4+1)14n
14:13o(4+1)15n
15:13o(2+1)16d
16:16o(2+1)17n
17:16d(4+1)18n
18:16o(4+1)19n
NON
NON1
Parent:1
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH2](O)[3CH2][2C@H](O)[1C](=O)O
Description:(S)-2,4-dihydroxybutanoic acid
NON2
Parent:4
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH2](O)[3CH2][2C@H](O)[1C](=O)O
Description:(S)-2,4-dihydroxybutanoic acid
NON3
Parent:7
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH2](O)[3CH2][2C@H](O)[1C](=O)O
Description:(S)-2,4-dihydroxybutanoic acid
NON4
Parent:10
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH2](O)[3CH2][2C@H](O)[1C](=O)O
Description:(S)-2,4-dihydroxybutanoic acid
NON5
Parent:13
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH2](O)[3CH2][2C@H](O)[1C](=O)O
Description:(S)-2,4-dihydroxybutanoic acid
NON6
Parent:16
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH2](O)[3CH2][2C@H](O)[1C](=O)O
Description:(S)-2,4-dihydroxybutanoic acid
