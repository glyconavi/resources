RES
1b:a-dglc-HEX-1:5
2s:phosphate
3s:amino
4n:n1
5n:n2
6b:b-dglc-HEX-1:5
7s:diphospho-ethanolamine
8s:amino
9n:n3
10n:n4
11a:a1
12n:n5
LIN
1:1o(1+1)2n
2:1d(2+1)3n
3:1o(3+1)4n
4:1o(2+1)5n
5:1o(6+1)6d
6:6o(4+1)7n
7:6d(2+1)8n
8:6o(2+1)9n
9:6o(3+1)10n
10:9n(3+1)11n
11:10n(3+1)12n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:13
RES
13n:n6
ALTSUBGRAPH2
LEAD-IN RES:14
RES
14n:n7
ALTSUBGRAPH3
LEAD-IN RES:15
RES
15n:n8
NON
NON1
Parent:1
Linkage:o(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-dodecanoic acid
NON2
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-tetradecanoic acid
NON3
Parent:6
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-tetradecanoic acid
NON4
Parent:6
Linkage:o(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-dodecanoic acid
NON5
Parent:10
Linkage:n(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:lauric acid
NON6
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:palmitic acid
NON7
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:myristic acid
NON8
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:lauric acid
