RES
1r:r1
REP
REP1:8o(3+1)2d=-1--1
RES
2b:a-dglc-HEX-1:5
3s:n-acetyl
4s:acetyl
5b:a-lglc-HEX-1:5|6:d
6s:n-acetyl
7b:a-dglc-HEX-1:5
8b:b-dglc-HEX-1:5|6:d
9s:amino
10n:n1
11n:n2
LIN
1:2d(2+1)3n
2:2o(6+1)4n
3:2o(3+1)5d
4:5d(2+1)6n
5:5o(3+1)7d
6:7o(6+1)8d
7:8d(4+1)9n
8:8o(4+1)10n
9:10n(2+1)11n
NON
NON1
Parent:8
Linkage:o(4+1)n
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine
NON2
Parent:10
Linkage:n(2+1)n
SmallMolecule:SMILES [4CH3][3C@H](O)[2CH2][1C](=O)O
Description:(S)-3-hydroxybutanoic acid
