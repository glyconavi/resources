RES
1r:r1
REP
REP1:6o(4+1)2d=-1--1
RES
2b:b-dgal-HEX-1:5|6:d
3s:n-acetyl
4s:amino
5b:b-dgal-HEX-1:5
6b:b-dgal-HEX-1:5
7s:n-acetyl
LIN
1:2d(2+1)3n
2:2d(4+1)4n
3:2o(3+1)5d
4:5o(4+1)6d
5:6d(2+1)7n
