RES
1r:r1
REP
REP1:7o(2+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3s:n-acetyl
4b:b-dgal-HEX-1:5|6:a
5n:n1
6b:b-dglc-HEX-1:5|6:a
7b:a-dglc-HEX-1:5|6:d
8s:n-acetyl
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4o(6+2)5n
4:4o(3+1)6d
5:6o(4+1)7d
6:7d(4+1)8n
NON
NON1
Parent:4
Linkage:o(6+2)n
SmallMolecule:SMILES [3CH2](O)[2C@H](N)[1C](=O)O
Description:(L)-serine
