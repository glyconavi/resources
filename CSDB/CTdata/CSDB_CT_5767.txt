RES
1b:o-dman-HEX-0:0|1:a
2b:a-dman-OCT-2:6|1:a|2:keto|3:d
3s:anhydro
LIN
1:1o(6+2)2d
2:1d(2+1)3n
3:1o(5+1)3n
