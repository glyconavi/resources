RES
1r:r1
REP
REP1:4o(4+1)2n=-1--1
RES
2s:phosphate
3b:o-dgro-TRI-0:0|1:aldi
4b:a-dgal-HEX-1:5
5b:b-dara-HEX-2:5|1:aldi|2:keto
6b:b-dara-HEX-2:5|1:aldi|2:keto
7n:n1
8n:n2
LIN
1:2n(1+1)3o
2:3o(2+1)4d
3:4o(2+2)5d
4:4o(3+2)6d
5:5o(3+1)7n
6:6o(3+1)8n
NON
NON1
Parent:5
Linkage:o(3+1)n
SmallMolecule:SMILES {1}OP(OC)(N)=O
Description:O-methyl phosphamide (OHPO(NH2)OMe)
NON2
Parent:6
Linkage:o(3+1)n
SmallMolecule:SMILES {1}OP(OC)(N)=O
Description:O-methyl phosphamide (OHPO(NH2)OMe)
