RES
1b:b-lara-PEN-1:5
2s:amino
3s:phosphate
4b:a-dglc-HEX-1:5
5s:amino
6n:n1
7b:b-dglc-HEX-1:5
8s:amino
9n:n2
10s:phosphate
11b:a-dman-OCT-2:6|1:a|2:keto|3:d
12b:a-dman-OCT-2:6|1:a|2:keto|3:d
13b:b-lara-PEN-1:5
14b:b-lara-PEN-1:5
15b:a-lgro-dman-HEP-1:5
16b:b-dglc-HEX-1:5
17s:amino
18s:amino
19b:a-lgro-dman-HEP-1:5
20b:a-lgro-dman-HEP-1:5
21b:a-dgal-HEX-1:5|6:a
22b:a-dgro-dman-HEP-1:5
23b:b-dgal-HEX-1:5|6:a
24b:a-dgal-HEX-1:5
25b:a-lgro-dman-HEP-1:5
26s:amino
27n:n3
28b:o-dgal-HEX-0:0
29b:b-dgal-HEX-1:5
30s:n-acetyl
LIN
1:1d(4+1)2n
2:1o(1+1)3n
3:3n(1+1)4o
4:4d(2+1)5n
5:4o(2+1)6n
6:4o(6+1)7d
7:7d(2+1)8n
8:7o(2+1)9n
9:7o(4+1)10n
10:7o(6+2)11d
11:11o(4+2)12d
12:10n(1+1)13o
13:11o(8+1)14d
14:11o(5+1)15d
15:15o(4+1)16d
16:14d(4+1)17n
17:13d(4+1)18n
18:15o(3+1)19d
19:19o(7+1)20d
20:19o(3+1)21d
21:21o(2+1)22d
22:20o(7+1)23d
23:21o(4+1)24d
24:22o(2+1)25d
25:24d(2+1)26n
26:23o(6+-1)27n
27:24o(4+1)28d
28:24o(6+1)28d
29:28o(4+1)29d
30:28d(2+1)30n
NON
NON1
Parent:4
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-tetradecanoic acid
NON2
Parent:7
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-tetradecanoic acid
NON3
Parent:23
Linkage:o(6+-1)n
SmallMolecule:SMILES N{1}CCC{4}CN
Description:putrescine
