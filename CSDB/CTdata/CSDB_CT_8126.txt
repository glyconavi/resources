RES
1b:b-dman-HEX-1:5|6:d
2s:methyl
3s:amino
4n:n1
LIN
1:1o(1+1)2n
2:1d(4+1)3n
3:1o(4+1)4n
NON
NON1
Parent:1
Linkage:o(4+1)n
SmallMolecule:SMILES [3CH3][2CH2][1C](=O)O
Description:propanoic acid
