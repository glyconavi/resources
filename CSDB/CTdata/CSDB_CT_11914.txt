RES
1r:r1
REP
REP1:4o(3+1)2d=-1--1
RES
2b:b-dgal-HEX-1:5
3s:n-acetyl
4b:b-dgal-HEX-1:5
5b:a-dgal-HEX-1:5
6b:b-lgro-lman-NON-2:6|1:a|2:keto|3:d|9:d
7s:n-acetyl
8s:amino
9n:n1
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4o(6+1)5d
4:5o(6+2)6d
5:6d(5+1)7n
6:6d(7+1)8n
7:6o(7+1)9n
NON
NON1
Parent:6
Linkage:o(7+1)n
SmallMolecule:SMILES [4CH3][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxybutanoic acid
