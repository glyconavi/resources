RES
1b:a-dglc-HEX-1:5
2s:amino
3n:n1
4n:n2
5s:phosphate
6b:b-dglc-HEX-1:5
7s:amino
8n:n3
9b:b-lara-PEN-1:5
10s:phosphate
11n:n4
12s:amino
13n:n5
14b:b-lara-PEN-1:5
15s:amino
LIN
1:1d(2+1)2n
2:1o(3+1)3n
3:1o(2+1)4n
4:1o(1+1)5n
5:1o(6+1)6d
6:6d(2+1)7n
7:6o(3+1)8n
8:5n(1+1)9o
9:6o(4+1)10n
10:6o(2+1)11n
11:9d(4+1)12n
12:11n(3+1)13n
13:10n(1+1)14o
14:14d(4+1)15n
NON
NON1
Parent:1
Linkage:o(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-tetradecanoic acid
NON2
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-hexadecanoic acid
NON3
Parent:6
Linkage:o(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-tetradecanoic acid
NON4
Parent:6
Linkage:o(2+1)n
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-hexadecanoic acid
NON5
Parent:11
Linkage:n(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:myristic acid
