RES
1b:a-dman-OCT-x:x|1:a|2:keto|3:d
2b:a-lgro-dman-HEP-1:5
3b:a-lgro-dman-HEP-1:5
4n:n1
5b:a-dgal-HEX-1:5
6s:amino
7n:n2
LIN
1:1o(5+1)2d
2:2o(3+1)3d
3:3o(7+1)4n
4:3o(3+1)5d
5:5d(2+1)6n
6:5o(2+1)7n
NON
NON1
Parent:3
Linkage:o(7+1)n
SmallMolecule:SMILES [1C](=O)(O)N
Description:carbamic acid
NON2
Parent:5
Linkage:o(2+1)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine
