RES
1b:b-dglc-HEX-1:5|6:d
2s:amino
3n:n1
4s:acetyl
LIN
1:1d(3+1)2n
2:1o(3+1)3n
3:3n(2+1)4n
NON
NON1
Parent:1
Linkage:o(3+1)n
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine
