RES
1r:r1
REP
REP1:7o(4+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3s:n-acetyl
4b:a-lgal-HEX-1:5|6:d
5s:amino
6n:n1
7b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d
8s:n-acetyl
9s:acetyl
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4d(2+1)5n
4:4o(2+1)6n
5:4o(3+2)7d
6:7d(5+1)8n
7:7o(9+1)9n
NON
NON1
Parent:4
Linkage:o(2+1)n
SmallMolecule:SMILES [2CH3][1C](=N)O
Description:acetimidic acid
