RES
1b:a-dglc-HEX-1:5
2s:amino
3n:n1
4b:a-dman-HEX-1:5
5b:a-dman-HEX-1:5
LIN
1:1d(2+1)2n
2:1o(1+6)3n
3:1o(4+1)4d
4:4o(3+1)5d
NON
NON1
Parent:1
Linkage:o(1+6)n
SmallMolecule:SMILES [6C@H](O)1[5C@H](O)[4C@@H](O)[3C@H](O)[2C@H](O)[1C@H](O)1
Description:myo-inositol
