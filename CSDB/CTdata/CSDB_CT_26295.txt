RES
1b:a-dglc-HEX-1:5
2s:phosphate
3s:amino
4n:n1
5n:n2
6b:b-dglc-HEX-1:5
7s:phosphate
8s:amino
9n:n3
10b:a-dman-OCT-2:6|1:a|2:keto|3:d
11n:n4
12n:n5
13b:a-dman-OCT-2:6|1:a|2:keto|3:d
14b:a-dman-OCT-2:6|1:a|2:keto|3:d
LIN
1:1o(1+1)2n
2:1d(2+1)3n
3:1o(3+1)4n
4:1o(2+1)5n
5:1o(6+1)6d
6:6o(4+1)7n
7:6d(2+1)8n
8:6o(3+1)9n
9:6o(6+2)10d
10:6o(2+1)11n
11:11n(3+1)12n
12:10o(4+2)13d
13:13o(8+2)14d
NON
NON1
Parent:1
Linkage:o(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:myristic acid
NON2
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [20CH3][19CH2][18CH2][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-icosanoic acid
NON3
Parent:6
Linkage:o(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:myristic acid
NON4
Parent:6
Linkage:o(2+1)n
SmallMolecule:SMILES [20CH3][19CH2][18CH2][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-icosanoic acid
NON5
Parent:11
Linkage:n(3+1)n
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:stearic acid
