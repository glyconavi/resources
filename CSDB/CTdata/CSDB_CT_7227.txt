RES
1r:r1
REP
REP1:2o(2+1)2d=-1--1
RES
2b:a-dman-HEX-1:5|6:d
3s:amino
4n:n1
LIN
1:2d(4+1)3n
2:2o(4+1)4n
NON
NON1
Parent:2
Linkage:o(4+1)n
SmallMolecule:SMILES [3CH2](O)[2CH2][1C](=O)O
Description:beta-lactic acid
