RES
1n:n1
2n:n2
3b:a-dgal-HEX-1:5
4b:b-dgal-HEX-1:5
LIN
1:1n(1+1)2n
2:1n(3+1)3d
3:3o(4+1)4d
NON
NON1
SmallMolecule:SMILES CC1C(NC(C)=O){3}C(O)C(NC(C)=O){1}C(O)O1
Description:2,4-diacetamido-2,4,6-trideoxyhexose
NON2
Parent:1
Linkage:n(1+1)n
SmallMolecule:SMILES [3CH2](O)[2C@H](N)[1C](=O)O
Description:(L)-serine
