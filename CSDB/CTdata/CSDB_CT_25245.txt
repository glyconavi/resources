RES
1b:a-dman-HEX-1:5
2b:a-dman-HEX-1:5
3a:a1
4b:a-dman-HEX-1:5
5a:a2
6b:a-dman-HEX-1:5
7a:a3
LIN
1:1o(6+1)2d
2:2o(2+1)3n
3:2o(6+1)4d
4:4o(2+1)5n
5:4o(6+1)6d
6:6o(2+1)7n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:8
RES
8b:a-dman-HEX-1:5
9b:a-dman-HEX-1:5
LIN
1:8o(2+1)9d
ALTSUBGRAPH2
LEAD-IN RES:10
RES
10n:n1
11b:b-dman-HEX-1:5
12b:b-dman-HEX-1:5
13b:b-dman-HEX-1:5
LIN
1:10n(2+1)11d
2:11o(2+1)12d
3:12o(2+1)13d
ALT2
ALTSUBGRAPH1
LEAD-IN RES:14
RES
14b:a-dman-HEX-1:5
15b:a-dman-HEX-1:5
LIN
1:14o(2+1)15d
ALTSUBGRAPH2
LEAD-IN RES:16
RES
16n:n2
17b:b-dman-HEX-1:5
18b:b-dman-HEX-1:5
19b:b-dman-HEX-1:5
LIN
1:16n(2+1)17d
2:17o(2+1)18d
3:18o(2+1)19d
ALT3
ALTSUBGRAPH1
LEAD-IN RES:20
RES
20b:a-dman-HEX-1:5
21b:a-dman-HEX-1:5
LIN
1:20o(2+1)21d
ALTSUBGRAPH2
LEAD-IN RES:22
RES
22n:n3
23b:b-dman-HEX-1:5
24b:b-dman-HEX-1:5
25b:b-dman-HEX-1:5
LIN
1:22n(2+1)23d
2:23o(2+1)24d
3:24o(2+1)25d
NON
NON1
HistoricalEntity:unassigned monosaccharide
NON2
HistoricalEntity:unassigned monosaccharide
NON3
HistoricalEntity:unassigned monosaccharide
