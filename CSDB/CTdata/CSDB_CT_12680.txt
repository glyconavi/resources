RES
1b:b-dglc-HEX-1:5|6:d
2s:n-acetyl
3b:a-dman-HEX-1:5
4b:a-dman-HEX-1:5
5b:a-dman-HEX-1:5|6:d
6s:n-acetyl
7b:a-dman-HEX-1:5|6:d
8s:n-acetyl
9b:a-dman-HEX-1:5|6:d
10s:n-acetyl
11b:a-dman-HEX-1:5|6:d
12s:n-acetyl
13b:a-dman-HEX-1:5|6:d
14s:n-acetyl
15b:a-dman-HEX-1:5|6:d
16s:n-acetyl
LIN
1:1d(2+1)2n
2:1o(3+1)3d
3:3o(3+1)4d
4:4o(4+1)5d
5:5d(4+1)6n
6:5o(2+1)7d
7:7d(4+1)8n
8:7o(2+1)9d
9:9d(4+1)10n
10:9o(2+1)11d
11:11d(4+1)12n
12:11o(2+1)13d
13:13d(4+1)14n
14:13o(2+1)15d
15:15d(4+1)16n
