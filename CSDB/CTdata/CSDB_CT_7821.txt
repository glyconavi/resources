RES
1b:b-dglc-HEX-1:5
2b:a-lgal-HEX-1:5|6:d
3s:n-acetyl
LIN
1:1o(3+1)2d
2:1d(2+1)3n
