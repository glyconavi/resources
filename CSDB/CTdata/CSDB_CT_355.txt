RES
1r:r1
REP
REP1:7o(3+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5|6:a
3b:b-dman-HEX-1:5
4b:b-dman-HEX-1:5
5b:a-lman-HEX-1:5|6:d
6b:b-dgal-HEX-1:5
7b:a-dglc-HEX-1:5
8s:n-acetyl
LIN
1:2o(4+1)3d
2:3o(3+1)4d
3:4o(3+1)5d
4:4o(4+1)6d
5:6o(2+1)7d
6:7d(2+1)8n
