RES
1r:r1
REP
REP1:9o(3+1)2d=-1--1
RES
2b:b-dgal-HEX-1:5|6:d
3s:n-acetyl
4s:acetyl
5b:b-dglc-HEX-1:5
6s:n-acetyl
7b:b-dgal-HEX-1:5
8b:a-dgal-HEX-1:5
9b:a-dglc-HEX-1:5
10s:n-acetyl
LIN
1:2d(3+1)3n
2:2o(4+1)4n
3:2o(2+1)5d
4:5d(2+1)6n
5:5o(3+1)7d
6:7o(4+1)8d
7:8o(4+1)9d
8:9d(2+1)10n
