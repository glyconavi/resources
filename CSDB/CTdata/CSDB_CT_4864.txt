RES
1b:o-xgro-TRI-0:0|1:aldi
2n:n1
3n:n2
4s:phosphate
5n:n3
6b:a-dglc-HEX-1:5
7s:amino
8b:a-dman-HEX-1:5
9b:b-dgal-HEX-1:5
10b:a-dman-HEX-1:5
11s:n-acetyl
12b:a-dman-HEX-1:5
13s:phospho-ethanolamine
LIN
1:1o(1+1)2n
2:1o(2+1)3n
3:1o(3+1)4n
4:4n(1+1)5n
5:5n(6+1)6d
6:6d(2+1)7n
7:6o(4+1)8d
8:8o(4+1)9d
9:8o(6+1)10d
10:9d(2+1)11n
11:10o(2+1)12d
12:12o(6+1)13n
NON
NON1
Parent:1
Linkage:o(1+1)n
HistoricalEntity:superclass: lipid residue
NON2
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [1*]
Description:octadecenoic acid
NON3
Parent:4
Linkage:n(1+1)n
SmallMolecule:SMILES [6C@H](O)1[5C@H](O)[4C@@H](O)[3C@H](O)[2C@H](O)[1C@H](O)1
Description:myo-inositol
