RES
1r:r1
REP
REP1:4o(4+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3b:b-lman-HEX-1:5|6:d
4b:b-dgal-HEX-1:5
5b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d
6s:n-acetyl
LIN
1:2o(4+1)3d
2:3o(4+1)4d
3:4o(3+2)5d
4:5d(5+1)6n
