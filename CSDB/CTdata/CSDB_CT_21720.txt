RES
1r:r1
REP
REP1:12o(6+1)2n=-1--1
RES
2s:phosphate
3b:o-drib-PEN-0:0|1:aldi
4b:b-dgal-HEX-1:5
5s:n-acetyl
6s:phosphate
7b:a-dgal-HEX-1:5
8s:n-acetyl
9n:n1
10s:phosphate
11b:a-dgal-HEX-1:5|6:d
12b:b-dglc-HEX-1:5
13s:n-acetyl
14s:amino
15n:n2
LIN
1:2n(1+5)3o
2:3o(1+1)4d
3:4d(2+1)5n
4:4o(6+1)6n
5:4o(3+1)7d
6:7d(2+1)8n
7:6n(1+1)9n
8:7o(6+1)10n
9:7o(4+1)11d
10:11o(3+1)12d
11:11d(2+1)13n
12:11d(4+1)14n
13:10n(1+1)15n
NON
NON1
Parent:6
Linkage:n(1+1)n
SmallMolecule:SMILES C[N+](C)(C)[2CH2][1CH2]O
Description:choline
NON2
Parent:10
Linkage:n(1+1)n
SmallMolecule:SMILES C[N+](C)(C)[2CH2][1CH2]O
Description:choline
