RES
1b:a-dglc-HEX-1:5
2s:amino
3n:n1
4n:n2
5b:b-dglc-HEX-1:5
6s:amino
7s:phosphate
8b:a-dman-OCT-2:6|1:a|2:keto|3:d
9n:n3
10n:n4
11b:a-dman-OCT-2:6|1:a|2:keto|3:d
12n:n5
13n:n6
LIN
1:1d(2+1)2n
2:1o(2+1)3n
3:1o(3+1)4n
4:1o(6+1)5d
5:5d(2+1)6n
6:5o(4+1)7n
7:5o(6+2)8d
8:5o(2+1)9n
9:5o(3+1)10n
10:8o(4+2)11d
11:9n(3+1)12n
12:10n(3+1)13n
NON
NON1
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON2
Parent:1
Linkage:o(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON3
Parent:5
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON4
Parent:5
Linkage:o(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON5
Parent:9
Linkage:n(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:lauric acid
NON6
Parent:10
Linkage:n(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:myristic acid
