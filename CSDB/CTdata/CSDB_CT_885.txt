RES
1r:r1
REP
REP1:5o(8+1)2d=-1--1
RES
2b:b-dgal-HEX-1:4|6:d
3s:n-acetyl
4n:n1
5b:a-lgro-dgal-NON-2:6|1:a|2:keto|3:d|9:d
6s:amino
7s:n-acetyl
8n:n2
LIN
1:2d(2+1)3n
2:2h(4+1)4n
3:2o(3+2)5d
4:5d(5+1)6n
5:5d(7+1)7n
6:5o(5+1)8n
NON
NON1
Parent:2
Linkage:h(4+1)n
SmallMolecule:SMILES [1C]C(O)(O)C(=O)N
Description:3'-carboxamide-2',2'-dihydroxypropyl
NON2
Parent:5
Linkage:o(5+1)n
SmallMolecule:SMILES [2CH3][1C](=N)O
Description:acetimidic acid
