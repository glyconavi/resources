RES
1b:x-dglc-HEX-1:5|6:a
2n:n1
LIN
1:1o(6+2)2n
NON
NON1
Parent:1
Linkage:o(6+2)n
SmallMolecule:SMILES [6CH2](N)[5CH2][4CH2][3CH2][2C@@H](N[8C@@H]([9CH3])[7C](=O)O)[1C](=O)O
Description:(2R,8S)-N(epsilon)-(1-carboxyethyl)lysine
