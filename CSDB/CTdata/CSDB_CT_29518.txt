RES
1b:o-dgro-TRI-0:0|1:aldi
2n:n1
3n:n2
4b:a-dglc-HEX-1:5
5n:n3
LIN
1:1o(2+1)2n
2:1o(3+1)3n
3:1o(1+1)4d
4:4o(3+1)5n
NON
NON1
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [1*]
Description:octadecenoic acid
NON2
Parent:1
Linkage:o(3+1)n
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:palmitic acid
NON3
Parent:4
Linkage:o(3+1)n
HistoricalEntity:repeating units of the lipoteichoic acid (ID 29183, ID 29517)
