RES
1r:r1
REP
REP1:3o(3+1)2d=-1--1
RES
2b:a-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4b:a-dgal-HEX-1:5
5b:a-dglc-HEX-1:5
LIN
1:2o(3+1)3d
2:2o(2+1)4d
3:4o(6+1)5d
