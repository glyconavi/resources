RES
1r:r1
REP
REP1:9o(4+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3b:b-dman-HEX-1:5
4s:n-acetyl
5s:acetyl
6b:a-dgal-HEX-1:5
7b:a-dglc-HEX-1:5|6:a
8s:acetyl
9b:a-dglc-HEX-1:5
10s:acetyl
LIN
1:2o(4+1)3d
2:3d(2+1)4n
3:3o(-1+1)5n
4:3o(3+1)6d
5:6o(3+1)7d
6:7o(-1+1)8n
7:7o(4+1)9d
8:9o(-1+1)10n
