RES
1r:r1
REP
REP1:9o(4+1)2n=-1--1
RES
2s:phosphate
3b:a-lman-HEX-1:5|6:d
4b:b-dman-HEX-1:5
5s:n-acetyl
6b:b-dglc-HEX-1:5
7b:a-lman-HEX-1:5|6:d
8b:b-drib-PEN-1:4
9b:b-dman-HEX-1:5
10s:n-acetyl
LIN
1:2n(1+1)3o
2:3o(4+1)4d
3:4d(2+1)5n
4:4o(4+1)6d
5:4o(3+1)7d
6:7o(4+1)8d
7:6o(4+1)9d
8:9d(2+1)10n
