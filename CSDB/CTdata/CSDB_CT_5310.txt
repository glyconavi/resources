RES
1r:r1
REP
REP1:7o(3+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5|6:a
3b:a-dglc-HEX-1:5
4b:b-dgal-HEX-1:5|6:d
5s:amino
6n:n1
7b:a-dglc-HEX-1:5
8s:n-acetyl
LIN
1:2o(4+1)3d
2:3o(6+1)4d
3:4d(3+1)5n
4:4o(3+1)6n
5:4o(2+1)7d
6:7d(2+1)8n
NON
NON1
Parent:4
Linkage:o(3+1)n
SmallMolecule:SMILES [4CH3][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxybutanoic acid
