RES
1b:a-lman-HEX-1:5|6:d
2b:a-lman-HEX-1:5|6:d
3b:a-lman-HEX-1:5|6:d
4b:b-dglc-HEX-1:5|6:d
5s:amino
6s:methyl
7n:n1
LIN
1:1o(2+1)2d
2:2o(3+1)3d
3:3o(3+1)4d
4:4d(4+1)5n
5:4o(2+1)6n
6:4o(4+1)7n
NON
NON1
Parent:4
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH3][3C]([5CH3])(O)[2CH2][1C](=O)O
Description:3-hydroxy-isovaleric acid
