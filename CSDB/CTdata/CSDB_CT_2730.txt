RES
1b:b-dlyx-HEX-2:6|1:a|2:keto
2b:b-dglc-HEX-1:5
3b:a-lgal-HEX-1:5|6:d
LIN
1:1o(5+1)2d
2:2o(4+1)3d
