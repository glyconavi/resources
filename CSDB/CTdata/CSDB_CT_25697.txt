RES
1b:a-dman-OCT-2:6|1:a|2:keto|3:d
2n:n1
LIN
1:1o(2+2)2n
NON
NON1
Parent:1
Linkage:o(2+2)n
HistoricalEntity:O-antigen (ID 25696)
