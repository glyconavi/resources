RES
1b:a-lgal-HEX-1:5|6:a
2s:n-acetyl
3s:n-acetyl
4b:a-lgal-HEX-1:5|6:a
5s:n-acetyl
6s:n-acetyl
7b:a-lgal-HEX-1:5|6:a
8s:n-acetyl
9s:n-acetyl
10b:a-lgal-HEX-1:5|6:a
11s:n-acetyl
12s:n-acetyl
13b:a-lgal-HEX-1:5|6:a
14s:n-formyl
15s:n-acetyl
16s:amino
17s:amino
18n:n1
19s:methyl
LIN
1:1d(2+1)2n
2:1d(3+1)3n
3:1o(4+1)4d
4:4d(2+1)5n
5:4d(3+1)6n
6:4o(4+1)7d
7:7d(2+1)8n
8:7d(3+1)9n
9:7o(4+1)10d
10:10d(2+1)11n
11:10d(3+1)12n
12:10o(4+1)13d
13:13d(3+1)14n
14:13d(2+1)15n
15:13d(6+1)16n
16:13d(4+1)17n
17:13o(4+1)18n
18:18n(2+1)19n
NON
NON1
Parent:13
Linkage:o(4+1)n
SmallMolecule:SMILES [3CH3][2CH](O)[1C](=O)O
Description:lactate
