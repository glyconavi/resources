RES
1r:r1
REP
REP1:5o(4+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3s:n-acetyl
4n:n1
5b:b-dglc-HEX-1:5|6:a
6b:o-xgro-TRI-0:0|1:aldi
7s:amino
LIN
1:2d(2+1)3n
2:2o(4+1)4n
3:2o(3+1)5d
4:5o(6+2)6d
5:6d(2+1)7n
NON
NON1
Parent:2
Linkage:o(4+1)n
SmallMolecule:SMILES {1}OP(OC)(N)=O
Description:O-methyl phosphamide (OHPO(NH2)OMe)
