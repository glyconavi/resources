RES
1s:pyrophosphate
2b:a-dglc-HEX-1:5
3s:amino
4n:n1
5n:n2
6b:b-dglc-HEX-1:5
7s:amino
8s:phosphate
9n:n3
10n:n4
11n:n5
12n:n6
13b:b-lara-PEN-1:5
14s:amino
LIN
1:1n(1+1)2o
2:2d(2+1)3n
3:2o(2+1)4n
4:2o(3+1)5n
5:2o(6+1)6d
6:6d(2+1)7n
7:6o(4+1)8n
8:6o(2+1)9n
9:6o(3+1)10n
10:10n(3+1)11n
11:9n(3+1)12n
12:8n(1+1)13o
13:13d(4+1)14n
NON
NON1
Parent:2
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON2
Parent:2
Linkage:o(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON3
Parent:6
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON4
Parent:6
Linkage:o(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON5
Parent:10
Linkage:n(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:lauric acid
NON6
Parent:9
Linkage:n(3+1)n
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH]=[9CH][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:palmitoleic acid
