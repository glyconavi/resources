RES
1b:b-dglc-HEX-1:5
2n:n1
3b:b-dgal-HEX-1:5
4b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d
5s:n-acetyl
LIN
1:1o(1+1)2n
2:1o(4+1)3d
3:3o(3+2)4d
4:4d(5+1)5n
NON
NON1
Parent:1
Linkage:o(1+1)n
HistoricalEntity:superclass: ceramide
