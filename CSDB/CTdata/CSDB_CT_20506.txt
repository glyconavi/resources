RES
1r:r1
REP
REP1:7o(3+1)2d=-1--1
RES
2b:b-dgal-HEX-1:4
3s:acetyl
4b:b-dglc-HEX-1:5
5b:b-dgal-HEX-1:4
6b:a-dgal-HEX-1:5
7b:b-dgal-HEX-1:5
8b:a-dgal-HEX-1:5
LIN
1:2o(2+1)3n
2:2o(5+1)4d
3:4o(3+1)5d
4:5o(3+1)6d
5:6o(3+1)7d
6:6o(2+1)8d
