RES
1b:b-dglc-HEX-1:5
2n:n1
3b:b-dglc-HEX-1:5|6:a
4b:b-dglc-HEX-1:5
LIN
1:1o(1+1)2n
2:1o(4+1)3d
3:3o(3+1)4d
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES NCC{1}CO
Description:3-aminopropanol
