RES
1b:a-dman-OCT-x:x|1:a|2:keto|3:d
2b:a-lgro-dman-HEP-1:5
3b:a-lgro-dman-HEP-1:5
4n:n1
5b:a-dgal-HEX-1:5
6s:n-acetyl
7b:a-dglc-HEX-1:5
8b:b-dglc-HEX-1:5
9b:b-dglc-HEX-1:5
10b:a-lman-HEX-1:5|6:d
LIN
1:1o(5+1)2d
2:2o(3+1)3d
3:3o(7+1)4n
4:3o(3+1)5d
5:5d(2+1)6n
6:5o(4+1)7d
7:5o(3+1)8d
8:8o(6+1)9d
9:7o(6+1)10d
NON
NON1
Parent:3
Linkage:o(7+1)n
SmallMolecule:SMILES [1C](=O)(O)N
Description:carbamic acid
