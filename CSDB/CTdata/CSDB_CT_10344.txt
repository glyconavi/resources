RES
1b:o-dery-TET-0:0|1:d
2b:a-dgal-HEX-1:5
3b:b-dgal-HEX-1:5
4s:n-acetyl
5b:b-dglc-HEX-1:5
6s:n-acetyl
LIN
1:1o(3+1)2d
2:2o(3+1)3d
3:3d(2+1)4n
4:3o(4+1)5d
5:5d(2+1)6n
