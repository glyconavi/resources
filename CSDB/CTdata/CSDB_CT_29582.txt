RES
1b:x-dman-OCT-2:6|1:a|2:keto|3:d
2b:x-dman-OCT-2:6|1:a|2:keto|3:d
3b:a-lgro-dman-HEP-1:5
4b:a-lgro-dman-HEP-1:5
5a:a1
LIN
1:1o(-1+2)2d
2:1o(5+1)3d
3:3o(3+1)4d
4:3o(6+1)5n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:6
RES
6s:phosphate
ALTSUBGRAPH2
LEAD-IN RES:7
RES
7s:phospho-ethanolamine
