RES
1b:o-dglc-HEX-0:0|1:aldi
2b:a-dman-HEX-1:5
3n:n1
4b:a-lman-HEX-1:5|6:d
5b:a-dgal-HEX-1:5
6b:b-dgal-HEX-1:4
LIN
1:1o(3+1)2d
2:2o(6+-1)3n
3:2o(3+1)4d
4:4o(2+1)5d
5:5o(3+1)6d
NON
NON1
Parent:2
Linkage:o(6+-1)n
SmallMolecule:SMILES O{1}P(CCNC{5}CO)(O)=O
Description:N-(2-hydroxyethyl)-2-aminoethylphosphonic acid
