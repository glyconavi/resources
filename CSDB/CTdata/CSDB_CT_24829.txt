RES
1r:r1
REP
REP1:7o(3+1)2d=-1--1
RES
2b:a-lgal-HEX-1:5|6:d
3s:amino
4n:n1
5b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d
6s:n-acetyl
7b:b-dglc-HEX-1:5
8s:n-acetyl
9s:acetyl
LIN
1:2d(2+1)3n
2:2o(2+1)4n
3:2o(3+2)5d
4:5d(5+1)6n
5:5o(7+1)7d
6:7d(2+1)8n
7:7o(6+1)9n
NON
NON1
Parent:2
Linkage:o(2+1)n
SmallMolecule:SMILES [2CH3][1C](=N)O
Description:acetimidic acid
