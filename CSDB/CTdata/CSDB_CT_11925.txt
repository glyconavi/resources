RES
1b:b-lgro-lman-NON-2:6|1:a|2:keto|3:d|9:d
2s:n-acetyl
3s:amino
4n:n1
LIN
1:1d(5+1)2n
2:1d(7+1)3n
3:1o(7+1)4n
NON
NON1
Parent:1
Linkage:o(7+1)n
SmallMolecule:SMILES O=C{1}(O)C(OC)CC(O)C(O)CO
Description:4,5,6-trihydroxy-2-methoxyhexanoic acid
