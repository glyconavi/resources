RES
1r:r1
REP
REP1:7o(4+1)2d=-1--1
RES
2b:b-lglc-HEX-1:5|6:a
3s:amino
4n:n1
5b:a-dxyl-HEX-1:5|4:keto|6:d
6s:n-acetyl
7b:a-lgal-HEX-1:5
8s:acetyl
LIN
1:2d(2+1)3n
2:2o(2+4)4n
3:2o(4+1)5d
4:5d(2+1)6n
5:5o(3+1)7d
6:7o(-1+1)8n
NON
NON1
Parent:2
Linkage:o(2+4)n
SmallMolecule:SMILES [4C](=O)(O)[3CH2][2CH](O)[1C](=O)(O)
Description:malic acid
