RES
1r:r1
REP
REP1:7o(3+1)2d=-1--1
RES
2b:a-dglc-HEX-1:5
3s:n-acetyl
4b:b-dgal-HEX-1:5
5b:a-dman-HEX-1:5
6b:a-lman-HEX-1:5|6:d
7b:b-dglc-HEX-1:5|6:d
8s:n-acetyl
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4o(4+1)5d
4:5o(3+1)6d
5:5o(2+1)7d
6:7d(4+1)8n
