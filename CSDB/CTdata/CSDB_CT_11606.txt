RES
1b:x-lman-HEX-x:x|6:d
2b:a-dglc-HEX-1:5
3b:b-lman-HEX-1:5|6:d
4b:a-dgal-HEX-1:5|6:d
5b:a-dgal-HEX-1:5
6s:n-acetyl
LIN
1:1o(3+1)2d
2:1o(4+1)3d
3:3o(3+1)4d
4:4o(3+1)5d
5:4d(2+1)6n
