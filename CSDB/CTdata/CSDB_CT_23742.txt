RES
1r:r1
REP
REP1:5o(4+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3s:n-acetyl
4b:a-lman-HEX-1:5|6:d
5b:a-dglc-HEX-1:5
6s:n-acetyl
7b:b-lman-HEX-1:5|6:d
8a:a1
9s:n-acetyl
10s:amino
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4o(3+1)5d
4:5d(2+1)6n
5:5o(3+1)7d
6:7o(3+1)8n
7:7d(2+1)9n
8:7d(3+1)10n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:11
RES
11n:n1
ALTSUBGRAPH2
LEAD-IN RES:12
RES
12n:n2
NON
NON1
SmallMolecule:SMILES [4CH3][3C@H](O)[2CH2][1C](=O)O
Description:(S)-3-hydroxybutanoic acid
NON2
SmallMolecule:SMILES [4CH3][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxybutanoic acid
