RES
1r:r1
REP
REP1:4o(3+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3b:a-dgal-HEX-1:5
4b:b-dgal-HEX-1:5
5s:acetyl
6b:b-dglc-HEX-1:5|6:a
7n:n1
LIN
1:2o(3+1)3d
2:3o(3+1)4d
3:4o(2+1)5n
4:4o(4+1)6d
5:6o(6+2)7n
NON
NON1
Parent:6
Linkage:o(6+2)n
SmallMolecule:SMILES [5C](=O)(O)[4CH2][3CH2][2C@H](N)[1C](=O)O
Description:(L)-glutamic acid
