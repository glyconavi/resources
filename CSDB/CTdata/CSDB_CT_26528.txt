RES
1r:r1
REP
REP1:6n(4+2)2d=-1--1
RES
2b:b-lgro-lman-NON-2:6|1:a|2:keto|3:d|9:d
3b:a-dgal-HEX-1:5
4s:n-acetyl
5s:amino
6n:n1
LIN
1:2o(4+1)3d
2:2d(5+1)4n
3:2d(7+1)5n
4:2o(7+1)6n
NON
NON1
Parent:2
Linkage:o(7+1)n
SmallMolecule:SMILES [4CH2](O)[3CH](O)[2CH2][1C](=O)O
Description:3,4-dihydroxybutanoic acid
