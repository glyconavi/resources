RES
1b:a-lgro-dman-HEP-1:5
2b:b-dglc-HEX-1:5
3b:a-lgro-dman-HEP-1:5
4n:n1
LIN
1:1o(4+1)2d
2:1o(3+1)3d
3:1o(1+1)4n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES [2CH2](N)[1CH2](O)
Description:ethanolamine
