RES
1r:r1
REP
REP1:7o(6+1)2d=-1--1
RES
2b:b-dgal-HEX-1:5
3b:a-dglc-HEX-1:5
4s:n-acetyl
5b:a-dgal-HEX-1:5
6s:n-acetyl
7b:b-dglc-HEX-1:5
8b:a-lxyl-HEX-1:5|3:d|6:d
LIN
1:2o(6+1)3d
2:2d(2+1)4n
3:2o(3+1)5d
4:5d(2+1)6n
5:5o(3+1)7d
6:7o(3+1)8d
