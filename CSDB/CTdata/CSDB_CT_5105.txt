RES
1b:a-dgal-HEX-1:5
2s:n-acetyl
3b:a-dgal-HEX-1:5|6:a
4b:a-lman-HEX-1:5|6:d
5b:a-lman-HEX-1:5|6:d
6b:b-lman-HEX-1:5|6:d
LIN
1:1d(2+1)2n
2:1o(3+1)3d
3:3o(2+1)4d
4:4o(2+1)5d
5:5o(4+1)6d
