RES
1n:n1
2n:n2
3b:a-dglc-HEX-1:5
4b:a-dglc-HEX-1:5
5b:b-dglc-HEX-1:5
6b:a-dglc-HEX-1:5
7s:succinate
8b:a-dglc-HEX-1:5
9s:methyl
10b:b-dglc-HEX-1:5
11b:a-dglc-HEX-1:5
12s:succinate
13s:methyl
14b:a-dglc-HEX-1:5
15s:methyl
16b:a-dglc-HEX-1:5
17s:methyl
18b:a-dglc-HEX-1:5
19s:methyl
20b:a-dglc-HEX-1:5
21s:methyl
22b:a-dglc-HEX-1:5
23s:methyl
24b:a-dglc-HEX-1:5
25a:a1
26b:a-dglc-HEX-1:5
27a:a2
28b:a-dglc-HEX-1:5
29a:a3
30b:a-dglc-HEX-1:5
31a:a5
32a:a4
33s:methyl
LIN
1:1n(1+1)2n
2:2n(2+1)3d
3:3o(6+1)4d
4:4o(3+1)5d
5:4o(4+1)6d
6:5o(6+1)7n
7:6o(4+1)8d
8:8o(6+1)9n
9:8o(3+1)10d
10:8o(4+1)11d
11:10o(6+1)12n
12:11o(6+1)13n
13:11o(4+1)14d
14:14o(6+1)15n
15:14o(4+1)16d
16:16o(6+1)17n
17:16o(4+1)18d
18:18o(6+1)19n
19:18o(4+1)20d
20:20o(6+1)21n
21:20o(4+1)22d
22:22o(6+1)23n
23:22o(4+1)24d
24:24o(6+1)25n
25:24o(4+1)26d
26:26o(6+1)27n
27:26o(4+1)28d
28:28o(6+1)29n
29:28o(4+1)30d
30:30o(4+1)31n
31:30o(6+1)32n
32:30o(3+1)33n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:34
RES
34n:n3
ALTSUBGRAPH2
LEAD-IN RES:35
RES
35n:n4
ALTSUBGRAPH3
LEAD-IN RES:36
RES
36s:acetyl
ALT2
ALTSUBGRAPH1
LEAD-IN RES:37
RES
37n:n5
ALTSUBGRAPH2
LEAD-IN RES:38
RES
38n:n6
ALTSUBGRAPH3
LEAD-IN RES:39
RES
39s:acetyl
ALT3
ALTSUBGRAPH1
LEAD-IN RES:40
RES
40n:n7
ALTSUBGRAPH2
LEAD-IN RES:41
RES
41n:n8
ALTSUBGRAPH3
LEAD-IN RES:42
RES
42s:acetyl
ALT4
ALTSUBGRAPH1
LEAD-IN RES:43
RES
43n:n9
ALTSUBGRAPH2
LEAD-IN RES:44
RES
44n:n10
ALTSUBGRAPH3
LEAD-IN RES:45
RES
45s:acetyl
ALT5
ALTSUBGRAPH1
LEAD-IN RES:46
RES
46n:n11
ALTSUBGRAPH2
LEAD-IN RES:47
RES
47n:n12
ALTSUBGRAPH3
LEAD-IN RES:48
RES
48s:acetyl
NON
NON1
SmallMolecule:SMILES [8CH3][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:octanoic acid
NON2
Parent:1
Linkage:n(1+1)n
SmallMolecule:SMILES [3CH2](O)[2C@@H](O)[1C](=O)O
Description:(D)-glyceric acid
NON3
SmallMolecule:SMILES [3CH3][2CH]([4CH3])[1C](=O)O
Description:isobutyric acid
NON4
SmallMolecule:SMILES [3CH3][2CH2][1C](=O)O
Description:propanoic acid
NON5
SmallMolecule:SMILES [3CH3][2CH]([4CH3])[1C](=O)O
Description:isobutyric acid
NON6
SmallMolecule:SMILES [3CH3][2CH2][1C](=O)O
Description:propanoic acid
NON7
SmallMolecule:SMILES [3CH3][2CH]([4CH3])[1C](=O)O
Description:isobutyric acid
NON8
SmallMolecule:SMILES [3CH3][2CH2][1C](=O)O
Description:propanoic acid
NON9
SmallMolecule:SMILES [3CH3][2CH]([4CH3])[1C](=O)O
Description:isobutyric acid
NON10
SmallMolecule:SMILES [3CH3][2CH2][1C](=O)O
Description:propanoic acid
NON11
SmallMolecule:SMILES [3CH3][2CH]([4CH3])[1C](=O)O
Description:isobutyric acid
NON12
SmallMolecule:SMILES [3CH3][2CH2][1C](=O)O
Description:propanoic acid
