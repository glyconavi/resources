RES
1b:o-dman-HEX-0:0|1:aldi
2n:n1
3s:pyruvate
LIN
1:1o(3+2)2n
2:2n(9+2)3n
3:2n(7+2)3n
NON
NON1
Parent:1
Linkage:o(3+2)n
SmallMolecule:SMILES O{8}C({7}C([C@@H]1O{2}[C@](C(O)=O)(C{4}[C@@H]({5}[C@@H]1N)O)O)O){9}CO
Description:5-amino-3,5-dideoxy-a-lyxo-nonulopyranosonic (rhodaminic) acid
