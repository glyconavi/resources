RES
1b:o-dtal-HEX-0:0|1:a
2s:anhydro
3b:b-dglc-HEX-1:5
4b:a-dgal-HEX-1:5
5b:a-dgal-HEX-1:5|6:d
6s:amino
7n:n1
LIN
1:1d(2+1)2n
2:1o(5+1)2n
3:1o(3+1)3d
4:3o(6+1)4d
5:4o(4+1)5d
6:5d(3+1)6n
7:5o(3+1)7n
NON
NON1
Parent:5
Linkage:o(3+1)n
SmallMolecule:SMILES [4CH3][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxybutanoic acid
