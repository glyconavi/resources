RES
1n:n1
2s:pyrophosphate
3b:a-dglc-HEX-1:5
4s:amino
5n:n2
6n:n3
7b:b-dglc-HEX-1:5
8s:diphospho-ethanolamine
9s:amino
10n:n4
11n:n5
12n:n6
13b:a-dman-OCT-2:6|1:a|2:keto|3:d
14b:a-dman-OCT-2:6|1:a|2:keto|3:d
15n:n7
16b:a-lgro-dman-HEP-1:5
17b:b-dglc-HEX-1:5
18b:a-lgro-dman-HEP-1:5
19b:b-dglc-HEX-1:5
20s:phospho-ethanolamine
21s:phospho-ethanolamine
22b:a-dglc-HEX-x:x
23b:b-dgal-HEX-1:5
24s:n-acetyl
25b:b-dglc-HEX-x:x
26b:b-dgal-HEX-1:5
27s:n-acetyl
LIN
1:1n(1+1)2n
2:2n(1+1)3o
3:3d(2+1)4n
4:3o(3+1)5n
5:3o(2+1)6n
6:3o(6+1)7d
7:7o(4+1)8n
8:7d(2+1)9n
9:7o(3+1)10n
10:6n(3+1)11n
11:7o(2+1)12n
12:7o(6+2)13d
13:13o(4+2)14d
14:12n(3+1)15n
15:13o(5+1)16d
16:16o(4+1)17d
17:16o(3+1)18d
18:18o(3+1)19d
19:18o(6+1)20n
20:18o(7+1)21n
21:18o(2+1)22d
22:17o(4+1)23d
23:22d(2+1)24n
24:23o(3+1)25d
25:25o(4+1)26d
26:25d(2+1)27n
NON
NON1
SmallMolecule:SMILES [2CH2](N)[1CH2](O)
Description:ethanolamine
NON2
Parent:3
Linkage:o(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-dodecanoic acid
NON3
Parent:3
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON4
Parent:7
Linkage:o(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-dodecanoic acid
NON5
Parent:6
Linkage:n(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:lauric acid
NON6
Parent:7
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON7
Parent:12
Linkage:n(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:lauric acid
