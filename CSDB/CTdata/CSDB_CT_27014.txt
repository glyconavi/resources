RES
1n:n1
2b:a-lgro-dman-HEP-1:5
3b:b-dglc-HEX-1:5
4b:a-dglc-HEX-1:5
5b:a-lgro-dman-HEP-1:5
6b:a-lgro-dman-HEP-1:5
7b:a-lgro-dman-HEP-1:5
8b:a-dglc-HEX-1:5
9b:a-dman-HEX-1:5|6:d
10s:amino
11s:amino
12n:n2
13n:n3
14b:a-dman-HEX-1:5|6:d
15s:amino
16n:n4
17n:n5
18b:a-dman-HEX-1:5|6:d
19s:amino
20n:n6
21b:a-dman-HEX-1:5|6:d
22s:methyl
23s:amino
24n:n7
LIN
1:1n(5+1)2d
2:2o(4+1)3d
3:2o(6+1)4d
4:2o(3+1)5d
5:5o(6+1)6d
6:5o(2+1)7d
7:7o(7+1)8d
8:7o(-1+1)9d
9:8d(2+1)10n
10:9d(4+1)11n
11:9o(4+1)12n
12:8o(2+1)13n
13:9o(2+1)14d
14:14d(4+1)15n
15:13n(2+-1)16n
16:14o(4+1)17n
17:14o(2+1)18d
18:18d(4+1)19n
19:18o(4+1)20n
20:18o(2+1)21d
21:21o(2+1)22n
22:21d(4+1)23n
23:21o(4+1)24n
NON
NON1
SmallMolecule:SMILES O[C@H]1{5}[C@H](O)[C@H](O[C@H]1CO)CC(C(O)=O)=O
Description:4,7-anhydro-3-deoxy-D-manno-octonic acid
NON2
Parent:9
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH2](O)[3CH2][2C@H](O)[1C](=O)O
Description:(S)-2,4-dihydroxybutanoic acid
NON3
Parent:8
Linkage:o(2+1)n
SmallMolecule:SMILES O=C1{1}C(N)={2}C(N)C1=O
Description:1,2-diaminocyclobutene-3,4-dione
NON4
Parent:13
Linkage:n(2+-1)n
HistoricalEntity:BSA
NON5
Parent:14
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH2](O)[3CH2][2C@H](O)[1C](=O)O
Description:(S)-2,4-dihydroxybutanoic acid
NON6
Parent:18
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH2](O)[3CH2][2C@H](O)[1C](=O)O
Description:(S)-2,4-dihydroxybutanoic acid
NON7
Parent:21
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH2](O)[3CH2][2C@H](O)[1C](=O)O
Description:(S)-2,4-dihydroxybutanoic acid
