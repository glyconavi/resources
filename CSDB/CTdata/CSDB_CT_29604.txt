RES
1b:o-xgro-TRI-0:0
2b:a-lman-HEX-1:5|6:d
3s:phospho-ethanolamine
4b:b-dglc-HEX-1:5
5b:a-lman-HEX-1:5|6:d
6s:n-acetyl
LIN
1:1o(2+1)2d
2:2o(3+1)3n
3:2o(2+1)4d
4:4o(3+1)5d
5:4d(2+1)6n
