RES
1n:n1
2s:phosphate
3b:x-lgro-lman-NON-2:6|1:a|2:keto|3:d|9:d
4s:n-acetyl
5s:n-acetyl
LIN
1:1n(5+1)2n
2:2n(1+2)3o
3:3d(5+1)4n
4:3d(7+1)5n
NON
NON1
SmallMolecule:SMILES [5CH2](O)[4C@@H](O1)[3C@@H](O)[2C@@H](O)[1C@H](N2[6C](=O)N=[7C](N)[8CH]=[9CH]2)1
Description:cytidine
