RES
1r:r1
REP
REP1:5o(4+1)2n=-1--1
RES
2n:n1
3b:x-xglc-HEX-1:5
4s:acetyl
5b:b-xglc-HEX-1:5
6s:acetyl
LIN
1:2n(17+1)3d
2:3o(6+1)4n
3:3o(2+1)5d
4:5o(6+1)6n
NON
NON1
SmallMolecule:SMILES [18CH3][17CH](O)[16CH2][15CH2][14CH2][13CH2][12CH2][11CH2]/[10CH]=[9CH]\[8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:17-hydroxy-cis-9-octadecenoic acid
