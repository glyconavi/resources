RES
1b:o-xgro-TRI-0:0|1:aldi
2b:a-dman-HEX-1:5|6:d
3b:a-dman-HEX-1:5|6:d
4b:a-dman-HEX-1:5|6:d
5b:a-dman-HEX-1:5|6:d
6b:a-dman-HEX-1:5|6:d
7s:methyl
8b:a-dman-HEX-1:5|6:d
9b:a-dman-HEX-1:5|6:d
10b:a-dman-HEX-1:5|6:d
LIN
1:1o(2+1)2d
2:2o(3+1)3d
3:3o(3+1)4d
4:4o(3+1)5d
5:5o(3+1)6d
6:6o(3+1)7n
7:6o(2+1)8d
8:8o(3+1)9d
9:9o(3+1)10d
