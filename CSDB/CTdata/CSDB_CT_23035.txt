RES
1b:a-lman-HEX-1:5|6:d
2n:n1
3b:a-lman-HEX-1:5|6:d
4b:b-dglc-HEX-1:5|6:d
5s:amino
6s:methyl
7n:n2
LIN
1:1o(1+1)2n
2:1o(2+1)3d
3:3o(3+1)4d
4:4d(4+1)5n
5:4o(2+1)6n
6:4o(4+1)7n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES [3CH2](O)[2CH](N)[1C](=O)O
Description:serine
NON2
Parent:4
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH3][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxybutanoic acid
