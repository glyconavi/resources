RES
1b:a-dman-OCT-2:6|1:a|2:keto|3:d
2b:a-lgro-dman-HEP-1:5
3b:b-dglc-HEX-1:5
4b:a-lgro-dman-HEP-1:5
5b:a-lgro-dman-HEP-1:5
6b:a-dgal-HEX-1:5|6:a
7b:a-dgro-dman-HEP-1:5
8b:a-dglc-HEX-1:5
9b:a-lgro-dman-HEP-1:5
10s:amino
11b:a-dglc-HEX-1:5
12b:b-dglc-HEX-1:5
13b:a-dglc-HEX-1:5
LIN
1:1o(5+1)2d
2:2o(4+1)3d
3:2o(3+1)4d
4:4o(7+1)5d
5:4o(3+1)6d
6:6o(2+1)7d
7:6o(4+1)8d
8:7o(2+1)9d
9:8d(2+1)10n
10:8o(4+1)11d
11:11o(6+1)12d
12:12o(2+1)13d
