RES
1r:r1
REP
REP1:6o(3+1)2d=-1--1
RES
2b:b-dgal-HEX-1:5
3s:n-acetyl
4b:b-dgal-HEX-1:5
5s:n-acetyl
6b:a-dgal-HEX-1:5
7b:b-dman-HEX-1:5
8n:n1
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4d(2+1)5n
4:4o(4+1)6d
5:6o(4+1)7d
6:7o(4+2)8n
NON
NON1
Parent:7
Linkage:o(4+2)n
SmallMolecule:SMILES [5CH3][4C@@H](O)[3CH2][2C@@H](O)[1C](=O)O
Description:2R,4R-dihydroxy-valeric acid
