RES
1b:a-dglc-HEX-1:5
2s:amino
3n:n1
4n:n2
5b:b-dglc-HEX-1:5
6s:phosphate
7s:amino
8n:n3
9n:n4
10n:n5
LIN
1:1d(2+1)2n
2:1o(3+1)3n
3:1o(2+1)4n
4:1o(6+1)5d
5:5o(4+1)6n
6:5d(2+1)7n
7:5o(3+1)8n
8:5o(2+1)9n
9:9n(3+1)10n
NON
NON1
Parent:1
Linkage:o(3+1)n
SmallMolecule:SMILES [10CH3][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-decanoic acid
NON2
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C](=O)[2CH2][1C](=O)O
Description:3-oxo-tetradecanoic acid
NON3
Parent:5
Linkage:o(3+1)n
SmallMolecule:SMILES [10CH3][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-decanoic acid
NON4
Parent:5
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON5
Parent:9
Linkage:n(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH]=[6CH][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:tetradec-6-enoic acid
