RES
1b:a-dman-OCT-2:6|1:a|2:keto|3:d
2b:a-dman-OCT-2:6|1:a|2:keto|3:d
3b:a-lgro-dman-HEP-1:5
4s:pyrophosphate
5s:pyrophosphate
6b:a-lgro-dman-HEP-1:5
7s:pyrophosphate
8n:n1
9b:a-dgal-HEX-1:5
10s:amino
11n:n2
12b:b-dglc-HEX-1:5
13b:a-dglc-HEX-1:5
14b:b-dglc-HEX-1:5
15b:a-lman-HEX-1:5|6:d
16a:a1
17s:n-acetyl
LIN
1:1o(4+2)2d
2:1o(5+1)3d
3:3o(2+1)4n
4:3o(4+1)5n
5:3o(3+1)6d
6:6o(6+1)7n
7:6o(7+1)8n
8:6o(3+1)9d
9:9d(2+1)10n
10:9o(2+1)11n
11:9o(3+1)12d
12:9o(4+1)13d
13:12o(2+1)14d
14:13o(6+1)15d
15:15o(-1+-1)16n
16:14d(2+1)17n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:18
RES
18s:acetyl
ALTSUBGRAPH2
LEAD-IN RES:19
RES
19s:acetyl
ALTSUBGRAPH3
LEAD-IN RES:20
RES
20s:acetyl
NON
NON1
Parent:6
Linkage:o(7+1)n
SmallMolecule:SMILES [1C](=O)(O)N
Description:carbamic acid
NON2
Parent:9
Linkage:o(2+1)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine
