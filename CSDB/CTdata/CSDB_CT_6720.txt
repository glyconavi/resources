RES
1r:r1
REP
REP1:5o(4+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3s:n-acetyl
4b:b-dgal-HEX-1:5
5b:b-lgro-lman-NON-2:6|1:a|2:keto|3:d|9:d
6s:n-acetyl
7s:n-acetyl
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4o(4+2)5d
4:5d(5+1)6n
5:5d(7+1)7n
