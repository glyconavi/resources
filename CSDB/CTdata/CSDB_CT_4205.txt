RES
1r:r1
REP
REP1:5o(3+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3b:a-dgal-HEX-1:5
4b:a-dgal-HEX-1:5
5b:b-dgal-HEX-1:5
6b:b-dglc-HEX-1:5|6:a
7a:a1
LIN
1:2o(3+1)3d
2:3o(2+1)4d
3:3o(3+1)5d
4:5o(4+1)6d
5:6o(-1+-1)7n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:8
RES
8s:acetyl
ALTSUBGRAPH2
LEAD-IN RES:9
RES
9s:acetyl
ALTSUBGRAPH3
LEAD-IN RES:10
RES
10s:acetyl
