RES
1b:a-HEP-1:5
2n:n1
LIN
1:1o(7+1)2n
NON
NON1
Parent:1
Linkage:o(7+1)n
SmallMolecule:SMILES [1C](=O)(O)N
Description:carbamic acid
