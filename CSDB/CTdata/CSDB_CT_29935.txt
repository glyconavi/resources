RES
1n:n1
2s:pyrophosphate
3b:a-dgal-HEX-1:5|6:d
4s:n-acetyl
LIN
1:1n(5+1)2n
2:2n(1+1)3o
3:3d(3+1)4n
NON
NON1
SmallMolecule:SMILES [5CH2](O)[4C@@H](O1)[3C@@H](O)[2CH2][1C@H](N2[6C](=O)[53NH][7C](=O)[8C]([10CH3])=[9CH]2)1
Description:deoxythymidine
