RES
1b:x-lgal-HEX-1:5|6:d
2b:a-dgal-HEX-1:5
3b:b-dglc-HEX-1:5|6:a
LIN
1:1o(3+1)2d
2:2o(3+1)3d
