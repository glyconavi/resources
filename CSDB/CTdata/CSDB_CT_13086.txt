RES
1r:r1
REP
REP1:6o(3+1)2d=-1--1
RES
2b:a-dgal-HEX-1:5|6:a
3n:n1
4b:a-dgal-HEX-1:5|6:a
5n:n2
6b:b-dglc-HEX-1:5|6:d
7s:n-acetyl
LIN
1:2o(6+2)3n
2:2o(4+1)4d
3:4o(6+2)5n
4:4o(4+1)6d
5:6d(2+1)7n
NON
NON1
Parent:2
Linkage:o(6+2)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine
NON2
Parent:4
Linkage:o(6+2)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine
