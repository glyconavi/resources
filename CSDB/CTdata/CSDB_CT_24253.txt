RES
1b:a-dglc-HEX-1:5
2s:phosphate
3s:amino
4b:b-dglc-HEX-1:5
5s:amino
6b:x-dman-OCT-2:6|1:a|2:keto|3:d
7b:a-dgro-dman-HEP-1:5
8b:b-dglc-HEX-1:5
9b:a-lgro-dman-HEP-1:5
10b:a-lgro-dman-HEP-1:5
11b:b-lgro-lman-NON-2:6|1:a|2:keto|3:d|9:d
12s:n-acetyl
13s:n-acetyl
LIN
1:1o(1+1)2n
2:1d(2+1)3n
3:1o(6+1)4d
4:4d(2+1)5n
5:4o(6+2)6d
6:6o(5+1)7d
7:7o(4+1)8d
8:7o(3+1)9d
9:9o(2+1)10d
10:8o(6+2)11d
11:11d(5+1)12n
12:11d(7+1)13n
