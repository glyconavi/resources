RES
1r:r1
REP
REP1:4o(5+1)2d=-1--1
RES
2b:b-dgal-HEX-1:5
3s:n-acetyl
4b:b-dman-OCT-2:6|1:a|2:keto|3:d
5a:a1
LIN
1:2d(2+1)3n
2:2o(6+2)4d
3:4o(8+1)5n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:6
RES
6n:n1
ALTSUBGRAPH2
LEAD-IN RES:7
RES
7s:acetyl
NON
NON1
SmallMolecule:SMILES [3CH3][2CH2][1C](=O)O
Description:propanoic acid
