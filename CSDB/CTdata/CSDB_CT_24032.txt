RES
1r:r1
REP
REP1:11o(2+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5|6:d
3s:amino
4s:n-acetyl
5n:n1
6b:a-lgal-HEX-1:5|6:a
7s:n-acetyl
8s:acetyl
9b:a-dglc-HEX-1:5
10s:n-acetyl
11b:a-lman-HEX-1:5|6:d
12s:acetyl
LIN
1:2d(4+1)3n
2:2d(2+1)4n
3:2o(4+1)5n
4:2o(3+1)6d
5:6d(2+1)7n
6:6o(3+1)8n
7:6o(4+1)9d
8:9d(2+1)10n
9:9o(6+1)11d
10:11o(3+1)12n
NON
NON1
Parent:2
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH3][3C@H](O)[2CH2][1C](=O)O
Description:(S)-3-hydroxybutanoic acid
