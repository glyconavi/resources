RES
1n:n1
2s:acetyl
3n:n2
LIN
1:1n(5+1)2n
2:1n(7+1)3n
NON
NON1
SmallMolecule:SMILES C{8}[C@@H]({7}[C@@H]([C@@H]1O[C@]2(C[C@@H]({5}[C@@H]1N)O2)C(O)=O)N)O
Description:b-2,4-anhydro-Pse5NAc7N
NON2
Parent:1
Linkage:n(7+1)n
SmallMolecule:SMILES [4CH2](O)[3CH2][2CH2][1C](=O)O
Description:4-hydroxybutanoic acid
