RES
1b:o-xgro-TRI-0:0|1:aldi
2n:n1
3n:n2
4s:phosphate
5n:n3
6n:n4
7b:a-dglc-HEX-1:5
8s:amino
9b:a-dman-HEX-1:5
10b:a-dman-HEX-1:5
11b:a-dman-HEX-1:5
12b:a-dman-HEX-1:5
13s:phospho-ethanolamine
LIN
1:1o(1+1)2n
2:1o(2+1)3n
3:1o(3+1)4n
4:4n(1+1)5n
5:5n(2+1)6n
6:5n(6+1)7d
7:7d(2+1)8n
8:7o(4+1)9d
9:9o(6+1)10d
10:10o(2+1)11d
11:11o(2+1)12d
12:11o(6+1)13n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:stearic acid
NON2
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2]/[10CH]=[9CH]\[8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:oleic acid
NON3
Parent:4
Linkage:n(1+1)n
SmallMolecule:SMILES [6C@H](O)1[5C@H](O)[4C@@H](O)[3C@H](O)[2C@H](O)[1C@H](O)1
Description:myo-inositol
NON4
Parent:5
Linkage:n(2+1)n
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:palmitic acid
