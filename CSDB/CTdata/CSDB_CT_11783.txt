RES
1r:r1
REP
REP1:3o(1+1)2n=-1--1
RES
2s:phosphate
3b:o-xgro-TRI-0:0|1:aldi
4n:n1
LIN
1:2n(1+3)3o
2:3o(2+1)4n
NON
NON1
Parent:3
Linkage:o(2+1)n
SmallMolecule:SMILES [6CH2](N)[5CH2][4CH2][3CH2][2C@H](N)[1C](=O)O
Description:(L)-lysine
