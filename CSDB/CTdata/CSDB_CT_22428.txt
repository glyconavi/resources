RES
1b:a-dman-OCT-2:6|1:a|2:keto|3:d
2b:a-dman-OCT-2:6|1:a|2:keto|3:d
3n:n1
LIN
1:1o(8+2)2d
2:1o(2+1)3n
NON
NON1
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [3CH2]=[2CH][1CH2]O
Description:allyl alcohol
