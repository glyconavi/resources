RES
1n:n1
2b:a-dglc-HEX-1:5
3s:phosphate
4s:n-acetyl
5b:o-xgro-TRI-0:0|1:aldi
6n:n2
7n:n3
LIN
1:1n(2+1)2d
2:1n(3+1)3n
3:2d(2+1)4n
4:3n(1+1)5o
5:5o(2+1)6n
6:5o(3+1)7n
NON
NON1
SmallMolecule:SMILES [3CH2](O)[2CH](O)[1C](=O)O
Description:glyceric acid
NON2
Parent:5
Linkage:o(2+1)n
SmallMolecule:SMILES [17CH3][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:margaric acid
NON3
Parent:5
Linkage:o(3+1)n
SmallMolecule:SMILES [15CH3][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:pentadecanoic acid
