RES
1b:o-xgro-TRI-0:0
2b:a-lman-HEX-1:5|6:d
3b:b-dglc-HEX-1:5
4b:a-lman-HEX-1:5|6:d
5s:n-acetyl
LIN
1:1o(2+1)2d
2:2o(3+1)3d
3:3o(3+1)4d
4:3d(2+1)5n
