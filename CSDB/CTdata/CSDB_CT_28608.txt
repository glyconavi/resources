RES
1b:o-dgro-TRI-0:0|1:aldi
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4b:b-dglc-HEX-1:5
5s:phosphate
6n:n1
7s:phosphate
8b:a-dglc-HEX-1:5
9s:n-acetyl
10n:n2
11b:a-dglc-HEX-1:5
12s:n-acetyl
LIN
1:1o(1+1)2d
2:2o(6+1)3d
3:3o(6+1)4d
4:4o(6+1)5n
5:5n(1+1)6n
6:6n(6+1)7n
7:7n(1+6)8o
8:8d(2+1)9n
9:8o(1+2)10n
10:8o(3+1)11d
11:11d(2+1)12n
NON
NON1
Parent:5
Linkage:n(1+1)n
HistoricalEntity:repeating unit LTA (ID 28606, ID 28607)
NON2
Parent:8
Linkage:o(1+2)n
SmallMolecule:SMILES [3CH2](O)[2C@@H](O)[1C](=O)O
Description:(D)-glyceric acid
