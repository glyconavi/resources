RES
1n:n1
2s:pyrophosphate
3b:b-lgal-HEX-1:5|6:d
LIN
1:1n(5+1)2n
2:2n(1+1)3o
NON
NON1
SmallMolecule:SMILES [5CH2](O)[4C@@H](O1)[3C@@H](O)[2C@@H](O)[1C@H](N2[10CH]=N[8C]3=[7C]2N=[6C](N)N[9C](=O)3)1
Description:guanosine
