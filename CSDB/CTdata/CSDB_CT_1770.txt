RES
1b:b-dgal-HEX-1:5
2b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d
3b:b-dgal-HEX-1:5
4b:b-dgal-HEX-1:5
5s:n-acetyl
6s:n-acetyl
LIN
1:1o(3+2)2d
2:1o(4+1)3d
3:3o(3+1)4d
4:2d(5+1)5n
5:3d(2+1)6n
