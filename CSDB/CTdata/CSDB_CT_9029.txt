RES
1r:r1
REP
REP1:4o(2+1)2d=-1--1
RES
2b:b-lalt-HEX-1:4|6:d
3b:b-lalt-HEX-1:4|6:d
4b:b-lalt-HEX-1:4|6:d
5b:a-lalt-HEX-1:4|6:d
LIN
1:2o(2+1)3d
2:3o(3+1)4d
3:3o(2+1)5d
