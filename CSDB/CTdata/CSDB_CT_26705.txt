RES
1r:r1
REP
REP1:6o(3+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3s:n-acetyl
4b:b-dgal-HEX-1:5
5b:b-dglc-HEX-1:5
6b:a-dgal-HEX-1:5
7s:phosphate
8n:n1
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4o(3+1)5d
4:5o(6+1)6d
5:6o(6+1)7n
6:7n(1+1)8n
NON
NON1
Parent:7
Linkage:n(1+1)n
SmallMolecule:SMILES [5CH3][4C@@H](N[2CH2][1CH2]O)[3C](=O)O
Description:(R)-2-(1-carboxyethylamino)ethanol [N-(2-hydroxyethyl)alanine]
