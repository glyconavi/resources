RES
1b:b-xglc-HEX-1:5
2s:n-acetyl
3n:n1
4b:a-xman-HEX-1:5|6:d
5s:methyl
6b:a-xman-HEX-1:5|6:d
7b:a-xgal-HEX-1:5|6:d
8s:amino
9n:n2
10n:n3
11n:n4
LIN
1:1d(2+1)2n
2:1o(1+3)3n
3:1o(3+1)4d
4:4o(3+1)5n
5:4o(2+1)6d
6:6o(3+1)7d
7:7d(3+1)8n
8:7o(3+1)9n
9:9n(2+1)10n
10:10n(2+2)11n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES [3CH2](O)[2CH](N)[1C](=O)O
Description:serine
NON2
Parent:7
Linkage:o(3+1)n
SmallMolecule:SMILES [2CH2](N)[1C](=O)O
Description:glycine
NON3
Parent:9
Linkage:n(2+1)n
SmallMolecule:SMILES [3CH3][2CH](N)[1C](=O)O
Description:alanine
NON4
Parent:10
Linkage:n(2+2)n
SmallMolecule:SMILES OS(=O)(CC{2}N)=O
Description:taurine
