RES
1b:x-dara-HEX-x:x|2:d|6:a
2b:b-dglc-HEX-1:5
3b:a-dglc-HEX-1:5
4b:b-dglc-HEX-1:5
LIN
1:1o(4+1)2d
2:2o(6+1)3d
3:3o(6+1)4d
