RES
1n:n1
2b:b-lgul-HEX-1:5
3s:phosphate
4b:o-dgro-TRI-0:0|1:aldi
LIN
1:1n(51+1)2d
2:1n(1+1)3n
3:3n(1+1)4o
NON
NON1
SmallMolecule:SMILES O{51}CC1OCC[C@H](C)CCCC2CCC([C@H](C)CCC[C@H](C)CC[C@@H](C)CCC[C@@H](C)CCC[C@H](CCC[C@@H](C)CCOCC({1}CO)OCC[C@H](C)CCCC3CCC([C@@H](CCC[C@@H](CC[C@H](CCC[C@H](CCC[C@H](CCC[C@H](CCOC1)C)C)C)C)C)C)C3)C)C2
Description:tetraether lipid (C95H189O16P)
