RES
1b:b-dman-HEX-1:5|6:d
2s:methyl
3s:methyl
4s:amino
5n:n1
LIN
1:1o(2+1)2n
2:1o(1+1)3n
3:1d(4+1)4n
4:1o(4+1)5n
NON
NON1
Parent:1
Linkage:o(4+1)n
SmallMolecule:SMILES [3CH2](O)[2CH](O)[1C](=O)O
Description:glyceric acid
