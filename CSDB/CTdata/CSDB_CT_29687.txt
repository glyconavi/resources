RES
1r:r1
REP
REP1:5o(3+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3s:n-acetyl
4b:a-lgal-HEX-1:5|6:d
5b:b-dglc-HEX-1:5
6s:n-acetyl
7b:a-lgal-HEX-1:5|6:d
8b:a-dglc-HEX-1:5|6:a
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4o(3+1)5d
4:5d(2+1)6n
5:5o(4+1)7d
6:7o(3+1)8d
