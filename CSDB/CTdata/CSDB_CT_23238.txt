RES
1b:x-dman-OCT-x:x|1:a|2:keto|3:d
2b:a-lgro-dman-HEP-1:5
3s:phosphate
4s:phosphate
5b:a-lgro-dman-HEP-1:5
6s:phosphate
7n:n1
8b:a-dgal-HEX-1:5
9s:amino
10n:n2
11b:a-dglc-HEX-1:5
12s:acetyl
LIN
1:1o(5+1)2d
2:2o(2+1)3n
3:2o(4+1)4n
4:2o(3+1)5d
5:5o(6+1)6n
6:5o(7+1)7n
7:5o(3+1)8d
8:8d(2+1)9n
9:8o(2+1)10n
10:8o(4+1)11d
11:11o(6+1)12n
NON
NON1
Parent:5
Linkage:o(7+1)n
SmallMolecule:SMILES [1C](=O)(O)N
Description:carbamic acid
NON2
Parent:8
Linkage:o(2+1)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine
