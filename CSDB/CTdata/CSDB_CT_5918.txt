RES
1b:x-lgal-HEX-x:x|6:d
2b:b-dglc-HEX-1:5
3b:a-dgal-HEX-1:5
4b:b-dglc-HEX-1:5|6:a
5b:a-dgal-HEX-1:5
6s:pyruvate
LIN
1:1o(4+1)2d
2:1o(3+1)3d
3:3o(3+1)4d
4:4o(4+1)5d
5:5o(6+2)6n
6:5o(4+2)6n
