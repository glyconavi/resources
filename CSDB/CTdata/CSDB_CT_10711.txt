RES
1b:a-dman-OCT-x:x|1:a|2:keto|3:d
2b:a-lgro-dman-HEP-1:5
3s:triphosphate
4s:phosphate
5b:a-lgro-dman-HEP-1:5
6s:phosphate
7b:a-dgal-HEX-1:5
8s:amino
9n:n1
LIN
1:1o(5+1)2d
2:2o(2+1)3n
3:2o(4+1)4n
4:2o(3+1)5d
5:5o(6+1)6n
6:5o(3+1)7d
7:7d(2+1)8n
8:7o(2+1)9n
NON
NON1
Parent:7
Linkage:o(2+1)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine
