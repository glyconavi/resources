RES
1n:n1
2s:methyl
3b:b-dglc-HEX-1:5
4s:n-acetyl
5b:b-dglc-HEX-1:5|6:a
6b:b-dglc-HEX-1:5
7s:n-acetyl
8b:b-dglc-HEX-1:5|6:a
9b:b-dglc-HEX-1:5
10b:b-dglc-HEX-1:5|6:a
11s:n-acetyl
LIN
1:1n(4+1)2n
2:1n(7+1)3d
3:3d(2+1)4n
4:3o(3+1)5d
5:5o(4+1)6d
6:6d(2+1)7n
7:6o(3+1)8d
8:8o(4+1)9d
9:9o(3+1)10d
10:9d(2+1)11n
NON
NON1
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3CH][2CH]=[1C]1[7C](=O)O
Description:4-hydroxybenzoic acid
