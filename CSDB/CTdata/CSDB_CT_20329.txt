RES
1b:b-dgal-HEX-1:5|6:d
2s:n-acetyl
3b:b-dman-HEX-1:5|6:a
4s:n-acetyl
5s:n-acetyl
6b:b-dman-HEX-1:5|6:a
7s:n-acetyl
8s:amino
9n:n1
LIN
1:1d(2+1)2n
2:1o(3+1)3d
3:3d(2+1)4n
4:3d(3+1)5n
5:3o(4+1)6d
6:6d(2+1)7n
7:6d(3+1)8n
8:6o(3+1)9n
NON
NON1
Parent:6
Linkage:o(3+1)n
SmallMolecule:SMILES [2CH3][1C](=N)O
Description:acetimidic acid
