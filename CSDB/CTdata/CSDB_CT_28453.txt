RES
1b:a-lgro-dman-HEP-1:5
2b:a-lgro-dman-HEP-1:5
3n:n1
LIN
1:1o(7+1)2d
2:1o(1+1)3n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES [3CH2]=[2CH][1CH2]O
Description:allyl alcohol
