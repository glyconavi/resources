RES
1b:a-dgal-HEX-1:5|6:a
2b:a-dglc-HEX-1:5
3s:amino
4s:amino
5n:n1
6n:n2
7b:b-dglc-HEX-1:5
8s:amino
9s:phosphate
10s:amino
11n:n3
12n:n4
13n:n5
14n:n6
LIN
1:1o(1+1)2d
2:2d(3+1)3n
3:2d(2+1)4n
4:2o(3+1)5n
5:2o(2+1)6n
6:2o(6+1)7d
7:7d(3+1)8n
8:7o(4+1)9n
9:7d(2+1)10n
10:7o(2+1)11n
11:7o(3+1)12n
12:11n(3+1)13n
13:12n(3+1)14n
NON
NON1
Parent:2
Linkage:o(3+1)n
SmallMolecule:SMILES [12CH3][11CH]([13CH3])[10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-iso-tridecanoic acid
NON2
Parent:2
Linkage:o(2+1)n
SmallMolecule:SMILES [20CH3][19CH2][18CH2][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-icosanoic acid
NON3
Parent:7
Linkage:o(2+1)n
SmallMolecule:SMILES [20CH3][19CH2][18CH2][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-icosanoic acid
NON4
Parent:7
Linkage:o(3+1)n
SmallMolecule:SMILES [12CH3][11CH]([13CH3])[10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-iso-tridecanoic acid
NON5
Parent:11
Linkage:n(3+1)n
SmallMolecule:SMILES [28CH3][27CH](O)[26CH2][25CH2][24CH2][23CH2][22CH2][21CH2][20CH2][19CH2][18CH2][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:27-hydroxy-octacosanoic acid
NON6
Parent:12
Linkage:n(3+1)n
SmallMolecule:SMILES [20CH3][19CH2][18CH2][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:arachidic acid
