RES
1b:a-dglc-HEX-1:5
2b:b-dgal-HEX-1:4
3b:b-dgal-HEX-1:5
4s:n-acetyl
5n:n1
LIN
1:1o(4+1)2d
2:1o(6+1)3d
3:1d(2+1)4n
4:1o(1+3)5n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES [4CH3][3C@@H](O)[2C@H](N)[1C](=O)O
Description:(L)-threonine
