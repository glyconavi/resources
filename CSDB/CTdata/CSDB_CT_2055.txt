RES
1b:x-dglc-HEX-x:x
2b:a-dgal-HEX-1:5
3b:a-dgal-HEX-1:5
4b:a-dglc-HEX-1:5
5b:a-dglc-HEX-1:5
6b:a-dglc-HEX-1:5
7s:n-acetyl
8s:n-acetyl
9b:a-dgal-HEX-1:5
10b:a-lman-HEX-1:5|6:d
11b:a-lman-HEX-1:5|6:d
LIN
1:1o(6+1)2d
2:1o(3+1)3d
3:3o(2+1)4d
4:4o(2+1)5d
5:4o(4+1)6d
6:6d(2+1)7n
7:5d(2+1)8n
8:6o(3+1)9d
9:9o(2+1)10d
10:10o(3+1)11d
