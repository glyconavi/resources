RES
1b:a-lman-HEX-1:5|6:d
2b:a-dara-HEX-1:5
3s:methyl
4b:a-dman-HEX-1:5|6:d
5b:a-lman-HEX-1:5|6:d
6b:a-dara-HEX-1:5
7b:a-dman-HEX-1:5|6:d
8s:methyl
LIN
1:1o(3+1)2d
2:2h(3+1)3n
3:2o(2+1)4d
4:4o(3+1)5d
5:5o(3+1)6d
6:6o(2+1)7d
7:6h(3+1)8n
