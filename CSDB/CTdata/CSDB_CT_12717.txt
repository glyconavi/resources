RES
1r:r1
REP
REP1:31o(3+1)2n=-1--1
RES
2s:phosphate
3b:o-drib-PEN-0:0|1:aldi
4a:a1
5s:phosphate
6b:b-dglc-HEX-1:5
7s:n-acetyl
8b:o-drib-PEN-0:0|1:aldi
9a:a2
10b:b-dglc-HEX-1:5
11s:phosphate
12s:n-acetyl
13b:o-drib-PEN-0:0|1:aldi
14a:a3
15s:phosphate
16b:b-dglc-HEX-1:5
17s:n-acetyl
18b:o-drib-PEN-0:0|1:aldi
19a:a4
20s:phosphate
21b:b-dglc-HEX-1:5
22s:n-acetyl
23b:o-xgro-TRI-0:0|1:aldi
24s:phosphate
25b:o-xgro-TRI-0:0|1:aldi
26s:phosphate
27b:o-xgro-TRI-0:0|1:aldi
28s:phosphate
29b:o-xgro-TRI-0:0|1:aldi
30s:phosphate
31b:o-xgro-TRI-0:0|1:aldi
LIN
1:2n(1+1)3o
2:3o(-1+-1)4n
3:3o(5+1)5n
4:3o(4+1)6d
5:6d(2+1)7n
6:5n(1+1)8o
7:8o(-1+-1)9n
8:8o(4+1)10d
9:8o(5+1)11n
10:10d(2+1)12n
11:11n(1+1)13o
12:13o(-1+-1)14n
13:13o(5+1)15n
14:13o(4+1)16d
15:16d(2+1)17n
16:15n(1+1)18o
17:18o(-1+-1)19n
18:18o(5+1)20n
19:18o(4+1)21d
20:21d(2+1)22n
21:20n(1+1)23o
22:23o(3+1)24n
23:24n(1+1)25o
24:25o(3+1)26n
25:26n(1+1)27o
26:27o(3+1)28n
27:28n(1+1)29o
28:29o(3+1)30n
29:30n(1+1)31o
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:32
RES
32n:n1
ALTSUBGRAPH2
LEAD-IN RES:33
RES
33n:n2
ALT2
ALTSUBGRAPH1
LEAD-IN RES:34
RES
34n:n3
ALTSUBGRAPH2
LEAD-IN RES:35
RES
35n:n4
ALT3
ALTSUBGRAPH1
LEAD-IN RES:36
RES
36n:n5
ALTSUBGRAPH2
LEAD-IN RES:37
RES
37n:n6
ALT4
ALTSUBGRAPH1
LEAD-IN RES:38
RES
38n:n7
ALTSUBGRAPH2
LEAD-IN RES:39
RES
39n:n8
NON
NON1
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine
NON2
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine
NON3
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine
NON4
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine
NON5
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine
NON6
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine
NON7
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine
NON8
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine
