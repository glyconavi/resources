import sys

def extract_wurcs_lines(file_path):
    with open(file_path, 'r') as file:
        lines = file.readlines()

    wurcs_lines = [line.strip() for line in lines if 'WURCS' in line]
    return wurcs_lines

def output_wurcs_file(file_path, wurcs_lines):
    output_path = file_path + ".output.txt"
    with open(output_path, 'w') as file:
        for line in wurcs_lines:
            fields = line.split('\t')
            if len(fields) >= 3 and 'WURCS' in fields[2]:
                file.write(fields[2] + '\n')

def main():
    if len(sys.argv) != 2:
        print("Usage: python extract_wurcs.py input_file.txt")
        return

    input_file = sys.argv[1]

    wurcs_lines = extract_wurcs_lines(input_file)

    output_wurcs_file(input_file, wurcs_lines)

if __name__ == '__main__':
    main()

