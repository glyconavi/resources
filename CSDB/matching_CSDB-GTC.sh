#!/bin/bash

# 引数の数が正しいか確認
if [ "$#" -ne 3 ]; then
    echo "Usage: $0 input1_file input2_file output_file"
    exit 1
fi

# 引数からファイルのパスを取得
input1_file="$1"
input2_file="$2"
output_file="$3"

# 入力ファイル1を読み込み
while IFS=$'\t' read -r col1_input1 col2_input1; do
    # 入力ファイル2を読み込み
    while IFS=$'\t' read -r col1_input2 col2_input2; do
        # WURCSが一致するか確認
        if [[ "$col2_input1" == "$col2_input2" ]]; then
            # 一致した場合、tsv形式で出力
            echo -e "$col1_input1\t$col1_input2" >> "$output_file"
        fi
    done < "$input2_file"
done < "$input1_file"

