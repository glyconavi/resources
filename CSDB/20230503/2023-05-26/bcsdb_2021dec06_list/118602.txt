RES
1b:x-dglc-HEX-1:5
2b:x-dara-PEN-1:4
3s:n-acetyl
4b:b-dglc-HEX-1:5
5s:n-acetyl
6b:b-dglc-HEX-1:5
7s:n-acetyl
8b:b-dglc-HEX-1:5
9s:n-acetyl
10b:b-dglc-HEX-1:5
11a:a1
12s:n-methyl
13n:n1
LIN
1:1o(6+1)2d
2:1d(2+1)3n
3:1o(4+1)4d
4:4d(2+1)5n
5:4o(4+1)6d
6:6d(2+1)7n
7:6o(4+1)8d
8:8d(2+1)9n
9:8o(4+1)10d
10:10o(2+1)11n
11:10d(2+1)12n
12:10o(6+1)13n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:14
RES
14n:n2
ALTSUBGRAPH2
LEAD-IN RES:15
RES
15n:n3
NON
NON1
Parent:10
Linkage:o(6+1)n
SmallMolecule:SMILES [1C](=O)(O)N
Description:carbamic acid
NON2
SmallMolecule:SMILES [1*]
Description:octadecenoic acid
NON3
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:stearic acid