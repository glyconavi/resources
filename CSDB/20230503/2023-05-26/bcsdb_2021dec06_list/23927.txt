RES
1b:x-dglc-HEX-1:5
2s:n-acetyl
3b:b-dglc-HEX-1:5
4s:n-acetyl
5b:b-dglc-HEX-1:5
6s:n-acetyl
7b:b-dglc-HEX-1:5
8s:n-acetyl
9b:b-dglc-HEX-1:5
10s:n-methyl
11n:n1
12n:n2
LIN
1:1d(2+1)2n
2:1o(4+1)3d
3:3d(2+1)4n
4:3o(4+1)5d
5:5d(2+1)6n
6:5o(4+1)7d
7:7d(2+1)8n
8:7o(4+1)9d
9:9d(2+1)10n
10:9o(2+1)11n
11:9o(6+1)12n
NON
NON1
Parent:9
Linkage:o(2+1)n
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:palmitic acid
NON2
Parent:9
Linkage:o(6+1)n
SmallMolecule:SMILES [1C](=O)(O)N
Description:carbamic acid