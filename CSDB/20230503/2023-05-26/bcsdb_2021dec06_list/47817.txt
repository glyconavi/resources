RES
1b:o-xgro-TRI-0:0|1:aldi
2s:phosphate
3b:o-xgro-TRI-0:0|1:aldi
4n:n1
LIN
1:1o(3+1)2n
2:2n(1+1)3o
3:3o(3+1)4n
NON
NON1
Parent:3
Linkage:o(3+1)n
SmallMolecule:SMILES C[As](=O)(C)C[C@H]1O[C@@H](O)[C@H](O){1}[C@@H]1O
Description:bDRibf derivative