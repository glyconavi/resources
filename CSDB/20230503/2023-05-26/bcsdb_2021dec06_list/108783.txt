RES
1b:o-lgro-TRI-0:0|1:aldi
2n:n1
3n:n2
4b:b-dgal-HEX-1:5
5b:b-dgal-HEX-1:5
6n:n3
LIN
1:1o(1+1)2n
2:1o(2+1)3n
3:1o(3+1)4d
4:4o(2+1)5d
5:4o(3+1)6n
NON
NON1
Parent:1
Linkage:o(1+1)n
HistoricalEntity:superclass: lipid residue
NON2
Parent:1
Linkage:o(2+1)n
HistoricalEntity:superclass: lipid residue
NON3
Parent:4
Linkage:o(3+1)n
HistoricalEntity:superclass: lipid residue