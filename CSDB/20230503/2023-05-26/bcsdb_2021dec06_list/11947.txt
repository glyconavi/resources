RES
1r:r1
REP
REP1:10o(3+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5|6:d
3s:n-acetyl
4b:b-dglc-HEX-1:5
5s:n-acetyl
6b:a-lman-HEX-1:5|6:d
7s:acetyl
8b:b-dglc-HEX-1:5
9s:n-acetyl
10b:a-lman-HEX-1:5|6:d
11s:acetyl
LIN
1:2d(4+1)3n
2:2o(2+1)4d
3:4d(2+1)5n
4:4o(3+1)6d
5:6o(2+1)7n
6:6o(3+1)8d
7:8d(2+1)9n
8:8o(3+1)10d
9:10o(2+1)11n