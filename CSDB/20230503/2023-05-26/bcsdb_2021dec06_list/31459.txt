RES
1b:a-dglc-HEX-1:5
2s:phosphate
3s:amino
4n:n1
5n:n2
6b:b-dglc-HEX-1:5
7s:amino
8n:n3
9n:n4
10n:n5
LIN
1:1o(1+1)2n
2:1d(2+1)3n
3:1o(2+1)4n
4:1o(3+1)5n
5:1o(6+1)6d
6:6d(2+1)7n
7:6o(3+1)8n
8:6o(2+1)9n
9:9n(3+1)10n
NON
NON1
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [16CH3][15CH]([17CH3])[14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-iso-heptadecanoic acid
NON2
Parent:1
Linkage:o(3+1)n
SmallMolecule:SMILES [15CH3][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-pentadecanoic acid
NON3
Parent:6
Linkage:o(3+1)n
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-hexadecanoic acid
NON4
Parent:6
Linkage:o(2+1)n
SmallMolecule:SMILES [16CH3][15CH]([17CH3])[14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-iso-heptadecanoic acid
NON5
Parent:9
Linkage:n(3+1)n
SmallMolecule:SMILES [14CH3][13CH]([15CH3])[12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:iso-pentadecanoic acid