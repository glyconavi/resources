RES
1b:a-dman-OCT-2:6|1:a|2:keto|3:d
2b:a-lgro-dman-HEP-1:5
3a:a1
4b:a-lgro-dman-HEP-1:5
5s:phosphate
6b:a-dglc-HEX-1:5
7a:a2
LIN
1:1o(5+1)2d
2:2o(4+1)3n
3:2o(3+1)4d
4:4o(4+1)5n
5:4o(3+1)6d
6:6o(3+1)7n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:8
RES
8s:phospho-ethanolamine
ALTSUBGRAPH2
LEAD-IN RES:9
RES
9s:phosphate
ALT2
ALTSUBGRAPH1
LEAD-IN RES:10
RES
10b:a-dglc-HEX-1:5
ALTSUBGRAPH2
LEAD-IN RES:11
RES
11b:a-dgal-HEX-1:5