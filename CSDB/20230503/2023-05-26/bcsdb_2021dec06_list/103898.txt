RES
1n:n1
2b:b-dglc-HEX-1:5
3b:x-lery-HEX-x:x|2:d|6:d
4b:x-dxyl-HEX-x:x|4:d|6:d
5s:methyl
6s:methyl
7s:n-methyl
LIN
1:1n(3+1)2d
2:1n(-1+1)3d
3:1n(-1+1)4d
4:4o(3+1)5n
5:3h(3+1)6n
6:4d(3+1)7n
NON
NON1
SmallMolecule:SMILES CC[C@@H](O1)[C@](O)(C)[C@H](O)[C@@H](C){9}[C@@H](O)[C@H](C)C[C@](O)(C){5}[C@H](O)[C@@H](C){3}[C@H](O)[C@@H](C)C1=O
Description:(9S)-9-dihydroerythronolide A