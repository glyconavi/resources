RES
1r:r1
REP
REP1:7o(3+1)2d=-1--1
RES
2b:a-ltal-HEX-1:5|6:d
3s:acetyl
4b:b-dglc-HEX-1:5|6:d
5s:amino
6n:n1
7b:b-dgal-HEX-1:5|6:d
8s:n-acetyl
LIN
1:2o(4+1)3n
2:2o(3+1)4d
3:4d(3+1)5n
4:4o(3+1)6n
5:4o(4+1)7d
6:7d(2+1)8n
NON
NON1
Parent:4
Linkage:o(3+1)n
HistoricalEntity:superclass: lipid residue