RES
1r:r1
REP
REP1:8o(3+1)2d=-1--1
RES
2b:a-dglc-HEX-1:5|6:d
3a:a1
4s:n-acetyl
5s:amino
6b:b-dman-HEX-1:5|6:a
7s:n-acetyl
8b:b-dman-HEX-1:5|6:a
9s:n-acetyl
LIN
1:2o(4+1)3n
2:2d(2+1)4n
3:2d(4+1)5n
4:2o(3+1)6d
5:6d(2+1)7n
6:6o(4+1)8d
7:8d(2+1)9n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:10
RES
10n:n1
ALTSUBGRAPH2
LEAD-IN RES:11
RES
11s:acetyl
NON
NON1
SmallMolecule:SMILES [4CH3][3C@H](O)[2CH2][1C](=O)O
Description:(S)-3-hydroxybutanoic acid