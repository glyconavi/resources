RES
1r:r1
REP
REP1:7o(2+1)2d=-1--1
RES
2b:b-dgal-HEX-1:5
3s:n-acetyl
4b:a-dgal-HEX-1:5
5s:n-acetyl
6b:a-dglc-HEX-1:5
7b:a-dglc-HEX-1:5
8s:amino
9n:n1
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4d(2+1)5n
4:4o(4+1)6d
5:6o(6+1)7d
6:6d(2+1)8n
7:6o(2+1)9n
NON
NON1
Parent:6
Linkage:o(2+1)n
SmallMolecule:SMILES [4CH3][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxybutanoic acid