RES
1b:a-dglc-HEX-1:5
2s:amino
3n:n1
4b:a-dglc-HEX-1:5
5s:amino
LIN
1:1d(6+1)2n
2:1o(1+2)3n
3:3n(1+1)4d
4:4d(3+1)5n
NON
NON1
Parent:1
Linkage:o(1+2)n
SmallMolecule:SMILES O[C@H]1{1}[C](O)[C@H](C[C@H]({2}[C]1O)N)N