RES
1r:r1
REP
REP1:12o(4+1)2d=-1--1
RES
2b:a-lido-HEX-1:5|6:a
3b:a-dglc-HEX-1:5|6:d
4b:b-dglc-HEX-1:5|6:d
5s:amino
6s:amino
7s:n-acetyl
8s:n-acetyl
9n:n1
10n:n2
11b:b-dglc-HEX-1:5|6:a
12b:b-dglc-HEX-1:5|6:a
LIN
1:2o(4+1)3d
2:2o(2+1)4d
3:4d(4+1)5n
4:4d(2+1)6n
5:3d(2+1)7n
6:3d(4+1)8n
7:4o(2+1)9n
8:4o(4+1)10n
9:4o(3+1)11d
10:11o(4+1)12d
NON
NON1
Parent:4
Linkage:o(2+1)n
SmallMolecule:SMILES [4CH3][3C@H](O)[2CH2][1C](=O)O
Description:(S)-3-hydroxybutanoic acid
NON2
Parent:4
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH3][3C@H](O)[2CH2][1C](=O)O
Description:(S)-3-hydroxybutanoic acid