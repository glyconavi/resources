RES
1r:r1
REP
REP1:2o(4+2)2d=-1--1
RES
2b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d|9:d
3s:n-acetyl
4s:acetyl
5s:amino
6n:n1
LIN
1:2d(7+1)3n
2:2o(8+1)4n
3:2d(5+1)5n
4:2o(5+1)6n
NON
NON1
Parent:2
Linkage:o(5+1)n
SmallMolecule:SMILES [2CH3][1C](=N)O
Description:acetimidic acid