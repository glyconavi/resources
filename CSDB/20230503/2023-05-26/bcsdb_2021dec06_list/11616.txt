RES
1b:a-lman-HEX-x:x|6:d
2b:a-dglc-HEX-1:5
3b:a-lman-HEX-1:5|6:d
4b:a-lman-HEX-1:5|6:d
5b:b-dglc-HEX-1:5
6s:n-acetyl
LIN
1:1o(4+1)2d
2:1o(3+1)3d
3:3o(2+1)4d
4:4o(2+1)5d
5:5d(2+1)6n