RES
1b:o-xgro-TRI-0:0|1:aldi
2a:a1
3n:n1
4s:phosphate
5n:n2
6b:a-dglc-HEX-1:5
7s:amino
8b:a-dman-HEX-1:5
9b:a-dman-HEX-1:5
10b:a-dman-HEX-1:5
11n:n3
12n:n4
LIN
1:1o(2+1)2n
2:1o(1+1)3n
3:1o(3+1)4n
4:4n(1+1)5n
5:5n(6+1)6d
6:6d(2+1)7n
7:6o(4+1)8d
8:8o(6+1)9d
9:9o(2+1)10d
10:10o(6+1)11n
11:11n(2+1)12n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:13
RES
13n:n5
ALTSUBGRAPH2
LEAD-IN RES:14
RES
14n:n6
ALTSUBGRAPH3
LEAD-IN RES:15
RES
15n:n7
ALTSUBGRAPH4
LEAD-IN RES:16
RES
16n:n8
NON
NON1
Parent:1
Linkage:o(1+1)n
HistoricalEntity:superclass: alcohol residue
NON2
Parent:4
Linkage:n(1+1)n
SmallMolecule:SMILES [6C@H](O)1[5C@H](O)[4C@@H](O)[3C@H](O)[2C@H](O)[1C@H](O)1
Description:myo-inositol
NON3
Parent:10
Linkage:o(6+1)n
HistoricalEntity:phospho-ethanolamine
NON4
Parent:11
Linkage:n(2+1)n
SmallMolecule:SMILES [4C](=O)(N)[3CH2][2CH](N)[1C](=O)O
Description:asparagine
NON5
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:lauric acid
NON6
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:myristic acid
NON7
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:palmitic acid
NON8
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:stearic acid