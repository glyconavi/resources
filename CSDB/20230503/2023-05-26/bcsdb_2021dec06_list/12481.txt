RES
1r:r1
REP
REP1:6o(6+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3s:amino
4b:b-dglc-HEX-1:5
5s:n-acetyl
6b:b-dglc-HEX-1:5
7s:n-acetyl
LIN
1:2d(2+1)3n
2:2o(6+1)4d
3:4d(2+1)5n
4:4o(6+1)6d
5:6d(2+1)7n