RES
1n:n1
2b:a-ltre-HEX-1:5|2:d|3:d|6:d
3b:a-ltre-HEX-1:5|2:d|3:d|6:d
LIN
1:1n(7+1)2d
2:1n(11+1)3d
NON
NON1
SmallMolecule:SMILES CCC4(O)C{7}[C@H](O)c3c(O)c2c(=O)c1c(O)cccc1c(=O)c2c(O)c3{10}[C@H]4O