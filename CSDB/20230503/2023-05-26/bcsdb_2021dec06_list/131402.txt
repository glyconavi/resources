RES
1b:x-dgal-HEX-x:x
2s:n-acetyl
3b:b-dman-OCT-2:6|1:a|2:keto|3:d
4s:methyl
LIN
1:1d(2+1)2n
2:1o(3+2)3d
3:3o(1+1)4n