RES
1b:x-dman-OCT-2:6|1:a|2:keto|3:d
2b:x-dman-OCT-2:6|1:a|2:keto|3:d
3a:a1
4s:phospho-ethanolamine
5b:a-lgro-dman-HEP-1:5
6b:a-lgro-dman-HEP-1:5
LIN
1:1o(4+2)2d
2:1o(-1+-1)3n
3:2o(7+1)4n
4:3n(5+1)5d
5:5o(3+1)6d
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:7
LEAD-OUT RES:7+5
RES
7b:x-dman-OCT-2:6|1:a|2:keto|3:d
ALTSUBGRAPH2
LEAD-IN RES:8
LEAD-OUT RES:8+5
RES
8b:x-dman-OCT-2:6|1:a|2:keto|3:d