RES
1b:o-xgro-TRI-0:0|1:aldi
2b:a-dgal-HEX-1:4
3n:n1
4n:n2
LIN
1:1o(3+1)2d
2:1o(1+1)3n
3:1o(2+1)4n
NON
NON1
Parent:1
Linkage:o(1+1)n
HistoricalEntity:superclass: lipid residue
NON2
Parent:1
Linkage:o(2+1)n
HistoricalEntity:superclass: lipid residue