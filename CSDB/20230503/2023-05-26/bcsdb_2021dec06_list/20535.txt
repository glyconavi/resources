RES
1n:n1
2s:pyrophosphate
3n:n2
4s:acetyl
LIN
1:1n(5+1)2n
2:2n(1+1)3n
3:3n(2+1)4n
NON
NON1
SmallMolecule:SMILES [5CH2](O)[4C@@H](O1)[3C@@H](O)[2C@@H](O)[1C@H](N2[6C](=O)N[7C](=O)[8CH]=[9CH]2)1
Description:uridine
NON2
Parent:2
Linkage:n(1+1)n
SmallMolecule:SMILES O{1}[C@H]1O[C@@H](C)C([C@H](O){2}[C@H]1N)(O)O
Description:6daraHexpN-4-gem-diol