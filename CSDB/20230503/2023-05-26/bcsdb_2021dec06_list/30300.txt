RES
1b:b-dglc-HEX-1:5
2s:n-acetyl
3b:a-lman-HEX-1:5|6:d
4b:a-lman-HEX-1:5|6:d
5b:a-lman-HEX-1:5|6:d
6b:b-dglc-HEX-1:5
7s:n-acetyl
8b:a-lman-HEX-1:5|6:d
9b:a-lman-HEX-1:5|6:d
10b:a-lman-HEX-1:5|6:d
LIN
1:1d(2+1)2n
2:1o(3+1)3d
3:3o(3+1)4d
4:4o(2+1)5d
5:5o(2+1)6d
6:6d(2+1)7n
7:6o(3+1)8d
8:8o(3+1)9d
9:9o(2+1)10d