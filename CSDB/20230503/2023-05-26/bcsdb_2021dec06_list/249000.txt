RES
1r:r1
REP
REP1:4o(4+1)2d=-1--1
RES
2b:b-lglc-HEX-1:5|6:d
3s:n-acetyl
4b:b-lglc-HEX-1:5|6:d
5b:a-dglc-HEX-1:5|6:d
6a:a1
7s:n-acetyl
8s:n-acetyl
LIN
1:2d(3+1)3n
2:2o(4+1)4d
3:2o(2+1)5d
4:5o(-1+-1)6n
5:4d(3+1)7n
6:5d(2+1)8n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:9
RES
9n:n1
ALTSUBGRAPH2
LEAD-IN RES:10
RES
10n:n2
NON
NON1
SmallMolecule:SMILES [3CH3][2CH2][1C](=O)O
Description:propanoic acid
NON2
SmallMolecule:SMILES [3CH3][2CH2][1C](=O)O
Description:propanoic acid