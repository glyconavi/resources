RES
1b:a-dman-HEX-1:5
2b:b-dgal-HEX-1:5
3n:n1
LIN
1:1o(3+1)2d
2:1o(1+2)3n
NON
NON1
Parent:1
Linkage:o(1+2)n
SmallMolecule:SMILES O{2}C(CO)C1OC(CO)CO1
Description:dioxolane derivative