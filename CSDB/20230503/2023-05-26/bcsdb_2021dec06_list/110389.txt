RES
1b:b-dgal-HEX-1:5
2b:a-lman-HEX-1:5|6:d
3b:b-dman-HEX-1:5
LIN
1:1o(3+1)2d
2:2o(4+1)3d