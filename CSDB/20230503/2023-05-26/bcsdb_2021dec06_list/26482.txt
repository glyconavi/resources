RES
1b:a-dgal-HEX-1:5
2b:b-dglc-HEX-1:5
3s:n-acetyl
4s:acetyl
LIN
1:1o(6+1)2d
2:2d(2+1)3n
3:2o(6+1)4n