RES
1b:x-lgul-HEX-1:5|6:a
2s:n-acetyl
3s:n-acetyl
4b:b-dman-HEX-1:5|6:a
5s:n-acetyl
6s:n-acetyl
7s:amino
8b:b-dglc-HEX-1:5|6:a
9s:n-acetyl
10s:amino
11s:amino
12n:n1
LIN
1:1d(2+1)2n
2:1d(3+1)3n
3:1o(4+1)4d
4:4d(2+1)5n
5:4d(3+1)6n
6:4d(6+1)7n
7:4o(4+1)8d
8:8d(2+1)9n
9:8d(3+1)10n
10:8d(6+1)11n
11:8o(3+1)12n
NON
NON1
Parent:8
Linkage:o(3+1)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine