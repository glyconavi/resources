RES
1r:r1
REP
REP1:4o(6+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3b:b-dgal-HEX-1:5
4b:b-dglc-HEX-1:5
5s:n-acetyl
6b:b-dgal-HEX-1:5
7b:a-dgal-HEX-1:5
8s:phosphate
9b:o-xgro-TRI-0:0|1:aldi
LIN
1:2o(4+1)3d
2:3o(3+1)4d
3:4d(2+1)5n
4:4o(4+1)6d
5:6o(2+1)7d
6:6o(3+1)8n
7:8n(1+2)9o