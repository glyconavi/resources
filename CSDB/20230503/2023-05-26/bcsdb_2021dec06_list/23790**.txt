RES
1r:r1
REP
REP1:6o(2+1)2d=-1--1
RES
2b:a-dgal-HEX-1:5
3n:n1
4b:b-dglc-HEX-1:5
5s:n-acetyl
6b:a-dalt-HEP-1:5|6:d
7s:methyl
LIN
1:2o(2+1)3n
2:2o(3+1)4d
3:4d(2+1)5n
4:4o(3+1)6d
5:6o(3+1)7n
NON
NON1
Parent:2
Linkage:o(2+1)n
SmallMolecule:SMILES {1}OP(OC)(N)=O
Description:O-methyl phosphamide (MeOPN)