RES
1n:n1
2b:b-dglc-HEX-1:5|6:d
3s:amino
4b:a-dgal-HEX-1:5
5n:n2
6s:n-acetyl
7s:acetyl
LIN
1:1n(1+1)2d
2:2d(3+1)3n
3:2o(2+1)4d
4:2o(3+1)5n
5:4d(2+1)6n
6:5n(2+1)7n
NON
NON1
SmallMolecule:SMILES [2CH2](O)[1CH2](O)
Description:ethylene glycol
NON2
Parent:2
Linkage:o(3+1)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine