RES
1b:b-dglc-HEX-1:5
2s:acetyl
3s:n-acetyl
4r:r1
5b:b-dglc-HEX-1:5
6s:n-acetyl
7b:b-dglc-HEX-1:5
8a:a1
9s:amino
10s:acetyl
LIN
1:1o(6+1)2n
2:1d(2+1)3n
3:1o(4+1)4n
4:4n(4+1)5d
5:5d(2+1)6n
6:5o(4+1)7d
7:7o(2+1)8n
8:7d(2+1)9n
9:7o(6+1)10n
REP
REP1:11o(4+1)11d=1-2
RES
11b:b-dglc-HEX-1:5
12s:n-acetyl
LIN
1:11d(2+1)12n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:13
RES
13n:n1
ALTSUBGRAPH2
LEAD-IN RES:14
RES
14n:n2
NON
NON1
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2]/[12CH]=[11CH]\[10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:cis-vaccenic
NON2
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2]/[12CH]=[11CH]\[10CH2][9CH2][8CH2]/[7CH]=[6CH]/[5CH]=[4CH]/[3CH]=[2CH]/[1C](=O)O
Description:2,4,6-trans-11-cis-octadecatetraenoic acid