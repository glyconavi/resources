RES
1b:b-dgro-dgal-NON-x:x|1:a|2:keto|3:d|9:d
2s:n-acetyl
3s:amino
4n:n1
LIN
1:1d(7+1)2n
2:1d(5+1)3n
3:1o(5+1)4n
NON
NON1
Parent:1
Linkage:o(5+1)n
SmallMolecule:SMILES [2CH3][1C](=N)O
Description:acetimidic acid