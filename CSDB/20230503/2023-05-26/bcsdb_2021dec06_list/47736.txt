RES
1n:n1
2b:a-dman-HEX-1:5
3b:a-dglc-HEX-1:5|6:a
4s:amino
5n:n2
LIN
1:1n(1+1)2d
2:1n(2+1)3d
3:3d(2+1)4n
4:3o(2+1)5n
NON
NON1
SmallMolecule:SMILES CN[C@H]6C(=O)N[C@@H]4Cc1ccc(cc1)Oc%11cc3cc(Oc2ccc(cc2Cl)C(O)C%10NC(=O)[C@H](NC(=O)[C@@H]3NC(=O)[C@@H](NC4=O)c5cc(cc(O)c5Cl)Oc7cc6ccc7O)c8cccc(c8)c9{1}c(O)cc(O)cc9[C@@H](C(=O)O)NC%10=O){2}c%11O
NON2
Parent:3
Linkage:o(2+1)n
SmallMolecule:SMILES [11CH3][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:undecanoic acid