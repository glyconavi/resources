RES
1b:a-lman-HEX-1:5|6:d
2n:n1
3b:a-lman-HEX-1:5|6:d
4b:b-dglc-HEX-1:5
5s:n-acetyl
6b:a-lman-HEX-1:5|6:d
7b:a-lman-HEX-1:5|6:d
LIN
1:1o(1+7)2n
2:1o(2+1)3d
3:1o(3+1)4d
4:4d(2+1)5n
5:3o(2+1)6d
6:6o(3+1)7d
NON
NON1
Parent:1
Linkage:o(1+7)n
SmallMolecule:SMILES [6CH]1=[5CH][4CH]=[3CH][2CH]=[1C]1[7CH2]O
Description:benzoic alcohol