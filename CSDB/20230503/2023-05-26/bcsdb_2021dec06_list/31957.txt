RES
1b:x-dman-OCT-x:x|1:a|2:keto|3:d
2a:a1
3b:x-HEP-1:5
4s:diphospho-ethanolamine
5b:x-HEP-1:5
6b:x-HEP-1:5
7s:phosphate
8b:x-dglc-HEX-1:5
9b:x-dglc-HEX-1:5
LIN
1:1o(-1+-1)2n
2:2n(7+1)3d
3:2n(4+1)4n
4:2n(3+1)5d
5:5o(7+1)6d
6:5o(4+1)7n
7:5o(3+1)8d
8:8o(3+1)9d
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:10
LEAD-OUT RES:10+3|10+4|10+5
RES
10b:x-HEP-1:5
ALTSUBGRAPH2
LEAD-IN RES:11
LEAD-OUT RES:11+3|11+4|11+5
RES
11b:x-HEP-1:5