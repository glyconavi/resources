RES
1r:r1
REP
REP1:9o(3+1)2d=-1--1
RES
2b:a-lgal-HEX-1:5|6:d
3s:amino
4n:n1
5b:b-dgro-lgal-NON-x:x|1:a|2:keto|3:d|9:d
6s:n-acetyl
7s:amino
8n:n2
9b:a-dglc-HEX-1:5
10s:n-acetyl
LIN
1:2d(2+1)3n
2:2o(2+1)4n
3:2o(3+2)5d
4:5d(7+1)6n
5:5d(5+1)7n
6:5o(5+1)8n
7:5o(8+1)9d
8:9d(2+1)10n
NON
NON1
Parent:2
Linkage:o(2+1)n
SmallMolecule:SMILES [2CH3][1C](=N)O
Description:acetimidic acid
NON2
Parent:5
Linkage:o(5+1)n
SmallMolecule:SMILES [4CH3][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxybutanoic acid