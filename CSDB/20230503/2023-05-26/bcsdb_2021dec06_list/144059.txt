RES
1n:n1
2s:pyrophosphate
3b:a-dglc-HEX-1:5
4s:amino
5n:n2
6n:n3
7b:b-dglc-HEX-1:5
8s:amino
9n:n4
10b:a-dman-OCT-2:6|1:a|2:keto|3:d
11s:phosphate
12n:n5
13n:n6
14b:a-dgal-HEX-1:5
15b:a-dman-OCT-2:6|1:a|2:keto|3:d
16n:n7
17n:n8
LIN
1:1n(1+1)2n
2:2n(1+1)3o
3:3d(2+1)4n
4:3o(3+1)5n
5:3o(2+1)6n
6:3o(6+1)7d
7:7d(2+1)8n
8:6n(3+1)9n
9:7o(6+2)10d
10:7o(4+1)11n
11:7o(3+1)12n
12:7o(2+1)13n
13:11n(1+1)14o
14:10o(4+2)15d
15:13n(3+1)16n
16:12n(3+1)17n
NON
NON1
SmallMolecule:SMILES [2CH2](N)[1CH2](O)
Description:ethanolamine
NON2
Parent:3
Linkage:o(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON3
Parent:3
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON4
Parent:6
Linkage:n(3+1)n
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:palmitic acid
NON5
Parent:7
Linkage:o(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON6
Parent:7
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON7
Parent:13
Linkage:n(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:lauric acid
NON8
Parent:12
Linkage:n(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:myristic acid