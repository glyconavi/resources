RES
1b:o-dery-TET-0:0|1:aldi
2b:b-dgal-HEX-1:5
3b:b-dglc-HEX-1:5
4b:b-dglc-HEX-1:5
5s:n-acetyl
6b:b-dgal-HEX-1:5
7b:b-dgal-HEX-1:5
8b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d
9b:b-dglc-HEX-1:5
10s:n-acetyl
11s:n-acetyl
12b:b-dgal-HEX-1:5
13b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d
14s:n-acetyl
LIN
1:1o(3+1)2d
2:2o(4+1)3d
3:2o(3+1)4d
4:4d(2+1)5n
5:4o(4+1)6d
6:3o(4+1)7d
7:6o(3+2)8d
8:7o(3+1)9d
9:8d(5+1)10n
10:9d(2+1)11n
11:9o(4+1)12d
12:12o(3+2)13d
13:13d(5+1)14n