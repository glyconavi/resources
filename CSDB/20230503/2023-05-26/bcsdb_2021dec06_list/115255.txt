RES
1r:r1
REP
REP1:5o(3+1)2d=-1--1
RES
2b:a-dgal-HEX-1:5
3b:a-dgal-HEX-1:5
4b:b-dgal-HEX-1:5
5b:b-dglc-HEX-1:5
6b:b-dglc-HEX-1:5|6:a
LIN
1:2o(2+1)3d
2:2o(3+1)4d
3:4o(3+1)5d
4:4o(4+1)6d