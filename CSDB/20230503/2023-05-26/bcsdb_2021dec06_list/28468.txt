RES
1b:o-xgro-TRI-0:0|1:aldi
2a:a1
3b:b-dgal-HEX-1:5
4b:a-dman-HEX-1:5
LIN
1:1o(-1+-1)2n
2:1o(1+1)3d
3:3o(4+1)4d
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:5
RES
5s:phosphate
ALTSUBGRAPH2
LEAD-IN RES:6
RES
6s:phosphate