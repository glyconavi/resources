RES
1b:o-dglc-HEX-0:0
2a:a1
3s:(r)-carboxyethyl
4s:amino
5b:b-dglc-HEX-1:5
6n:n1
7s:n-acetyl
8n:n2
9n:n3
10n:n4
LIN
1:1o(2+1)2n
2:1o(3+1)3n
3:1d(2+1)4n
4:1o(4+1)5d
5:1o(8+2)6n
6:5d(2+1)7n
7:6n(1+2)8n
8:8n(5+2)9n
9:9n(1+2)10n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:11
RES
11s:formyl
ALTSUBGRAPH2
LEAD-IN RES:12
RES
12s:acetyl
ALTSUBGRAPH3
LEAD-IN RES:13
RES
13s:glycolyl
NON
NON1
Parent:1
Linkage:o(8+2)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine
NON2
Parent:6
Linkage:n(1+2)n
SmallMolecule:SMILES [5C](=O)(O)[4CH2][3CH2][2C@@H](N)[1C](=O)O
Description:(D)-glutamic acid
NON3
Parent:8
Linkage:n(5+2)n
SmallMolecule:SMILES [7C](=O)(O)[6C@H](N)[5CH2][4CH2][3CH2][2C@H](N)[1C](=O)O
Description:meso-diaminopimelic acid
NON4
Parent:9
Linkage:n(1+2)n
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine