RES
1n:n1
2b:a-ltre-HEX-1:5|2:d|3:d|6:d
3b:b-dara-HEX-1:5|1:d|2:d|6:d
4n:n2
5n:n3
LIN
1:1n(3+1)2d
2:1n(9+1)3d
3:2o(4+1)4n
4:3o(4+1)5n
5:3o(3+2)5n
NON
NON1
SmallMolecule:SMILES C{3}[C@]4(O)CC(=O)c2c(ccc3c(=O)c1c(O){9}cccc1c(=O)c23)C4
Description:tetrangomycin
NON2
Parent:2
Linkage:o(4+1)n
SmallMolecule:SMILES C[C@@H]1O{1}[C@@H](O)/C=C\C1=O
Description:aculose
NON3
Parent:3
Linkage:o(4+1)n|o(3+2)n
SmallMolecule:SMILES C[C@@H]1O{1}[C@@H](O){2}[C@@H](O)CC1=O
Description:��-L-cinerulose