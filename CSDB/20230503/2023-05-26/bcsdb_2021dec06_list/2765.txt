RES
1r:r1
REP
REP1:6o(3+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3s:n-acetyl
4b:a-dgal-HEX-1:5|6:a
5n:n1
6b:b-dglc-HEX-1:5|6:a
7n:n2
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4o(6+2)5n
4:4o(3+1)6d
5:6o(6+2)7n
NON
NON1
Parent:4
Linkage:o(6+2)n
SmallMolecule:SMILES [3CH3][2CH](N)[1C](=O)O
Description:alanine
NON2
Parent:6
Linkage:o(6+2)n
SmallMolecule:SMILES [6CH2](N)[5CH2][4CH2][3CH2][2CH](N)[1C](=O)O
Description:lysine