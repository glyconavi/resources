RES
1b:x-dara-PEN-1:4
2b:x-dara-PEN-1:4
3n:n1
LIN
1:1o(5+1)2d
2:2o(5+1)3n
NON
NON1
Parent:2
Linkage:o(5+1)n
HistoricalEntity:superclass: any monosaccharide