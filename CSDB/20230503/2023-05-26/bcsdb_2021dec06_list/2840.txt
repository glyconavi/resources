RES
1r:r1
REP
REP1:9o(4+1)2d=-1--1
RES
2b:a-dgal-HEX-1:5|6:a
3b:a-dgal-HEX-1:5|6:d
4b:b-dgal-HEX-1:5
5s:n-acetyl
6s:n-formyl
7b:a-dgal-HEX-1:5
8s:n-acetyl
9b:a-dgal-HEX-1:5
10s:n-acetyl
LIN
1:2o(4+1)3d
2:2o(3+1)4d
3:4d(2+1)5n
4:3d(3+1)6n
5:4o(3+1)7d
6:7d(2+1)8n
7:7o(4+1)9d
8:9d(2+1)10n