RES
1r:r1
REP
REP1:5n(16+1)2n=-1--1
RES
2n:n1
3b:o-xgro-TRI-0:0|1:aldi
4b:b-dglc-HEX-1:5
5n:n2
LIN
1:2n(16+3)3d
2:3o(1+1)4d
3:3o(2+1)5n
NON
NON1
SmallMolecule:SMILES [16CH2](O)[15CH]([20CH3])[14CH2][13CH2][12CH2][11CH]([19CH3])[10CH2][9CH2][8CH2][7CH]([18CH3])[6CH2][5CH2][4CH2][3CH]([17CH3])[2CH2][1CH2](O)
Description:3,7,11,15-tetramethyl-hexadecan-1,16-diol
NON2
Parent:3
Linkage:o(2+1)n
SmallMolecule:SMILES [16CH2](O)[15CH]([20CH3])[14CH2][13CH2][12CH2][11CH]([19CH3])[10CH2][9CH2][8CH2][7CH]([18CH3])[6CH2][5CH2][4CH2][3CH]([17CH3])[2CH2][1CH2](O)
Description:3,7,11,15-tetramethyl-hexadecan-1,16-diol