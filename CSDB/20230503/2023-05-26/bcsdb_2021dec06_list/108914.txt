RES
1b:a-dglc-HEX-1:5
2b:a-dglc-HEX-1:5
3n:n1
LIN
1:1o(3+1)2d
2:1o(-1+1)3n
NON
NON1
Parent:1
Linkage:o(-1+1)n
HistoricalEntity:superclass: lipid residue