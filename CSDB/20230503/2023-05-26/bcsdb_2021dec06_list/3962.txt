RES
1r:r1
REP
REP1:7o(3+1)2d=-1--1
RES
2b:a-dglc-HEX-1:5
3s:n-acetyl
4s:(s)-carboxyethyl
5b:a-dglc-HEX-1:5
6s:n-acetyl
7b:a-lglc-HEX-1:5|6:d
8s:n-acetyl
LIN
1:2d(2+1)3n
2:2o(3+2)4n
3:2o(6+1)5d
4:5d(2+1)6n
5:5o(3+1)7d
6:7d(2+1)8n