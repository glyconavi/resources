RES
1r:r1
REP
REP1:7o(3+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3s:n-acetyl
4b:a-dgal-HEX-1:5
5s:acetyl
6b:a-lman-HEX-1:5|6:d
7b:a-lman-HEX-1:5|6:d
8b:b-dglc-HEX-1:5
9b:b-dglc-HEX-1:5
10s:(s)-carboxyethyl
11s:n-acetyl
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4o(6+1)5n
4:4o(2+1)6d
5:6o(2+1)7d
6:7o(2+1)8d
7:8o(3+1)9d
8:8o(4+2)10n
9:8d(2+1)11n