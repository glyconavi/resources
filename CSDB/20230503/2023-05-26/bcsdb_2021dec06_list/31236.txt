RES
1n:n1
2s:phosphate
3b:a-lara-PEN-1:5
4s:amino
LIN
1:1n(1+1)2n
2:2n(1+1)3o
3:3d(4+1)4n
NON
NON1
SmallMolecule:SMILES [11CH3][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1CH2]O
Description:undecanol