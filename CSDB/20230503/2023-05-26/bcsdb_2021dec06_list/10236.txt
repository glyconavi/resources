RES
1n:n1
2s:phosphate
3b:a-dman-HEX-1:5
4b:b-dgal-HEX-1:5
5b:b-dglc-HEX-1:5
LIN
1:1n(1+1)2n
2:2n(1+1)3o
3:3o(4+1)4d
4:4o(3+1)5d
NON
NON1
SmallMolecule:SMILES [10CH2]=[9CH][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:dec-9-enoic acid