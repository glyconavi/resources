RES
1r:r1
REP
REP1:4o(3+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3b:a-dgal-HEX-1:5
4b:b-dgal-HEX-1:5
5b:b-dglc-HEX-1:5|6:a
6b:a-dgal-HEX-1:5
7s:pyruvate
LIN
1:2o(6+1)3d
2:3o(3+1)4d
3:4o(4+1)5d
4:5o(4+1)6d
5:6o(6+2)7n
6:6o(4+2)7n