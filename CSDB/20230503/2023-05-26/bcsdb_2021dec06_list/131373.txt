RES
1r:r1
REP
REP1:5o(3+1)2d=-1--1
RES
2b:a-dgal-HEX-1:5
3a:a1
4b:a-lman-HEX-1:5|6:d
5b:a-dglc-HEX-1:5
LIN
1:2o(2+1)3n
2:3n(3+1)4d
3:4o(3+1)5d
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:6
LEAD-OUT RES:7+4
RES
6s:phosphate
7b:o-xrib-PEN-0:0|1:aldi
LIN
1:6n(1+1)7o
ALTSUBGRAPH2
LEAD-IN RES:8
LEAD-OUT RES:9+4
RES
8s:phosphate
9b:o-xrib-PEN-0:0|1:aldi
LIN
1:8n(1+2)9o