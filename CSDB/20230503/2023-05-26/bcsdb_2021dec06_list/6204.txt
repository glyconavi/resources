RES
1b:x-dery-dgul-DEC-1:5|3:d|6:d|10:d
2n:n1
LIN
1:1h(4+1)2n
NON
NON1
Parent:1
Linkage:h(4+1)n
SmallMolecule:SMILES [1C@H](O)C
Description:(R)-hydroxyethyl