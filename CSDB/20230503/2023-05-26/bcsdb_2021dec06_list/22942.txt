RES
1r:r1
REP
REP1:5o(4+1)2d=-1--1
RES
2b:a-dman-HEX-1:5
3n:n1
4n:n2
5b:a-lman-HEX-1:5|6:d
6s:methyl
LIN
1:2o(2+1)3n
2:2o(4+1)4n
3:2o(3+1)5d
4:5o(2+1)6n
NON
NON1
Parent:2
Linkage:o(2+1)n
SmallMolecule:SMILES [4CH3][3C@H](O)[2CH2][1C](=O)O
Description:(S)-3-hydroxybutanoic acid
NON2
Parent:2
Linkage:o(4+1)n
SmallMolecule:SMILES OCC(O{1}C(O)C(NC(C)=O)CO)CO
Description:(HOCH2)2CHOCH(OH)CH(NHAc)CH2OH N-(1-((1,3-dihydroxypropan-2-yl)oxy)-1,3-dihydroxypropan-2-yl)acetamide