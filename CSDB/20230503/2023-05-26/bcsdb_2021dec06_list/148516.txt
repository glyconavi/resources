RES
1b:b-dxyl-PEN-1:5
2b:b-dgal-HEX-1:5|6:d
3s:n-acetyl
4s:acetyl
5b:a-lgro-lman-NON-2:6|1:a|2:keto|3:d|9:d
6s:n-formyl
7s:amino
8n:n1
LIN
1:1o(4+1)2d
2:2d(2+1)3n
3:2o(4+1)4n
4:2o(3+2)5d
5:5d(7+1)6n
6:5d(5+1)7n
7:5o(5+1)8n
NON
NON1
Parent:5
Linkage:o(5+1)n
SmallMolecule:SMILES [4CH3][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxybutanoic acid