RES
1n:n1
2s:pyrophosphate
3b:o-dglc-HEX-0:0
4a:a1
5s:(r)-carboxyethyl
6s:amino
7n:n2
8n:n3
9n:n4
10n:n5
LIN
1:1n(5+1)2n
2:2n(1+1)3o
3:3o(2+1)4n
4:3o(3+1)5n
5:3d(2+1)6n
6:3o(8+2)7n
7:7n(1+2)8n
8:8n(5+2)9n
9:9n(1+2)10n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:11
RES
11s:glycolyl
ALTSUBGRAPH2
LEAD-IN RES:12
RES
12s:formyl
ALTSUBGRAPH3
LEAD-IN RES:13
RES
13s:acetyl
NON
NON1
SmallMolecule:SMILES [5CH2](O)[4C@@H](O1)[3C@@H](O)[2C@@H](O)[1C@H](N2[6C](=O)N[7C](=O)[8CH]=[9CH]2)1
Description:uridine
NON2
Parent:3
Linkage:o(8+2)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine
NON3
Parent:7
Linkage:n(1+2)n
SmallMolecule:SMILES [5C](=O)(O)[4CH2][3CH2][2C@@H](N)[1C](=O)O
Description:(D)-glutamic acid
NON4
Parent:8
Linkage:n(5+2)n
SmallMolecule:SMILES [7C](=O)(O)[6C@H](N)[5CH2][4CH2][3CH2][2C@H](N)[1C](=O)O
Description:meso-diaminopimelic acid
NON5
Parent:9
Linkage:n(1+2)n
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine