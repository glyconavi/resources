RES
1b:x-dman-OCT-2:6|1:a|2:keto|3:d
2s:phosphate
3b:x-lgro-dman-HEP-x:x
4b:x-dglc-HEX-1:5
5b:x-lgro-dman-HEP-x:x
6b:x-lgro-dman-HEP-x:x
7b:x-dglc-HEX-1:5
8s:phospho-ethanolamine
9b:x-dglc-HEX-1:5
10b:x-dglc-HEX-1:5
LIN
1:1o(4+1)2n
2:1o(5+1)3d
3:3o(4+1)4d
4:3o(3+1)5d
5:5o(2+1)6d
6:4o(4+1)7d
7:5o(4+1)8n
8:5o(3+1)9d
9:9o(4+1)10d