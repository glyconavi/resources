RES
1r:r1
REP
REP1:8o(2+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5|6:d
3s:n-acetyl
4b:a-dgal-HEX-1:5|6:a
5s:n-acetyl
6s:acetyl
7b:a-lman-HEX-1:5|6:d
8b:a-lman-HEX-1:5|6:d
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4d(2+1)5n
4:4o(3+1)6n
5:4o(4+1)7d
6:7o(3+1)8d