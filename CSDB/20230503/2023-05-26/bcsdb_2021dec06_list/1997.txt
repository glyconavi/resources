RES
1b:x-lgal-HEX-x:x|6:d
2b:a-lman-HEX-1:5|6:d
3b:b-dglc-HEX-1:5
4s:n-acetyl
5b:a-dgal-HEX-1:5
6b:b-dgal-HEX-1:5
7s:n-acetyl
LIN
1:1o(2+1)2d
2:2o(2+1)3d
3:3d(2+1)4n
4:3o(3+1)5d
5:5o(3+1)6d
6:5d(2+1)7n