RES
1b:o-xgro-TRI-0:0|1:aldi
2b:x-dglc-HEX-1:5
3b:a-lgal-HEX-1:5|6:d
4s:n-acetyl
5b:b-dglc-HEX-1:5
6s:n-acetyl
7b:b-dglc-HEX-1:5
8s:n-acetyl
9b:b-dglc-HEX-1:5
10a:a1
11s:amino
12n:n1
13n:n2
14n:n3
LIN
1:1o(-1+1)2d
2:2o(6+1)3d
3:2d(2+1)4n
4:2o(4+1)5d
5:5d(2+1)6n
6:5o(4+1)7d
7:7d(2+1)8n
8:7o(4+1)9d
9:9o(2+1)10n
10:9d(2+1)11n
11:9o(3+1)12n
12:9o(6+1)13n
13:9o(4+1)14n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:15
RES
15n:n4
ALTSUBGRAPH2
LEAD-IN RES:16
RES
16n:n5
NON
NON1
Parent:9
Linkage:o(3+1)n
SmallMolecule:SMILES [1C](=O)(O)N
Description:carbamic acid
NON2
Parent:9
Linkage:o(6+1)n
SmallMolecule:SMILES [1C](=O)(O)N
Description:carbamic acid
NON3
Parent:9
Linkage:o(4+1)n
SmallMolecule:SMILES [1C](=O)(O)N
Description:carbamic acid
NON4
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH]=[11CH][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:vaccenic acid
NON5
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2]/[10CH]=[9CH]\[8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:oleic acid