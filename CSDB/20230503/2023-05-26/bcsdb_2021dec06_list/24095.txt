RES
1n:n1
2b:a-lman-HEX-1:5|6:d
3b:a-dglc-HEX-1:5|6:d
4s:n-acetyl
LIN
1:1n(1+1)2d
2:2o(3+1)3d
3:3d(3+1)4n
NON
NON1
SmallMolecule:SMILES [2CH2](O)[1CH2](O)
Description:ethylene glycol