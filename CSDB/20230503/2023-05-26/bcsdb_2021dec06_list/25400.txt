RES
1r:r1
REP
REP1:6o(2+1)2d=-1--1
RES
2b:a-dglc-HEX-1:5|6:a
3b:o-xgro-TRI-0:0|1:aldi
4b:b-dgal-HEX-1:4
5b:b-dgro-lglc-HEP-1:5
6b:b-drib-PEN-1:4
7s:n-acetyl
8s:methyl
9s:methyl
10s:amino
11n:n1
12n:n2
LIN
1:2o(6+2)3d
2:2o(4+1)4d
3:2o(3+1)5d
4:4o(5+1)6d
5:4d(2+1)7n
6:5o(3+1)8n
7:5o(6+1)9n
8:3d(2+1)10n
9:5o(4+1)11n
10:4o(3+1)12n
NON
NON1
Parent:5
Linkage:o(4+1)n
SmallMolecule:SMILES {1}OP(OC)(N)=O
Description:O-methyl phosphamide (OHPO(NH2)OMe)
NON2
Parent:4
Linkage:o(3+1)n
SmallMolecule:SMILES {1}OP(OC)(N)=O
Description:O-methyl phosphamide (OHPO(NH2)OMe)