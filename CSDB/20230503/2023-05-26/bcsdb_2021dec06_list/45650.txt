RES
1b:x-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+12)2n
NON
NON1
Parent:1
Linkage:o(1+12)n
SmallMolecule:SMILES C{12}C(O)C(O)c2cnc1[nH]c(N)nc(=O)c1n2
Description:biopterin