RES
1r:r1
REP
REP1:7o(2+1)2n=-1--1
RES
2n:n1
3b:o-xgro-TRI-0:0|1:aldi
4s:phosphate
5n:n2
6n:n3
7b:o-xgro-TRI-0:0|1:aldi
8b:b-dglc-HEX-1:5
9b:b-dglc-HEX-1:5
10b:b-dglc-HEX-1:5
LIN
1:2n(32+1)3d
2:3o(3+1)4n
3:3o(2+1)5n
4:4n(1+1)6n
5:5n(32+1)7d
6:7o(3+1)8d
7:8o(6+1)9d
8:9o(6+1)10d
NON
NON1
SmallMolecule:SMILES O{1}CCC(C)CCCC(C)CCCC(C)CCCC(C)CCC(C)CCCC(C)CCCC(C)CCCC(C)C{32}CO
Description:diphytanol (3R,7R,11R,15R,15'R,11'R,7'R,3'R-octamethyl-dotriacontan-1,32-diol)
NON2
Parent:3
Linkage:o(2+1)n
SmallMolecule:SMILES O{1}CCC(C)CCCC(C)CCCC(C)CCCC(C)CCC(C)CCCC(C)CCCC(C)CCCC(C)C{32}CO
Description:diphytanol (3R,7R,11R,15R,15'R,11'R,7'R,3'R-octamethyl-dotriacontan-1,32-diol)
NON3
Parent:4
Linkage:n(1+1)n
HistoricalEntity:superclass: any inositol