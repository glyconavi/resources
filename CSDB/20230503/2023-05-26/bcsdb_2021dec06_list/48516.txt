RES
1b:b-dglc-HEX-1:5
2s:n-acetyl
3r:r1
4b:x-xgal-HEX-x:x|6:d
5s:acetyl
6b:b-dglc-HEX-1:5
7s:n-methyl
8n:n1
9n:n2
10n:n3
LIN
1:1d(2+1)2n
2:1o(4+1)3n
3:1o(6+1)4d
4:4o(4+1)5n
5:3n(4+1)6d
6:6d(2+1)7n
7:6o(2+1)8n
8:6o(3+1)9n
9:6o(4+1)10n
REP
REP1:11o(4+1)11d=2-2
RES
11b:b-dglc-HEX-1:5
12s:n-acetyl
LIN
1:11d(2+1)12n
NON
NON1
Parent:6
Linkage:o(2+1)n
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2]/[12CH]=[11CH]\[10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:cis-vaccenic
NON2
Parent:6
Linkage:o(3+1)n
SmallMolecule:SMILES [1C](=O)(O)N
Description:carbamic acid
NON3
Parent:6
Linkage:o(4+1)n
SmallMolecule:SMILES [1C](=O)(O)N
Description:carbamic acid