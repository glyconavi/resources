RES
1r:r1
REP
REP1:5o(3+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3s:n-acetyl
4b:a-lgul-HEX-1:5|6:a
5b:a-dgal-HEX-1:5
6s:n-acetyl
LIN
1:2d(2+1)3n
2:2o(4+1)4d
3:4o(4+1)5d
4:4d(2+1)6n