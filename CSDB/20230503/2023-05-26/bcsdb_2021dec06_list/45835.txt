RES
1b:b-dman-HEX-1:5|6:d
2s:amino
3n:n1
LIN
1:1d(3+1)2n
2:1o(1+19)3n
NON
NON1
Parent:1
Linkage:o(1+19)n
SmallMolecule:SMILES C[C@H]1/C=C\C=C/CC/C=C\C=C/C=C\C=C/{19}[C@H](O)C[C@@H]2O[C@](O)(C[C@@H](O)C[C@@H](O)[C@H](O)CC[C@@H](O)C[C@@H](O)CC(=O)O[C@@H](C)[C@H](C)[C@@H]1O)C[C@H](O)[C@H]2C