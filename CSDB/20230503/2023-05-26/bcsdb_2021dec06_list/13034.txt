RES
1r:r1
REP
REP1:3o(6+2)2d=-1--1
RES
2b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d
3b:a-dglc-HEX-1:5
4a:a1
5s:n-acetyl
LIN
1:2o(4+1)3d
2:2o(-1+-1)4n
3:2d(5+1)5n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:6
RES
6s:acetyl
ALTSUBGRAPH2
LEAD-IN RES:7
RES
7s:acetyl