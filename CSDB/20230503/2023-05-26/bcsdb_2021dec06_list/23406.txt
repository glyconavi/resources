RES
1r:r1
REP
REP1:12o(3+1)2d=-1--1
RES
2b:b-lglc-HEX-1:5|6:a
3a:a2
4a:a1
5s:amino
6s:amino
7b:b-dglc-HEX-1:5|6:d
8a:a4
9a:a3
10s:amino
11s:amino
12b:a-lgal-HEX-1:5|6:d
13s:n-acetyl
LIN
1:2o(2+1)3n
2:2o(3+1)4n
3:2d(3+1)5n
4:2d(2+1)6n
5:2o(4+1)7d
6:7o(2+1)8n
7:7o(4+1)9n
8:7d(2+1)10n
9:7d(4+1)11n
10:7o(3+1)12d
11:12d(2+1)13n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:14
RES
14n:n1
ALTSUBGRAPH2
LEAD-IN RES:15
RES
15s:acetyl
ALT2
ALTSUBGRAPH1
LEAD-IN RES:16
RES
16n:n2
ALTSUBGRAPH2
LEAD-IN RES:17
RES
17s:acetyl
ALT3
ALTSUBGRAPH1
LEAD-IN RES:18
RES
18n:n3
19s:acetyl
LIN
1:18n(2+1)19n
ALTSUBGRAPH2
LEAD-IN RES:20
RES
20n:n4
ALT4
ALTSUBGRAPH1
LEAD-IN RES:21
RES
21n:n5
22s:acetyl
LIN
1:21n(2+1)22n
ALTSUBGRAPH2
LEAD-IN RES:23
RES
23n:n6
NON
NON1
SmallMolecule:SMILES [4CH3][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxybutanoic acid
NON2
SmallMolecule:SMILES [4CH3][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxybutanoic acid
NON3
SmallMolecule:SMILES [3CH3][2CH](N)[1C](=O)O
Description:alanine
NON4
SmallMolecule:SMILES [4CH3][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxybutanoic acid
NON5
SmallMolecule:SMILES [3CH3][2CH](N)[1C](=O)O
Description:alanine
NON6
SmallMolecule:SMILES [4CH3][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxybutanoic acid