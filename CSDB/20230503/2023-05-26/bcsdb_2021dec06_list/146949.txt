RES
1b:b-dglc-HEX-1:5
2s:n-acetyl
3s:sulfate
4b:b-dglc-HEX-1:5
5s:n-acetyl
6b:b-dglc-HEX-1:5
7s:n-acetyl
8b:b-dglc-HEX-1:5
9s:amino
10n:n1
LIN
1:1d(2+1)2n
2:1o(6+1)3n
3:1o(4+1)4d
4:4d(2+1)5n
5:4o(4+1)6d
6:6d(2+1)7n
7:6o(4+1)8d
8:8d(2+1)9n
9:8o(2+1)10n
NON
NON1
Parent:8
Linkage:o(2+1)n
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:palmitic acid