RES
1r:r1
REP
REP1:3o(6+1)2n=-1--1
RES
2s:phosphate
3b:a-dgal-HEX-1:5
4s:n-acetyl
LIN
1:2n(1+1)3o
2:3d(2+1)4n