RES
1b:a-llyx-HEX-1:5|2:d|6:d
2s:n-dimethyl
3n:n1
4b:a-llyx-HEX-1:5|2:d|6:d
5b:a-ltre-HEX-1:5|2:d|3:d|6:d
LIN
1:1d(3+1)2n
2:1o(1+11)3n
3:1o(4+1)4d
4:4o(4+1)5d
NON
NON1
Parent:1
Linkage:o(1+11)n
SmallMolecule:SMILES CCC4(O)CCc3c(O)c2c(=O)c1c(O)cccc1c(=O)c2c(O)c3{10}C4O
Description:��-rhodomycinone