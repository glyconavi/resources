RES
1b:b-dall-HEX-1:4|6:a
2s:amino
3s:amino
4n:n1
5n:n2
LIN
1:1d(5+1)2n
2:1d(1+1)3n
3:1o(5+1)4n
4:1o(1+1)5n
NON
NON1
Parent:1
Linkage:o(5+1)n
SmallMolecule:SMILES C[C@@H]([C@H](N){1}C(N)=O)[C@H](O)c1ccc(O)cn1
Description:hydroxypyridylhomothreonine
NON2
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES O=c1{5}cc{1}[nH]c(=O)[nH]1
Description:uracil