RES
1r:r1
REP
REP1:8o(4+1)2d=-1--1
RES
2b:a-dgal-HEX-1:5|6:d
3s:n-acetyl
4b:b-dman-HEX-1:5|6:a
5s:n-acetyl
6s:amino
7n:n1
8b:b-dman-HEX-1:5|6:a
9s:n-acetyl
10s:amino
11n:n2
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4d(2+1)5n
4:4d(3+1)6n
5:4o(3+1)7n
6:4o(4+1)8d
7:8d(2+1)9n
8:8d(3+1)10n
9:8o(3+1)11n
NON
NON1
Parent:4
Linkage:o(3+1)n
SmallMolecule:SMILES [2CH3][1C](=N)O
Description:acetimidic acid
NON2
Parent:8
Linkage:o(3+1)n
SmallMolecule:SMILES [2CH3][1C](=N)O
Description:acetimidic acid