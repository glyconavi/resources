RES
1b:b-lara-PEN-1:5
2s:amino
3s:phosphate
4b:a-dglc-HEX-1:5
5s:amino
6n:n1
7n:n2
8b:b-dglc-HEX-1:5
9s:amino
10n:n3
11s:phosphate
12n:n4
13n:n5
14b:b-lara-PEN-1:5
15s:amino
LIN
1:1d(4+1)2n
2:1o(1+1)3n
3:3n(1+1)4o
4:4d(2+1)5n
5:4o(3+1)6n
6:4o(2+1)7n
7:4o(6+1)8d
8:8d(2+1)9n
9:8o(3+1)10n
10:8o(4+1)11n
11:8o(2+1)12n
12:12n(3+1)13n
13:11n(1+1)14o
14:14d(4+1)15n
NON
NON1
Parent:4
Linkage:o(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON2
Parent:4
Linkage:o(2+1)n
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-hexadecanoic acid
NON3
Parent:8
Linkage:o(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON4
Parent:8
Linkage:o(2+1)n
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-hexadecanoic acid
NON5
Parent:12
Linkage:n(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:myristic acid