RES
1b:a-dgal-HEX-1:5
2s:amino
3n:n1
4b:a-dglc-HEX-1:5
5s:acetyl
LIN
1:1d(2+1)2n
2:1o(2+1)3n
3:1o(4+1)4d
4:4o(6+1)5n
NON
NON1
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine