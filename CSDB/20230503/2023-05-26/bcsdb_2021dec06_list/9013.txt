RES
1b:b-dglc-HEX-1:5
2s:n-acetyl
3s:methyl
4b:a-lman-HEX-1:5|6:d
5b:a-lman-HEX-1:5|6:d
6b:a-dglc-HEX-1:5
LIN
1:1d(2+1)2n
2:1o(1+1)3n
3:1o(3+1)4d
4:4o(3+1)5d
5:5o(3+1)6d