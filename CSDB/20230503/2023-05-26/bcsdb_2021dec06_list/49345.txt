RES
1b:a-dglc-HEX-1:5
2n:n1
3n:n2
4b:a-dglc-HEX-1:5
5n:n3
6b:b-dglc-HEX-1:5
7b:b-dglc-HEX-1:5
8b:a-lman-HEX-1:5|6:d
9s:methyl
10b:b-dxyl-PEN-1:5
11a:a1
12b:o-drib-HEX-0:0|2:d|6:d
13a:a2
14b:o-drib-HEX-0:0|2:d|6:d
LIN
1:1o(4+1)2n
2:1o(6+1)3n
3:1o(1+1)4d
4:4o(2+1)5n
5:4o(4+1)6d
6:6o(4+1)7d
7:7o(3+1)8d
8:8o(3+1)9n
9:8o(4+1)10d
10:10o(3+1)11n
11:11n(4+1)12d
12:12o(3+1)13n
13:13n(4+1)14d
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:15
LEAD-OUT RES:15+12
RES
15b:a-dxyl-HEX-1:5|3:d|6:d
ALTSUBGRAPH2
LEAD-IN RES:16
LEAD-OUT RES:16+12
RES
16b:a-dgal-HEX-1:5|6:d
ALT2
ALTSUBGRAPH1
LEAD-IN RES:17
LEAD-OUT RES:17+14
RES
17b:a-dxyl-HEX-1:5|3:d|6:d
ALTSUBGRAPH2
LEAD-IN RES:18
LEAD-OUT RES:18+14
RES
18b:a-dgal-HEX-1:5|6:d
NON
NON1
Parent:1
Linkage:o(4+1)n
HistoricalEntity:superclass: lipid residue
NON2
Parent:1
Linkage:o(6+1)n
HistoricalEntity:superclass: lipid residue
NON3
Parent:4
Linkage:o(2+1)n
HistoricalEntity:superclass: lipid residue