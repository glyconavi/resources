RES
1r:r1
REP
REP1:5o(4+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3s:(r)-carboxyethyl
4s:n-glycolyl
5b:b-dglc-HEX-1:5
6s:phosphate
7n:n1
8s:n-acetyl
9n:n2
10n:n3
11n:n4
12n:n5
LIN
1:2o(3+1)3n
2:2d(2+1)4n
3:2o(4+1)5d
4:2o(6+1)6n
5:2o(8+2)7n
6:5d(2+1)8n
7:6n(1+1)9n
8:7n(1+2)10n
9:10n(5+2)11n
10:11n(1+2)12n
NON
NON1
Parent:2
Linkage:o(8+2)n
SmallMolecule:SMILES [3CH3][2CH](N)[1C](=O)O
Description:alanine
NON2
Parent:6
Linkage:n(1+1)n
HistoricalEntity:galactoarabinan (Gal2-Ara5)
NON3
Parent:7
Linkage:n(1+2)n
SmallMolecule:SMILES [5C](=O)(O)[4CH2][3CH2][2CH](N)[1C](=O)O
Description:glutamic acid
NON4
Parent:10
Linkage:n(5+2)n
SmallMolecule:SMILES [7C](=O)(O)[6C@H](N)[5CH2][4CH2][3CH2][2C@H](N)[1C](=O)O
Description:meso-diaminopimelic acid
NON5
Parent:11
Linkage:n(1+2)n
SmallMolecule:SMILES [3CH3][2CH](N)[1C](=O)O
Description:alanine