RES
1b:a-dglc-HEX-1:5
2s:phosphate
3n:n1
4b:b-dglc-HEX-1:5
5s:phosphate
6n:n2
7n:n3
8n:n4
9n:n5
10s:methyl
LIN
1:1o(1+1)2n
2:1o(2+1)3n
3:1o(6+1)4d
4:4o(4+1)5n
5:4o(3+1)6n
6:4o(2+1)7n
7:7n(3+1)8n
8:8n(27+1)9n
9:9n(3+1)10n
NON
NON1
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-stearic acid
NON2
Parent:4
Linkage:o(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON3
Parent:4
Linkage:o(2+1)n
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-stearic acid
NON4
Parent:7
Linkage:n(3+1)n
SmallMolecule:SMILES [28CH3][27CH](O)[26CH2][25CH2][24CH2][23CH2][22CH2][21CH2][20CH2][19CH2][18CH2][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:27-hydroxy-octacosanoic acid
NON5
Parent:8
Linkage:n(27+1)n
SmallMolecule:SMILES [4CH3][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxybutanoic acid