RES
1r:r1
REP
REP1:5o(3+1)2d=-1--1
RES
2b:b-dgro-dman-HEP-1:5
3b:a-lman-HEX-1:5|6:d
4b:b-dgal-HEX-1:5
5b:b-dglc-HEX-1:5
6s:n-acetyl
7s:n-acetyl
LIN
1:2o(3+1)3d
2:2o(4+1)4d
3:2o(7+1)5d
4:5d(2+1)6n
5:4d(2+1)7n