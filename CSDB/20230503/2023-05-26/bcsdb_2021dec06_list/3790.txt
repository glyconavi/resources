RES
1b:a-dman-HEX-1:5|6:d
2s:amino
3n:n1
4b:a-dman-HEX-1:5|6:d
5s:amino
6n:n2
LIN
1:1d(4+1)2n
2:1o(4+1)3n
3:1o(2+1)4d
4:4d(4+1)5n
5:4o(4+1)6n
NON
NON1
Parent:1
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH2](O)[3CH2][2C@H](O)[1C](=O)O
Description:(S)-2,4-dihydroxybutanoic acid
NON2
Parent:4
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH2](O)[3CH2][2C@H](O)[1C](=O)O
Description:(S)-2,4-dihydroxybutanoic acid