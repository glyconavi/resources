RES
1b:b-dglc-HEX-1:5|6:d
2s:n-acetyl
3s:amino
4n:n1
5n:n2
6b:a-lgal-HEX-1:5|6:d
7b:a-dglc-HEX-1:5
8s:amino
9n:n3
LIN
1:1d(2+1)2n
2:1d(4+1)3n
3:1o(4+1)4n
4:1o(1+4)5n
5:1o(3+1)6d
6:6o(3+1)7d
7:6d(2+1)8n
8:6o(2+1)9n
NON
NON1
Parent:1
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH3][3C@H](O)[2CH2][1C](=O)O
Description:(S)-3-hydroxybutanoic acid
NON2
Parent:1
Linkage:o(1+4)n
HistoricalEntity:2-amino-2-deoxy-2-C-methyl-pentonic acid
NON3
Parent:6
Linkage:o(2+1)n
SmallMolecule:SMILES [2CH3][1C](=N)O
Description:acetimidic acid