RES
1b:o-lgro-TRI-0:0|1:aldi
2n:n1
3n:n2
4b:a-dglc-HEX-1:5
5b:a-dglc-HEX-1:5
6s:phosphate
7b:o-xgro-TRI-0:0|1:aldi
8n:n3
9n:n4
LIN
1:1o(1+1)2n
2:1o(2+1)3n
3:1o(3+1)4d
4:4o(2+1)5d
5:4o(6+1)6n
6:6n(1+1)7o
7:7o(2+1)8n
8:7o(3+1)9n
NON
NON1
Parent:1
Linkage:o(1+1)n
HistoricalEntity:superclass: lipid residue
NON2
Parent:1
Linkage:o(2+1)n
HistoricalEntity:superclass: lipid residue
NON3
Parent:7
Linkage:o(2+1)n
HistoricalEntity:superclass: lipid residue
NON4
Parent:7
Linkage:o(3+1)n
HistoricalEntity:superclass: lipid residue