RES
1b:x-dman-OCT-x:x|1:a|2:keto|3:d
2b:b-lara-PEN-1:5
3b:a-lgro-dman-HEP-1:5
4b:b-dglc-HEX-1:5
5s:amino
6b:a-lgro-dman-HEP-1:5
7b:a-lgro-dman-HEP-1:5
8b:a-dgal-HEX-1:5|6:a
9b:a-dgal-HEX-1:5
10b:a-dgro-dman-HEP-1:5
11b:b-dgal-HEX-1:5|6:a
12b:a-lgro-dman-HEP-1:5
13a:a1
14s:amino
LIN
1:1o(8+1)2d
2:1o(5+1)3d
3:3o(4+1)4d
4:2d(4+1)5n
5:3o(3+1)6d
6:6o(7+1)7d
7:6o(3+1)8d
8:8o(4+1)9d
9:8o(2+1)10d
10:7o(7+1)11d
11:10o(2+1)12d
12:11o(6+1)13n
13:9d(2+1)14n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:15
RES
15n:n1
ALTSUBGRAPH2
LEAD-IN RES:16
RES
16n:n2
NON
NON1
SmallMolecule:SMILES N{1}CCCCN
Description:putrescine
NON2
SmallMolecule:SMILES N{1}CCCNCCCCN
Description:spermidine