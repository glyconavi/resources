RES
1r:r1
REP
REP1:5o(3+1)2d=-1--1
RES
2b:b-dido-HEP-1:5|6:d
3n:n1
4n:n2
5b:b-dglc-HEX-1:5
6s:n-acetyl
LIN
1:2o(2+1)3n
2:2o(7+1)4n
3:2o(3+1)5d
4:5d(2+1)6n
NON
NON1
Parent:2
Linkage:o(2+1)n
SmallMolecule:SMILES {1}OP(OC)(N)=O
Description:H or O-methylphosphamide
NON2
Parent:2
Linkage:o(7+1)n
SmallMolecule:SMILES {1}OP(OC)(N)=O
Description:H or O-methylphosphamide