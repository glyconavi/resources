RES
1r:r1
REP
REP1:8o(4+1)2d=-1--1
RES
2b:a-dman-HEX-1:5
3b:x-lman-HEX-1:5|6:d
4b:a-dglc-HEX-1:5
5s:n-acetyl
6s:acetyl
7b:b-dglc-HEX-1:5
8b:b-dgal-HEX-1:5
9s:n-acetyl
LIN
1:2o(3+1)3d
2:2o(2+1)4d
3:4d(2+1)5n
4:3o(-1+1)6n
5:4o(4+1)7d
6:7o(3+1)8d
7:7d(2+1)9n