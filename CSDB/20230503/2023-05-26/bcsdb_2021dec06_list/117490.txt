RES
1b:o-xgal-HEX-0:0|1:aldi
2b:x-xgal-HEX-1:4
3b:x-xgal-HEX-1:4
4n:n1
LIN
1:1o(5+1)2d
2:2o(3+1)3d
3:3o(5+-1)4n
NON
NON1
Parent:3
Linkage:o(5+-1)n
HistoricalEntity:superclass: any residue