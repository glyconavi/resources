RES
1b:a-dman-OCT-2:6|1:a|2:keto|3:d
2s:phosphate
3b:a-lgro-dman-HEP-1:5
4b:b-dglc-HEX-1:5
5b:a-lgro-dman-HEP-1:5
6b:a-lgro-dman-HEP-1:5
7b:a-dglc-HEX-1:5
8b:b-dglc-HEX-1:5
9b:b-dgal-HEX-1:5
10b:b-dgal-HEX-1:5
11b:b-dglc-HEX-1:5
12b:a-dgal-HEX-1:5
13b:b-dgal-HEX-1:5
14b:a-dgal-HEX-1:5
LIN
1:1o(4+1)2n
2:1o(5+1)3d
3:3o(4+1)4d
4:3o(3+1)5d
5:5o(2+1)6d
6:5o(3+1)7d
7:4o(4+1)8d
8:6o(2+1)9d
9:8o(4+1)10d
10:7o(4+1)11d
11:10o(4+1)12d
12:11o(4+1)13d
13:13o(4+1)14d