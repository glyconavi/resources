RES
1b:o-lgro-TRI-0:0|1:aldi
2n:n1
3n:n2
4b:a-dglc-HEX-1:5
5b:a-dgal-HEX-1:5
6s:phosphate
7b:o-xgro-TRI-0:0|1:aldi
LIN
1:1o(2+1)2n
2:1o(1+1)3n
3:1o(3+1)4d
4:4o(3+1)5d
5:5o(6+1)6n
6:6n(1+1)7o
NON
NON1
Parent:1
Linkage:o(2+1)n
HistoricalEntity:superclass: lipid residue
NON2
Parent:1
Linkage:o(1+1)n
HistoricalEntity:superclass: lipid residue