RES
1n:n1
2b:a-dgal-HEX-1:5|6:a
3n:n2
LIN
1:1n(1+1)2d
2:1n(2+1)3n
NON
NON1
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2C@@H](N)[1CH2]O
Description:SR-sphinganine
NON2
Parent:1
Linkage:n(2+1)n
SmallMolecule:SMILES [15CH3][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:pentadecanoic acid