RES
1b:x-dman-OCT-2:5|1:a|2:keto|3:d
2b:a-lgro-dman-HEP-1:5
3b:a-lgro-dman-HEP-1:5
4b:a-lgro-dman-HEP-1:5
5b:a-lgro-dman-HEP-1:5
6b:b-dglc-HEX-1:5
7b:a-dglc-HEX-x:x
8s:amino
LIN
1:1o(6+1)2d
2:2o(6+1)3d
3:2o(2+1)4d
4:2o(3+1)5d
5:4o(4+1)6d
6:5o(7+1)7d
7:7d(2+1)8n