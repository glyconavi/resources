RES
1n:n1
2s:pyrophosphate
3b:b-dman-HEX-1:5
LIN
1:1n(5+1)2n
2:2n(1+1)3o
NON
NON1
SmallMolecule:SMILES [5CH2](O)[4C@@H](O1)[3C@@H](O)[2C@@H](O)[1C@H](N2[10CH]=N[8C]3=[7C]2N=[6CH]N=[9C](N)3)1
Description:adenosine