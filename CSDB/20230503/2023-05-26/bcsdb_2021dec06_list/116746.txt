RES
1r:r1
REP
REP1:8o(3+2)2d=-1--1
RES
2b:a-lgro-dgal-NON-2:6|1:a|2:keto|3:d|9:d
3s:n-acetyl
4s:amino
5n:n1
6b:a-dglc-HEX-1:5
7s:n-acetyl
8b:a-lgal-HEX-1:5|6:d
9s:amino
10n:n2
LIN
1:2d(7+1)3n
2:2d(5+1)4n
3:2o(5+1)5n
4:2o(8+1)6d
5:6d(2+1)7n
6:6o(3+1)8d
7:8d(2+1)9n
8:8o(2+1)10n
NON
NON1
Parent:2
Linkage:o(5+1)n
SmallMolecule:SMILES [4CH3][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxybutanoic acid
NON2
Parent:8
Linkage:o(2+1)n
SmallMolecule:SMILES [2CH3][1C](=N)O
Description:acetimidic acid