RES
1b:o-dman-HEX-0:0|1:aldi
2s:anhydro
3b:b-dgal-HEX-1:5|6:d
4s:n-acetyl
5b:b-dman-HEX-1:5|6:a
6s:n-acetyl
7s:amino
8n:n1
9b:b-dman-HEX-1:5|6:a
10s:n-acetyl
11s:amino
12n:n2
13b:a-dgal-HEX-1:5|6:d
14s:n-acetyl
15b:b-dman-HEX-1:5|6:a
16s:n-acetyl
17s:amino
18n:n3
19b:b-dman-HEX-1:5|6:a
20s:n-acetyl
21s:amino
22n:n4
LIN
1:1d(2+1)2n
2:1o(5+1)2n
3:1o(6+1)3d
4:3d(2+1)4n
5:3o(3+1)5d
6:5d(2+1)6n
7:5d(3+1)7n
8:5o(3+1)8n
9:5o(4+1)9d
10:9d(2+1)10n
11:9d(3+1)11n
12:9o(3+1)12n
13:9o(4+1)13d
14:13d(2+1)14n
15:13o(3+1)15d
16:15d(2+1)16n
17:15d(3+1)17n
18:15o(3+1)18n
19:15o(4+1)19d
20:19d(2+1)20n
21:19d(3+1)21n
22:19o(3+1)22n
NON
NON1
Parent:5
Linkage:o(3+1)n
SmallMolecule:SMILES [2CH3][1C](=N)O
Description:acetimidic acid
NON2
Parent:9
Linkage:o(3+1)n
SmallMolecule:SMILES [2CH3][1C](=N)O
Description:acetimidic acid
NON3
Parent:15
Linkage:o(3+1)n
SmallMolecule:SMILES [2CH3][1C](=N)O
Description:acetimidic acid
NON4
Parent:19
Linkage:o(3+1)n
SmallMolecule:SMILES [2CH3][1C](=N)O
Description:acetimidic acid