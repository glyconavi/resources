RES
1b:a-dman-HEX-1:5
2b:a-dgal-HEX-1:5
3b:a-dxyl-HEX-1:5|3:d|6:d
LIN
1:1o(2+1)2d
2:1o(3+1)3d