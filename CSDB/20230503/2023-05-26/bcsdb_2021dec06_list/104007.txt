RES
1r:r1
REP
REP1:4o(3+1)2d=-1--1
RES
2b:a-dgal-HEX-1:5
3s:pyruvate
4b:b-dglc-HEX-1:5
5s:succinate
LIN
1:2o(6+2)3n
2:2o(4+2)3n
3:2o(3+1)4d
4:4o(-1+1)5n