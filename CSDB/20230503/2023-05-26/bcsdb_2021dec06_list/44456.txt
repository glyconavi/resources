RES
1b:a-dgal-HEX-1:4|6:d
2n:n1
LIN
1:1o(1+4)2n
NON
NON1
Parent:1
Linkage:o(1+4)n
SmallMolecule:SMILES C=Cc1cc(OC)c2c(c1)c(=O)oc4c2cc(OC)c3c(O)cc{4}c(O)c34