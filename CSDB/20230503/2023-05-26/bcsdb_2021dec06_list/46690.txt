RES
1n:n1
2b:a-dglc-HEX-1:5
3s:methyl
LIN
1:1n(53+1)2d
2:1n(54+1)3n
NON
NON1
SmallMolecule:SMILES O=C1C[C@H](OC2=C1{5}C(O)=C{7}C(O)=C2)C3=C{53}C(O)={54}C(O)C=C3
Description:eriodictyol