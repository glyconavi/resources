RES
1b:a-dglc-HEX-1:5
2n:n1
3n:n2
4b:a-dglc-HEX-1:5
5n:n3
6b:b-dglc-HEX-1:5
7b:b-dgal-HEX-1:5
8b:a-lman-HEX-1:5|6:d
9s:methyl
10b:b-lxyl-PEN-1:5
11b:b-lxyl-PEN-1:5
12b:b-lxyl-PEN-1:5
13b:b-lxyl-PEN-1:5
14b:b-lxyl-PEN-1:5
15b:b-lxyl-PEN-1:5
16n:n4
LIN
1:1o(4+1)2n
2:1o(6+1)3n
3:1o(1+1)4d
4:4o(2+1)5n
5:4o(4+1)6d
6:6o(3+1)7d
7:7o(3+1)8d
8:8o(3+1)9n
9:8o(4+1)10d
10:10o(4+1)11d
11:11o(4+1)12d
12:12o(4+1)13d
13:13o(4+1)14d
14:14o(4+1)15d
15:15o(3+1)16n
NON
NON1
Parent:1
Linkage:o(4+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11C@H](O)[10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH]([16CH3])[3CH2][2CH]([15CH3])[1C](=O)O
Description:2,4-dimethyl-tetradecanoic acid
NON2
Parent:1
Linkage:o(6+1)n
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:palmitic acid
NON3
Parent:4
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11C@H](O)[10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH]([16CH3])[3CH2][2CH]([15CH3])[1C](=O)O
Description:2,4-dimethyl-tetradecanoic acid
NON4
Parent:15
Linkage:o(3+1)n
SmallMolecule:SMILES O{1}[C@H]1O[C@H](C)[C@](C[C@H]1O)(C(OC)CC(OC)C(O)C(O)C(O)CO)O
Description:3,6-dideoxy-4-C-(1,3-dimethoxy-4,5,6,7-tetrahydroxy-heptyl)-xylHex