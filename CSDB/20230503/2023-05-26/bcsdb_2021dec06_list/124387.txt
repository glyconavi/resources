RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+7)2n
NON
NON1
Parent:1
Linkage:o(1+7)n
SmallMolecule:SMILES [9CH]1=[8CH][7C](O)=[6CH][5CH]=[4C]1[3CH2][2CH](N)[1C](=O)O
Description:tyrosine