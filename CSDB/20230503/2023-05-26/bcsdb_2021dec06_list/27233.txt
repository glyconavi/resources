RES
1b:a-dgal-HEX-1:5
2s:amino
3s:phosphate
4b:a-dglc-HEX-1:5
5a:a1
6s:amino
7n:n1
8b:b-dglc-HEX-1:5
9s:amino
10n:n2
11a:a2
LIN
1:1d(2+1)2n
2:1o(1+1)3n
3:3n(1+1)4o
4:4o(3+1)5n
5:4d(2+1)6n
6:4o(2+1)7n
7:4o(6+1)8d
8:8d(2+1)9n
9:8o(2+1)10n
10:10n(3+1)11n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:12
RES
12n:n3
ALTSUBGRAPH2
LEAD-IN RES:13
RES
13n:n4
ALT2
ALTSUBGRAPH1
LEAD-IN RES:14
RES
14n:n5
ALTSUBGRAPH2
LEAD-IN RES:15
RES
15n:n6
NON
NON1
Parent:4
Linkage:o(2+1)n
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-stearic acid
NON2
Parent:8
Linkage:o(2+1)n
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-stearic acid
NON3
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-hexadecanoic acid
NON4
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-stearic acid
NON5
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:palmitic acid
NON6
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:myristic acid