RES
1r:r1
REP
REP1:8o(4+1)2d=-1--1
RES
2b:a-dglc-HEX-1:5|6:d
3s:n-acetyl
4b:a-lgal-HEX-1:5|6:d
5s:acetyl
6s:amino
7n:n1
8b:b-dglc-HEX-1:5|6:a
9s:amino
10s:n-acetyl
11n:n2
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4o(3+1)5n
4:4d(2+1)6n
5:4o(2+1)7n
6:4o(4+1)8d
7:8d(3+1)9n
8:8d(2+1)10n
9:8o(3+1)11n
NON
NON1
Parent:4
Linkage:o(2+1)n
SmallMolecule:SMILES [2CH3][1C](=N)O
Description:acetimidic acid
NON2
Parent:8
Linkage:o(3+1)n
SmallMolecule:SMILES [4CH3][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxybutanoic acid