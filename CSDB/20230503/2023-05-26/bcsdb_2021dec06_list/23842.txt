RES
1r:r1
REP
REP1:7o(4+1)2d=-1--1
RES
2b:a-dglc-HEX-1:5|6:d
3s:n-acetyl
4b:a-lgal-HEX-1:5|6:a
5s:amino
6n:n1
7b:b-dglc-HEX-1:5|6:a
8a:a1
9s:n-acetyl
10s:amino
11s:amino
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4d(2+1)5n
4:4o(2+1)6n
5:4o(4+1)7d
6:7o(3+4)8n
7:7d(2+1)9n
8:7d(6+1)10n
9:7d(3+1)11n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:12
RES
12n:n2
13s:acetyl
LIN
1:12n(2+1)13n
ALTSUBGRAPH2
LEAD-IN RES:14
RES
14n:n3
NON
NON1
Parent:4
Linkage:o(2+1)n
SmallMolecule:SMILES [2CH3][1C](=N)O
Description:acetimidic acid
NON2
SmallMolecule:SMILES [4C](=O)(O)[3CH2][2C@@H](O)[1C](=O)(O)
Description:(R)-malic acid
NON3
SmallMolecule:SMILES [4C](=O)(O)[3CH2][2C@@H](O)[1C](=O)(O)
Description:(R)-malic acid