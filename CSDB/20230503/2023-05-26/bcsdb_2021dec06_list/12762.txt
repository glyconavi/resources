RES
1r:r1
REP
REP1:7o(4+1)2d=-1--1
RES
2b:a-dglc-HEX-1:5|6:d
3s:n-acetyl
4b:a-lgal-HEX-1:5|6:a
5s:amino
6n:n1
7b:b-dglc-HEX-1:5|6:a
8s:amino
9s:amino
10s:n-acetyl
11n:n2
12s:acetyl
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4d(2+1)5n
4:4o(2+1)6n
5:4o(4+1)7d
6:7d(3+1)8n
7:7d(6+1)9n
8:7d(2+1)10n
9:7o(3+4)11n
10:11n(2+1)12n
NON
NON1
Parent:4
Linkage:o(2+1)n
SmallMolecule:SMILES [2CH3][1C](=N)O
Description:acetimidic acid
NON2
Parent:7
Linkage:o(3+4)n
SmallMolecule:SMILES [4C](=O)(O)[3CH2][2C@@H](O)[1C](=O)(O)
Description:(R)-malic acid