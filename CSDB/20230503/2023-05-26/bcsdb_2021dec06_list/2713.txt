RES
1b:x-dglc-HEX-1:5|6:d
2s:amino
3b:a-dgal-HEX-1:5|6:a
4s:amino
5b:a-dgal-HEX-1:5|6:a
6s:amino
LIN
1:1d(2+1)2n
2:1o(3+1)3d
3:3d(2+1)4n
4:3o(4+1)5d
5:5d(2+1)6n