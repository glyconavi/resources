RES
1r:r1
REP
REP1:10o(6+2)2d=-1--1
RES
2b:b-dgro-dgal-NON-2:6|1:a|2:keto|3:d|9:d
3a:a1
4s:amino
5s:n-acetyl
6b:a-dglc-HEX-1:5
7s:n-acetyl
8b:a-lgal-HEX-1:5|6:d
9s:n-acetyl
10b:a-dglc-HEX-1:5
11s:n-acetyl
LIN
1:2o(5+1)3n
2:2d(5+1)4n
3:2d(7+1)5n
4:2o(4+1)6d
5:6d(2+1)7n
6:6o(3+1)8d
7:8d(2+1)9n
8:8o(3+1)10d
9:10d(2+1)11n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:12
RES
12n:n1
ALTSUBGRAPH2
LEAD-IN RES:13
RES
13s:acetyl
NON
NON1
SmallMolecule:SMILES [4CH3][3C@H](O)[2CH2][1C](=O)O
Description:(S)-3-hydroxybutanoic acid