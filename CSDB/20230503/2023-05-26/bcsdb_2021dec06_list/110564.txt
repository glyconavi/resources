RES
1r:r1
REP
REP1:4o(2+1)2d=-1--1
RES
2b:a-dman-HEX-1:5|6:d
3b:a-dman-HEX-1:5|6:d
4b:a-dman-HEX-1:5|6:d
5b:b-dxyl-PEN-1:5
6b:b-dxyl-PEN-1:5
LIN
1:2o(3+1)3d
2:3o(3+1)4d
3:3o(4+1)5d
4:3o(2+1)6d