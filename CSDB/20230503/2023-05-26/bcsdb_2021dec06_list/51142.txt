RES
1b:a-llyx-HEX-1:5|2:d|6:d
2s:n-dimethyl
3n:n1
4b:a-ltre-HEX-1:5|2:d|3:d|6:d
5b:a-ltre-HEX-1:5|2:d|3:d|6:d
LIN
1:1d(3+1)2n
2:1o(1+4)3n
3:1o(4+1)4d
4:4o(4+1)5d
NON
NON1
Parent:1
Linkage:o(1+4)n
SmallMolecule:SMILES O{4}[C@H]1C[C@@](C)(O)[C@H](C(OC)=O)C2=CC3=C(C(C4=C(C=CC=C4O)C3=O)=O)C(O)=C12
Description:auramycinone