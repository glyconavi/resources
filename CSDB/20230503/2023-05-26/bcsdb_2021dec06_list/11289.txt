RES
1b:b-dgal-HEX-1:5
2n:n1
3b:a-lman-HEX-1:5|6:d
4b:a-lman-HEX-1:5|6:d
LIN
1:1o(1+1)2n
2:1o(2+1)3d
3:3o(2+1)4d
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES NCC{1}CO
Description:3-aminopropanol