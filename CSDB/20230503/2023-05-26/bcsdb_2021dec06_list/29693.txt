RES
1r:r1
REP
REP1:6o(4+1)2d=-1--1
RES
2b:b-dgal-HEX-1:5
3s:n-acetyl
4b:b-dglc-HEX-1:5|6:a
5b:a-dglc-HEX-1:5
6b:a-dgal-HEX-1:5
7s:n-acetyl
8b:b-dglc-HEX-1:5|6:d
9s:amino
10n:n1
11s:(s)-carboxyethyl
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4o(4+1)5d
4:5o(6+1)6d
5:6d(2+1)7n
6:6o(6+1)8d
7:8d(4+1)9n
8:8o(4+1)10n
9:10n(2+2)11n
NON
NON1
Parent:8
Linkage:o(4+1)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine