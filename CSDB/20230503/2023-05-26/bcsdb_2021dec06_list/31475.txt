RES
1n:n1
2s:pyrophosphate
3b:a-dglc-HEX-1:5
4s:amino
5n:n2
6n:n3
7b:b-dglc-HEX-1:5
8s:phospho-ethanolamine
9s:amino
10n:n4
11n:n5
12n:n6
LIN
1:1n(1+1)2n
2:2n(1+1)3o
3:3d(2+1)4n
4:3o(2+1)5n
5:3o(3+1)6n
6:3o(6+1)7d
7:7o(4+1)8n
8:7d(2+1)9n
9:7o(2+1)10n
10:7o(3+1)11n
11:11n(3+1)12n
NON
NON1
SmallMolecule:SMILES [2CH2](N)[1CH2](O)
Description:ethanolamine
NON2
Parent:3
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C](=O)[2CH2][1C](=O)O
Description:3-oxo-tetradecanoic acid
NON3
Parent:3
Linkage:o(3+1)n
SmallMolecule:SMILES [10CH3][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-decanoic acid
NON4
Parent:7
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C](=O)[2CH2][1C](=O)O
Description:3-oxo-tetradecanoic acid
NON5
Parent:7
Linkage:o(3+1)n
SmallMolecule:SMILES [10CH3][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-decanoic acid
NON6
Parent:11
Linkage:n(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH]=[5CH][4CH2][3CH2][2CH2][1C](=O)O
Description:dodec-5-enoic acid