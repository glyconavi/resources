RES
1r:r1
REP
REP1:6o(6+1)2n=-1--1
RES
2s:phosphate
3b:a-dglc-HEX-1:5
4s:n-acetyl
5n:n1
6b:a-dglc-HEX-1:5
7s:n-acetyl
LIN
1:2n(1+6)3o
2:3d(2+1)4n
3:3o(1+2)5n
4:3o(3+1)6d
5:6d(2+1)7n
NON
NON1
Parent:3
Linkage:o(1+2)n
SmallMolecule:SMILES [3CH2](O)[2C@@H](O)[1C](=O)O
Description:(D)-glyceric acid