RES
1r:r1
REP
REP1:7o(3+1)2d=-1--1
RES
2b:a-lman-HEX-1:5|6:d
3b:a-lman-HEX-1:5|6:d
4b:a-dgal-HEX-1:5|6:d
5s:n-acetyl
6b:a-lman-HEX-1:5|6:d
7b:a-lman-HEX-1:5|6:d
LIN
1:2o(2+1)3d
2:2o(3+1)4d
3:4d(3+1)5n
4:3o(2+1)6d
5:6o(3+1)7d