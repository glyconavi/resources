RES
1b:x-dglc-HEX-1:5
2s:n-acetyl
3b:a-dgal-HEX-1:5
4b:b-lgal-HEX-1:5|6:d
LIN
1:1d(2+1)2n
2:1o(3+1)3d
3:3o(2+1)4d