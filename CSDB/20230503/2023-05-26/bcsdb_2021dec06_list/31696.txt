RES
1r:r1
REP
REP1:7o(4+1)2d=-1--1
RES
2b:b-dgal-HEX-1:5
3b:b-lgro-lman-NON-2:6|1:a|2:keto|3:d|9:d
4s:n-acetyl
5s:amino
6n:n1
7b:b-dglc-HEX-1:5|6:d
8s:n-acetyl
LIN
1:2o(4+2)3d
2:3d(7+1)4n
3:3d(5+1)5n
4:3o(5+1)6n
5:3o(4+1)7d
6:7d(2+1)8n
NON
NON1
Parent:3
Linkage:o(5+1)n
SmallMolecule:SMILES [2CH3][1C](=N)O
Description:acetimidic acid