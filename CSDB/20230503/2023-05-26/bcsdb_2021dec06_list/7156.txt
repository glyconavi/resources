RES
1r:r1
REP
REP1:10o(2+1)2d=-1--1
RES
2b:b-dgal-HEX-1:5
3s:n-acetyl
4b:a-dgal-HEX-1:5
5s:n-acetyl
6b:a-dglc-HEX-1:5
7s:amino
8n:n1
9s:phosphate
10b:a-dglc-HEX-1:5
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4d(2+1)5n
4:4o(4+1)6d
5:6d(2+1)7n
6:6o(2+1)8n
7:6o(6+1)9n
8:9n(1+1)10o
NON
NON1
Parent:6
Linkage:o(2+1)n
SmallMolecule:SMILES [4CH3][3C@H](O)[2CH2][1C](=O)O
Description:(S)-3-hydroxybutanoic acid