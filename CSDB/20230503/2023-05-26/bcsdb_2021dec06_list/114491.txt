RES
1b:x-dman-HEX-x:x
2b:b-dglc-HEX-1:5
3b:a-ltre-HEX-1:5|4,5:en|6:a
LIN
1:1o(3+1)2d
2:2o(4+1)3d