RES
1b:a-dgal-HEX-1:5|6:a
2b:a-dglc-HEX-1:5
3s:amino
4s:amino
5n:n1
6n:n2
7b:b-dglc-HEX-1:5
8b:a-dman-HEX-1:5
9s:amino
10s:amino
11n:n3
12n:n4
13n:n5
14n:n6
15s:acetyl
LIN
1:1o(1+1)2d
2:2d(3+1)3n
3:2d(2+1)4n
4:2o(2+1)5n
5:2o(3+1)6n
6:2o(6+1)7d
7:7o(4+1)8d
8:7d(2+1)9n
9:7d(3+1)10n
10:7o(2+1)11n
11:7o(3+1)12n
12:12n(3+1)13n
13:11n(3+1)14n
14:14n(25+1)15n
NON
NON1
Parent:2
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON2
Parent:2
Linkage:o(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON3
Parent:7
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON4
Parent:7
Linkage:o(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON5
Parent:12
Linkage:n(3+1)n
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:palmitic acid
NON6
Parent:11
Linkage:n(3+1)n
HistoricalEntity:superclass: lipid residue