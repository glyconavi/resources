RES
1b:b-dglc-HEX-1:5
2s:(r)-carboxyethyl
3s:n-acetyl
4n:n1
5b:b-dglc-HEX-1:5
6s:n-acetyl
7n:n2
8b:b-dglc-HEX-1:5
9s:(r)-carboxyethyl
10s:n-acetyl
11b:b-dglc-HEX-1:5
12n:n3
13n:n4
14s:n-acetyl
15n:n5
16n:n6
17n:n7
18n:n8
LIN
1:1o(3+1)2n
2:1d(2+1)3n
3:1o(8+2)4n
4:1o(4+1)5d
5:5d(2+1)6n
6:4n(1+2)7n
7:5o(4+1)8d
8:8o(3+1)9n
9:8d(2+1)10n
10:8o(4+1)11d
11:7n(5+2)12n
12:8o(8+2)13n
13:11d(2+1)14n
14:12n(1+2)15n
15:13n(1+2)16n
16:16n(5+2)17n
17:17n(1+2)18n
NON
NON1
Parent:1
Linkage:o(8+2)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine
NON2
Parent:4
Linkage:n(1+2)n
SmallMolecule:SMILES [5C](=O)(O)[4CH2][3CH2][2C@@H](N)[1C](=O)O
Description:(D)-glutamic acid
NON3
Parent:7
Linkage:n(5+2)n
SmallMolecule:SMILES [7C](=O)(O)[6C@H](N)[5CH2][4CH2][3CH2][2C@H](N)[1C](=O)O
Description:meso-diaminopimelic acid
NON4
Parent:8
Linkage:o(8+2)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine
NON5
Parent:12
Linkage:n(1+2)n
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine
NON6
Parent:13
Linkage:n(1+2)n
SmallMolecule:SMILES [5C](=O)(O)[4CH2][3CH2][2C@@H](N)[1C](=O)O
Description:(D)-glutamic acid
NON7
Parent:16
Linkage:n(5+2)n
SmallMolecule:SMILES [7C](=O)(O)[6C@H](N)[5CH2][4CH2][3CH2][2C@H](N)[1C](=O)O
Description:meso-diaminopimelic acid
NON8
Parent:17
Linkage:n(1+2)n
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine