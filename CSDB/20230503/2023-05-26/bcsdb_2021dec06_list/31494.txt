RES
1b:a-dglc-HEX-1:5
2s:phosphate
3s:amino
4n:n1
5b:b-dglc-HEX-1:5
6s:amino
7n:n2
8n:n3
LIN
1:1o(1+1)2n
2:1d(2+1)3n
3:1o(2+1)4n
4:1o(6+1)5d
5:5d(2+1)6n
6:5o(2+1)7n
7:7n(3+1)8n
NON
NON1
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [16CH3][15CH]([17CH3])[14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-iso-heptadecanoic acid
NON2
Parent:5
Linkage:o(2+1)n
SmallMolecule:SMILES [16CH3][15CH]([17CH3])[14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-iso-heptadecanoic acid
NON3
Parent:7
Linkage:n(3+1)n
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:palmitic acid