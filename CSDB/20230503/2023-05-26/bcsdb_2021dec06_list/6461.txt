RES
1b:o-xgro-TRI-0:0|1:aldi
2n:n1
3n:n2
4b:b-dgal-HEX-1:5
5r:r1
6b:b-dgal-HEX-1:4
7r:r2
8s:phosphate
9b:o-xgro-TRI-0:0|1:aldi
10n:n3
LIN
1:1o(2+1)2n
2:1o(1+1)3n
3:1o(3+1)4d
4:4o(6+1)5n
5:5n(6+1)6d
6:6o(5+1)7n
7:6o(6+1)8n
8:8n(1+1)9o
9:9o(3+1)10n
REP
REP1:11o(6+1)11d=8-12
RES
11b:b-dglc-HEX-1:5
REP2:12o(5+1)12d=10-17
RES
12b:b-dgal-HEX-1:4
13s:phosphate
14b:o-xgro-TRI-0:0|1:aldi
15n:n4
LIN
1:12o(6+1)13n
2:13n(1+1)14o
3:14o(3+1)15n
NON
NON1
Parent:1
Linkage:o(2+1)n
HistoricalEntity:superclass: lipid residue
NON2
Parent:1
Linkage:o(1+1)n
HistoricalEntity:superclass: lipid residue
NON3
Parent:9
Linkage:o(3+1)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine
NON4
Parent:14
Linkage:o(3+1)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine