RES
1b:b-dglc-HEX-1:5
2s:n-acetyl
3b:a-lgal-HEX-1:5|6:d
4b:b-dglc-HEX-1:5
5s:n-acetyl
6s:methyl
7b:b-dglc-HEX-1:5
8s:n-acetyl
9b:b-dglc-HEX-1:5
10s:n-acetyl
11b:b-dglc-HEX-1:5
12a:a1
13s:n-methyl
14n:n1
LIN
1:1d(2+1)2n
2:1o(6+1)3d
3:1o(4+1)4d
4:4d(2+1)5n
5:3o(2+1)6n
6:4o(4+1)7d
7:7d(2+1)8n
8:7o(4+1)9d
9:9d(2+1)10n
10:9o(4+1)11d
11:11o(-1+-1)12n
12:11d(2+1)13n
13:11o(2+1)14n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:15
RES
15n:n2
ALTSUBGRAPH2
LEAD-IN RES:16
RES
16n:n3
ALTSUBGRAPH3
LEAD-IN RES:17
RES
17n:n4
NON
NON1
Parent:11
Linkage:o(2+1)n
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2]/[10CH]=[9CH]/[8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:trans-9-octadecenoic acid
NON2
SmallMolecule:SMILES [1C](=O)(O)N
Description:carbamic acid
NON3
SmallMolecule:SMILES [1C](=O)(O)N
Description:carbamic acid
NON4
SmallMolecule:SMILES [1C](=O)(O)N
Description:carbamic acid