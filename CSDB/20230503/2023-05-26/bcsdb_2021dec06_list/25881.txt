RES
1b:o-xgro-TRI-0:0|1:aldi
2b:b-dgal-HEX-1:5|6:d
3s:amino
4n:n1
LIN
1:1o(1+1)2d
2:2d(3+1)3n
3:2o(3+1)4n
NON
NON1
Parent:2
Linkage:o(3+1)n
SmallMolecule:SMILES [4CH3][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxybutanoic acid