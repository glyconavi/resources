RES
1b:b-dall-HEX-1:4|6:a
2s:amino
3s:amino
4n:n1
5n:n2
LIN
1:1d(5+1)2n
2:1d(1+1)3n
3:1o(5+1)4n
4:1o(1+1)5n
NON
NON1
Parent:1
Linkage:o(5+1)n
SmallMolecule:SMILES CC(C(N)C{1}(N)=O)C(O)C1=CN=CC=C1
NON2
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES O=C(NC1=O)C=C{1}N1