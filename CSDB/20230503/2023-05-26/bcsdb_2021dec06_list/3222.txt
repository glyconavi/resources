RES
1b:a-dman-HEX-1:5|6:d
2s:methyl
3s:amino
4n:n1
5b:a-dman-HEX-1:5|6:d
6s:amino
7n:n2
8b:a-dman-HEX-1:5|6:d
9s:methyl
10s:amino
11n:n3
LIN
1:1o(1+1)2n
2:1d(4+1)3n
3:1o(4+1)4n
4:1o(2+1)5d
5:5d(4+1)6n
6:5o(4+1)7n
7:5o(2+1)8d
8:8o(2+1)9n
9:8d(4+1)10n
10:8o(4+1)11n
NON
NON1
Parent:1
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH2](O)[3CH2][2C@H](O)[1C](=O)O
Description:(S)-2,4-dihydroxybutanoic acid
NON2
Parent:5
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH2](O)[3CH2][2C@H](O)[1C](=O)O
Description:(S)-2,4-dihydroxybutanoic acid
NON3
Parent:8
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH2](O)[3CH2][2C@H](O)[1C](=O)O
Description:(S)-2,4-dihydroxybutanoic acid