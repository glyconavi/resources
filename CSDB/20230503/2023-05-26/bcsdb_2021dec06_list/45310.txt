RES
1b:a-lman-HEX-1:5|6:d
2s:methyl
3n:n1
4b:a-lman-HEX-1:5|6:d
5b:a-lgal-HEX-1:5|6:d
LIN
1:1o(2+1)2n
2:1o(1+1)3n
3:1o(3+1)4d
4:4o(3+1)5d
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES CCCCCCCCCCCCCCCCCCCCC(C)CC(C)CC(C)CC(C)CC(C)C(=O)OC(CCCCC(C)C(C)OC)CC(CCCCCCCCCCCCCCCCc1ccc{1}(O)cc1)OC(=O)C(C)CC(C)CC(C)CC(C)CC(C)CCCCCCCCCCCCCCCCCCC
Description:phenolphthiocerol dimycocerosate