RES
1r:r1
REP
REP1:10o(4+1)2d=-1--1
RES
2b:b-dgal-HEX-1:5|6:d
3s:n-acetyl
4s:amino
5b:b-dglc-HEX-1:5|6:a
6s:n-formyl
7s:amino
8s:amino
9n:n1
10b:b-dgro-lglc-NON-2:6|1:a|2:keto|3:d|9:d
11s:amino
12s:n-acetyl
13n:n2
LIN
1:2d(2+1)3n
2:2d(4+1)4n
3:2o(3+1)5d
4:5d(3+1)6n
5:5d(6+1)7n
6:5d(2+1)8n
7:5o(2+1)9n
8:5o(4+2)10d
9:10d(5+1)11n
10:10d(7+1)12n
11:10o(5+1)13n
NON
NON1
Parent:5
Linkage:o(2+1)n
SmallMolecule:SMILES [3CH3][2CH2][1C](=O)O
Description:propanoic acid
NON2
Parent:10
Linkage:o(5+1)n
SmallMolecule:SMILES [2CH3][1C](=N)O
Description:acetimidic acid