RES
1r:r1
REP
REP1:9o(3+1)2d=-1--1
RES
2b:a-dglc-HEX-1:5
3s:n-acetyl
4s:acetyl
5b:a-lglc-HEX-1:5|6:d
6s:n-acetyl
7b:a-dglc-HEX-1:5
8s:n-acetyl
9b:b-dglc-HEX-1:5|6:d
10s:amino
11n:n1
12n:n2
LIN
1:2d(2+1)3n
2:2o(6+1)4n
3:2o(3+1)5d
4:5d(2+1)6n
5:5o(3+1)7d
6:7d(2+1)8n
7:7o(6+1)9d
8:9d(4+1)10n
9:9o(4+1)11n
10:11n(2+1)12n
NON
NON1
Parent:9
Linkage:o(4+1)n
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine
NON2
Parent:11
Linkage:n(2+1)n
SmallMolecule:SMILES [4CH3][3C@H](O)[2CH2][1C](=O)O
Description:(S)-3-hydroxybutanoic acid