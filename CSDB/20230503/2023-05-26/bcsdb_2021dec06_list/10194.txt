RES
1r:r1
REP
REP1:20o(2+1)2d=-1--1
RES
2b:a-dman-HEX-1:5
3b:a-dman-HEX-1:5
4b:a-dman-HEX-1:5
5b:a-dman-HEX-1:5
6b:a-dman-HEX-1:5
7b:a-dman-HEX-1:5
8b:a-dman-HEX-1:5
9b:a-dman-HEX-1:5
10b:a-dman-HEX-1:5
11b:a-dman-HEX-1:5
12b:a-dman-HEX-1:5
13b:a-dman-HEX-1:5
14b:a-dman-HEX-1:5
15b:a-dman-HEX-1:5
16b:a-dman-HEX-1:5
17b:a-dman-HEX-1:5
18b:a-dman-HEX-1:5
19b:a-dman-HEX-1:5
20b:a-dman-HEX-1:5
21b:a-dman-HEX-1:5
22b:a-dman-HEX-1:5
LIN
1:2o(2+1)3d
2:3o(2+1)4d
3:4o(6+1)5d
4:4o(2+1)6d
5:5o(3+1)7d
6:6o(6+1)8d
7:6o(2+1)9d
8:9o(6+1)10d
9:8o(3+1)11d
10:9o(2+1)12d
11:12o(2+1)13d
12:13o(2+1)14d
13:14o(6+1)15d
14:14o(2+1)16d
15:15o(3+1)17d
16:16o(6+1)18d
17:16o(2+1)19d
18:19o(2+1)20d
19:18o(3+1)21d
20:19o(6+1)22d