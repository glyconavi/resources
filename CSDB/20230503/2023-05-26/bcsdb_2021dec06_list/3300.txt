RES
1n:n1
2s:pyrophosphate
3b:a-dglc-HEX-1:5
4s:amino
5n:n2
6n:n3
7b:b-dglc-HEX-1:5
8s:amino
9n:n4
10s:phosphate
11n:n5
12n:n6
13n:n7
14n:n8
15b:b-lara-PEN-1:5
16s:amino
LIN
1:1n(1+1)2n
2:2n(1+1)3o
3:3d(2+1)4n
4:3o(3+1)5n
5:3o(2+1)6n
6:3o(6+1)7d
7:7d(2+1)8n
8:6n(3+1)9n
9:7o(4+1)10n
10:7o(3+1)11n
11:7o(2+1)12n
12:11n(3+1)13n
13:12n(3+1)14n
14:10n(1+1)15o
15:15d(4+1)16n
NON
NON1
SmallMolecule:SMILES [2CH2](N)[1CH2](O)
Description:ethanolamine
NON2
Parent:3
Linkage:o(3+1)n
SmallMolecule:SMILES [10CH3][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-decanoic acid
NON3
Parent:3
Linkage:o(2+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-dodecanoic acid
NON4
Parent:6
Linkage:n(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH](O)[1C](=O)O
Description:2-hydroxy-dodecanoic acid
NON5
Parent:7
Linkage:o(3+1)n
SmallMolecule:SMILES [10CH3][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-decanoic acid
NON6
Parent:7
Linkage:o(2+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-dodecanoic acid
NON7
Parent:11
Linkage:n(3+1)n
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:palmitic acid
NON8
Parent:12
Linkage:n(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH](O)[1C](=O)O
Description:2-hydroxy-dodecanoic acid