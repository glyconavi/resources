RES
1b:a-dglc-HEX-1:5
2s:phosphate
3s:amino
4n:n1
5b:b-dglc-HEX-1:5
6s:phosphate
7s:amino
8n:n2
9n:n3
10n:n4
11n:n5
LIN
1:1o(1+1)2n
2:1d(2+1)3n
3:1o(2+1)4n
4:1o(6+1)5d
5:5o(4+1)6n
6:5d(2+1)7n
7:5o(3+1)8n
8:4n(3+1)9n
9:5o(2+1)10n
10:10n(3+1)11n
NON
NON1
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-dodecanoic acid
NON2
Parent:5
Linkage:o(3+1)n
SmallMolecule:SMILES [10CH3][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-decanoic acid
NON3
Parent:4
Linkage:n(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH](O)[1C](=O)O
Description:2-hydroxy-dodecanoic acid
NON4
Parent:5
Linkage:o(2+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-dodecanoic acid
NON5
Parent:10
Linkage:n(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:lauric acid