RES
1b:o-dglc-HEX-0:0|1:aldi
2s:n-acetyl
3b:a-dgal-HEX-1:5|6:a
4n:n1
5b:b-dglc-HEX-1:5|6:a
6n:n2
7s:acetyl
LIN
1:1d(2+1)2n
2:1o(3+1)3d
3:3o(6+2)4n
4:3o(3+1)5d
5:5o(6+2)6n
6:6n(6+1)7n
NON
NON1
Parent:3
Linkage:o(6+2)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine
NON2
Parent:5
Linkage:o(6+2)n
SmallMolecule:SMILES [6CH2](N)[5CH2][4CH2][3CH2][2C@H](N)[1C](=O)O
Description:(L)-lysine