RES
1b:b-dglc-HEX-1:5|6:a
2n:n1
LIN
1:1o(1+3)2n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O{54}C1=CC=C(/C=C/C2=C{3}C(O)=C{5}C(O)=C2)C=C1
Description:resveratrol