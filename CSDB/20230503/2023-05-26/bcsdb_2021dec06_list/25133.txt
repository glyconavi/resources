RES
1r:r1
REP
REP1:5o(4+1)2d=-1--1
RES
2b:b-lman-HEX-1:5|6:d
3s:acetyl
4b:a-dglc-HEX-1:5
5b:b-dglc-HEX-1:5
6b:b-dglc-HEX-1:5
7b:a-lman-HEX-1:5|6:d
8s:phosphate
9b:o-xgro-TRI-0:0|1:aldi
LIN
1:2o(2+1)3n
2:2o(3+1)4d
3:4o(4+1)5d
4:4o(6+1)6d
5:4o(3+1)7d
6:7o(4+1)8n
7:8n(1+3)9o