RES
1r:r1
REP
REP1:2o(9+2)2d=-1--1
RES
2b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d
3a:a1
4s:n-acetyl
LIN
1:2o(-1+-1)3n
2:2d(5+1)4n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:5
RES
5s:acetyl
ALTSUBGRAPH2
LEAD-IN RES:6
RES
6s:acetyl