RES
1n:n1
2b:b-drib-PEN-1:4
3b:a-dglc-HEX-1:5
4s:amino
5s:amino
LIN
1:1n(5+1)2d
2:1n(4+1)3d
3:3d(6+1)4n
4:3d(2+1)5n
NON
NON1
SmallMolecule:SMILES [6CH](O)1[5CH](O)[4CH](O)[3CH](N)[2CH2][1CH](O)1
Description:2,3-dideoxy-3-amino-myo-inositol