RES
1r:r1
REP
REP1:5o(4+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3s:(r)-carboxyethyl
4s:n-acetyl
5b:b-dglc-HEX-1:5
6n:n1
7s:n-acetyl
8n:n2
9n:n3
10n:n4
11n:n5
LIN
1:2o(3+1)3n
2:2d(2+1)4n
3:2o(4+1)5d
4:2o(8+2)6n
5:5d(2+1)7n
6:6n(1+2)8n
7:8n(1+2)9n
8:9n(1+2)10n
9:10n(1+2)11n
NON
NON1
Parent:2
Linkage:o(8+2)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine
NON2
Parent:6
Linkage:n(1+2)n
SmallMolecule:SMILES [5C](=O)(N)[4CH2][3CH2][2C@@H](N)[1C](=O)O
Description:(D)-glutamine
NON3
Parent:8
Linkage:n(1+2)n
SmallMolecule:SMILES [6CH2](N)[5CH2][4CH2][3CH2][2C@H](N)[1C](=O)O
Description:(L)-lysine
NON4
Parent:9
Linkage:n(1+2)n
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine
NON5
Parent:10
Linkage:n(1+2)n
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine