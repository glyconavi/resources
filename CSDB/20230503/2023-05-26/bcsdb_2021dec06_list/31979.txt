RES
1b:a-dglc-HEX-1:5
2s:amino
3n:n1
4n:n2
5b:b-dglc-HEX-1:5
6b:a-dgal-HEX-1:5|6:a
7s:amino
8n:n3
9n:n4
10n:n5
11n:n6
LIN
1:1d(2+1)2n
2:1o(3+1)3n
3:1o(2+1)4n
4:1o(6+1)5d
5:5o(4+1)6d
6:5d(2+1)7n
7:5o(3+1)8n
8:5o(2+1)9n
9:9n(3+1)10n
10:10n(27+1)11n
NON
NON1
Parent:1
Linkage:o(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-tetradecanoic acid
NON2
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-hexadecanoic acid
NON3
Parent:5
Linkage:o(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-tetradecanoic acid
NON4
Parent:5
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-tetradecanoic acid
NON5
Parent:9
Linkage:n(3+1)n
SmallMolecule:SMILES [28CH3][27CH](O)[26CH2][25CH2][24CH2][23CH2][22CH2][21CH2][20CH2][19CH2][18CH2][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:27-hydroxy-octacosanoic acid
NON6
Parent:10
Linkage:n(27+1)n
SmallMolecule:SMILES [4CH3][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxybutanoic acid