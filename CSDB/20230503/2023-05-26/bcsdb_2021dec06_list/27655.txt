RES
1r:r1
REP
REP1:12o(3+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5|6:d
3s:amino
4s:n-acetyl
5n:n1
6b:a-lgal-HEX-1:5|6:d
7s:n-acetyl
8b:a-lgal-HEX-1:5|6:d
9s:n-acetyl
10b:a-lgal-HEX-1:5|6:d
11s:n-acetyl
12b:b-ltal-HEX-1:5|6:d
13s:n-acetyl
14s:acetyl
LIN
1:2d(4+1)3n
2:2d(2+1)4n
3:2o(4+1)5n
4:2o(3+1)6d
5:6d(2+1)7n
6:6o(4+1)8d
7:8d(2+1)9n
8:8o(4+1)10d
9:10d(2+1)11n
10:10o(4+1)12d
11:12d(2+1)13n
12:12o(4+1)14n
NON
NON1
Parent:2
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH3][3C@H](O)[2CH2][1C](=O)O
Description:(S)-3-hydroxybutanoic acid