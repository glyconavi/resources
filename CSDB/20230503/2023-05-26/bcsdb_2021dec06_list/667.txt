RES
1r:r1
REP
REP1:7o(4+1)2d=-1--1
RES
2b:a-lman-HEX-1:5|6:d
3b:b-dglc-HEX-1:5|6:d
4s:n-acetyl
5s:amino
6n:n1
7b:a-lgal-HEX-1:5|6:d
8s:n-acetyl
LIN
1:2o(3+1)3d
2:3d(2+1)4n
3:3d(4+1)5n
4:3o(4+1)6n
5:3o(3+1)7d
6:7d(2+1)8n
NON
NON1
Parent:3
Linkage:o(4+1)n
SmallMolecule:SMILES [6CH3][5CH](O)[4CH2][3CH](O)[2CH2][1C](=O)O
Description:3,5-dihydroxyhexanoic acid