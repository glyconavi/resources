RES
1b:x-dman-OCT-2:6|1:a|2:keto|3:d
2b:x-dman-OCT-2:6|1:a|2:keto|3:d
3b:a-lgro-dman-HEP-1:5
4b:b-dglc-HEX-1:5
5a:a1
6b:a-lgro-dman-HEP-1:5
7b:b-dglc-HEX-1:5
8b:b-dgal-HEX-1:5
LIN
1:1o(-1+2)2d
2:1o(5+1)3d
3:3o(4+1)4d
4:3o(6+1)5n
5:3o(3+1)6d
6:6o(2+1)7d
7:6o(3+1)8d
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:9
RES
9s:phosphate
ALTSUBGRAPH2
LEAD-IN RES:10
RES
10s:phospho-ethanolamine