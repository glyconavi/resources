RES
1n:n1
2s:pyrophosphate
3b:a-dgal-HEX-1:5
LIN
1:1n(5+1)2n
2:2n(1+1)3o
NON
NON1
SmallMolecule:SMILES [5CH2](O)[4C@@H](O1)[3C@@H](O)[2C@@H](O)[1C@H](N2[6C](=O)N[7C](=O)[8CH]=[9CH]2)1
Description:uridine