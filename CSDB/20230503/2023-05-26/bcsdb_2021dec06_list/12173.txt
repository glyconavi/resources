RES
1b:b-dglc-HEX-1:5
2b:a-lgal-HEX-x:x|6:d
3s:n-acetyl
4n:n1
5b:b-dgal-HEX-x:x
6a:a1
LIN
1:1o(3+1)2d
2:1d(2+1)3n
3:1o(1+3)4n
4:1o(4+1)5d
5:5o(2+1)6n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:7
RES
7b:b-lgal-HEX-1:5|6:d
ALTSUBGRAPH2
LEAD-IN RES:8
RES
8b:a-lgal-HEX-1:5|6:d
NON
NON1
Parent:1
Linkage:o(1+3)n
HistoricalEntity:O-antigen-core oligosaccharide -lipid A