RES
1b:a-dman-OCT-2:6|1:a|2:keto|3:d
2b:a-dman-OCT-2:6|1:a|2:keto|3:d
3b:a-lgro-dman-HEP-1:5
4a:a1
5b:a-lgro-dman-HEP-1:5
6a:a2
7b:a-dgal-HEX-1:5|6:a
8b:a-dglc-HEX-1:5
9a:a3
10s:amino
LIN
1:1o(4+2)2d
2:1o(5+1)3d
3:3o(4+1)4n
4:3o(3+1)5d
5:5o(7+1)6n
6:5o(3+1)7d
7:7o(4+1)8d
8:8o(6+2)9n
9:8d(2+1)10n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:11
RES
11b:b-dglc-HEX-1:5
12b:b-dgal-HEX-1:5|6:a
LIN
1:11o(-1+1)12d
ALTSUBGRAPH2
LEAD-IN RES:13
RES
13b:b-dglc-HEX-1:5
ALT2
ALTSUBGRAPH1
LEAD-IN RES:14
RES
14b:a-lgro-dman-HEP-1:5
15b:b-dgal-HEX-1:5|6:a
LIN
1:14o(7+1)15d
ALTSUBGRAPH2
LEAD-IN RES:16
RES
16b:a-lgro-dman-HEP-1:5
ALT3
ALTSUBGRAPH1
LEAD-IN RES:17
RES
17b:a-dman-OCT-2:6|1:a|2:keto|3:d
18b:a-lgro-dman-HEP-1:5
LIN
1:17o(-1+1)18d
ALTSUBGRAPH2
LEAD-IN RES:19
RES
19b:a-dman-OCT-2:6|1:a|2:keto|3:d