RES
1b:a-dglc-HEX-1:5
2b:a-dglc-HEX-1:5
3n:n1
4n:n2
5n:n3
LIN
1:1o(1+1)2d
2:2o(2+1)3n
3:2o(6+1)4n
4:2o(3+1)5n
NON
NON1
Parent:2
Linkage:o(2+1)n
HistoricalEntity:superclass: lipid residue
NON2
Parent:2
Linkage:o(6+1)n
HistoricalEntity:superclass: lipid residue
NON3
Parent:2
Linkage:o(3+1)n
HistoricalEntity:superclass: lipid residue