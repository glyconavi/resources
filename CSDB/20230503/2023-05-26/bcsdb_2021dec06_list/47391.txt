RES
1n:n1
2n:n2
3s:methyl
4s:methyl
5b:b-dgal-HEX-1:5
6b:b-dxyl-PEN-1:5
LIN
1:1n(1+1)2n
2:2n(14+1)3n
3:2n(26+1)4n
4:2n(7+1)5d
5:5o(2+1)6d
NON
NON1
SmallMolecule:SMILES [1CH3]O
Description:methyl
NON2
Parent:1
Linkage:n(1+1)n
SmallMolecule:SMILES O=C(O)C(CCC/N=C/2CC(O)({7}CO)C/C(NCCCC(N/C1={14}C(O)/C(=O)CC(O)(CO)C1)C(=O)O)={1}C2/O)N/C3={26}C(O)/C(=O)CC(O)(CO)C3