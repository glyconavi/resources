RES
1r:r1
REP
REP1:5n(2+1)2d=-1--1
RES
2b:a-dgal-HEX-1:5|6:d
3s:acetyl
4s:amino
5n:n1
LIN
1:2o(2+1)3n
2:2d(3+1)4n
3:2o(3+1)5n
NON
NON1
Parent:2
Linkage:o(3+1)n
SmallMolecule:SMILES [3CH2](O)[2CH](O)[1C](=O)O
Description:glyceric acid