RES
1b:o-xgro-TRI-0:0|1:aldi
2n:n1
3n:n2
4b:a-dglc-HEX-1:5
5s:phosphate
6b:a-dglc-HEX-1:5
7s:phosphate
8b:a-dglc-HEX-1:5
9b:o-xgro-TRI-0:0|1:aldi
10n:n3
11n:n4
12s:phosphate
13b:o-xgro-TRI-0:0|1:aldi
14n:n5
15n:n6
16b:o-xgro-TRI-0:0|1:aldi
17n:n7
18n:n8
LIN
1:1o(2+1)2n
2:1o(1+1)3n
3:1o(3+1)4d
4:4o(6+1)5n
5:4o(2+1)6d
6:6o(6+1)7n
7:6o(2+1)8d
8:5n(1+1)9o
9:9o(2+1)10n
10:9o(3+1)11n
11:8o(6+1)12n
12:7n(1+1)13o
13:13o(2+1)14n
14:13o(3+1)15n
15:12n(1+1)16o
16:16o(2+1)17n
17:16o(3+1)18n
NON
NON1
Parent:1
Linkage:o(2+1)n
HistoricalEntity:superclass: lipid residue
NON2
Parent:1
Linkage:o(1+1)n
HistoricalEntity:superclass: lipid residue
NON3
Parent:9
Linkage:o(2+1)n
HistoricalEntity:superclass: lipid residue
NON4
Parent:9
Linkage:o(3+1)n
HistoricalEntity:superclass: lipid residue
NON5
Parent:13
Linkage:o(2+1)n
HistoricalEntity:superclass: lipid residue
NON6
Parent:13
Linkage:o(3+1)n
HistoricalEntity:superclass: lipid residue
NON7
Parent:16
Linkage:o(2+1)n
HistoricalEntity:superclass: lipid residue
NON8
Parent:16
Linkage:o(3+1)n
HistoricalEntity:superclass: lipid residue