RES
1b:a-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+12)2n
NON
NON1
Parent:1
Linkage:o(1+12)n
SmallMolecule:SMILES C{12}[C@H](O)[C@H](O)c2cnc1nc(N)[nH]c(=O)c1n2
Description:L-erythro-biopterin