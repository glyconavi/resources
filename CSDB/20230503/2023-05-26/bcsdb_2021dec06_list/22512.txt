RES
1r:r1
REP
REP1:3o(3+1)2d=-1--1
RES
2b:a-lman-HEX-1:5|6:d
3b:b-dglc-HEX-1:5|6:a
4a:a1
LIN
1:2o(3+1)3d
2:3o(6+2)4n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:5
RES
5n:n1
ALTSUBGRAPH2
LEAD-IN RES:6
RES
6n:n2
NON
NON1
SmallMolecule:SMILES [4CH3][3C@@H](O)[2C@H](N)[1C](=O)O
Description:(L)-threonine
NON2
SmallMolecule:SMILES [3CH2](O)[2C@H](N)[1C](=O)O
Description:(L)-serine