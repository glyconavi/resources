RES
1b:a-dman-OCT-2:6|1:a|2:keto|3:d
2b:a-lgro-dman-HEP-1:5
3s:phosphate
4s:phosphate
5s:phosphate
6b:a-lgro-dman-HEP-1:5
7n:n1
8b:a-dgal-HEX-1:5
9b:a-dglc-HEX-1:5
10s:amino
11n:n2
LIN
1:1o(5+1)2d
2:2o(2+1)3n
3:2o(6+1)4n
4:2o(4+1)5n
5:2o(3+1)6d
6:6o(7+1)7n
7:6o(3+1)8d
8:8o(4+1)9d
9:8d(2+1)10n
10:8o(2+1)11n
NON
NON1
Parent:6
Linkage:o(7+1)n
SmallMolecule:SMILES [1C](=O)(O)N
Description:carbamic acid
NON2
Parent:8
Linkage:o(2+1)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine