RES
1b:a-dglc-HEX-1:5
2s:phosphate
3s:amino
4n:n1
5n:n2
6b:b-dglc-HEX-1:5
7a:a1
8s:phosphate
9s:amino
10n:n3
11n:n4
12b:a-dman-OCT-2:6|1:a|2:keto|3:d
13a:a2
14s:phosphate
15b:a-dman-HEX-1:5
16b:a-dgal-HEX-1:5|6:a
17b:b-dglc-HEX-1:5
18s:phosphate
19b:a-dgal-HEX-1:5|6:a
20b:a-dman-HEX-1:5
21b:a-dman-HEX-1:5
LIN
1:1o(1+1)2n
2:1d(2+1)3n
3:1o(2+1)4n
4:1o(3+1)5n
5:1o(6+1)6d
6:5n(3+1)7n
7:6o(4+1)8n
8:6d(2+1)9n
9:6o(2+1)10n
10:6o(3+1)11n
11:6o(6+2)12d
12:11n(3+1)13n
13:12o(4+1)14n
14:12o(5+1)15d
15:14n(1+1)16o
16:15o(4+1)17d
17:15o(3+1)18n
18:18n(1+1)19o
19:17o(3+1)20d
20:20o(3+1)21d
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:22
RES
22n:n5
ALTSUBGRAPH2
LEAD-IN RES:23
RES
23n:n6
ALT2
ALTSUBGRAPH1
LEAD-IN RES:24
RES
24n:n7
ALTSUBGRAPH2
LEAD-IN RES:25
RES
25n:n8
NON
NON1
Parent:1
Linkage:o(2+1)n
HistoricalEntity:superclass: lipid residue
NON2
Parent:1
Linkage:o(3+1)n
HistoricalEntity:superclass: lipid residue
NON3
Parent:6
Linkage:o(2+1)n
HistoricalEntity:superclass: lipid residue
NON4
Parent:6
Linkage:o(3+1)n
HistoricalEntity:superclass: lipid residue
NON5
SmallMolecule:SMILES [11CH3][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:undecanoic acid
NON6
SmallMolecule:SMILES [10CH3][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:capric acid
NON7
SmallMolecule:SMILES [11CH3][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:undecanoic acid
NON8
SmallMolecule:SMILES [10CH3][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:capric acid