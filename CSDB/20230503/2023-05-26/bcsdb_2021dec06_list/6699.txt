RES
1r:r1
REP
REP1:5o(4+1)2d=-1--1
RES
2b:b-dgal-HEX-1:5
3s:n-acetyl
4b:b-dgal-HEX-1:5
5b:b-dgal-HEX-1:5
6b:a-dglc-HEX-1:5
7s:n-acetyl
8b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d|9:d
9a:a1
10s:n-acetyl
11s:amino
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4o(6+1)5d
4:4o(3+1)6d
5:6d(2+1)7n
6:5o(6+2)8d
7:8o(7+1)9n
8:8d(5+1)10n
9:8d(7+1)11n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:12
RES
12n:n1
ALTSUBGRAPH2
LEAD-IN RES:13
RES
13s:acetyl
NON
NON1
SmallMolecule:SMILES [4CH3][3C@H](O)[2CH2][1C](=O)O
Description:(S)-3-hydroxybutanoic acid