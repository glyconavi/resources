RES
1n:n1
2b:a-lrib-HEX-1:5|2:d|6:d
3n:n2
4n:n3
5b:a-lrib-HEX-1:5|2:d|6:d
6b:b-lrib-HEX-1:5|2:d|6:d
7s:methyl
LIN
1:1n(9+1)2d
2:1n(17+1)3n
3:3n(4+1)4n
4:2o(3+1)5d
5:5o(4+1)6d
6:6o(4+1)7n
NON
NON1
SmallMolecule:SMILES C/C2=C/C{17}[C@H](O)/C(C)=C\[C@H]1\C=C(CO)/[C@H](C)CC15OC(=O)/C(=C(O)\[C@]4(C)[C@H]2/C=C\[C@@H]3{9}[C@@H](O)[C@@H](C)C[C@H](C)[C@H]34)C5=O
Description:3-hydroxy-26-oxo-kijanolide
NON2
Parent:1
Linkage:n(17+1)n
SmallMolecule:SMILES C[C@H]1O{1}[C@@H](O)C[C@](C)(N(=O)=O){4}[C@H]1N
Description:3C-methyl-3-nitro-4-amino-2,3,4,6-tetradeoxy-xylo-hexose
NON3
Parent:3
Linkage:n(4+1)n
SmallMolecule:SMILES CO{1}C(N)=O
Description:carbamic acid methyl ester