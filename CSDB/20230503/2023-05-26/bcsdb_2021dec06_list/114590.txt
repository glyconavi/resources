RES
1r:r1
REP
REP1:7o(4+1)2d=-1--1
RES
2b:a-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4b:b-dglc-HEX-1:5
5b:b-dglc-HEX-1:5|6:a
6b:b-dglc-HEX-1:5
7b:b-dglc-HEX-1:5|6:a
8b:b-dglc-HEX-1:5
9b:b-dglc-HEX-1:5|6:a
10b:a-dgal-HEX-1:5
11b:b-dglc-HEX-1:5
12s:pyruvate
LIN
1:2o(4+1)3d
2:2o(6+1)4d
3:3o(4+1)5d
4:4o(4+1)6d
5:5o(4+1)7d
6:6o(4+1)8d
7:8o(4+1)9d
8:9o(4+1)10d
9:10o(6+1)11d
10:11o(6+2)12n
11:11o(4+2)12n