RES
1b:a-dglc-HEX-1:5
2n:n1
3n:n2
4n:n3
5b:a-dglc-HEX-1:5
6b:b-dglc-HEX-1:5
7n:n4
LIN
1:1o(4+1)2n
2:1o(3+1)3n
3:1o(6+1)4n
4:1o(1+1)5d
5:5o(6+1)6d
6:5o(2+1)7n
NON
NON1
Parent:1
Linkage:o(4+1)n
HistoricalEntity:superclass: lipid residue
NON2
Parent:1
Linkage:o(3+1)n
HistoricalEntity:superclass: lipid residue
NON3
Parent:1
Linkage:o(6+1)n
HistoricalEntity:superclass: lipid residue
NON4
Parent:5
Linkage:o(2+1)n
HistoricalEntity:superclass: lipid residue