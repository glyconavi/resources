RES
1r:r1
REP
REP1:4o(4+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3s:n-acetyl
4b:a-lman-HEX-1:5|6:d
5a:a1
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4o(-1+-1)5n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:6
RES
6s:acetyl
ALTSUBGRAPH2
LEAD-IN RES:7
RES
7s:acetyl