RES
1b:x-dglc-HEX-x:x
2s:n-acetyl
3b:b-dglc-HEX-1:5
4b:a-lgal-HEX-1:5|6:d
5a:a1
6s:n-acetyl
7s:methyl
8b:b-dglc-HEX-1:5
9s:n-acetyl
10b:b-dglc-HEX-1:5
11s:n-acetyl
12b:b-dglc-HEX-1:5
13s:n-methyl
14n:n1
15n:n2
16n:n3
LIN
1:1d(2+1)2n
2:1o(4+1)3d
3:1o(6+1)4d
4:4o(-1+-1)5n
5:3d(2+1)6n
6:4o(2+1)7n
7:3o(4+1)8d
8:8d(2+1)9n
9:8o(4+1)10d
10:10d(2+1)11n
11:10o(4+1)12d
12:12d(2+1)13n
13:12o(2+1)14n
14:12o(3+1)15n
15:12o(4+1)16n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:17
RES
17s:acetyl
ALTSUBGRAPH2
LEAD-IN RES:18
RES
18s:sulfate
NON
NON1
Parent:12
Linkage:o(2+1)n
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2]/[12CH]=[11CH]\[10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:cis-vaccenic
NON2
Parent:12
Linkage:o(3+1)n
SmallMolecule:SMILES [1C](=O)(O)N
Description:carbamic acid
NON3
Parent:12
Linkage:o(4+1)n
SmallMolecule:SMILES [1C](=O)(O)N
Description:carbamic acid