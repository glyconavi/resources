RES
1b:b-dara-HEX-1:5|1:d|2:d|6:d
2n:n1
3n:n2
LIN
1:1o(4+1)2n
2:1h(1+9)3n
NON
NON1
Parent:1
Linkage:o(4+1)n
SmallMolecule:SMILES CCCCC/C=C/C=C/{1}C(=O)O
Description:2,4-decadienoic acid
NON2
Parent:1
Linkage:h(1+9)n
SmallMolecule:SMILES C/C2=C/C(=O)C1(O)c4c(/C=C\C1(O)C2)c(=O)c3c(O){9}cccc3c4=O