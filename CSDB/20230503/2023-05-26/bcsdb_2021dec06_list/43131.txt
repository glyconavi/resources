RES
1b:x-dman-HEX-1:5
2b:b-dgal-HEX-1:4
3n:n1
LIN
1:1o(3+1)2d
2:1o(1+1)3n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES C=CCCCCCCC{1}CO
Description:9-decen-1-ol