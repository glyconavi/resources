RES
1r:r1
REP
REP1:4o(4+1)2d=-1--1
RES
2b:b-dglc-HEX-x:x
3s:n-acetyl
4b:b-dglc-HEX-x:x
5s:n-acetyl
6s:(r)-carboxyethyl
7n:n1
8n:n2
9n:n3
10n:n4
11s:amino
12n:n5
13n:n6
14n:n7
15n:n8
16n:n9
17n:n10
18n:n11
19n:n12
20s:amino
21n:n13
LIN
1:2d(2+1)3n
2:2o(4+1)4d
3:4d(2+1)5n
4:4o(3+1)6n
5:4o(8+2)7n
6:7n(1+2)8n
7:8n(1+2)9n
8:8n(5+2)10n
9:9n(1+1)11n
10:10n(1+2)12n
11:12n(1+2)13n
12:13n(1+2)14n
13:14n(1+6)15n
14:15n(1+2)16n
15:15n(2+5)17n
16:17n(1+2)18n
17:17n(2+1)19n
18:18n(1+1)20n
19:19n(2+8)21n
NON
NON1
Parent:4
Linkage:o(8+2)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine
NON2
Parent:7
Linkage:n(1+2)n
SmallMolecule:SMILES [5C](=O)(O)[4CH2][3CH2][2C@@H](N)[1C](=O)O
Description:(D)-glutamic acid
NON3
Parent:8
Linkage:n(1+2)n
SmallMolecule:SMILES [2CH2](N)[1C](=O)O
Description:glycine
NON4
Parent:8
Linkage:n(5+2)n
SmallMolecule:SMILES [7C](=O)(O)[6C@H](N)[5CH2][4CH2][3CH2][2C@H](N)[1C](=O)O
Description:meso-diaminopimelic acid
NON5
Parent:10
Linkage:n(1+2)n
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine
NON6
Parent:12
Linkage:n(1+2)n
SmallMolecule:SMILES [5C](=O)(O)[4CH2][3CH2][2C@@H](N)[1C](=O)O
Description:(D)-glutamic acid
NON7
Parent:13
Linkage:n(1+2)n
SmallMolecule:SMILES [4C](=O)(O)[3CH2][2C@@H](N)[1C](=O)O
Description:(D)-aspartic acid
NON8
Parent:14
Linkage:n(1+6)n
SmallMolecule:SMILES [7C](=O)(O)[6C@H](N)[5CH2][4CH2][3CH2][2C@@H](N)[1C](=O)O
Description:diaminopimelic acid
NON9
Parent:15
Linkage:n(1+2)n
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine
NON10
Parent:15
Linkage:n(2+5)n
SmallMolecule:SMILES [5C](=O)(O)[4CH2][3CH2][2C@@H](N)[1C](=O)O
Description:(D)-glutamic acid
NON11
Parent:17
Linkage:n(1+2)n
SmallMolecule:SMILES [2CH2](N)[1C](=O)O
Description:glycine
NON12
Parent:17
Linkage:n(2+1)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine
NON13
Parent:19
Linkage:n(2+8)n
HistoricalEntity:other -4)[Ac(1-2)bXMur(1-4)bDGlcNAc(1- chain