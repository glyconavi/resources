RES
1r:r1
REP
REP1:9o(2+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3s:n-acetyl
4b:b-dgal-HEX-1:5
5b:a-dgal-HEX-1:5
6b:a-dglc-HEX-1:5
7s:n-acetyl
8s:(s)-pyruvate
9b:b-dgal-HEX-1:5|6:d
10s:n-acetyl
11s:acetyl
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4o(4+1)5d
4:5o(4+1)6d
5:6d(2+1)7n
6:6o(6+2)8n
7:6o(4+2)8n
8:6o(3+1)9d
9:9d(3+1)10n
10:9o(4+1)11n