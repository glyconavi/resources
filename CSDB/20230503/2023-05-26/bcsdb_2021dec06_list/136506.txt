RES
1b:a-dglc-HEX-1:5
2n:n1
3b:a-dglc-HEX-1:5
4n:n2
5b:b-dglc-HEX-1:5
6b:b-dgal-HEX-1:5
7b:a-lman-HEX-1:5|6:d
8s:methyl
9b:b-lxyl-PEN-1:5
10b:b-lxyl-PEN-1:5
11b:b-lxyl-PEN-1:5
12b:b-lxyl-PEN-1:5
13b:b-lxyl-PEN-1:5
14b:b-lxyl-PEN-1:5
15n:n3
LIN
1:1o(6+1)2n
2:1o(1+1)3d
3:3o(2+1)4n
4:3o(4+1)5d
5:5o(3+1)6d
6:6o(3+1)7d
7:7o(3+1)8n
8:7o(4+1)9d
9:9o(4+1)10d
10:10o(4+1)11d
11:11o(4+1)12d
12:12o(4+1)13d
13:13o(4+1)14d
14:14o(3+1)15n
NON
NON1
Parent:1
Linkage:o(6+1)n
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:palmitic acid
NON2
Parent:3
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11C@H](O)[10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH]([16CH3])[3CH2][2CH]([15CH3])[1C](=O)O
Description:2,4-dimethyl-tetradecanoic acid
NON3
Parent:14
Linkage:o(3+1)n
HistoricalEntity:C4-alkyl-3,6-dideoxy-Hex