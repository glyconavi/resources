RES
1b:b-dglc-HEX-1:5
2s:n-acetyl
3b:b-dglc-HEX-1:5
4s:n-acetyl
5s:(r)-carboxyethyl
6n:n1
7n:n2
8s:phosphate
9b:a-dglc-HEX-1:5
10s:n-acetyl
11b:a-lman-HEX-1:5|6:d
12b:b-dgal-HEX-1:4
13b:b-dgal-HEX-1:4
14b:b-dgal-HEX-1:4
15r:r1
16b:b-dgal-HEX-1:4
17b:b-dgal-HEX-1:4
18b:a-dara-PEN-1:4
19r:r2
20b:a-dara-PEN-1:4
21n:n3
22b:a-dara-PEN-1:4
23b:a-dara-PEN-1:4
24b:a-dara-PEN-1:4
25n:n4
26n:n5
27b:a-dara-PEN-1:4
28b:a-dara-PEN-1:4
29n:n6
30n:n7
LIN
1:1d(2+1)2n
2:1o(4+1)3d
3:3d(2+1)4n
4:3o(3+1)5n
5:3o(8+2)6n
6:3o(4+1)7n
7:3o(6+1)8n
8:8n(1+1)9o
9:9d(2+1)10n
10:9o(3+1)11d
11:11o(4+1)12d
12:12o(5+1)13d
13:13o(6+1)14d
14:14o(5+1)15n
15:15n(5+1)16d
16:16o(6+1)17d
17:16o(5+1)18d
18:18o(5+1)19n
19:19n(5+1)20d
20:20o(3+1)21n
21:20o(5+1)22d
22:22o(3+1)23d
23:22o(5+1)24d
24:24o(-1+1)25n
25:23o(-1+1)26n
26:24o(2+1)27d
27:23o(2+1)28d
28:28o(-1+1)29n
29:27o(-1+1)30n
REP
REP1:32o(5+1)31d=30-30
RES
31b:b-dgal-HEX-1:4
32b:b-dgal-HEX-1:4
LIN
1:31o(6+1)32d
REP2:33o(5+1)33d=-1--1
RES
33b:a-dara-PEN-1:4
NON
NON1
Parent:3
Linkage:o(8+2)n
HistoricalEntity:superclass: aminoacid
NON2
Parent:3
Linkage:o(4+1)n
HistoricalEntity:superclass: any residue
NON3
Parent:20
Linkage:o(3+1)n
HistoricalEntity:superclass: any residue
NON4
Parent:24
Linkage:o(-1+1)n
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH]([19CH2][20CH2][21CH2][22CH2][23CH2][24CH2][25CH2][26CH2][27CH2][28CH2][29CH2][30CH2][31CH2][32CH3])[1C](=O)O
Description:corynomycolic acid
NON5
Parent:23
Linkage:o(-1+1)n
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH]([19CH2][20CH2][21CH2][22CH2][23CH2][24CH2][25CH2][26CH2][27CH2][28CH2][29CH2][30CH2][31CH2][32CH3])[1C](=O)O
Description:corynomycolic acid
NON6
Parent:28
Linkage:o(-1+1)n
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH]([19CH2][20CH2][21CH2][22CH2][23CH2][24CH2][25CH2][26CH2][27CH2][28CH2][29CH2][30CH2][31CH2][32CH3])[1C](=O)O
Description:corynomycolic acid
NON7
Parent:27
Linkage:o(-1+1)n
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH]([19CH2][20CH2][21CH2][22CH2][23CH2][24CH2][25CH2][26CH2][27CH2][28CH2][29CH2][30CH2][31CH2][32CH3])[1C](=O)O
Description:corynomycolic acid