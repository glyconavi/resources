RES
1r:r1
REP
REP1:7o(4+1)2d=-1--1
RES
2b:a-dgal-HEX-1:5
3b:b-dglc-HEX-1:5|6:d
4s:amino
5n:n1
6b:b-dglc-HEX-1:5
7b:b-dglc-HEX-1:5|6:a
8s:n-acetyl
9n:n2
LIN
1:2o(3+1)3d
2:3d(4+1)4n
3:3o(4+1)5n
4:3o(2+1)6d
5:6o(3+1)7d
6:6d(2+1)8n
7:5n(2+1)9n
NON
NON1
Parent:3
Linkage:o(4+1)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine
NON2
Parent:5
Linkage:n(2+1)n
SmallMolecule:SMILES [4CH3][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxybutanoic acid