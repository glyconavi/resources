RES
1b:a-lman-HEX-1:5|6:d
2n:n1
3b:a-lman-HEX-1:5|6:d
4n:n2
LIN
1:1o(1+-1)2n
2:1o(3+1)3d
3:3o(3+1)4n
NON
NON1
Parent:1
Linkage:o(1+-1)n
HistoricalEntity:peptide
NON2
Parent:3
Linkage:o(3+1)n
HistoricalEntity:S-layer glycan