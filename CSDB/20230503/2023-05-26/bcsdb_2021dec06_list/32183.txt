RES
1b:x-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+4)2n
NON
NON1
Parent:1
Linkage:o(1+4)n
SmallMolecule:SMILES O=C(O)C1=CC=C{4}(O)C2=NC3=CC=CC=C3N=C21
Description:4-hydroxyphenazine-1-carboxylic acid