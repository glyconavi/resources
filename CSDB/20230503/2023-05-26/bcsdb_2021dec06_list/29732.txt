RES
1r:r1
REP
REP1:6n(1+1)2o=-1--1
RES
2b:a-dglc-HEX-1:5
3s:n-acetyl
4b:b-dglc-HEX-1:5
5a:a1
6s:phosphate
LIN
1:2d(2+1)3n
2:2o(6+1)4d
3:4o(-1+-1)5n
4:4o(4+1)6n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:7
RES
7s:acetyl
ALTSUBGRAPH2
LEAD-IN RES:8
RES
8s:acetyl