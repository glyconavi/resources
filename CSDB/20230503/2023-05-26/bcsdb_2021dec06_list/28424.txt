RES
1b:a-dglc-HEX-1:5
2s:amino
3n:n1
4n:n2
5s:phosphate
6b:b-dglc-HEX-1:5
7s:amino
8b:b-lara-PEN-1:5
9s:phosphate
10n:n3
11n:n4
12s:amino
13n:n5
14n:n6
15b:b-lara-PEN-1:5
16s:amino
LIN
1:1d(2+1)2n
2:1o(2+1)3n
3:1o(3+1)4n
4:1o(1+1)5n
5:1o(6+1)6d
6:6d(2+1)7n
7:5n(1+1)8o
8:6o(4+1)9n
9:6o(2+1)10n
10:6o(3+1)11n
11:8d(4+1)12n
12:10n(3+1)13n
13:11n(3+1)14n
14:9n(1+1)15o
15:15d(4+1)16n
NON
NON1
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON2
Parent:1
Linkage:o(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON3
Parent:6
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON4
Parent:6
Linkage:o(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON5
Parent:10
Linkage:n(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:lauric acid
NON6
Parent:11
Linkage:n(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:lauric acid