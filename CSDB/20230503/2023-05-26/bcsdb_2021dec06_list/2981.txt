RES
1b:a-dman-OCT-2:6|1:a|2:keto|3:d
2n:n1
3n:n2
4b:a-dman-OCT-2:6|1:a|2:keto|3:d
LIN
1:1o(2+1)2n
2:1o(4+2)3n
3:3n(8+2)4d
NON
NON1
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [3CH2]=[2CH][1CH2]O
Description:allyl alcohol
NON2
Parent:1
Linkage:o(4+2)n
SmallMolecule:SMILES O{2}[C@@]1(CO)O[C@H]([C@H](O){8}CO)[C@@H]([C@H](O)C1)O
Description:3-deoxy-D-manno-2-octulose