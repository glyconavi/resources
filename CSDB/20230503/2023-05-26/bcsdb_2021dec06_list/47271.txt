RES
1b:b-dall-HEX-1:4|6:a
2s:amino
3s:amino
4n:n1
5n:n2
LIN
1:1d(5+1)2n
2:1d(1+1)3n
3:1o(5+1)4n
4:1o(1+1)5n
NON
NON1
Parent:1
Linkage:o(5+1)n
SmallMolecule:SMILES [9CH]1=[8CH][7C](O)=[6CH][5CH]=[4C]1[3CH2][2C@H](N)[1C](=O)O
Description:(L)-tyrosine
NON2
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES O=c1cc{1}[nH]c(=O)[nH]1
Description:uracil