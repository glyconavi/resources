RES
1b:o-drib-PEN-0:0|1:aldi
2b:b-dgal-HEX-1:5
3s:n-acetyl
4b:a-dgal-HEX-1:5
5s:n-acetyl
6b:a-dgal-HEX-1:5|6:d
7b:b-dglc-HEX-1:5
8s:n-acetyl
9s:amino
10n:n1
LIN
1:1o(5+1)2d
2:2d(2+1)3n
3:2o(3+1)4d
4:4d(2+1)5n
5:4o(4+1)6d
6:6o(3+1)7d
7:6d(2+1)8n
8:6d(4+1)9n
9:6o(4+1)10n
NON
NON1
Parent:6
Linkage:o(4+1)n
SmallMolecule:SMILES O={1}C(O)OCC1C2=C(C3=C1C=CC=C3)C=CC=C2
Description:9-fluorenylmethoxycarbonyl (Fmoc)