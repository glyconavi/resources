RES
1b:b-lall-HEX-1:5|6:d
2s:methyl
3s:acetyl
4s:amino
5n:n1
6n:n2
7n:n3
8n:n4
LIN
1:1o(2+1)2n
2:1o(3+1)3n
3:1d(1+1)4n
4:1o(4+1)5n
5:1o(1+1)6n
6:6n(6+1)7n
7:7n(7+6)8n
NON
NON1
Parent:1
Linkage:o(4+1)n
SmallMolecule:SMILES [1C](=O)(O)N
Description:carbamic acid
NON2
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES C=C2CC{6}[C@@H](O)[C@@H]3/C=C\[C@H](C)[C@H](C(=O)/C1=C(O)/{1}C(C(C)C)NC1=O)[C@@H]23
Description:amycolamicin aglycon 2
NON3
Parent:6
Linkage:n(6+1)n
SmallMolecule:SMILES C{7}C(N)[C@]1(O)C{1}[C@H](O)O[C@H](C)[C@H]1O
Description:amycolose, 3C-��-aminoethyl-2,6-dideoxy-ribo-hexopyranose, 3C-��-aminoethyl-digitoxose
NON4
Parent:7
Linkage:n(7+6)n
SmallMolecule:SMILES Cc1[nH]c({6}C(=O)O)c(Cl)c1Cl
Description:amycolamicin aglycon 1