RES
1r:r1
REP
REP1:7o(4+1)2d=-1--1
RES
2b:a-dgal-HEX-1:5
3b:b-dman-HEX-1:5
4b:b-dgal-HEX-1:5
5s:n-acetyl
6n:n1
7b:b-dgal-HEX-1:5
8s:n-acetyl
LIN
1:2o(4+1)3d
2:2o(3+1)4d
3:4d(2+1)5n
4:3o(4+2)6n
5:4o(3+1)7d
6:7d(2+1)8n
NON
NON1
Parent:3
Linkage:o(4+2)n
SmallMolecule:SMILES C[C@@H](OC1=O)C{2}[C@H]1O
Description:(2R,4R)-2,4-dihydroxypentanoic acid 1,4-lactone