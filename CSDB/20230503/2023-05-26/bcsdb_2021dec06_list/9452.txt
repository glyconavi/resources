RES
1b:a-dman-HEX-1:5
2b:a-dman-HEX-1:5
3n:n1
4b:a-dman-HEX-1:5
5b:a-dman-HEX-1:5
6b:a-lara-PEN-1:4
LIN
1:1o(2+1)2d
2:1o(1+1)3n
3:1o(6+1)4d
4:4o(2+1)5d
5:5o(2+1)6d
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES [3CH2]=[2CH][1CH2]O
Description:allyl alcohol