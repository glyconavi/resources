RES
1b:a-dglc-HEX-1:5
2s:amino
3n:n1
4s:phosphate
5b:b-dglc-HEX-1:5
6s:amino
7n:n2
8s:phosphate
9b:b-lara-PEN-1:5
10n:n3
11b:a-dman-OCT-2:6|1:a|2:keto|3:d
12b:a-dgro-dtal-OCT-2:6|1:a|2:keto
13s:amino
14n:n4
15b:b-lara-PEN-1:5
16b:a-lgro-dman-HEP-1:5
17b:b-dglc-HEX-1:5
18s:amino
19b:a-lgro-dman-HEP-1:5
20b:b-dglc-HEX-1:5
21b:a-lgro-dman-HEP-1:5
22b:a-dgal-HEX-1:5
23b:a-dglc-HEX-1:5
24b:a-lgro-dman-HEP-1:5
25b:b-dgal-HEX-1:5
26b:a-lgro-dman-HEP-1:5
27b:b-dglc-HEX-1:5|6:d
28s:n-acetyl
LIN
1:1d(2+1)2n
2:1o(2+1)3n
3:1o(1+1)4n
4:1o(6+1)5d
5:5d(2+1)6n
6:5o(3+1)7n
7:5o(4+1)8n
8:4n(1+1)9o
9:5o(2+1)10n
10:5o(6+2)11d
11:11o(4+2)12d
12:9d(4+1)13n
13:10n(3+1)14n
14:8n(1+1)15o
15:11o(5+1)16d
16:16o(4+1)17d
17:15d(4+1)18n
18:16o(3+1)19d
19:19o(2+1)20d
20:19o(7+1)21d
21:19o(3+1)22d
22:22o(6+1)23d
23:22o(2+1)24d
24:23o(3+1)25d
25:24o(3+1)26d
26:24o(7+1)27d
27:27d(2+1)28n
NON
NON1
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-hexadecanoic acid
NON2
Parent:5
Linkage:o(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-tetradecanoic acid
NON3
Parent:5
Linkage:o(2+1)n
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-hexadecanoic acid
NON4
Parent:10
Linkage:n(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:myristic acid