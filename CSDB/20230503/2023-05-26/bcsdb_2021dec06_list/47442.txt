RES
1n:n1
2s:methyl
3b:b-dglc-HEX-1:5
4s:methyl
5s:amino
6b:a-ltre-PEN-1:5|2:d
7s:amino
LIN
1:1n(6+1)2n
2:1n(12+1)3d
3:3o(4+1)4n
4:3d(1+1)5n
5:3o(6+1)6d
6:6d(4+1)7n
NON
NON1
SmallMolecule:SMILES O=c1{6}[nH]c(=O)c6c1c3c2ccccc2[nH]c3c5{12}[nH]c4c(Cl)cccc4c56
Description:11-chloro-arcyriaflavin A