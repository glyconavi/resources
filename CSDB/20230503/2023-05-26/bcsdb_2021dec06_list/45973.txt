RES
1b:b-dall-HEX-1:4|6:a
2s:amino
3s:amino
4n:n1
5n:n2
6b:o-lxyl-PEN-0:0|1:a
7s:amino
8n:n3
LIN
1:1d(5+1)2n
2:1d(1+1)3n
3:1o(6+1)4n
4:1o(1+1)5n
5:1o(5+1)6d
6:6d(2+1)7n
7:6o(5+1)8n
NON
NON1
Parent:1
Linkage:o(6+1)n
SmallMolecule:SMILES C/C=C\1C{1}N[C@@H]1C(=O)O
Description:POIA aglycon
NON2
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES O=c1{1}[nH]cc(CO)c(=O)[nH]1
Description:5-hydroxymethyluracil
NON3
Parent:6
Linkage:o(5+1)n
SmallMolecule:SMILES [1C](=O)(O)N
Description:carbamic acid