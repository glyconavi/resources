RES
1b:o-dglc-HEX-0:0
2s:(r)-carboxyethyl
3s:n-acetyl
4b:b-dglc-HEX-1:5
5n:n1
6s:amino
7n:n2
8n:n3
9n:n4
LIN
1:1o(3+1)2n
2:1d(2+1)3n
3:1o(4+1)4d
4:1o(8+2)5n
5:4d(2+1)6n
6:5n(1+2)7n
7:7n(5+2)8n
8:8n(1+2)9n
NON
NON1
Parent:1
Linkage:o(8+2)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine
NON2
Parent:5
Linkage:n(1+2)n
SmallMolecule:SMILES [5C](=O)(O)[4CH2][3CH2][2C@@H](N)[1C](=O)O
Description:(D)-glutamic acid
NON3
Parent:7
Linkage:n(5+2)n
SmallMolecule:SMILES [7C](=O)(O)[6C@H](N)[5CH2][4CH2][3CH2][2C@H](N)[1C](=O)O
Description:meso-diaminopimelic acid
NON4
Parent:8
Linkage:n(1+2)n
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine