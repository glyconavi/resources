RES
1b:a-ltre-HEX-1:5|2:d|3:d|6:d
2n:n1
3b:b-dara-HEX-1:5|1:d|2:d|6:d
4b:a-ltre-HEX-1:5|2:d|3:d|6:d
5n:n2
LIN
1:1o(1+12)2n
2:2n(9+1)3d
3:3o(3+1)4d
4:4o(4+1)5n
NON
NON1
Parent:1
Linkage:o(1+12)n
SmallMolecule:SMILES CC2(O)CC(=O){12}[C@]1(O)c4c(/C=C\[C@]1(O)C2)c(=O)c3c(O){9}cccc3c4=O
NON2
Parent:4
Linkage:o(4+1)n
SmallMolecule:SMILES C[C@H]1O{1}[C@@H](O)CC(=O)[C@@H]1O
Description:D-2,6-dideoxyerythro-hexos-3-ulose (kerriose)