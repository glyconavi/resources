RES
1r:r1
REP
REP1:4o(7+1)2n=-1--1
RES
2s:phosphate
3b:o-xgro-TRI-0:0|1:aldi
4b:a-dman-OCT-2:6|1:a|2:keto|3:d
5s:acetyl
LIN
1:2n(1+1)3o
2:3o(3+2)4d
3:4o(4+1)5n