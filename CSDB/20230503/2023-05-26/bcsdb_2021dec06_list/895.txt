RES
1r:r1
REP
REP1:9o(4+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3s:n-acetyl
4b:b-dglc-HEX-1:5|6:a
5n:n1
6b:b-dglc-HEX-1:5
7s:n-acetyl
8b:b-dgal-HEX-1:5
9b:a-dglc-HEX-1:5|6:d
10s:amino
11n:n2
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4o(6+2)5n
4:4o(4+1)6d
5:6d(2+1)7n
6:6o(4+1)8d
7:8o(4+1)9d
8:9d(3+1)10n
9:9o(3+1)11n
NON
NON1
Parent:4
Linkage:o(6+2)n
SmallMolecule:SMILES [2CH2](N)[1C](=O)O
Description:glycine
NON2
Parent:9
Linkage:o(3+1)n
SmallMolecule:SMILES [4CH3][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxybutanoic acid