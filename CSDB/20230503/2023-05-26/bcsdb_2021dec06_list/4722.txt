RES
1b:a-dgal-HEX-1:5|6:a
2n:n1
LIN
1:1o(6+2)2n
NON
NON1
Parent:1
Linkage:o(6+2)n
SmallMolecule:SMILES [3CH2](O)[2C@H](N)[1C](=O)O
Description:(L)-serine