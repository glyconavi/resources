RES
1b:o-xgro-TRI-0:0|1:aldi
2n:n1
3s:phosphate
4n:n2
5b:a-dglc-HEX-1:5
6s:amino
7b:a-dman-HEX-1:5
8b:a-dman-HEX-1:5
9s:phosphate
10b:b-dgal-HEX-1:4
11b:a-dglc-HEX-1:5
12b:a-dgal-HEX-1:5
13b:a-dgal-HEX-1:5
14s:phosphate
15n:n3
LIN
1:1o(1+1)2n
2:1o(3+1)3n
3:3n(1+1)4n
4:4n(6+1)5d
5:5d(2+1)6n
6:5o(4+1)7d
7:7o(3+1)8d
8:8o(6+1)9n
9:8o(3+1)10d
10:9n(1+1)11o
11:10o(3+1)12d
12:12o(6+1)13d
13:13o(6+1)14n
14:14n(1+1)15n
NON
NON1
Parent:1
Linkage:o(1+1)n
HistoricalEntity:C24-C26
NON2
Parent:3
Linkage:n(1+1)n
HistoricalEntity:superclass: any inositol
NON3
Parent:14
Linkage:n(1+1)n
HistoricalEntity:polysaccharide