RES
1b:x-dman-OCT-x:x|1:a|2:keto|3:d
2b:a-lgro-dman-HEP-x:x
3b:a-lgro-dman-HEP-x:x
4b:a-dglc-HEX-1:5
5b:x-dgal-HEX-1:5
6b:a-dgal-HEX-1:5
7b:a-dglc-HEX-1:5
8b:x-dglc-HEX-1:5
9s:n-acetyl
LIN
1:1o(5+1)2d
2:2o(3+1)3d
3:3o(3+1)4d
4:4o(6+1)5d
5:4o(3+1)6d
6:6o(2+1)7d
7:7o(2+1)8d
8:8d(2+1)9n