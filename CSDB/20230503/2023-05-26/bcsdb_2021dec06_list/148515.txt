RES
1b:b-dxyl-PEN-1:5
2b:b-dgal-HEX-1:5|6:d
3s:n-acetyl
4b:a-lgro-lman-NON-2:6|1:a|2:keto|3:d|9:d
5s:amino
6s:n-formyl
7n:n1
LIN
1:1o(4+1)2d
2:2d(2+1)3n
3:2o(3+2)4d
4:4d(5+1)5n
5:4d(7+1)6n
6:4o(5+1)7n
NON
NON1
Parent:4
Linkage:o(5+1)n
SmallMolecule:SMILES [4CH3][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxybutanoic acid