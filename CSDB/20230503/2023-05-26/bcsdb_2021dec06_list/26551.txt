RES
1b:o-dman-HEX-0:0|1:aldi
2b:b-dgal-HEX-1:5
3s:anhydro
LIN
1:1o(4+1)2d
2:1d(2+1)3n
3:1o(5+1)3n