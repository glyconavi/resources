RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(6+1)2n
NON
NON1
Parent:1
Linkage:o(6+1)n
HistoricalEntity:mainly 16:1 and 16:0