RES
1b:o-xgro-TRI-0:0|1:aldi
2n:n1
3n:n2
4s:phosphate
5n:n3
6b:a-dman-HEX-1:5
7b:a-dman-HEX-1:5
8n:n4
9b:a-dman-HEX-1:5
10b:a-dman-HEX-1:5
11b:a-dman-HEX-1:5
12b:a-dman-HEX-1:5
13b:a-dman-HEX-1:5
14b:a-dman-HEX-1:5
15b:a-dman-HEX-1:5
16b:x-dara-PEN-1:4
17b:a-dman-HEX-1:5
18b:a-dman-HEX-1:5
19b:a-dara-PEN-1:4
20b:a-dman-HEX-1:5
21b:a-dman-HEX-1:5
22r:r1
23b:a-dman-HEX-1:5
24b:a-dman-HEX-1:5
25b:a-dara-PEN-1:4
26b:a-dman-HEX-1:5
27b:a-dman-HEX-1:5
28b:a-dman-HEX-1:5
29b:a-dara-PEN-1:4
30b:a-dman-HEX-1:5
31s:succinate
32b:a-dara-PEN-1:4
33b:a-dara-PEN-1:4
34b:a-dman-HEX-1:5
35b:a-dman-HEX-1:5
36b:a-dman-HEX-1:5
37r:r2
38r:r6
39b:a-dara-PEN-1:4
40b:a-dara-PEN-1:4
41b:a-dara-PEN-1:4
42b:a-dara-PEN-1:4
43b:a-dara-PEN-1:4
44b:a-dara-PEN-1:4
45r:r3
46b:a-dara-PEN-1:4
47b:a-dara-PEN-1:4
48b:a-dara-PEN-1:4
49b:a-dara-PEN-1:4
50r:r8
51r:r7
52b:a-dara-PEN-1:4
53b:a-dara-PEN-1:4
54b:a-dara-PEN-1:4
55b:a-dara-PEN-1:4
56b:a-dara-PEN-1:4
57b:a-dara-PEN-1:4
58b:b-dara-PEN-1:4
59b:a-dara-PEN-1:4
60r:r4
61b:a-dara-PEN-1:4
62b:a-dara-PEN-1:4
63b:a-dara-PEN-1:4
64b:a-dara-PEN-1:4
65b:a-dara-PEN-1:4
66b:b-dara-PEN-1:4
67b:b-dara-PEN-1:4
68b:b-dara-PEN-1:4
69b:b-dara-PEN-1:4
70r:r5
71b:b-dara-PEN-1:4
72b:a-dara-PEN-1:4
73b:b-dara-PEN-1:4
74b:a-dman-HEX-1:5
75b:b-dara-PEN-1:4
76b:a-dman-HEX-1:5
77b:a-dman-HEX-1:5
78b:a-dman-HEX-1:5
79b:a-dman-HEX-1:5
80b:a-dman-HEX-1:5
81b:a-dman-HEX-1:5
82n:n5
LIN
1:1o(2+1)2n
2:1o(1+1)3n
3:1o(3+1)4n
4:4n(1+1)5n
5:5n(-1+1)6d
6:5n(-1+1)7d
7:7o(-1+1)8n
8:6o(6+1)9d
9:9o(6+1)10d
10:10o(6+1)11d
11:11o(2+1)12d
12:11o(6+1)13d
13:13o(2+1)14d
14:13o(6+1)15d
15:15o(-1+1)16d
16:15o(6+1)17d
17:17o(2+1)18d
18:16o(5+1)19d
19:17o(6+1)20d
20:20o(2+1)21d
21:19o(5+1)22n
22:20o(6+1)23d
23:23o(2+1)24d
24:22n(5+1)25d
25:23o(6+1)26d
26:26o(2+1)27d
27:26o(6+1)28d
28:25o(5+1)29d
29:28o(2+1)30d
30:29o(2+1)31n
31:29o(3+1)32d
32:29o(5+1)33d
33:28o(6+1)34d
34:34o(2+1)35d
35:34o(6+1)36d
36:32o(5+1)37n
37:33o(5+1)38n
38:37n(5+1)39d
39:38n(5+1)40d
40:39o(3+1)41d
41:39o(5+1)42d
42:40o(5+1)43d
43:40o(3+1)44d
44:41o(5+1)45n
45:42o(5+1)46d
46:44o(5+1)47d
47:43o(5+1)48d
48:46o(5+1)49d
49:48o(5+1)50n
50:47o(5+1)51n
51:45n(5+1)52d
52:52o(3+1)53d
53:52o(5+1)54d
54:49o(5+1)55d
55:51n(5+1)56d
56:50n(5+1)57d
57:53o(2+1)58d
58:55o(5+1)59d
59:54o(5+1)60n
60:55o(3+1)61d
61:56o(3+1)62d
62:57o(5+1)63d
63:57o(3+1)64d
64:56o(5+1)65d
65:62o(2+1)66d
66:60n(2+1)67d
67:65o(2+1)68d
68:63o(2+1)69d
69:61o(5+1)70n
70:64o(2+1)71d
71:59o(5+1)72d
72:70n(2+1)73d
73:69o(2+1)74d
74:72o(2+1)75d
75:68o(2+1)76d
76:71o(2+1)77d
77:76o(2+1)78d
78:77o(2+1)79d
79:75o(2+1)80d
80:80o(2+1)81d
81:79o(4+1)82n
REP
REP1:83o(5+1)83d=11-11
RES
83b:a-dara-PEN-1:4
REP2:84o(5+1)84d=3-3
RES
84b:a-dara-PEN-1:4
REP3:85o(5+1)85d=2-2
RES
85b:a-dara-PEN-1:4
REP4:86o(5+1)86d=5-5
RES
86b:a-dara-PEN-1:4
REP5:87o(5+1)87d=6-6
RES
87b:a-dara-PEN-1:4
REP6:88o(5+1)88d=2-2
RES
88b:a-dara-PEN-1:4
REP7:89o(5+1)89d=2-2
RES
89b:a-dara-PEN-1:4
REP8:90o(5+1)90d=2-2
RES
90b:a-dara-PEN-1:4
NON
NON1
Parent:1
Linkage:o(2+1)n
HistoricalEntity:superclass: lipid residue
NON2
Parent:1
Linkage:o(1+1)n
HistoricalEntity:superclass: lipid residue
NON3
Parent:4
Linkage:n(1+1)n
SmallMolecule:SMILES [6C@H](O)1[5C@H](O)[4C@@H](O)[3C@H](O)[2C@H](O)[1C@H](O)1
Description:myo-inositol
NON4
Parent:7
Linkage:o(-1+1)n
HistoricalEntity:superclass: lipid residue
NON5
Parent:79
Linkage:o(4+1)n
SmallMolecule:SMILES O{1}[C@@H]1[C@@H]([C@H]([C@@H](CSC)O1)O)O
Description:5-methylthio-xylose