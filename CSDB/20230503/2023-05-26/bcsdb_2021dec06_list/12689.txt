RES
1b:a-dglc-HEX-1:5
2s:phosphate
3s:n-acetyl
4n:n1
5b:a-dglc-HEX-1:5
6s:n-acetyl
7n:n2
LIN
1:1o(6+1)2n
2:1d(2+1)3n
3:1o(1+2)4n
4:1o(3+1)5d
5:5d(2+1)6n
6:5o(6+1)7n
NON
NON1
Parent:1
Linkage:o(1+2)n
SmallMolecule:SMILES [3CH2](O)[2CH](O)[1C](=O)O
Description:glyceric acid
NON2
Parent:5
Linkage:o(6+1)n
HistoricalEntity:superclass: any monosaccharide