RES
1b:x-dman-OCT-2:6|1:a|2:keto|3:d
2b:x-dman-OCT-2:6|1:a|2:keto|3:d
3a:a1
4b:a-lgro-dman-HEP-1:5
5s:phospho-ethanolamine
LIN
1:1o(4+2)2d
2:1o(-1+-1)3n
3:3n(5+1)4d
4:2o(7+1)5n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:6
LEAD-OUT RES:6+4
RES
6b:x-dman-OCT-2:6|1:a|2:keto|3:d
ALTSUBGRAPH2
LEAD-IN RES:7
LEAD-OUT RES:7+4
RES
7b:x-dman-OCT-2:6|1:a|2:keto|3:d