RES
1r:r1
REP
REP1:9o(6+1)2n=-1--1
RES
2s:phosphate
3b:o-drib-PEN-0:0|1:aldi
4b:a-dgal-HEX-1:5
5b:a-dgal-HEX-1:5
6b:b-dgal-HEX-1:5
7s:n-acetyl
8b:b-dgal-HEX-1:4
9b:b-dglc-HEX-1:5
LIN
1:2n(1+5)3o
2:3o(2+1)4d
3:4o(2+1)5d
4:4o(4+1)6d
5:6d(2+1)7n
6:6o(3+1)8d
7:8o(5+1)9d