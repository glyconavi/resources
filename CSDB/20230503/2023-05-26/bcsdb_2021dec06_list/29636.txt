RES
1r:r1
REP
REP1:10o(1+1)2d=-1--1
RES
2b:b-dgal-HEX-1:4
3s:acetyl
4b:b-dglc-HEX-1:5
5b:b-dgal-HEX-1:4
6s:acetyl
7s:acetyl
8b:b-dgal-HEX-1:5
9s:phosphate
10b:o-xman-HEX-0:0|1:aldi
LIN
1:2o(2+1)3n
2:2o(6+1)4d
3:4o(3+1)5d
4:5o(5+1)6n
5:5o(6+1)7n
6:5o(3+1)8d
7:8o(3+1)9n
8:9n(1+6)10o