RES
1b:x-dglc-HEX-1:5
2s:n-acetyl
3b:a-lgal-HEX-1:5|6:d
4b:b-dglc-HEX-1:5
5s:n-acetyl
6s:methyl
7b:b-dglc-HEX-1:5
8s:n-acetyl
9b:b-dglc-HEX-1:5
10a:a1
11s:amino
LIN
1:1d(2+1)2n
2:1o(6+1)3d
3:1o(4+1)4d
4:4d(2+1)5n
5:3o(2+1)6n
6:4o(4+1)7d
7:7d(2+1)8n
8:7o(4+1)9d
9:9o(2+1)10n
10:9d(2+1)11n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:12
RES
12n:n1
ALTSUBGRAPH2
LEAD-IN RES:13
RES
13n:n2
ALTSUBGRAPH3
LEAD-IN RES:14
RES
14n:n3
ALTSUBGRAPH4
LEAD-IN RES:15
RES
15n:n4
NON
NON1
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:stearic acid
NON2
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:palmitic acid
NON3
SmallMolecule:SMILES [1*]
Description:octadecenoic acid
NON4
HistoricalEntity:superclass: lipid residue