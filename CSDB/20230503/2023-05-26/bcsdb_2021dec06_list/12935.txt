RES
1b:a-dman-OCT-2:6|1:a|2:keto|3:d
2b:a-lgro-dman-HEP-1:5
3s:phospho-ethanolamine
4b:a-lgro-dman-HEP-1:5
5b:a-dgro-dman-HEP-1:5
6b:a-dgro-dman-HEP-1:5
7b:b-dgal-HEX-1:5
8b:a-dglc-HEX-1:5
9b:b-dglc-HEX-1:5
10s:n-acetyl
11b:a-lgal-HEX-1:5|6:d
12b:a-dgro-dman-HEP-1:5
13b:a-dglc-HEX-1:5
14r:r1
15b:a-dgro-dman-HEP-1:5
16r:r2
17b:a-dglc-HEX-1:5
18b:a-lgal-HEX-1:5|6:d
19s:n-acetyl
20b:b-dgal-HEX-1:5
21r:r3
22b:b-dglc-HEX-1:5
23b:a-lgal-HEX-1:5|6:d
24s:n-acetyl
25b:b-dgal-HEX-1:5
26b:a-lgal-HEX-1:5|6:d
LIN
1:1o(5+1)2d
2:2o(7+1)3n
3:2o(3+1)4d
4:4o(2+1)5d
5:5o(2+1)6d
6:5o(7+1)7d
7:7o(4+1)8d
8:6o(3+1)9d
9:9d(2+1)10n
10:9o(3+1)11d
11:11o(3+1)12d
12:12o(6+1)13d
13:13o(6+1)14n
14:14n(3+1)15d
15:15o(3+1)16n
16:16n(2+1)17d
17:17o(3+1)18d
18:17d(2+1)19n
19:17o(4+1)20d
20:20o(3+1)21n
21:21n(3+1)22d
22:22o(3+1)23d
23:22d(2+1)24n
24:22o(4+1)25d
25:25o(2+1)26d
REP
REP1:27o(6+1)27d=-1--1
RES
27b:a-dglc-HEX-1:5
REP2:28o(3+1)28d=-1--1
RES
28b:a-dgro-dman-HEP-1:5
REP3:30o(3+1)29d=-1--1
RES
29b:a-dglc-HEX-1:5
30b:b-dgal-HEX-1:5
31b:a-lgal-HEX-1:5|6:d
32s:n-acetyl
LIN
1:29o(4+1)30d
2:29o(3+1)31d
3:29d(2+1)32n