RES
1b:o-lgul-HEX-0:0|1:a|6:a
2s:n-acetyl
3s:n-acetyl
4b:b-dman-HEX-1:5|6:a
5s:n-acetyl
6s:n-acetyl
7n:n1
8b:a-dglc-HEX-1:5|6:d
9s:n-acetyl
10s:amino
11n:n2
LIN
1:1d(4+1)2n
2:1d(5+1)3n
3:1o(3+1)4d
4:4d(2+1)5n
5:4d(3+1)6n
6:4o(6+2)7n
7:4o(4+1)8d
8:8d(2+1)9n
9:8d(4+1)10n
10:8o(4+1)11n
NON
NON1
Parent:4
Linkage:o(6+2)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine
NON2
Parent:8
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH3][3C@H](O)[2CH2][1C](=O)O
Description:(S)-3-hydroxybutanoic acid