RES
1b:a-lgal-HEX-1:5|6:d
2s:methyl
3s:amino
4n:n1
5b:a-lgro-dgal-NON-2:6|1:a|2:keto|3:d|9:d
6s:n-acetyl
7s:n-acetyl
LIN
1:1o(1+1)2n
2:1d(2+1)3n
3:1o(2+1)4n
4:1o(3+2)5d
5:5d(5+1)6n
6:5d(7+1)7n
NON
NON1
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [2CH3][1C](=N)O
Description:acetimidic acid