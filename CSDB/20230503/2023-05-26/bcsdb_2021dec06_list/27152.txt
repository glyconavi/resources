RES
1b:o-dgro-TRI-0:0|1:aldi
2n:n1
3n:n2
4b:a-dglc-HEX-1:5
5b:a-dglc-HEX-1:5
6s:phosphate
7b:o-xgro-TRI-0:0|1:aldi
8r:r1
9a:a1
LIN
1:1o(2+1)2n
2:1o(1+1)3n
3:1o(3+1)4d
4:4o(2+1)5d
5:5o(6+1)6n
6:6n(1+1)7o
7:7o(3+1)8n
8:7o(2+1)9n
REP
REP1:11o(3+1)10n=-1--1
RES
10s:phosphate
11b:o-xgro-TRI-0:0|1:aldi
12a:a2
LIN
1:10n(1+1)11o
2:11o(2+1)12n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:13
RES
13n:n3
ALTSUBGRAPH2
LEAD-IN RES:14
RES
14b:a-dgal-HEX-1:5
ALT2
ALTSUBGRAPH1
LEAD-IN RES:15
RES
15n:n4
ALTSUBGRAPH2
LEAD-IN RES:16
RES
16b:a-dgal-HEX-1:5
NON
NON1
Parent:1
Linkage:o(2+1)n
HistoricalEntity:superclass: lipid residue
NON2
Parent:1
Linkage:o(1+1)n
HistoricalEntity:superclass: lipid residue
NON3
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine
NON4
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine