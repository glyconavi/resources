RES
1b:o-xgro-TRI-0:0|1:aldi
2n:n1
3n:n2
4s:phosphate
5n:n3
6b:a-dman-HEX-1:5
7b:a-dman-HEX-1:5
8n:n4
9n:n5
LIN
1:1o(2+1)2n
2:1o(1+1)3n
3:1o(3+1)4n
4:4n(1+1)5n
5:5n(6+1)6d
6:5n(2+1)7d
7:7o(6+1)8n
8:6o(6+1)9n
NON
NON1
Parent:1
Linkage:o(2+1)n
HistoricalEntity:superclass: lipid residue
NON2
Parent:1
Linkage:o(1+1)n
HistoricalEntity:superclass: lipid residue
NON3
Parent:4
Linkage:n(1+1)n
SmallMolecule:SMILES [6C@H](O)1[5C@H](O)[4C@@H](O)[3C@H](O)[2C@H](O)[1C@H](O)1
Description:myo-inositol
NON4
Parent:7
Linkage:o(6+1)n
HistoricalEntity:superclass: lipid residue
NON5
Parent:6
Linkage:o(6+1)n
HistoricalEntity:superclass: lipid residue