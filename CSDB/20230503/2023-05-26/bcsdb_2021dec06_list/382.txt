RES
1b:b-dglc-HEX-1:5
2s:(r)-carboxyethyl
3s:n-acetyl
4n:n1
5b:b-dglc-HEX-1:5
6s:n-acetyl
7n:n2
8b:b-dglc-HEX-1:5
9s:n-acetyl
10s:(r)-carboxyethyl
11b:b-dglc-HEX-1:5
12n:n3
13s:n-acetyl
14n:n4
LIN
1:1o(3+1)2n
2:1d(2+1)3n
3:1o(8+2)4n
4:1o(4+1)5d
5:5d(2+1)6n
6:4n(1+2)7n
7:5o(4+1)8d
8:8d(2+1)9n
9:8o(3+1)10n
10:8o(4+1)11d
11:7n(5+2)12n
12:11d(2+1)13n
13:12n(1+2)14n
NON
NON1
Parent:1
Linkage:o(8+2)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine
NON2
Parent:4
Linkage:n(1+2)n
SmallMolecule:SMILES [5C](=O)(O)[4CH2][3CH2][2C@@H](N)[1C](=O)O
Description:(D)-glutamic acid
NON3
Parent:7
Linkage:n(5+2)n
SmallMolecule:SMILES [7C](=O)(O)[6C@H](N)[5CH2][4CH2][3CH2][2C@H](N)[1C](=O)O
Description:meso-diaminopimelic acid
NON4
Parent:12
Linkage:n(1+2)n
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine