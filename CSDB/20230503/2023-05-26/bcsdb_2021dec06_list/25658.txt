RES
1b:a-dgal-HEX-1:5|6:d
2b:b-dgal-HEX-1:4
3b:a-dgal-HEX-1:5
4n:n1
LIN
1:1o(3+1)2d
2:1o(2+1)3d
3:1o(1+1)4n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES [2CH2](N)[1CH2](O)
Description:ethanolamine