RES
1b:a-dglc-HEX-1:5
2b:a-dgal-HEX-1:5
3b:o-xgro-TRI-0:0|1:aldi
4n:n1
5n:n2
6s:phosphate
7b:o-xgro-TRI-0:0|1:aldi
8r:r1
9a:a1
LIN
1:1o(2+1)2d
2:1o(1+1)3d
3:3o(3+1)4n
4:3o(2+1)5n
5:2o(6+1)6n
6:6n(1+1)7o
7:7o(3+1)8n
8:7o(2+1)9n
REP
REP1:11o(3+1)10n=19-19
RES
10s:phosphate
11b:o-xgro-TRI-0:0|1:aldi
12a:a2
LIN
1:10n(1+1)11o
2:11o(2+1)12n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:13
RES
13b:x-xgal-HEX-1:5
ALTSUBGRAPH2
LEAD-IN RES:14
RES
14n:n3
ALT2
ALTSUBGRAPH1
LEAD-IN RES:15
RES
15b:x-xgal-HEX-1:5
ALTSUBGRAPH2
LEAD-IN RES:16
RES
16n:n4
NON
NON1
Parent:3
Linkage:o(3+1)n
SmallMolecule:SMILES [17CH3][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:margaric acid
NON2
Parent:3
Linkage:o(2+1)n
SmallMolecule:SMILES [15CH3][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:pentadecanoic acid
NON3
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine
NON4
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine