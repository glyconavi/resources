RES
1n:n1
2n:n2
LIN
1:1n(6+1)2n
NON
NON1
SmallMolecule:SMILES CS[C@H]1O[C@H]({6}[C@H](N)[C@@H](C)O)[C@H](O)[C@H](O)[C@H]1O
Description:methylthiolincosamide
NON2
Parent:1
Linkage:n(6+1)n
SmallMolecule:SMILES CCC[C@@H]1C[C@@H]({1}C(=O)O)N(C)C1
Description:N-methyl-4-propyl-L-proline