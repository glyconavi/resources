RES
1b:x-dman-OCT-x:x|1:a|2:keto|3:d
2b:x-dman-OCT-x:x|1:a|2:keto|3:d
3b:x-dman-OCT-x:x|1:a|2:keto|3:d
4a:a1
5s:phosphate
6b:a-lgro-dman-HEP-1:5
7b:x-dglc-HEX-1:5
8b:a-dgal-HEX-1:5
9b:x-dglc-HEX-1:5
10b:a-dgal-HEX-1:5
LIN
1:1o(-1+2)2d
2:1o(-1+2)3d
3:3o(-1+-1)4n
4:4n(-1+1)5n
5:4n(3+1)6d
6:4n(-1+1)7d
7:7o(3+1)8d
8:6o(-1+1)9d
9:9o(3+1)10d
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:11
LEAD-OUT RES:11+7|11+5|11+6
RES
11b:a-lgro-dman-HEP-1:5
ALTSUBGRAPH2
LEAD-IN RES:12
LEAD-OUT RES:12+7|12+5|12+6
RES
12b:a-lgro-dman-HEP-1:5