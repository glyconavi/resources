RES
1b:a-dglc-HEX-1:5
2s:phosphate
3s:amino
4b:b-dglc-HEX-1:5
5s:phosphate
6s:amino
7b:a-dman-OCT-2:6|1:a|2:keto|3:d
8s:phosphate
9b:a-dgro-dman-HEP-1:5
10s:phosphate
11s:phosphate
12b:b-dgal-HEX-1:5
13b:a-dgal-HEX-1:5
LIN
1:1o(1+1)2n
2:1d(2+1)3n
3:1o(6+1)4d
4:4o(4+1)5n
5:4d(2+1)6n
6:4o(6+2)7d
7:7o(4+1)8n
8:7o(5+1)9d
9:9o(3+1)10n
10:9o(6+1)11n
11:9o(4+1)12d
12:12o(6+1)13d