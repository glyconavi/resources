RES
1b:o-xgro-TRI-0:0|1:aldi
2n:n1
3n:n2
4b:b-dgal-HEX-1:4
5b:b-dgal-HEX-1:4
LIN
1:1o(3+1)2n
2:1o(2+1)3n
3:1o(1+1)4d
4:4o(6+1)5d
NON
NON1
Parent:1
Linkage:o(3+1)n
SmallMolecule:SMILES [16CH3][15CH]([20CH3])[14CH2][13CH2][12CH2][11CH]([19CH3])[10CH2][9CH2][8CH2][7CH]([18CH3])[6CH2][5CH2][4CH2][3CH]([17CH3])[2CH2][1CH2](O)
Description:phytanol
NON2
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [16CH3][15CH]([20CH3])[14CH2][13CH2][12CH2][11CH]([19CH3])[10CH2][9CH2][8CH2][7CH]([18CH3])[6CH2][5CH2][4CH2][3CH]([17CH3])[2CH2][1CH2](O)
Description:phytanol