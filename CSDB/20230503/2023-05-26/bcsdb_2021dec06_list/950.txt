RES
1n:n1
2b:b-dglc-HEX-1:5
3s:amino
LIN
1:1n(6+1)2d
2:2d(2+1)3n
NON
NON1
SmallMolecule:SMILES O=C1C(N)=C[C@H](O)[C@@H]{6}(CO)O1
Description:2-amino-2,3-dideoxy-D-erythro-hex-2-enono 1,5-lactone