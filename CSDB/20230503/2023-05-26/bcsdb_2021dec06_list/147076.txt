RES
1b:b-dglc-HEX-1:5
2s:n-acetyl
3b:b-dglc-HEX-1:5
4s:n-acetyl
5b:b-dglc-HEX-1:5
6s:acetyl
7s:amino
8n:n1
LIN
1:1d(2+1)2n
2:1o(4+1)3d
3:3d(2+1)4n
4:3o(4+1)5d
5:5o(6+1)6n
6:5d(2+1)7n
7:5o(2+1)8n
NON
NON1
Parent:5
Linkage:o(2+1)n
HistoricalEntity:superclass: lipid residue