RES
1b:a-dglc-HEX-1:5
2s:n-acetyl
3s:phosphate
4b:b-dman-HEX-1:5
5s:n-acetyl
LIN
1:1d(2+1)2n
2:1o(1+1)3n
3:1o(4+1)4d
4:4d(2+1)5n