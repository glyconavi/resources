RES
1r:r1
REP
REP1:7o(3+1)2d=-1--1
RES
2b:b-dgal-HEX-1:5|6:d
3s:n-acetyl
4s:amino
5b:b-dglc-HEX-1:5
6s:acetyl
7b:b-dman-HEX-1:5
8s:n-acetyl
9s:(r)-carboxyethyl
LIN
1:2d(2+1)3n
2:2d(4+1)4n
3:2o(3+1)5d
4:5o(6+1)6n
5:5o(4+1)7d
6:7d(2+1)8n
7:7o(4+2)9n