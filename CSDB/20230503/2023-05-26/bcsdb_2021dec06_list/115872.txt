RES
1r:r1
REP
REP1:5o(4+1)2d=-1--1
RES
2b:a-lman-HEX-1:5|6:d
3b:b-dglc-HEX-1:5
4s:(r)-pyruvate
5b:b-dman-HEX-1:5
6s:acetyl
7s:acetyl
LIN
1:2o(4+1)3d
2:3o(6+2)4n
3:3o(4+2)4n
4:3o(3+1)5d
5:5o(2+1)6n
6:5o(3+1)7n