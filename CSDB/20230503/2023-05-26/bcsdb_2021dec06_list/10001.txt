RES
1r:r1
REP
REP1:7o(4+1)2d=-1--1
RES
2b:b-dgal-HEX-1:5
3s:n-acetyl
4b:b-dgal-HEX-1:5
5b:b-drib-PEN-1:4
6b:a-dgal-HEX-1:5|6:a
7b:a-dglc-HEX-1:5
8s:n-acetyl
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4o(4+1)5d
4:5o(2+1)6d
5:6o(4+1)7d
6:7d(2+1)8n