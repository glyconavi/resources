RES
1n:n1
2b:a-dglc-HEX-1:5
3b:a-dglc-HEX-1:5
4s:amino
5s:amino
6s:amino
LIN
1:1n(4+1)2d
2:1n(6+1)3d
3:2d(6+1)4n
4:3d(3+1)5n
5:3d(6+1)6n
NON
NON1
SmallMolecule:SMILES N[C@H]1{4}[C@H](O)[C@@H](O){6}[C@H](O)[C@@H](N)C1
Description:2-deoxy-streptamine