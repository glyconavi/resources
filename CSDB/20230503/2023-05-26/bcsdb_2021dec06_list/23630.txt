RES
1b:b-dgal-HEX-1:5
2n:n1
LIN
1:1o(6+1)2n
NON
NON1
Parent:1
Linkage:o(6+1)n
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:palmitic acid