RES
1n:n1
2s:pyrophosphate
3b:a-dgal-HEX-1:5
4b:a-lman-HEX-1:5|6:d
5b:b-dman-HEX-1:5
6b:a-dara-HEX-1:5|3:d|6:d
LIN
1:1n(1+1)2n
2:2n(1+1)3o
3:3o(3+1)4d
4:4o(4+1)5d
5:5o(3+1)6d
NON
NON1
SmallMolecule:SMILES [11CH3][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1CH2]O
Description:undecanol