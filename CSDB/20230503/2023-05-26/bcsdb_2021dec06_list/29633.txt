RES
1b:a-dglc-HEX-1:5
2s:n-acetyl
3s:phosphate
4b:b-dglc-HEX-1:5|6:a
5s:n-acetyl
6s:n-acetyl
LIN
1:1d(2+1)2n
2:1o(1+1)3n
3:1o(3+1)4d
4:4d(2+1)5n
5:4d(3+1)6n