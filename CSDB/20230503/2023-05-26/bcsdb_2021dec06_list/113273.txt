RES
1b:x-dglc-HEX-x:x
2b:a-dglc-HEX-1:5
3b:a-dglc-HEX-1:5
4b:a-dglc-HEX-1:5
LIN
1:1o(2+1)2d
2:2o(3+1)3d
3:3o(3+1)4d