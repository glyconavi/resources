RES
1b:o-dery-TET-0:0|1:aldi
2b:b-dman-HEX-1:5|6:a
3s:n-acetyl
4b:a-dglc-HEX-1:5
5s:n-acetyl
6b:a-dman-HEX-1:5
7s:n-acetyl
LIN
1:1o(2+1)2d
2:2d(2+1)3n
3:2o(4+1)4d
4:4d(2+1)5n
5:4o(4+1)6d
6:6d(2+1)7n