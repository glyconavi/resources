RES
1b:b-dara-HEX-1:5|1:d|2:d|6:d
2n:n1
LIN
1:1h(1+9)2n
NON
NON1
Parent:1
Linkage:h(1+9)n
SmallMolecule:SMILES Cc4cc(O)c3c(ccc2c(=O)c1c(O){9}cccc1c(=O)c23)c4