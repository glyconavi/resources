RES
1b:b-dara-HEX-1:5|1:d|2:d|6:d
2n:n1
3n:n2
LIN
1:1o(4+1)2n
2:1o(3+2)2n
3:1h(1+7)3n
NON
NON1
Parent:1
Linkage:o(4+1)n|o(3+2)n
SmallMolecule:SMILES C[C@@H]1O{1}[C@@H](O){2}[C@@H](O)CC1=O
Description:��-L-cinerulose
NON2
Parent:1
Linkage:h(1+7)n
SmallMolecule:SMILES CC(O)(CC(=O)O)Cc3ccc2c(=O)c1c(O){7}cccc1c(=O)c2c3O