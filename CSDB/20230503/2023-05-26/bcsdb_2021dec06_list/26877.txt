RES
1r:r1
REP
REP1:3o(2+1)2d=-1--1
RES
2b:a-dgal-HEX-1:5
3b:b-drib-PEN-1:4
4s:n-acetyl
LIN
1:2o(3+1)3d
2:2d(2+1)4n