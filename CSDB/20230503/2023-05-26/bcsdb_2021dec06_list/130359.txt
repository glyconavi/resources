RES
1b:o-xgro-TRI-0:0|1:aldi
2n:n1
3s:phosphate
4n:n2
5b:a-dglc-HEX-1:5
6s:amino
7b:a-dman-HEX-1:5
8b:a-dman-HEX-1:5
9b:a-dgal-HEX-1:4
10b:a-dgal-HEX-1:5
11b:a-dgal-HEX-1:5
12r:r1
13s:phosphate
14b:b-dgal-HEX-1:5
LIN
1:1o(1+1)2n
2:1o(3+1)3n
3:3n(1+1)4n
4:4n(6+1)5d
5:5d(2+1)6n
6:5o(4+1)7d
7:7o(3+1)8d
8:8o(3+1)9d
9:9o(3+1)10d
10:10o(6+1)11d
11:11o(6+1)12n
12:12n(6+1)13n
13:13n(1+1)14o
REP
REP1:17o(6+1)15n=16-16
RES
15s:phosphate
16b:a-dman-HEX-1:5
17b:b-dgal-HEX-1:5
LIN
1:15n(1+1)16o
2:16o(4+1)17d
NON
NON1
Parent:1
Linkage:o(1+1)n
HistoricalEntity:superclass: alcohol residue
NON2
Parent:3
Linkage:n(1+1)n
SmallMolecule:SMILES [6C@H](O)1[5C@H](O)[4C@@H](O)[3C@H](O)[2C@H](O)[1C@H](O)1
Description:myo-inositol