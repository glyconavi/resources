RES
1b:a-dman-OCT-2:6|1:a|2:keto|3:d
2b:a-lgro-dman-HEP-1:5
3b:a-lgro-dman-HEP-1:5
4s:diphospho-ethanolamine
5b:a-lgro-dman-HEP-1:5
6s:phosphate
7b:a-dglc-HEX-1:5
8b:a-dglc-HEX-1:5
LIN
1:1o(5+1)2d
2:2o(7+1)3d
3:2o(4+1)4n
4:2o(3+1)5d
5:5o(4+1)6n
6:5o(3+1)7d
7:7o(3+1)8d