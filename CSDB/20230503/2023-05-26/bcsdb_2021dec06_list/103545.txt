RES
1b:x-lman-HEX-1:5|6:d
2s:methyl
3s:methyl
4n:n1
5n:n2
6n:n3
7b:b-ltal-HEX-1:5|6:d
8n:n4
9n:n5
10b:a-lman-HEX-1:5|6:d
11b:a-lgal-HEX-1:5|6:d
12s:methyl
13b:a-lman-HEX-1:5|6:d
14s:methyl
LIN
1:1o(3+1)2n
2:1o(4+1)3n
3:1o(1+1)4n
4:4n(2+1)5n
5:5n(2+1)6n
6:6n(3+1)7d
7:6n(2+1)8n
8:8n(2+1)9n
9:7o(2+1)10d
10:10o(3+1)11d
11:11o(2+1)12n
12:11o(4+1)13d
13:13o(4+1)14n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES N{2}C(C){1}CO
Description:alaninol
NON2
Parent:4
Linkage:n(2+1)n
SmallMolecule:SMILES [3CH3][2CH](N)[1C](=O)O
Description:alanine
NON3
Parent:5
Linkage:n(2+1)n
SmallMolecule:SMILES [4CH3][3CH](O)[2CH](N)[1C](=O)O
Description:threonine
NON4
Parent:6
Linkage:n(2+1)n
SmallMolecule:SMILES [9CH]1=[8CH][7CH]=[6CH][5CH]=[4C]1[3CH2][2CH](N)[1C](=O)O
Description:phenylalanine
NON5
Parent:8
Linkage:n(2+1)n
SmallMolecule:SMILES CCCCCCCCCCCCCCCCCCC(O)C{1}C(O)=O
Description:3-hydroxy-Heneicosanoate