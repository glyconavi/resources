RES
1r:r1
REP
REP1:7o(4+1)2d=-1--1
RES
2b:b-lman-HEX-1:5|6:d
3s:acetyl
4b:a-dglc-HEX-1:5
5s:n-acetyl
6b:a-dglc-HEX-1:5|6:a
7b:b-dglc-HEX-1:5
8s:n-acetyl
LIN
1:2o(2+1)3n
2:2o(3+1)4d
3:4d(2+1)5n
4:4o(4+1)6d
5:6o(3+1)7d
6:7d(2+1)8n