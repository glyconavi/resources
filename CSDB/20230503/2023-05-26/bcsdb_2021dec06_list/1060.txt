RES
1b:x-dglc-HEX-1:5
2s:amino
3n:n1
4b:b-dglc-HEX-1:5
5s:phosphate
6s:amino
7n:n2
LIN
1:1d(2+1)2n
2:1o(2+1)3n
3:1o(6+1)4d
4:4o(4+1)5n
5:4d(2+1)6n
6:4o(2+1)7n
NON
NON1
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON2
Parent:4
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid