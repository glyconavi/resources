RES
1b:a-lman-HEX-1:5|6:d
2s:methyl
3b:a-lman-HEX-1:5|6:d
4s:methyl
5b:a-lgal-HEX-1:5|6:d
6s:methyl
LIN
1:1o(2+1)2n
2:1o(3+1)3d
3:3o(2+1)4n
4:3o(3+1)5d
5:5o(2+1)6n