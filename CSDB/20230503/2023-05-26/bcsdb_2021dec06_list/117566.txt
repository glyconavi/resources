RES
1b:a-dglc-HEX-1:5
2n:n1
3n:n2
4b:a-dglc-HEX-1:5
5n:n3
6b:b-dglc-HEX-1:5
7b:b-dglc-HEX-1:5
8b:b-dglc-HEX-1:5
9s:pyruvate
LIN
1:1o(3+1)2n
2:1o(4+1)3n
3:1o(1+1)4d
4:4o(2+1)5n
5:4o(6+1)6d
6:6o(4+1)7d
7:7o(3+1)8d
8:7o(6+2)9n
9:7o(4+2)9n
NON
NON1
Parent:1
Linkage:o(3+1)n
HistoricalEntity:superclass: lipid residue
NON2
Parent:1
Linkage:o(4+1)n
HistoricalEntity:superclass: lipid residue
NON3
Parent:4
Linkage:o(2+1)n
SmallMolecule:SMILES [22CH3][21CH2][20CH2][19CH2][18CH2][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:behenic acid