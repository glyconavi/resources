RES
1b:a-dglc-HEX-1:5
2s:n-acetyl
3b:a-dgal-HEX-1:5|6:a
4n:n1
5b:a-dgal-HEX-1:5|6:a
6b:a-dgal-HEX-1:5|6:a
7a:a1
LIN
1:1d(2+1)2n
2:1o(3+1)3d
3:3o(6+2)4n
4:3o(3+1)5d
5:5o(4+1)6d
6:6o(6+2)7n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:8
RES
8n:n2
ALTSUBGRAPH2
LEAD-IN RES:9
RES
9n:n3
NON
NON1
Parent:3
Linkage:o(6+2)n
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine
NON2
SmallMolecule:SMILES [3CH2](O)[2C@@H](N)[1C](=O)O
Description:(D)-serine
NON3
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine