RES
1b:a-dglc-HEX-1:5
2a:a1
3s:phosphate
4s:amino
5n:n1
6b:b-dglc-HEX-1:5
7s:amino
8s:phosphate
9n:n2
10n:n3
11n:n4
12n:n5
13b:x-lara-PEN-1:5
14s:amino
LIN
1:1o(2+1)2n
2:1o(1+1)3n
3:1d(2+1)4n
4:1o(3+1)5n
5:1o(6+1)6d
6:6d(2+1)7n
7:6o(4+1)8n
8:6o(3+1)9n
9:6o(2+1)10n
10:10n(3+1)11n
11:9n(3+1)12n
12:8n(1+1)13o
13:13d(4+1)14n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:15
RES
15n:n6
ALTSUBGRAPH2
LEAD-IN RES:16
RES
16n:n7
NON
NON1
Parent:1
Linkage:o(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-tetradecanoic acid
NON2
Parent:6
Linkage:o(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-tetradecanoic acid
NON3
Parent:6
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-tetradecanoic acid
NON4
Parent:10
Linkage:n(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-tetradecanoic acid
NON5
Parent:9
Linkage:n(3+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-tetradecanoic acid
NON6
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-tetradecanoic acid
NON7
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-hexadecanoic acid