RES
1b:a-dglc-HEX-1:5
2s:phosphate
3s:amino
4n:n1
5n:n2
6b:b-dglc-HEX-1:5
7s:phosphate
8s:amino
9n:n3
10s:phosphate
11n:n4
12n:n5
13a:a1
14n:n6
15b:o-xgro-TRI-0:0|1:aldi
16n:n7
17n:n8
LIN
1:1o(1+1)2n
2:1d(2+1)3n
3:1o(2+1)4n
4:1o(3+1)5n
5:1o(6+1)6d
6:6o(4+1)7n
7:6d(2+1)8n
8:4n(3+1)9n
9:5n(3+1)10n
10:6o(2+1)11n
11:6o(3+1)12n
12:11n(3+1)13n
13:12n(3+1)14n
14:10n(1+3)15o
15:15o(1+1)16n
16:15o(2+1)17n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:18
RES
18n:n9
ALTSUBGRAPH2
LEAD-IN RES:19
RES
19n:n10
NON
NON1
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-tetradecanoic acid
NON2
Parent:1
Linkage:o(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-dodecanoic acid
NON3
Parent:4
Linkage:n(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:lauric acid
NON4
Parent:6
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2]/[5CH]=[4CH]\[3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-tetradec-4Z-enoic acid
NON5
Parent:6
Linkage:o(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-dodecanoic acid
NON6
Parent:12
Linkage:n(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:lauric acid
NON7
Parent:15
Linkage:o(1+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:lauric acid
NON8
Parent:15
Linkage:o(2+1)n
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:palmitic acid
NON9
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-tetradecanoic acid
NON10
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:myristic acid