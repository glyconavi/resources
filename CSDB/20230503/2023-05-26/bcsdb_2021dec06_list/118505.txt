RES
1b:o-dglc-HEX-0:0|1:aldi
2b:a-dgal-HEX-1:5
3b:a-dglc-HEX-1:5
4b:b-dglc-HEX-1:5
5b:b-dglc-HEX-1:5
LIN
1:1o(6+1)2d
2:2o(6+1)3d
3:3o(6+1)4d
4:4o(4+1)5d