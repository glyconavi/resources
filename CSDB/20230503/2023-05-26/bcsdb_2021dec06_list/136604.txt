RES
1b:b-dglc-HEX-1:5
2s:n-acetyl
3s:(r)-carboxyethyl
4b:b-dglc-HEX-1:5
5n:n1
6s:n-acetyl
7n:n2
8n:n3
9n:n4
10n:n5
11n:n6
12n:n7
13n:n8
14n:n9
15n:n10
16n:n11
17n:n12
18b:x-dglc-HEX-x:x
19b:x-dglc-HEX-x:x
20s:n-acetyl
21s:(r)-carboxyethyl
22s:(r)-carboxyethyl
23s:n-acetyl
24b:b-dglc-HEX-1:5
25b:b-dglc-HEX-1:5
26s:n-acetyl
27s:n-acetyl
LIN
1:1d(2+1)2n
2:1o(3+1)3n
3:1o(4+1)4d
4:1o(8+2)5n
5:4d(2+1)6n
6:5n(1+2)7n
7:7n(5+6)8n
8:8n(2+1)9n
9:9n(2+-1)10n
10:9n(2+-1)11n
11:10n(6+1)12n
12:11n(6+1)13n
13:10n(2+5)14n
14:11n(2+5)15n
15:14n(2+1)16n
16:15n(2+1)17n
17:16n(2+8)18d
18:17n(2+8)19d
19:18d(2+1)20n
20:18o(3+1)21n
21:19o(3+1)22n
22:19d(2+1)23n
23:19o(4+1)24d
24:18o(4+1)25d
25:25d(2+1)26n
26:24d(2+1)27n
NON
NON1
Parent:1
Linkage:o(8+2)n
SmallMolecule:SMILES [3CH3][2CH](N)[1C](=O)O
Description:alanine
NON2
Parent:5
Linkage:n(1+2)n
SmallMolecule:SMILES [5C](=O)(O)[4CH2][3CH2][2CH](N)[1C](=O)O
Description:glutamic acid
NON3
Parent:7
Linkage:n(5+6)n
SmallMolecule:SMILES [7C](=O)(O)[6C@H](N)[5CH2][4CH2][3CH2][2C@@H](N)[1C](=O)O
Description:diaminopimelic acid
NON4
Parent:8
Linkage:n(2+1)n
SmallMolecule:SMILES [3CH3][2CH](N)[1C](=O)O
Description:alanine
NON5
Parent:9
Linkage:n(2+-1)n
SmallMolecule:SMILES [7C](=O)(O)[6C@H](N)[5CH2][4CH2][3CH2][2C@@H](N)[1C](=O)O
Description:diaminopimelic acid
NON6
Parent:9
Linkage:n(2+-1)n
SmallMolecule:SMILES [7C](=O)(O)[6C@H](N)[5CH2][4CH2][3CH2][2C@@H](N)[1C](=O)O
Description:diaminopimelic acid
NON7
Parent:10
Linkage:n(6+1)n
SmallMolecule:SMILES [3CH3][2CH](N)[1C](=O)O
Description:alanine
NON8
Parent:11
Linkage:n(6+1)n
SmallMolecule:SMILES [3CH3][2CH](N)[1C](=O)O
Description:alanine
NON9
Parent:10
Linkage:n(2+5)n
SmallMolecule:SMILES [5C](=O)(O)[4CH2][3CH2][2CH](N)[1C](=O)O
Description:glutamic acid
NON10
Parent:11
Linkage:n(2+5)n
SmallMolecule:SMILES [5C](=O)(O)[4CH2][3CH2][2CH](N)[1C](=O)O
Description:glutamic acid
NON11
Parent:14
Linkage:n(2+1)n
SmallMolecule:SMILES [3CH3][2CH](N)[1C](=O)O
Description:alanine
NON12
Parent:15
Linkage:n(2+1)n
SmallMolecule:SMILES [3CH3][2CH](N)[1C](=O)O
Description:alanine