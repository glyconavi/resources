RES
1b:x-dman-HEX-x:x
2b:b-dgal-HEX-1:5
3b:b-dglc-HEX-1:5
4s:phosphate
LIN
1:1o(4+1)2d
2:2o(3+1)3d
3:3o(6+1)4n