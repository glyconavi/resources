RES
1b:b-dglc-HEX-1:5
2s:n-acetyl
3n:n1
4b:b-dglc-HEX-1:5
5s:n-acetyl
6b:a-dman-OCT-2:6|1:a|2:keto|3:d
7b:a-dman-OCT-2:6|1:a|2:keto|3:d
8b:a-dman-OCT-2:6|1:a|2:keto|3:d
LIN
1:1d(2+1)2n
2:1o(1+1)3n
3:1o(6+1)4d
4:4d(2+1)5n
5:4o(6+2)6d
6:6o(4+2)7d
7:7o(8+2)8d
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES [3CH2]=[2CH][1CH2]O
Description:allyl alcohol