RES
1b:a-lman-HEX-1:5|6:d
2n:n1
3b:b-dglc-HEX-1:5
4s:n-acetyl
LIN
1:1o(1+1)2n
2:1o(2+1)3d
3:3d(2+1)4n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES [1*]
Description:branched 9-hydroxy-decanoic acid