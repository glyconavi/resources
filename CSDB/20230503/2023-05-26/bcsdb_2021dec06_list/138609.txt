RES
1b:a-lman-HEX-1:5|6:d
2b:a-lman-HEX-1:5|6:d
3n:n1
LIN
1:1o(3+1)2d
2:1o(1+1)3n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES [1*]
Description:branched 9-hydroxy-decanoic acid