RES
1r:r1
REP
REP1:11o(2+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5|6:d
3s:amino
4s:n-acetyl
5n:n1
6b:a-dglc-HEX-1:5|6:d
7s:n-acetyl
8s:amino
9n:n2
10b:a-dgal-HEX-1:5|6:a
11b:a-lman-HEX-1:5|6:d
12s:n-acetyl
LIN
1:2d(4+1)3n
2:2d(2+1)4n
3:2o(4+1)5n
4:2o(3+1)6d
5:6d(2+1)7n
6:6d(4+1)8n
7:6o(4+1)9n
8:6o(3+1)10d
9:10o(4+1)11d
10:10d(2+1)12n
NON
NON1
Parent:2
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH3][3C@H](O)[2CH2][1C](=O)O
Description:(S)-3-hydroxybutanoic acid
NON2
Parent:6
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH3][3C@H](O)[2CH2][1C](=O)O
Description:(S)-3-hydroxybutanoic acid