RES
1r:r1
REP
REP1:6o(3+1)2d=-1--1
RES
2b:b-dgal-HEX-1:5
3s:n-acetyl
4b:b-dglc-HEX-1:5
5b:b-dgal-HEX-1:5
6b:a-dgal-HEX-1:5
7n:n1
LIN
1:2d(2+1)3n
2:2o(4+1)4d
3:2o(3+1)5d
4:5o(3+1)6d
5:4o(4+2)7n
NON
NON1
Parent:4
Linkage:o(4+2)n
SmallMolecule:SMILES C[C@H](C{2}[C@H]1O)OC1=O
Description:(2R,4R)-2,4-dihydroxypentanoic acid 1,4-lactone