RES
1n:n1
2n:n2
3b:a-lman-HEX-1:5|6:d
4s:methyl
5b:a-lman-HEX-1:5|6:d
6b:a-lgal-HEX-1:5|6:d
7s:methyl
8s:methyl
9s:methyl
LIN
1:1n(1+7)2n
2:2n(4+1)3d
3:3o(2+1)4n
4:3o(3+1)5d
5:5o(3+1)6d
6:6o(2+1)7n
7:6o(4+1)8n
8:6o(3+1)9n
NON
NON1
SmallMolecule:SMILES [1CH3]O
Description:methyl
NON2
Parent:1
Linkage:n(1+7)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3CH][2CH]=[1C]1[7C](=O)O
Description:4-hydroxybenzoic acid