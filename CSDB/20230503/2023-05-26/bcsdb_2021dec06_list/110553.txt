RES
1b:a-lgro-lman-NON-2:6|1:a|2:keto|3:d|9:d
2s:n-formyl
3s:amino
4n:n1
5b:b-dgal-HEX-1:5|6:d
6b:b-dxyl-PEN-1:5
7s:acetyl
8s:n-acetyl
LIN
1:1d(7+1)2n
2:1d(5+1)3n
3:1o(5+1)4n
4:1o(4+1)5d
5:5o(3+1)6d
6:5o(4+1)7n
7:5d(2+1)8n
NON
NON1
Parent:1
Linkage:o(5+1)n
SmallMolecule:SMILES [4CH3][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxybutanoic acid