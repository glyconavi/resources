RES
1b:x-dglc-HEX-1:5
2s:amino
3n:n1
4b:b-dglc-HEX-1:5
5s:pyrophosphate
6s:amino
7n:n2
8n:n3
9n:n4
10n:n5
LIN
1:1d(2+1)2n
2:1o(2+1)3n
3:1o(6+1)4d
4:4o(4+1)5n
5:4d(2+1)6n
6:4o(3+1)7n
7:3n(3+1)8n
8:4o(2+1)9n
9:9n(3+1)10n
NON
NON1
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-dodecanoic acid
NON2
Parent:4
Linkage:o(3+1)n
SmallMolecule:SMILES [10CH3][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-decanoic acid
NON3
Parent:3
Linkage:n(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH](O)[1C](=O)O
Description:2-hydroxy-dodecanoic acid
NON4
Parent:4
Linkage:o(2+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-dodecanoic acid
NON5
Parent:9
Linkage:n(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:lauric acid