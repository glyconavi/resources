RES
1b:a-dxyl-HEX-1:5|3:d
2s:methyl
3b:a-lman-HEX-1:5|6:d
4b:a-lman-HEX-1:5|6:d
LIN
1:1o(1+1)2n
2:1o(2+1)3d
3:3o(3+1)4d