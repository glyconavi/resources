RES
1r:r1
REP
REP1:7n(4+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5|6:d
3s:acetyl
4s:n-acetyl
5s:amino
6n:n1
7n:n2
8s:acetyl
9n:n3
LIN
1:2o(3+1)3n
2:2d(2+1)4n
3:2d(4+1)5n
4:2o(4+5)6n
5:6n(2+2)7n
6:7n(5+1)8n
7:7n(7+1)9n
NON
NON1
Parent:2
Linkage:o(4+5)n
SmallMolecule:SMILES [5C](=O)(O)[4CH2][3CH2][2C@H](O)[1C](=O)(O)
Description:(S)-2-hydroxyglutaric acid
NON2
Parent:6
Linkage:n(2+2)n
SmallMolecule:SMILES CC({7}[C@@H]([C@@H]1O{2}[C@@](C(O)=O)(C{4}[C@@H]({5}[C@@H]1N)O)O)N)N
Description:5,7,8-triamino-3,5,7,8,9-pentadeoxy-?-glycero-L-manno-non-2-ulosonic acid
NON3
Parent:7
Linkage:n(7+1)n
SmallMolecule:SMILES [4CH3][3CH2][2C@H](O)[1C](=O)O
Description:(S)-2-hydroxybutanoic acid