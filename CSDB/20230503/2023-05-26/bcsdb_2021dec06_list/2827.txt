RES
1r:r1
REP
REP1:3o(7+1)2n=-1--1
RES
2n:n1
3b:a-lgro-lman-NON-2:6|1:a|2:keto|3:d|9:d
4s:n-acetyl
5s:amino
LIN
1:2n(3+2)3d
2:3d(5+1)4n
3:3d(7+1)5n
NON
NON1
SmallMolecule:SMILES [4CH3][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxybutanoic acid