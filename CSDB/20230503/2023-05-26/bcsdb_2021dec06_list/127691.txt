RES
1b:x-dgal-HEX-x:x
2b:b-dgal-HEX-1:5
3s:n-acetyl
4b:b-dgal-HEX-1:4
5b:b-dglc-HEX-1:5
6b:b-dman-HEX-1:5|6:d
7b:a-dgal-HEX-1:5
8s:n-acetyl
LIN
1:1o(3+1)2d
2:2d(2+1)3n
3:2o(6+1)4d
4:4o(6+1)5d
5:5o(4+1)6d
6:6o(3+1)7d
7:7d(2+1)8n