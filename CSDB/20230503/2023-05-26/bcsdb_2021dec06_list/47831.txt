RES
1b:o-xgro-TRI-0:0|1:aldi
2b:a-dman-HEX-1:5
3b:a-dman-HEX-1:5
4n:n1
LIN
1:1o(3+1)2d
2:2o(3+1)3d
3:2o(6+1)4n
NON
NON1
Parent:2
Linkage:o(6+1)n
SmallMolecule:SMILES [14CH3][13CH]([15CH3])[12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:iso-pentadecanoic acid