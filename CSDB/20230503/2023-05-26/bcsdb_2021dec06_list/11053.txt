RES
1r:r1
REP
REP1:6o(4+1)2d=-1--1
RES
2b:b-dgal-HEX-1:5
3s:n-acetyl
4b:a-ltal-HEX-1:5|6:d
5s:acetyl
6b:a-dman-HEX-1:5
7b:a-ltal-HEX-1:5|6:d
8s:acetyl
LIN
1:2d(2+1)3n
2:2o(3+1)4d
3:4o(2+1)5n
4:4o(3+1)6d
5:6o(3+1)7d
6:7o(-1+1)8n