RES
1b:a-lman-HEX-1:5|6:d
2n:n1
3b:a-dgal-HEX-1:5
4b:a-lman-HEX-1:5|6:d
LIN
1:1o(1+1)2n
2:1o(3+1)3d
3:3o(2+1)4d
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES [3CH3][2CH2][1CH2]O
Description:propanol