RES
1r:r1
REP
REP1:8o(6+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3b:a-lgal-HEX-1:5|6:d
4s:n-acetyl
5b:b-dglc-HEX-1:5|6:a
6b:b-dgal-HEX-1:5
7s:n-acetyl
8b:b-dglc-HEX-1:5
9b:a-lgal-HEX-1:5|6:d
LIN
1:2o(4+1)3d
2:2d(2+1)4n
3:2o(3+1)5d
4:5o(4+1)6d
5:6d(2+1)7n
6:6o(3+1)8d
7:8o(2+1)9d