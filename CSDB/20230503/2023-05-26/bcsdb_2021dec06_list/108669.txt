RES
1r:r1
REP
REP1:5o(3+1)2d=-1--1
RES
2b:a-dgal-HEX-1:5
3b:a-lman-HEX-1:5|6:d
4b:a-lman-HEX-1:5|6:d
5b:b-dglc-HEX-1:5
6b:a-dgal-HEX-1:5|6:d
7s:amino
8s:n-acetyl
9n:n1
LIN
1:2o(3+1)3d
2:3o(3+1)4d
3:4o(3+1)5d
4:4o(2+1)6d
5:6d(4+1)7n
6:5d(2+1)8n
7:6o(4+1)9n
NON
NON1
Parent:6
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH3][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxybutanoic acid