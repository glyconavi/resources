RES
1b:b-dgal-HEX-1:5|6:d
2s:methyl
3s:methyl
4s:amino
5n:n1
LIN
1:1o(3+1)2n
2:1o(1+1)3n
3:1d(4+1)4n
4:1o(4+1)5n
NON
NON1
Parent:1
Linkage:o(4+1)n
SmallMolecule:SMILES [5C](=O)1[4CH](O)[3C]([6CH3])(O)[2C@@H](N1C)[1C](=O)(O)
Description:(D)-3,4-dihydroxy-N,3-dimethyl-5-oxoproline