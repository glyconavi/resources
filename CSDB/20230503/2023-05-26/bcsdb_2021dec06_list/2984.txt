RES
1b:a-dman-OCT-2:6|1:a|2:keto|3:d
2n:n1
3b:b-dman-OCT-2:6|1:a|2:keto|3:d
4b:a-dman-OCT-2:6|1:a|2:keto|3:d
LIN
1:1o(2+1)2n
2:1o(4+2)3d
3:3o(8+2)4d
NON
NON1
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [3CH2]=[2CH][1CH2]O
Description:allyl alcohol