RES
1n:n1
2n:n2
3b:a-lman-HEX-1:5|6:d
4s:methyl
5s:methyl
6b:a-lman-HEX-1:5|6:d
7n:n3
8s:methyl
9s:methyl
10b:a-lman-HEX-1:5|6:d
11n:n4
12s:methyl
13n:n5
14s:methyl
LIN
1:1n(2+1)2n
2:1n(1+1)3d
3:3o(4+1)4n
4:3o(3+1)5n
5:3o(2+1)6d
6:2n(2+1)7n
7:6o(3+1)8n
8:6o(4+1)9n
9:7n(3+1)10d
10:7n(2+1)11n
11:10o(3+1)12n
12:11n(2+1)13n
13:13n(3+1)14n
NON
NON1
SmallMolecule:SMILES N{2}C(C){1}CO
Description:alaninol
NON2
Parent:1
Linkage:n(2+1)n
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine
NON3
Parent:2
Linkage:n(2+1)n
SmallMolecule:SMILES [4CH3][3C@@H](O)[2C@@H](N)[1C](=O)O
Description:(D)-allothreonine
NON4
Parent:7
Linkage:n(2+1)n
SmallMolecule:SMILES [9CH]1=[8CH][7CH]=[6CH][5CH]=[4C]1[3CH2][2C@@H](N)[1C](=O)O
Description:(D)-phenylalanine
NON5
Parent:11
Linkage:n(2+1)n
SmallMolecule:SMILES [26CH3][25CH2][24CH2][23CH2][22CH2][21CH2][20CH2][19CH2][18CH2][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-hexacosanoic acid