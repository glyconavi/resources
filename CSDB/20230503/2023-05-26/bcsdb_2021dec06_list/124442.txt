RES
1b:o-xgro-TRI-0:0|1:aldi
2b:b-dgal-HEX-1:5
3s:n-acetyl
4b:b-dglc-HEX-1:5
5b:b-dgal-HEX-1:5
6s:n-acetyl
7s:phosphate
8n:n1
LIN
1:1o(-1+1)2d
2:2d(2+1)3n
3:2o(3+1)4d
4:4o(4+1)5d
5:5d(2+1)6n
6:5o(3+1)7n
7:7n(1+1)8n
NON
NON1
Parent:7
Linkage:n(1+1)n
SmallMolecule:SMILES [2CH2](O)[1CH2](O)
Description:ethylene glycol