RES
1n:n1
2n:n2
3b:a-lman-HEX-1:5|6:d
4s:methyl
5s:methyl
6n:n3
7b:a-ltal-HEX-x:x|6:d
8n:n4
9n:n5
10b:a-lman-HEX-1:5|6:d
11b:a-lgal-HEX-1:5|6:d
12s:methyl
13s:methyl
LIN
1:1n(2+1)2n
2:1n(1+1)3d
3:3o(3+1)4n
4:3o(4+1)5n
5:2n(2+1)6n
6:6n(3+1)7d
7:6n(2+1)8n
8:8n(2+1)9n
9:7o(2+1)10d
10:10o(3+1)11d
11:11o(2+1)12n
12:11o(3+1)13n
NON
NON1
SmallMolecule:SMILES N{2}C(C){1}CO
Description:alaninol
NON2
Parent:1
Linkage:n(2+1)n
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine
NON3
Parent:2
Linkage:n(2+1)n
SmallMolecule:SMILES [4CH3][3C@@H](O)[2C@@H](N)[1C](=O)O
Description:(D)-allothreonine
NON4
Parent:6
Linkage:n(2+1)n
SmallMolecule:SMILES [9CH]1=[8CH][7CH]=[6CH][5CH]=[4C]1[3CH2][2C@@H](N)[1C](=O)O
Description:(D)-phenylalanine
NON5
Parent:8
Linkage:n(2+1)n
HistoricalEntity:superclass: lipid residue