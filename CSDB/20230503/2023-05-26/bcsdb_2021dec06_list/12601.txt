RES
1r:r1
REP
REP1:8o(2+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5|6:d
3s:n-acetyl
4s:amino
5n:n1
6b:a-lgal-HEX-1:5|6:d
7s:n-acetyl
8b:a-dgro-dman-HEP-1:5
9b:a-lara-HEX-x:x|3:d|6:d
LIN
1:2d(2+1)3n
2:2d(4+1)4n
3:2o(4+1)5n
4:2o(3+1)6d
5:6d(2+1)7n
6:6o(4+1)8d
7:8o(3+1)9d
NON
NON1
Parent:2
Linkage:o(4+1)n
SmallMolecule:SMILES [6CH3][5CH](O)[4CH2][3CH](O)[2CH2][1C](=O)O
Description:3,5-dihydroxyhexanoic acid