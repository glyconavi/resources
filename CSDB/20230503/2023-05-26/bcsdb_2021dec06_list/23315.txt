RES
1b:a-dgal-HEX-1:5
2b:b-dara-HEX-2:5|1:aldi|2:keto
3n:n1
LIN
1:1o(6+2)2d
2:1o(1+6)3n
NON
NON1
Parent:1
Linkage:o(1+6)n
HistoricalEntity:product of alkaline degradation of the aDGalp(1-3)