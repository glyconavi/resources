RES
1b:a-dman-OCT-2:6|1:a|2:keto|3:d
2b:x-dman-OCT-2:6|1:a|2:keto|3:d
3b:a-lgro-dman-HEP-1:5
4s:phospho-ethanolamine
5s:pyrophosphate
6b:a-lgro-dman-HEP-1:5
7s:pyrophosphate
8n:n1
9b:a-dgal-HEX-1:5
10s:amino
11n:n2
LIN
1:1o(4+2)2d
2:1o(5+1)3d
3:3o(2+1)4n
4:3o(4+1)5n
5:3o(3+1)6d
6:6o(6+1)7n
7:6o(7+1)8n
8:6o(3+1)9d
9:9d(2+1)10n
10:9o(2+1)11n
NON
NON1
Parent:6
Linkage:o(7+1)n
SmallMolecule:SMILES [1C](=O)(O)N
Description:carbamic acid
NON2
Parent:9
Linkage:o(2+1)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine