RES
1n:n1
2n:n2
3b:b-dglc-HEX-1:5
LIN
1:1n(1+2)2n
2:2n(1+1)3d
NON
NON1
SmallMolecule:SMILES C[C@@H]1O{1}[C@@H](O)C[C@](C)(N)C1=O
Description:3-amino-2,3,6-trideoxy-3-C-methyl-L-lyxo-hexos-4-ulose (dehydrovancosamine)
NON2
Parent:1
Linkage:n(1+2)n
SmallMolecule:SMILES CN[C@H](CC(C)C)C(=O)N[C@H]6C(=O)N[C@@H](CC(N)=O)C(=O)N[C@@H]4C(=O)N[C@H]2C(=O)N[C@H](C(=O)N[C@H](C(=O)O)c1cc(O)cc(O)c1c3cc2ccc3O){2}[C@H:1](O)c8ccc(Oc7cc4cc(Oc5ccc(cc5Cl)[C@H]6O){1}c7O)c(Cl)c8
Description:balhimycin aglycon