RES
1b:x-dman-OCT-x:x|1:a|2:keto|3:d
2b:b-lara-PEN-1:5
3b:a-lgro-dman-HEP-1:5
4b:b-dglc-HEX-1:5
5s:amino
6b:a-lgro-dman-HEP-1:5
7s:phospho-ethanolamine
8b:a-dgal-HEX-1:5|6:a
9b:a-lgro-dman-HEP-1:5
10b:b-dgal-HEX-1:5|6:a
11b:a-dglc-HEX-1:5
12b:a-dgro-dman-HEP-1:5
13s:amino
LIN
1:1o(8+1)2d
2:1o(5+1)3d
3:3o(4+1)4d
4:2d(4+1)5n
5:3o(3+1)6d
6:6o(6+1)7n
7:6o(3+1)8d
8:6o(7+1)9d
9:9o(7+1)10d
10:8o(4+1)11d
11:11o(6+1)12d
12:11d(2+1)13n