RES
1r:r1
REP
REP1:6o(4+1)2d=-1--1
RES
2b:b-dman-HEX-1:5
3a:a1
4b:b-dglc-HEX-1:5|6:a
5b:a-lman-HEX-1:5|6:d
6b:b-dglc-HEX-1:5
7s:carboxyethyl
LIN
1:2o(-1+-1)3n
2:2o(4+1)4d
3:4o(3+1)5d
4:5o(4+1)6d
5:5o(3+2)7n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:8
RES
8s:acetyl
ALTSUBGRAPH2
LEAD-IN RES:9
RES
9s:acetyl