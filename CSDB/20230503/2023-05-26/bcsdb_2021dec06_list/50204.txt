RES
1n:n1
2s:phosphate
3b:b-drib-PEN-1:4
LIN
1:1n(1+1)2n
2:2n(1+1)3o
NON
NON1
SmallMolecule:SMILES C/C(C)=C\CC/C(C)=C/CC/C(C)=C\CC/C(C)=C\CC/C(C)=C\CC/C(C)=C\CC/C(C)=C\CC/C(C)=C\CC/C(C)=C\CC/C(C)=C\{1}CO
Description:decaprenol