RES
1n:n1
2b:a-lman-HEX-1:5|6:d
3n:n2
4s:methyl
5n:n3
6b:a-ltal-HEX-1:5|6:d
7n:n4
8b:a-lman-HEX-1:5|6:d
9n:n5
LIN
1:1n(1+1)2d
2:1n(2+1)3n
3:2o(3+1)4n
4:3n(2+1)5n
5:5n(3+1)6d
6:5n(2+1)7n
7:6o(2+1)8d
8:7n(2+1)9n
NON
NON1
SmallMolecule:SMILES N{2}[C@@H](C){1}CO
Description:L-alaninol
NON2
Parent:1
Linkage:n(2+1)n
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine
NON3
Parent:3
Linkage:n(2+1)n
SmallMolecule:SMILES [4CH3][3C@@H](O)[2C@@H](N)[1C](=O)O
Description:(D)-allothreonine
NON4
Parent:5
Linkage:n(2+1)n
SmallMolecule:SMILES [9CH]1=[8CH][7CH]=[6CH][5CH]=[4C]1[3CH2][2C@@H](N)[1C](=O)O
Description:(D)-phenylalanine
NON5
Parent:7
Linkage:n(2+1)n
HistoricalEntity:superclass: lipid residue