RES
1n:n1
2s:pyrophosphate
3b:a-dglc-HEX-1:5
4n:n2
5n:n3
6b:b-dglc-HEX-1:5
7s:phosphate
8n:n4
9n:n5
10n:n6
11n:n7
12n:n8
13n:n9
LIN
1:1n(1+1)2n
2:2n(1+1)3o
3:3o(2+1)4n
4:3o(3+1)5n
5:3o(6+1)6d
6:6o(4+1)7n
7:4n(3+1)8n
8:5n(3+1)9n
9:6o(6+2)10n
10:6o(3+1)11n
11:6o(2+1)12n
12:12n(3+1)13n
NON
NON1
SmallMolecule:SMILES [2CH2](N)[1CH2](O)
Description:ethanolamine
NON2
Parent:3
Linkage:o(2+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-dodecanoic acid
NON3
Parent:3
Linkage:o(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-dodecanoic acid
NON4
Parent:4
Linkage:n(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:lauric acid
NON5
Parent:5
Linkage:n(3+1)n
SmallMolecule:SMILES [10CH3][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:capric acid
NON6
Parent:6
Linkage:o(6+2)n
HistoricalEntity:core oligosaccharide
NON7
Parent:6
Linkage:o(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-dodecanoic acid
NON8
Parent:6
Linkage:o(2+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-dodecanoic acid
NON9
Parent:12
Linkage:n(3+1)n
SmallMolecule:SMILES [10CH3][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:capric acid