RES
1b:b-dall-HEX-1:4|6:a
2s:amino
3s:amino
4n:n1
5n:n2
6n:n3
LIN
1:1d(5+1)2n
2:1d(1+1)3n
3:1o(6+2)4n
4:1o(1+1)5n
5:1o(5+1)6n
NON
NON1
Parent:1
Linkage:o(6+2)n
SmallMolecule:SMILES [5C](=O)(O)[4CH2][3CH2][2C@H](N)[1C](=O)O
Description:(L)-glutamic acid
NON2
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES Cc1c{1}[nH]c(=O)[nH]c1=O
Description:5-methyluracil
NON3
Parent:1
Linkage:o(5+1)n
SmallMolecule:SMILES CC(C(N){1}C(=O)O)C(O)c1ccc(O)cn1