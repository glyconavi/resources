RES
1b:b-dall-HEX-1:5|1:d
2s:acetyl
3n:n1
4n:n2
5b:a-llyx-HEX-1:5|2:d|6:d
6s:methyl
7n:n3
8n:n4
LIN
1:1o(6+1)2n
2:1o(4+1)3n
3:1h(1+5)4n
4:1o(3+1)5d
5:5o(3+1)6n
6:5o(7+1)7n
7:5h(4+1)8n
NON
NON1
Parent:1
Linkage:o(4+1)n
SmallMolecule:SMILES C/C=C(N=C=S)/{1}C(=O)O
Description:paulic acid
NON2
Parent:1
Linkage:h(1+5)n
SmallMolecule:SMILES N/C1=C(C(=O)O)/C(=O){5}C(O)CC1=O
NON3
Parent:5
Linkage:o(7+1)n
SmallMolecule:SMILES [3CH3][2CH]([4CH3])[1C](=O)O
Description:isobutyric acid
NON4
Parent:5
Linkage:h(4+1)n
SmallMolecule:SMILES [1C](O)C
Description:hydroxyethyl