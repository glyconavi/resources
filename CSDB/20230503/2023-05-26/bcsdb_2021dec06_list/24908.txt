RES
1r:r1
REP
REP1:9o(4+1)2n=-1--1
RES
2s:phosphate
3n:n1
4b:b-dglc-HEX-1:5|6:d
5s:n-acetyl
6s:amino
7n:n2
8b:a-lgal-HEX-1:5|6:d
9b:a-dglc-HEX-1:5
10s:amino
11n:n3
LIN
1:2n(1+5)3n
2:3n(4+1)4d
3:4d(2+1)5n
4:4d(4+1)6n
5:4o(4+1)7n
6:4o(3+1)8d
7:8o(3+1)9d
8:8d(2+1)10n
9:8o(2+1)11n
NON
NON1
Parent:2
Linkage:n(1+5)n
HistoricalEntity:2-amino-2-deoxy-2-C-methyl-pentonic acid
NON2
Parent:4
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH3][3C@H](O)[2CH2][1C](=O)O
Description:(S)-3-hydroxybutanoic acid
NON3
Parent:8
Linkage:o(2+1)n
SmallMolecule:SMILES [2CH3][1C](=N)O
Description:acetimidic acid