RES
1r:r1
REP
REP1:2o(4+1)2d=-1--1
RES
2b:b-dgal-HEX-1:5|6:a
3s:n-acetyl
4n:n1
LIN
1:2d(2+1)3n
2:2o(6+2)4n
NON
NON1
Parent:2
Linkage:o(6+2)n
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine