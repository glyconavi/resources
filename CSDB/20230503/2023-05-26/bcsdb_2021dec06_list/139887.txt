RES
1b:x-dara-PEN-1:4
2b:x-dara-PEN-1:4
3b:x-dara-PEN-1:4
4b:x-dara-PEN-1:4
5n:n1
LIN
1:1o(-1+1)2d
2:1o(-1+1)3d
3:3o(5+1)4d
4:4o(5+1)5n
NON
NON1
Parent:4
Linkage:o(5+1)n
HistoricalEntity:superclass: any monosaccharide