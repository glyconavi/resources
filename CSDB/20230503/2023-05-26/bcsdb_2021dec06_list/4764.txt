RES
1r:r1
REP
REP1:7o(4+1)2d=-1--1
RES
2b:a-dglc-HEX-1:5
3s:n-acetyl
4b:b-dglc-HEX-1:5
5s:n-acetyl
6b:a-dgal-HEX-1:5|6:a
7b:a-dgal-HEX-1:5|6:a
LIN
1:2d(2+1)3n
2:2o(4+1)4d
3:4d(2+1)5n
4:4o(3+1)6d
5:6o(3+1)7d