RES
1r:r1
REP
REP1:4o(4+1)2d=-1--1
RES
2b:b-dgal-HEX-1:5
3b:a-dglc-HEX-1:5
4b:b-dglc-HEX-1:5
5b:b-dglc-HEX-1:5
6b:b-dgal-HEX-1:5
LIN
1:2o(4+1)3d
2:3o(4+1)4d
3:3o(6+1)5d
4:5o(4+1)6d