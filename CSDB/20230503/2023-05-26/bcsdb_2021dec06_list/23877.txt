RES
1r:r1
REP
REP1:3o(3+1)2n=-1--1
RES
2s:phosphate
3b:o-xgro-TRI-0:0|1:aldi
4a:a1
LIN
1:2n(1+1)3o
2:3o(2+1)4n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:5
RES
5n:n1
ALTSUBGRAPH2
LEAD-IN RES:6
RES
6b:a-dglc-HEX-1:5
7s:n-acetyl
LIN
1:6d(2+1)7n
NON
NON1
SmallMolecule:SMILES [3CH3][2C@@H](N)[1C](=O)O
Description:(D)-alanine