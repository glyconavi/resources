RES
1b:a-lgro-dman-HEP-1:5
2b:a-dglc-HEX-1:5
3b:a-dgal-HEX-1:5
4b:a-dglc-HEX-1:5
5b:a-dglc-HEX-1:5
6s:n-acetyl
LIN
1:1o(3+1)2d
2:2o(3+1)3d
3:3o(2+1)4d
4:3o(3+1)5d
5:5d(2+1)6n