RES
1b:a-dglc-HEX-1:5
2n:n1
3b:a-dglc-HEX-1:5
4n:n2
LIN
1:1o(6+1)2n
2:1o(1+1)3d
3:3o(6+1)4n
NON
NON1
Parent:1
Linkage:o(6+1)n
SmallMolecule:SMILES CCCCCCCCCCCCCCCCCCCCCC(O)C(CCCCCCCCCCCC){1}C(=O)O
Description:2-dodecanoyl-3-hydroxy-docosanoic acid
NON2
Parent:3
Linkage:o(6+1)n
SmallMolecule:SMILES CCCCCCCCCCCCCCCCCCCCCC(O)C(CCCCCCCCCCCC){1}C(=O)O
Description:2-dodecanoyl-3-hydroxy-docosanoic acid