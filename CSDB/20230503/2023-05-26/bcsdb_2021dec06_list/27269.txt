RES
1r:r1
REP
REP1:4o(4+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3s:n-acetyl
4b:b-dglc-HEX-1:5
5n:n1
6s:n-acetyl
7n:n2
LIN
1:2d(2+1)3n
2:2o(4+1)4d
3:2o(3+2)5n
4:4d(2+1)6n
5:5n(1+2)7n
NON
NON1
Parent:2
Linkage:o(3+2)n
SmallMolecule:SMILES [3CH3][2C@@H](O)[1C](=O)O
Description:(R)-carboxyethyl
NON2
Parent:5
Linkage:n(1+2)n
SmallMolecule:SMILES [3CH3][2C@H](N)[1C](=O)O
Description:(L)-alanine