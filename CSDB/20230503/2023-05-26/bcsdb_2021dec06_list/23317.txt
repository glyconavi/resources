RES
1b:a-dglc-HEX-1:5
2s:phosphate
3s:amino
4n:n1
5b:b-dglc-HEX-1:5
6s:phosphate
7s:amino
8n:n2
9b:a-dman-OCT-2:6|1:a|2:keto|3:d
10s:amino
11b:a-dgro-dman-HEP-1:5
12s:phosphate
13b:a-dgal-HEX-1:5|6:a
14s:phosphate
15b:a-dgal-HEX-1:5
16b:a-dgal-HEX-1:5
17b:a-dgal-HEX-1:5
18b:b-dara-HEX-2:5|1:aldi|2:keto
LIN
1:1o(1+1)2n
2:1d(2+1)3n
3:1o(2+1)4n
4:1o(6+1)5d
5:5o(4+1)6n
6:5d(2+1)7n
7:5o(2+1)8n
8:5o(6+2)9d
9:9d(8+1)10n
10:9o(5+1)11d
11:9o(4+1)12n
12:12n(1+1)13o
13:11o(3+1)14n
14:14n(1+1)15o
15:15o(3+1)16d
16:16o(6+1)17d
17:17o(6+2)18d
NON
NON1
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid
NON2
Parent:5
Linkage:o(2+1)n
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxy-tetradecanoic acid