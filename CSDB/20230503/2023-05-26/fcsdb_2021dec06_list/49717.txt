RES
1b:o-dman-HEX-0:0|1:aldi
2n:n1
LIN
1:1o(3+11)2n
NON
NON1
Parent:1
Linkage:o(3+11)n
SmallMolecule:SMILES C/C2=C({11}CO)/[C@@]1(C)CCCC(C)(C)[C@@H]1CC2
Description:sporulositol A aglycon