RES
1b:b-dman-HEX-1:5
2n:n1
LIN
1:1o(1+34)2n
NON
NON1
Parent:1
Linkage:o(1+34)n
SmallMolecule:SMILES C[C@@H](C[C@@H](/{9}C(O)=C1C([C@]2(CCCN2C\1=O)[H])=O)C)CCCCCCC/C=C/C{23}[C@@H](CCC{27}[C@@H](CCCCCC{34}CO)O)O
Description:burnettramic acid C aglycon