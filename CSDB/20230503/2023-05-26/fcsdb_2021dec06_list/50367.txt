RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4b:b-dglc-HEX-1:5
5b:b-dglc-HEX-1:5
6b:b-dglc-HEX-1:5
LIN
1:1n(3+1)2d
2:1n(24+1)3d
3:2o(6+1)4d
4:3o(6+1)5d
5:3o(2+1)6d
NON
NON1
SmallMolecule:SMILES C[C@@H](CC{24}[C@H](O){25}C(C)(C)O)[C@H]3CC[C@@]4(C)[C@@H]2C/C=C/1[C@@H](CC{3}[C@H](O)C1(C)C)[C@]2(C)[C@H](O)C[C@]34C
Description:mogrol