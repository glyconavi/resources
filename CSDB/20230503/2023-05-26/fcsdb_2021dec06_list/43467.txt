RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+53)2n
NON
NON1
Parent:1
Linkage:o(1+53)n
SmallMolecule:SMILES O=C1CC(OC2=C1C=CC=C2)C3=CC={54}C({53}C(O)=C3)O
Description:3',4'-dihydroxyflavanone