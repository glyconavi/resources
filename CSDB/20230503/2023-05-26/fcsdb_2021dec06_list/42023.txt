RES
1b:a-xman-HEX-1:5
2b:a-xman-HEX-1:5
3b:a-xman-HEX-1:5
4b:a-xman-HEX-1:5
5b:a-xman-HEX-1:5
LIN
1:1o(2+1)2d
2:2o(2+1)3d
3:3o(2+1)4d
4:4o(2+1)5d