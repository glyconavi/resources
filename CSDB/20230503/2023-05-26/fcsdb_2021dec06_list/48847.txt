RES
1n:n1
2a:a1
3b:b-dglc-HEX-1:5
4n:n2
5n:n3
6n:n4
7b:b-dglc-HEX-1:5
8n:n5
9n:n6
10n:n7
11n:n8
LIN
1:1n(1+1)2n
2:1n(17+1)3d
3:3o(4+1)4n
4:3o(3+1)5n
5:3o(6+1)6n
6:3o(2+1)7d
7:7o(6+1)8n
8:7o(2+1)9n
9:7o(4+1)10n
10:7o(3+1)11n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:12
RES
12s:methyl
ALTSUBGRAPH2
LEAD-IN RES:13
RES
13n:n9
ALTSUBGRAPH3
LEAD-IN RES:14
RES
14n:n10
NON
NON1
SmallMolecule:SMILES [18CH3][17C@@H](O)[16CH2][15CH2][14CH2][13CH2][12CH2][11CH2]/[10CH]=[9CH]\[8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:(R)-17-hydroxy-cis-9-octadecenoic acid
NON2
Parent:3
Linkage:o(4+1)n
SmallMolecule:SMILES [3CH3][2CH2][1C](=O)O
Description:propanoic acid
NON3
Parent:3
Linkage:o(3+1)n
SmallMolecule:SMILES [3CH3][2CH2][1C](=O)O
Description:propanoic acid
NON4
Parent:3
Linkage:o(6+1)n
SmallMolecule:SMILES [3CH3][2CH2][1C](=O)O
Description:propanoic acid
NON5
Parent:7
Linkage:o(6+1)n
SmallMolecule:SMILES [3CH3][2CH2][1C](=O)O
Description:propanoic acid
NON6
Parent:7
Linkage:o(2+1)n
SmallMolecule:SMILES [3CH3][2CH2][1C](=O)O
Description:propanoic acid
NON7
Parent:7
Linkage:o(4+1)n
SmallMolecule:SMILES [3CH3][2CH2][1C](=O)O
Description:propanoic acid
NON8
Parent:7
Linkage:o(3+1)n
SmallMolecule:SMILES [3CH3][2CH2][1C](=O)O
Description:propanoic acid
NON9
SmallMolecule:SMILES [4CH3][3CH2][2CH2][1CH2]O
Description:butanol
NON10
SmallMolecule:SMILES CC(C){1}CO
Description:isobutanol