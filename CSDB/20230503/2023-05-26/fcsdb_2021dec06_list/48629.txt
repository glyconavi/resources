RES
1b:o-lery-TET-0:0|1:aldi
2b:b-dman-HEX-1:5
3a:a1
4s:acetyl
5s:acetyl
6n:n1
LIN
1:1o(4+1)2d
2:2o(2+1)3n
3:2o(4+1)4n
4:2o(6+1)5n
5:2o(3+1)6n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:7
RES
7n:n2
ALTSUBGRAPH2
LEAD-IN RES:8
RES
8n:n3
ALTSUBGRAPH3
LEAD-IN RES:9
RES
9s:acetyl
NON
NON1
Parent:2
Linkage:o(3+1)n
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:palmitic acid
NON2
SmallMolecule:SMILES [3CH3][2CH2][1C](=O)O
Description:propanoic acid
NON3
SmallMolecule:SMILES [3CH3][2CH]([4CH3])[1C](=O)O
Description:isobutyric acid