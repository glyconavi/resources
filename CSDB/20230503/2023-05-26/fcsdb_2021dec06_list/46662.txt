RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+3)2n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES CC(C)[C@@H](C)CC[C@@H](C)[C@H]1CC[C@H]2C3=CC=C4C{3}[C@@H](O)CC[C@]4(C)[C@H]3CC[C@@]21C
Description:22-dihydroergosterol