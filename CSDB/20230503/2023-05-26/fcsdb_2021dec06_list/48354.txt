RES
1r:r1
REP
REP1:13o(4+1)2d=-1--1
RES
2b:b-dgal-HEX-1:5
3b:b-dgal-HEX-1:5
4b:b-dglc-HEX-1:5
5a:a2
6a:a1
7b:b-dglc-HEX-1:5
8r:r6
9b:a-dman-HEX-1:5|6:d
10b:a-dman-HEX-1:5
11b:b-dman-HEX-1:5
12b:a-dman-HEX-1:4
13b:b-dman-HEX-1:5
14s:methyl
15b:b-dman-HEX-1:4
16b:b-dglc-HEX-1:5
17s:methyl
LIN
1:2o(6+1)3d
2:3o(6+1)4d
3:4o(2+1)5n
4:4o(3+1)6n
5:4o(6+1)7d
6:7o(6+1)8n
7:8n(6+1)9d
8:9o(2+1)10d
9:10o(6+1)11d
10:10o(3+6)12d
11:11o(4+1)13d
12:12o(1+1)14n
13:12o(2+6)15d
14:15o(2+1)16d
15:15o(1+1)17n
REP2:18o(6+1)18d=0-1
RES
18b:a-dgal-HEX-1:5
REP3:19o(6+1)19d=0-1
RES
19b:b-dgal-HEX-1:5
REP4:20o(6+1)20d=0-1
RES
20b:a-dgal-HEX-1:5
REP5:21o(6+1)21d=0-1
RES
21b:b-dgal-HEX-1:5
REP6:25o(6+1)22d=2-2
RES
22b:b-dgal-HEX-1:5
23b:b-dgal-HEX-1:5
24b:b-dglc-HEX-1:5
25b:b-dglc-HEX-1:5
26a:a4
27a:a3
LIN
1:22o(6+1)23d
2:23o(6+1)24d
3:24o(6+1)25d
4:24o(2+1)26n
5:24o(3+1)27n
REP7:28o(6+1)28d=0-1
RES
28b:a-dgal-HEX-1:5
REP8:29o(6+1)29d=0-1
RES
29b:b-dgal-HEX-1:5
REP9:30o(6+1)30d=0-1
RES
30b:a-dgal-HEX-1:5
REP10:31o(6+1)31d=0-1
RES
31b:b-dgal-HEX-1:5
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:32
RES
32b:b-dglc-HEX-1:5
ALTSUBGRAPH2
LEAD-IN RES:33
RES
33b:b-dgal-HEX-1:5
34b:b-dglc-HEX-1:5
LIN
1:33o(6+1)34d
ALTSUBGRAPH3
LEAD-IN RES:35
RES
35b:a-dgal-HEX-1:5
36r:r2
37r:r3
38b:b-dglc-HEX-1:5
LIN
1:35o(6+1)36n
2:36n(6+1)37n
3:37n(6+1)38d
ALT2
ALTSUBGRAPH1
LEAD-IN RES:39
RES
39b:b-dglc-HEX-1:5
ALTSUBGRAPH2
LEAD-IN RES:40
RES
40b:b-dgal-HEX-1:5
41b:b-dglc-HEX-1:5
LIN
1:40o(6+1)41d
ALTSUBGRAPH3
LEAD-IN RES:42
RES
42b:a-dgal-HEX-1:5
43r:r4
44r:r5
45b:b-dglc-HEX-1:5
LIN
1:42o(6+1)43n
2:43n(6+1)44n
3:44n(6+1)45d
ALT3
ALTSUBGRAPH1
LEAD-IN RES:46
RES
46b:b-dglc-HEX-1:5
ALTSUBGRAPH2
LEAD-IN RES:47
RES
47b:b-dgal-HEX-1:5
48b:b-dglc-HEX-1:5
LIN
1:47o(6+1)48d
ALTSUBGRAPH3
LEAD-IN RES:49
RES
49b:a-dgal-HEX-1:5
50r:r7
51r:r8
52b:b-dglc-HEX-1:5
LIN
1:49o(6+1)50n
2:50n(6+1)51n
3:51n(6+1)52d
ALT4
ALTSUBGRAPH1
LEAD-IN RES:53
RES
53b:b-dglc-HEX-1:5
ALTSUBGRAPH2
LEAD-IN RES:54
RES
54b:b-dgal-HEX-1:5
55b:b-dglc-HEX-1:5
LIN
1:54o(6+1)55d
ALTSUBGRAPH3
LEAD-IN RES:56
RES
56b:a-dgal-HEX-1:5
57r:r9
58r:r10
59b:b-dglc-HEX-1:5
LIN
1:56o(6+1)57n
2:57n(6+1)58n
3:58n(6+1)59d