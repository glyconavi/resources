RES
1n:n1
2s:methyl
3n:n2
4n:n3
5s:methyl
LIN
1:1n(4+1)2n
2:1n(1+17)3n
3:1n(3+1)4n
4:1n(3+2)4n
5:4n(11+1)5n
NON
NON1
SmallMolecule:SMILES C[C@H]1O{1}[C@@H](O)[C@@H](O){3}C(O)(O){4}[C@@H]1O
Description:6-deoxy-3-hydroxy-arabino-hexose
NON2
Parent:1
Linkage:n(1+17)n
SmallMolecule:SMILES CC(C)/C3=C/[C@@H]2C[C@]4({15}C(=O)O)[C@@H]1CC[C@@H](C)[C@H]1C[C@@]2({17}CO)[C@]34{13}C(=O)O
Description:sordaricin B
NON3
Parent:1
Linkage:n(3+1)n|n(3+2)n
SmallMolecule:SMILES O={10}C(O)CCCCCCC{2}[C@H](O){1}C(=O)O
Description:2S-hydroxysebasic acid, 2S-hydroxydecanedioic acid