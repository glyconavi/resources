RES
1b:b-dglc-HEX-1:5
2s:amino
3s:n-acetyl
4n:n1
5b:b-dglc-HEX-1:5
6s:n-acetyl
7b:b-dman-HEX-1:5
8a:a1
9b:a-dman-HEX-1:5
10b:a-dman-HEX-1:5
11b:a-dman-HEX-1:5
12b:a-dgal-HEX-1:4
LIN
1:1d(1+1)2n
2:1d(2+1)3n
3:1o(1+2)4n
4:1o(4+1)5d
5:5d(2+1)6n
6:5o(4+1)7d
7:7o(6+1)8n
8:7o(3+1)9d
9:9o(2+1)10d
10:10o(2+1)11d
11:10o(3+1)12d
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:13
RES
13b:a-dman-HEX-1:5
14b:a-dman-HEX-1:5
15b:a-dman-HEX-1:5
16b:a-dman-HEX-1:5
17b:a-dman-HEX-1:5
18b:a-dgal-HEX-1:4
LIN
1:13o(3+1)14d
2:13o(6+1)15d
3:15o(2+1)16d
4:14o(2+1)17d
5:17o(2+1)18d
ALTSUBGRAPH2
LEAD-IN RES:19
RES
19b:a-dman-HEX-1:5
20b:a-dman-HEX-1:5
21b:a-dman-HEX-1:5
22b:a-dman-HEX-1:5
23b:a-dman-HEX-1:5
24b:a-dgal-HEX-1:4
LIN
1:19o(3+1)20d
2:19o(6+1)21d
3:20o(2+1)22d
4:21o(2+1)23d
5:23o(2+1)24d
NON
NON1
Parent:1
Linkage:o(1+2)n
SmallMolecule:SMILES N{2}c1ccccn1
Description:2-aminopyridine