RES
1b:a-dman-HEX-1:5
2s:methyl
3n:n1
LIN
1:1o(4+1)2n
2:1o(1+19)3n
NON
NON1
Parent:1
Linkage:o(1+19)n
SmallMolecule:SMILES CC(C)[C@@H]1C{19}[C@@H](O)[C@H]2[C@@]1(C)CC[C@@]3(C)[C@H]5/C(=C\C[C@]23C)[C@]46CC[C@@H]7OC4O[C@H]5C[C@H]6C7(C)C
Description:myrotheside D aglycon