RES
1n:n1
2b:b-dxyl-PEN-1:5
3s:acetyl
LIN
1:1n(21+1)2d
2:1n(3+1)3n
NON
NON1
SmallMolecule:SMILES O{3}[C@@H]1CC[C@]2(C)C3=C([C@@]4(CC[C@H]([C@H]({21}[CH2]O)C/C=C(C)/C(C)=C)[C@]4(CC3)C)C)CC[C@H]2C1(C)C
Description:(Z)-24-methyl-5α-lanosta-8,23,25-trien-3α-ol-21-oic acid