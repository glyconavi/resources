RES
1n:n1
2b:b-dglc-HEX-1:5
3s:ethyl
LIN
1:1n(3+1)2d
2:1n(7+1)3n
NON
NON1
SmallMolecule:SMILES CC/C=C/C=C/CCC1=C({7}C(O)=O){2}C(O)={3}C{4}C(O)=C1
Description:carnemycin A, B, C aglycon