RES
1b:o-dery-TET-0:0|1:aldi
2b:b-dman-HEX-1:5
3a:a1
4a:a2
5s:acetyl
LIN
1:1o(4+1)2d
2:2o(3+1)3n
3:2o(2+1)4n
4:2o(4+1)5n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:6
RES
6n:n1
ALTSUBGRAPH2
LEAD-IN RES:7
RES
7n:n2
ALTSUBGRAPH3
LEAD-IN RES:8
RES
8n:n3
ALT2
ALTSUBGRAPH1
LEAD-IN RES:9
RES
9n:n4
ALTSUBGRAPH2
LEAD-IN RES:10
RES
10n:n5
ALTSUBGRAPH3
LEAD-IN RES:11
RES
11n:n6
NON
NON1
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:palmitic acid
NON2
SmallMolecule:SMILES [1*]
Description:hexadecadienoic acid
NON3
HistoricalEntity:superclass: lipid residue
NON4
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:palmitic acid
NON5
SmallMolecule:SMILES [1*]
Description:hexadecadienoic acid
NON6
HistoricalEntity:superclass: lipid residue