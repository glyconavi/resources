RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+4)2n
NON
NON1
Parent:1
Linkage:o(1+4)n
SmallMolecule:SMILES O=C(C1={2}C(O)C={4}C(O)C=C1)C2=CC=CC=C2
Description:2,4-dihydroxybenzophenone