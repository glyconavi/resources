RES
1b:b-dman-HEX-1:5
2n:n1
LIN
1:1o(1+11)2n
NON
NON1
Parent:1
Linkage:o(1+11)n
SmallMolecule:SMILES C/C(C{11}CO)=C/C(N(CCC[C@@H]1NC([C@@H](NC1=O)CCCN(C(/C=C(C{61}CO)\C)=O)O)=O)O)=O
Description:dimerumic acid