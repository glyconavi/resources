RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
LIN
1:1n(4+1)2d
2:1n(54+1)3d
NON
NON1
SmallMolecule:SMILES O=n1cc{54}c(O)c(O)c1c2c(O){4}c(O)ccn2=O
Description:orellanine