RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+2)2n
NON
NON1
Parent:1
Linkage:o(1+2)n
SmallMolecule:SMILES N{2}C(CC(N)C(O)CC(N)C(=O)O)C(=O)O
Description:2,4,7-triamino-5-hydroxyoctandoic acid