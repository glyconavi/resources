RES
1b:a-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+3)2n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES COC(C1={53}C(O)C=C(OC2=CC(C)=C{3}C(O)=C2)C=C1C)=O
Description:methoxycarbonyldiorcinol