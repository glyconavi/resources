RES
1b:o-dery-TET-0:0|1:aldi
2b:b-dman-HEX-1:5
3s:acetyl
4s:acetyl
5n:n1
6n:n2
LIN
1:1o(1+1)2d
2:2o(4+1)3n
3:2o(6+1)4n
4:2o(2+1)5n
5:2o(3+1)6n
NON
NON1
Parent:2
Linkage:o(2+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:lauric acid
NON2
Parent:2
Linkage:o(3+1)n
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:lauric acid