RES
1n:n1
2n:n2
3s:phosphate
4n:n3
5b:a-dman-HEX-1:5
6s:phosphate
7b:a-dman-HEX-1:5
8r:r1
LIN
1:1n(1+2)2n
2:2n(1+1)3n
3:3n(1+-1)4n
4:4n(-1+1)5d
5:5o(6+1)6n
6:6n(1+1)7o
7:7o(2+1)8n
REP
REP1:9o(2+1)9d=2-18
RES
9b:b-dman-HEX-1:5
NON
NON1
SmallMolecule:SMILES CCC(C)CCCCCCCCCCCCCCCCCC{2}C(O){1}C(=O)O
Description:2-hydroxy-21-methyltricosanoic acid
NON2
Parent:1
Linkage:n(1+2)n
HistoricalEntity:any sphingosine, any sphingenine
NON3
Parent:3
Linkage:n(1+-1)n
HistoricalEntity:superclass: any inositol