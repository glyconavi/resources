RES
1b:a-dman-HEX-1:5
2a:a1
3n:n1
LIN
1:1o(6+1)2n
2:1o(1+3)3n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:4
RES
4n:n2
ALTSUBGRAPH2
LEAD-IN RES:5
RES
5n:n3
ALTSUBGRAPH3
LEAD-IN RES:6
RES
6n:n4
ALTSUBGRAPH4
LEAD-IN RES:7
RES
7n:n5
ALTSUBGRAPH5
LEAD-IN RES:8
RES
8n:n6
ALTSUBGRAPH6
LEAD-IN RES:9
RES
9n:n7
ALTSUBGRAPH7
LEAD-IN RES:10
RES
10n:n8
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O{3}[C@@H]4C/C3=C/C=C1\[C@H](CC[C@]2([C@H]1CC[C@@H]2[C@@H](/C=C/[C@H](C)C(C)C)C)C)[C@@]3(C)CC4
Description:ergosterol
NON2
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:stearic acid
NON3
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:palmitic acid
NON4
SmallMolecule:SMILES [14CH3][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:myristic acid
NON5
SmallMolecule:SMILES [12CH3][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:lauric acid
NON6
SmallMolecule:SMILES [1*]
Description:octadecenoic acid
NON7
SmallMolecule:SMILES [1*]
Description:hexadecadienoic acid
NON8
HistoricalEntity:superclass: lipid residue