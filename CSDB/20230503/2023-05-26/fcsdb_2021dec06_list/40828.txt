RES
1b:x-xman-HEX-x:x
2n:n1
LIN
1:1o(1+3)2n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES [3CH2](O)[2CH](N)[1C](=O)O
Description:serine