RES
1n:n1
2s:methyl
3b:b-dglc-HEX-1:5
4s:methyl
LIN
1:1n(31+1)2n
2:1n(3+1)3d
3:3o(4+1)4n
NON
NON1
SmallMolecule:SMILES C=C(CC[C@@H](C)[C@H]1C{15}[C@@H](O)[C@@]2({30}C(=O)O)/C4=C(CC[C@]12C)/[C@@]3(C)CC{3}[C@H](O)C(=C)[C@@H]3CC4)C(C)C
Description:ascosteroside C aglycon