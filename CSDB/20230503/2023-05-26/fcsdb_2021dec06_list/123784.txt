RES
1b:a-dman-HEX-x:x
2b:a-dman-HEX-1:5
3n:n1
LIN
1:1o(5+1)2d
2:1o(1+3)3n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES [4CH3][3CH](O)[2CH](N)[1C](=O)O
Description:threonine