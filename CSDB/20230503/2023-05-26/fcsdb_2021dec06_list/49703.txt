RES
1n:n1
2n:n2
LIN
1:1n(1+9)2n
NON
NON1
SmallMolecule:SMILES CSC[C@H]1O{1}[C@@H](O)[C@H](O)[C@@H]1O
Description:5-methylthioribose
NON2
Parent:1
Linkage:n(1+9)n
SmallMolecule:SMILES c2ncc1nc{9}[nH]c1n2
Description:purine