RES
1n:n1
2b:a-dman-HEX-1:5
3s:phosphate
4b:a-dman-HEX-1:5
5n:n2
6b:a-dman-HEX-1:5
7n:n3
LIN
1:1n(2+1)2d
2:1n(1+1)3n
3:2o(3+1)4d
4:3n(1+1)5n
5:4o(2+1)6d
6:5n(2+1)7n
NON
NON1
SmallMolecule:SMILES [6C@H](O)1[5C@H](O)[4C@@H](O)[3C@H](O)[2C@H](O)[1C@H](O)1
Description:myo-inositol
NON2
Parent:3
Linkage:n(1+1)n
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4C@@H](O)[3C@H](O)[2C@@H](N)[1CH2]O
Description:phytosphingosine
NON3
Parent:5
Linkage:n(2+1)n
SmallMolecule:SMILES [24CH3][23CH2][22CH2][21CH2][20CH2][19CH2][18CH2][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH](O)[1C](=O)O
Description:2-hydroxy-tetracosanoic acid