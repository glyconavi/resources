RES
1b:x-dman-HEX-1:5
2b:a-dman-HEX-1:5
3b:a-dman-HEX-1:5
4b:a-dman-HEX-1:5
5b:a-dman-HEX-1:5
6b:a-dman-HEX-1:5
7b:a-dman-HEX-1:5
LIN
1:1o(2+1)2d
2:2o(2+1)3d
3:3o(2+1)4d
4:4o(6+1)5d
5:4o(3+1)6d
6:6o(2+1)7d