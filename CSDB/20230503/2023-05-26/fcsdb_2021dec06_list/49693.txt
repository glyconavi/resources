RES
1n:n1
2b:b-dglc-HEX-1:5
3s:methyl
LIN
1:1n(53+1)2d
2:1n(54+1)3n
NON
NON1
SmallMolecule:SMILES C1=C{54}C(={53}C(C=C1C2=CC(=O)C3={5}C({6}C={7}C({8}C=C3O2)O)O)O)O
Description:luteolin