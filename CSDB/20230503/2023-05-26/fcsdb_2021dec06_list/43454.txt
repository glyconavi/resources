RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+21)2n
NON
NON1
Parent:1
Linkage:o(1+21)n
SmallMolecule:SMILES O[3C@H]1CC[C@@]2(C)[C@](CC[C@]3(C)[C@]2([H])C[12C@@H](O)[C@@]4([H])[C@@]3(C)CC[C@@]4([20C@@](CC/C=C(C)/C)(C)O)[H])([H])C1(C)C
Description:20S-protopanaxadiol