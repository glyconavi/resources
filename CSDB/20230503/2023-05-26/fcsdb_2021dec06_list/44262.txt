RES
1r:r1
REP
REP1:5o(4+1)2d=-1--1
RES
2b:a-dman-HEX-1:5
3b:b-dglc-HEX-1:5|6:a
4b:b-dxyl-PEN-1:5
5b:b-dman-HEX-1:5
LIN
1:2o(2+1)3d
2:3o(4+1)4d
3:4o(3+1)5d