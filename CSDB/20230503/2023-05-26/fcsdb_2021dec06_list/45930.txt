RES
1b:a-dglc-HEX-1:5
2s:methyl
3n:n1
LIN
1:1o(4+1)2n
2:1o(1+3)3n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C=C(CC[C@@H](C)[C@H]1C{15}[C@@H](O)[C@@]2({30}C(=O)O)/C4=C(CC[C@]12C)/[C@@]3(C)CC{3}[C@H](O)C(=C)[C@@H]3CC4)C(C)C
Description:ascosteroside C aglycon