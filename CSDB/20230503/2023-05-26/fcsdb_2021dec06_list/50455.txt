RES
1b:a-drib-PEN-1:4
2s:acetyl
3s:acetyl
4n:n1
LIN
1:1o(3+1)2n
2:1o(2+1)3n
3:1o(1+6)4n
NON
NON1
Parent:1
Linkage:o(1+6)n
SmallMolecule:SMILES O{1}C(C=C(C=C1C(C2=C3{8}C(O)=C{6}C(O)=C2)=O)C)=C1C3=O
Description:emodin