RES
1n:n1
2s:phosphate
3b:a-dglc-HEX-1:5
4s:amino
5b:a-dman-HEX-1:5
6n:n2
7b:a-dman-HEX-1:5
8n:n3
LIN
1:1n(1+1)2n
2:1n(2+1)3d
3:3d(2+1)4n
4:3o(6+1)5d
5:2n(1+1)6n
6:5o(3+1)7d
7:6n(2+1)8n
NON
NON1
SmallMolecule:SMILES [6C@H](O)1[5C@H](O)[4C@@H](O)[3C@H](O)[2C@H](O)[1C@H](O)1
Description:myo-inositol
NON2
Parent:2
Linkage:n(1+1)n
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4C@@H](O)[3C@H](O)[2C@@H](N)[1CH2]O
Description:phytosphingosine
NON3
Parent:6
Linkage:n(2+1)n
SmallMolecule:SMILES [24CH3][23CH2][22CH2][21CH2][20CH2][19CH2][18CH2][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH](O)[1C](=O)O
Description:2-hydroxy-tetracosanoic acid