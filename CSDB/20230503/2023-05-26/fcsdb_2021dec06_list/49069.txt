RES
1b:a-drib-PEN-1:4
2n:n1
LIN
1:1o(1+3)2n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES CC(=O)c1cc(C)c(O)c{3}c1O
Description:methylacetophenone