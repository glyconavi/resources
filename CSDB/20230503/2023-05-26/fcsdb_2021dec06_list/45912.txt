RES
1n:n1
2s:methyl
3n:n2
LIN
1:1n(5+1)2n
2:1n(1+9)3n
NON
NON1
SmallMolecule:SMILES O{5}C[C@H]1O{1}[C@@H](O)[C@H](O)[C@@H]1O
Description:5-amino-5-deoxy-D-ribofuranose
NON2
Parent:1
Linkage:n(1+9)n
SmallMolecule:SMILES Nc1ncnc2{9}[nH]cnc12
Description:adenine