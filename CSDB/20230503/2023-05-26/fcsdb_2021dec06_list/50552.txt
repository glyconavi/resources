RES
1n:n1
2b:b-dglc-HEX-1:5
3s:methyl
4b:a-lman-HEX-1:5|6:d
5b:a-lman-HEX-1:5|6:d
LIN
1:1n(7+1)2d
2:1n(54+1)3n
3:1n(3+1)4d
4:4o(3+1)5d
NON
NON1
SmallMolecule:SMILES O{3}C1=C(C2=CC={54}C(O)C=C2)OC3=C({5}C(O)=C{7}C(O)=C3C/C=C(C)\C)C1=O
Description:des-O-methylanhydroicaritin