RES
1n:n1
2s:phosphate
3n:n2
4b:a-dman-HEX-1:5
5b:b-dgal-HEX-1:5
6b:a-dgal-HEX-1:5|6:d
7b:a-dgal-HEX-1:5
8b:a-dgal-HEX-1:5
LIN
1:1n(1+1)2n
2:2n(1+1)3n
3:3n(2+1)4d
4:4o(6+1)5d
5:5o(2+1)6d
6:5o(6+1)7d
7:5o(3+1)8d
NON
NON1
HistoricalEntity:superclass: ceramide
NON2
Parent:2
Linkage:n(1+1)n
HistoricalEntity:superclass: any inositol