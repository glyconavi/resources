RES
1b:b-dglc-HEX-1:5
2n:n1
3n:n2
LIN
1:1o(6+9)2n
2:1o(1+3)3n
NON
NON1
Parent:1
Linkage:o(6+9)n
SmallMolecule:SMILES [6CH]1=[5CH][4CH]=[3CH][2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:cinnamic acid
NON2
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O[3C]1=C(C2=C[53C](O)=[54C](O)C=C2)OC3=C([5C](O)=[6CH][7C](O)=[8CH]3)C1=O
Description:3,5,7,3',4'-pentahydroxyflavone