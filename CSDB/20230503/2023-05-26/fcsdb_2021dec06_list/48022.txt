RES
1b:b-drib-PEN-1:4
2n:n1
LIN
1:1o(1+9)2n
NON
NON1
Parent:1
Linkage:o(1+9)n
SmallMolecule:SMILES Nc1nc(Cl)nc2{9}[nH]cnc12
Description:2-chloroadenine