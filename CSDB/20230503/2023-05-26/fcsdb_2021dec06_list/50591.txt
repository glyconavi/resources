RES
1n:n1
2n:n2
3s:methyl
LIN
1:1n(3+7)2n
2:2n(2+1)3n
NON
NON1
SmallMolecule:SMILES [5CH2](O)[4C@@H](O1)[3C@@H](O)[2C@@H](O)[1C@H](N2[6C](=O)N[7C](=O)[8CH]=[9CH]2)1
Description:uridine
NON2
Parent:1
Linkage:n(3+7)n
SmallMolecule:SMILES [6C]([8CH3])1=[5CH][4C](O)=[3CH][2C](O)=[1C]1[7C](=O)O
Description:2,4-dihydroxy-6-methylbenzoic acid