RES
1b:b-dglc-HEX-1:5
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4n:n1
LIN
1:1o(6+1)2d
2:1o(2+1)3d
3:1o(1+18)4n
NON
NON1
Parent:1
Linkage:o(1+18)n
SmallMolecule:SMILES C=C1{21}C(=O)O[C@@H](CCCCCCCCCCCCC{18}[C@@H](C)O)[C@H]1C(=O)O
Description:allo-murolic acid