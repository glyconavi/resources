RES
1n:n1
2s:acetyl
3s:methyl
4b:a-dglc-HEX-1:5
5s:acetyl
6n:n2
LIN
1:1n(15+1)2n
2:1n(16+1)3n
3:1n(9+1)4d
4:4o(3+1)5n
5:4o(6+2)6n
NON
NON1
SmallMolecule:SMILES O{16}C[C@@H]1CCC2/C1=C\[C@@]3(C)C({9}[C@H](O){8}[C@@H](O)[C@H]2C)=C([C@@H](C){15}CO)C{12}[C@H]3O
Description:fusicoccin aglycon
NON2
Parent:4
Linkage:o(6+2)n
SmallMolecule:SMILES C=C{2}C(C)(C)O
Description:2-methylbut-3-en-2-ol