RES
1n:n1
2b:a-drib-PEN-1:4
3s:methyl
LIN
1:1n(4+1)2d
2:1n(2+1)3n
NON
NON1
SmallMolecule:SMILES O=C1C2=C(CC(C={4}C(C={2}C3O)O)=C3O1)C=C(C)C={11}C2O
Description:9-dehydroxyeurotinone