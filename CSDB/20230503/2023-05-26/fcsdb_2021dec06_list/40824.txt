RES
1b:b-dglc-HEX-1:5
2n:n1
3b:b-dglc-HEX-1:5
4b:b-dglc-HEX-1:5
5b:b-dglc-HEX-1:5
LIN
1:1o(1+-1)2n
2:1o(6+1)3d
3:3o(3+1)4d
4:4o(3+1)5d
NON
NON1
Parent:1
Linkage:o(1+-1)n
HistoricalEntity:superclass: any residue