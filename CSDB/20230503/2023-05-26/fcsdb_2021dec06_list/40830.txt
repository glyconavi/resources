RES
1b:a-dglc-HEX-1:5
2b:a-dgal-HEX-1:5
3n:n1
LIN
1:1o(1+1)2d
2:1o(3+1)3n
NON
NON1
Parent:1
Linkage:o(3+1)n
SmallMolecule:SMILES [22CH3][21CH2][20CH2][19CH2][18CH2][17CH](O)[16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:17-hydroxy-docosanoic acid