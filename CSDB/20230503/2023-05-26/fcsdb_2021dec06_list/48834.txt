RES
1n:n1
2a:a1
3b:b-dglc-HEX-1:5
4s:acetyl
5b:b-dglc-HEX-1:5
6s:acetyl
LIN
1:1n(1+1)2n
2:1n(17+1)3d
3:3o(6+1)4n
4:3o(2+1)5d
5:5o(6+1)6n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:7
RES
7s:methyl
ALTSUBGRAPH2
LEAD-IN RES:8
RES
8s:ethyl
ALTSUBGRAPH3
LEAD-IN RES:9
RES
9n:n2
NON
NON1
SmallMolecule:SMILES [18CH3][17C@@H](O)[16CH2][15CH2][14CH2][13CH2][12CH2][11CH2]/[10CH]=[9CH]\[8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:(R)-17-hydroxy-cis-9-octadecenoic acid
NON2
SmallMolecule:SMILES [4CH3][3CH2][2CH2][1CH2]O
Description:butanol