RES
1b:a-dman-HEX-1:5
2a:a1
LIN
1:1o(1+3)2n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:3
RES
3n:n1
ALTSUBGRAPH2
LEAD-IN RES:4
RES
4n:n2
NON
NON1
SmallMolecule:SMILES [4CH3][3CH](O)[2CH](N)[1C](=O)O
Description:threonine
NON2
SmallMolecule:SMILES [3CH2](O)[2CH](N)[1C](=O)O
Description:serine