RES
1r:r1
REP
REP1:4o(3+1)2d=-1--1
RES
2b:a-dglc-HEX-1:5
3n:n1
4b:a-dglc-HEX-1:5
5b:a-dglc-HEX-1:5
LIN
1:2o(4+1)3n
2:2o(3+1)4d
3:4o(4+1)5d
NON
NON1
Parent:2
Linkage:o(4+1)n
HistoricalEntity:-3,4)aDGlcp(1- moiety