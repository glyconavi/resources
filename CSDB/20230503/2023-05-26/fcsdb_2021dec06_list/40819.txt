RES
1r:r1
REP
REP1:4o(6+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4b:b-dglc-HEX-1:5
5n:n1
LIN
1:2o(6+1)3d
2:3o(6+1)4d
3:4o(3+1)5n
NON
NON1
Parent:4
Linkage:o(3+1)n
SmallMolecule:SMILES [3C](=O)(O)[2CH2][1C](=O)(O)
Description:malonic acid