RES
1b:a-lman-HEX-1:5|6:d
2n:n1
LIN
1:1o(1+3)2n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O{3}[C@H]1[C@@H](OC2=C{7}C(O)=C{5}C(O)=C2C1=O)C3=C{53}C(O)={54}C(C=C3)O
Description:(-)-taxifolin