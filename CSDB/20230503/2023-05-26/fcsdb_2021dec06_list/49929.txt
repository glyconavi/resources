RES
1b:b-drib-PEN-1:4
2s:phosphate
3n:n1
LIN
1:1o(5+1)2n
2:1o(1+1)3n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES {1}N=C(N)NC=O
Description:N-(diaminomethylidene)formamide