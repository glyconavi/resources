RES
1b:b-dglc-HEX-1:5
2s:n-acetyl
3s:amino
4n:n1
5b:b-dglc-HEX-1:5
6s:n-acetyl
7b:b-dman-HEX-1:5
8b:a-dman-HEX-1:5
9b:a-dman-HEX-1:5
10b:a-dman-HEX-1:5
11b:a-dman-HEX-1:5
12b:a-dman-HEX-1:5
13b:a-dman-HEX-1:5
14b:a-dman-HEX-1:5
15b:a-dman-HEX-1:5
16b:a-dman-HEX-1:5
17b:a-dman-HEX-1:5
LIN
1:1d(2+1)2n
2:1d(1+1)3n
3:1o(1+2)4n
4:1o(4+1)5d
5:5d(2+1)6n
6:5o(4+1)7d
7:7o(3+1)8d
8:7o(6+1)9d
9:9o(3+1)10d
10:9o(6+1)11d
11:8o(2+1)12d
12:12o(2+1)13d
13:11o(2+1)14d
14:13o(2+1)15d
15:14o(2+1)16d
16:16o(2+1)17d
NON
NON1
Parent:1
Linkage:o(1+2)n
SmallMolecule:SMILES N{2}c1ccccn1
Description:2-aminopyridine