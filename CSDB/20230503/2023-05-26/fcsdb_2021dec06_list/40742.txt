RES
1r:r1
REP
REP1:4o(6+1)2d=-1--1
RES
2b:b-xgal-HEX-1:4
3b:b-xgal-HEX-1:4
4b:b-xgal-HEX-1:4
5b:a-xglc-HEX-1:5
LIN
1:2o(5+1)3d
2:3o(5+1)4d
3:4o(2+1)5d