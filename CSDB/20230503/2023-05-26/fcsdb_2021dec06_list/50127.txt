RES
1r:r1
REP
REP1:8o(6+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3r:r2
4b:b-dglc-HEX-1:5
5b:b-dglc-HEX-1:5
6r:r3
7b:b-dglc-HEX-1:5
8b:b-dglc-HEX-1:5
9b:b-dglc-HEX-1:5
LIN
1:2o(6+1)3n
2:3n(6+1)4d
3:4o(2+1)5d
4:4o(6+1)6n
5:6n(6+1)7d
6:7o(3+1)8d
7:8o(4+1)9d
REP2:10o(6+1)10d=-1--1
RES
10b:b-dglc-HEX-1:5
REP3:11o(6+1)11d=-1--1
RES
11b:b-dglc-HEX-1:5