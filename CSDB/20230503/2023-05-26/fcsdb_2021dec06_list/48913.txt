RES
1b:b-dman-HEX-1:5
2s:methyl
3n:n1
LIN
1:1o(4+1)2n
2:1o(1+13)3n
NON
NON1
Parent:1
Linkage:o(1+13)n
SmallMolecule:SMILES CC[C@H](C)C[C@H](C)/C=C(C)/{13}[C@@H](O)[C@@H](C)/C=C(C)/{9}[C@@H](O)[C@@H](C)/C=C(C)/{5}[C@@H](O)[C@@H](C)/C=C(C)/{1}C(=O)O
Description:rogerson aglycon