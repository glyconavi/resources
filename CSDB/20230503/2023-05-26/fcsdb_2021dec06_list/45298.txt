RES
1b:o-dman-HEX-0:0|1:aldi
2n:n1
3s:acetyl
LIN
1:1o(6+1)2n
2:2n(3+1)3n
NON
NON1
Parent:1
Linkage:o(6+1)n
SmallMolecule:SMILES CCCCC{23}C(O)C{21}C(O)CC(=O)OC(CCCCC)C{15}C(O)CC(=O)OC(CCCCC)C{9}C(O)CC(=O)OC(CCCCC)CC{3}(O)C{1}C(=O)O
Description:exophilin B1