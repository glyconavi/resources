RES
1b:x-xglc-HEX-x:x
2s:n-acetyl
3s:amino
4n:n1
LIN
1:1d(2+1)2n
2:1d(1+1)3n
3:1o(1+4)4n
NON
NON1
Parent:1
Linkage:o(1+4)n
SmallMolecule:SMILES [4C](=O)(N)[3CH2][2CH](N)[1C](=O)O
Description:asparagine