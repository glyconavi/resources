RES
1n:n1
2b:b-dxyl-PEN-1:5
3n:n2
4s:methyl
LIN
1:1n(21+1)2d
2:1n(3+1)3n
3:3n(5+1)4n
NON
NON1
SmallMolecule:SMILES C[C@]([C@]1(C)CC2)(CC[C@@H]1[C@@H](CCC(C(C)C)=C){21}C(O)=O)C3=C2[C@]4(C)[C@](CC3)([H])C(C)(C){3}[C@H](O)CC4
Description:fomitoside K aglycon
NON2
Parent:1
Linkage:n(3+1)n
SmallMolecule:SMILES [5C](=O)(O)[4CH2][3C]([6CH3])(O)[2CH2][1C](=O)(O)
Description:(R)-3-hydroxy-3-methylglutaric acid