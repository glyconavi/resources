RES
1n:n1
2s:methyl
3b:a-dglc-HEX-1:5
4s:methyl
LIN
1:1n(16+1)2n
2:1n(9+1)3d
3:3o(6+1)4n
NON
NON1
SmallMolecule:SMILES CC(C)/C3=C/2{9}[C@@H](O){8}[C@H](O)[C@H](C)[C@@H]/1C/C=C({16}CO)\C1=C\[C@@]2(C)CC3
Description:dongtingnoid E aglycon