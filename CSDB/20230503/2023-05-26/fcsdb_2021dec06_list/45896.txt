RES
1b:o-dery-TET-0:0|1:aldi
2n:n1
LIN
1:1o(4+7)2n
NON
NON1
Parent:1
Linkage:o(4+7)n
SmallMolecule:SMILES [6C]([8CH3])1=[5CH][4C](O)=[3CH][2C](O)=[1C]1[7C](=O)O
Description:2,4-dihydroxy-6-methylbenzoic acid