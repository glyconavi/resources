RES
1n:n1
2s:phosphate
3n:n2
4b:a-dman-HEX-1:5
5b:a-dman-HEX-1:5
LIN
1:1n(1+1)2n
2:2n(1+1)3n
3:3n(2+1)4d
4:4o(3+1)5d
NON
NON1
HistoricalEntity:superclass: ceramide
NON2
Parent:2
Linkage:n(1+1)n
SmallMolecule:SMILES [6C@H](O)1[5C@H](O)[4C@@H](O)[3C@H](O)[2C@H](O)[1C@H](O)1
Description:myo-inositol