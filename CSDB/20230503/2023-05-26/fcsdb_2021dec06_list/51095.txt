RES
1n:n1
2s:methyl
3b:b-dglc-HEX-1:5
4b:b-dxyl-PEN-1:5
LIN
1:1n(2+1)2n
2:1n(4+1)3d
3:3o(6+1)4d
NON
NON1
SmallMolecule:SMILES O{4}C1=CC={1}C(O){2}C(O)=C1
Description:2,4-dihydroxyphenol