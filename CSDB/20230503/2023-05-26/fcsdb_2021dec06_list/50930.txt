RES
1n:n1
2s:acetyl
3b:o-dman-HEX-0:0|1:aldi
4s:acetyl
5s:acetyl
6s:acetyl
LIN
1:1n(3+1)2n
2:1n(1+1)3d
3:3o(2+1)4n
4:3o(3+1)5n
5:3o(4+1)6n
NON
NON1
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3C@@H](O)[2CH2][1C](=O)O
Description:(R)-3-hydroxy-hexadecanoic acid