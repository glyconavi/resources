RES
1b:o-dery-TET-0:0|1:aldi
2b:b-dman-HEX-1:5
3s:acetyl
4n:n1
5n:n2
LIN
1:1o(1+1)2d
2:2o(4+1)3n
3:2o(2+1)4n
4:2o(3+1)5n
NON
NON1
Parent:2
Linkage:o(2+1)n
HistoricalEntity:superclass: lipid residue
NON2
Parent:2
Linkage:o(3+1)n
HistoricalEntity:superclass: lipid residue