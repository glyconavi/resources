RES
1b:b-dglc-HEX-1:5
2s:amino
3n:n1
LIN
1:1d(1+1)2n
2:1o(1+19)3n
NON
NON1
Parent:1
Linkage:o(1+19)n
SmallMolecule:SMILES C[C@@H]1N{19}[C@H]2N(C3=C({17}[C@]2(C[C@H]4C(N{2}[C@H](C5=NC6=C(C(N54)=O)C=CC=C6)C)=O)O)C=CC=C3)C1=O
Description:fumigatoside D aglycon