RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+3)2n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES [H][C@@]12CC[C@]([C@@]1(C)CC[C@@]3([H])C2=C{6}[C@@H](O){5}[C@@]4(O)C{3}[C@@H](O)CC[C@]34C)([H])[C@H](C)/C=C/[C@H](C)C(C)C
Description:(22E)-ergosta-7,22-diene-3β,5α,6β-triol