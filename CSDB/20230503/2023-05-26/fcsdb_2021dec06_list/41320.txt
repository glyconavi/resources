RES
1n:n1
2s:phosphate
3n:n2
4b:a-dglc-HEX-1:5
5s:amino
6b:a-dman-HEX-1:5
7s:phosphate
8n:n3
LIN
1:1n(1+1)2n
2:2n(1+1)3n
3:3n(2+1)4d
4:4d(2+1)5n
5:4o(6+1)6d
6:6o(6+1)7n
7:7n(1+1)8n
NON
NON1
HistoricalEntity:superclass: ceramide
NON2
Parent:2
Linkage:n(1+1)n
SmallMolecule:SMILES [6C@H](O)1[5C@H](O)[4C@@H](O)[3C@H](O)[2C@H](O)[1C@H](O)1
Description:myo-inositol
NON3
Parent:7
Linkage:n(1+1)n
SmallMolecule:SMILES C[N+](C)(C)[2CH2][1CH2]O
Description:choline