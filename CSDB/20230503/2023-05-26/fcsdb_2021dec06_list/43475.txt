RES
1b:b-dglc-HEX-1:5
2s:methyl
3n:n1
LIN
1:1o(4+1)2n
2:1o(1+6)3n
NON
NON1
Parent:1
Linkage:o(1+6)n
SmallMolecule:SMILES O=C1CC(OC2=C1C={6}C(O)C=C2)C3=CC=CC=C3
Description:6-hydroxyflavanone