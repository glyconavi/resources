RES
1n:n1
2b:b-dglc-HEX-1:5
3b:a-lman-HEX-1:5|6:d
LIN
1:1n(18+1)2d
2:1n(21+1)3d
NON
NON1
SmallMolecule:SMILES C{18}[C@@H](O)CCCCCCCCCCCCC[C@@H]1OC(=O)[C@H](C)[C@@H]1{21}C(=O)O
Description:18R-hydroxydihydroalloprotolichesterinic acid