RES
1b:a-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+1)2n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES O{1}c1cccc2cccc(O)c12
Description:naphthalen-1,8-diol