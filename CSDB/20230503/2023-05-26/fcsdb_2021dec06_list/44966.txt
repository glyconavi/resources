RES
1b:a-dman-HEX-1:5
2b:a-dman-HEX-1:5
3b:a-dman-HEX-1:5
4b:a-dman-HEX-1:5
5a:a1
LIN
1:1o(2+1)2d
2:2o(2+1)3d
3:3o(2+1)4d
4:4o(-1+-1)5n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:6
RES
6b:a-dman-HEX-1:5
7b:a-dman-HEX-1:5
LIN
1:6o(2+1)7d
ALTSUBGRAPH2
LEAD-IN RES:8
RES
8b:a-dman-HEX-1:5
9b:a-dman-HEX-1:5
LIN
1:8o(3+1)9d