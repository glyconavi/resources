RES
1n:n1
2b:b-dman-HEX-1:5
3n:n2
4n:n3
5s:acetyl
6n:n4
LIN
1:1n(5+1)2d
2:1n(1+5)3n
3:3n(1+5)4n
4:4n(3+1)5n
5:4n(1+5)6n
NON
NON1
SmallMolecule:SMILES CCCCC{5}C(O)C{3}C(O)C{1}C(O)=O
Description:3,5-dihydroxydecanoic acid
NON2
Parent:1
Linkage:n(1+5)n
SmallMolecule:SMILES CCCCC{5}C(O)C{3}C(O)C{1}C(O)=O
Description:3,5-dihydroxydecanoic acid
NON3
Parent:3
Linkage:n(1+5)n
SmallMolecule:SMILES CCCCC{5}C(O)C{3}C(O)C{1}C(O)=O
Description:3,5-dihydroxydecanoic acid
NON4
Parent:4
Linkage:n(1+5)n
SmallMolecule:SMILES CCCCC{5}C(O)C{3}C(O)C{1}C(O)=O
Description:3,5-dihydroxydecanoic acid