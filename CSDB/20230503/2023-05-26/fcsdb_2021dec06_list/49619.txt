RES
1b:b-dglc-HEX-1:5
2s:methyl
3n:n1
LIN
1:1o(4+1)2n
2:1o(1+5)3n
NON
NON1
Parent:1
Linkage:o(1+5)n
SmallMolecule:SMILES C/C=C/C=C/[C@H]1CC2=C{5}C(O)=C{7}C(O)=C2O1
Description:asperfuran