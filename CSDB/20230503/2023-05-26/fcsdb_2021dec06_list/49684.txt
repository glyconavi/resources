RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+53)2n
NON
NON1
Parent:1
Linkage:o(1+53)n
SmallMolecule:SMILES C1=C{54}C(={53}C(C=C1C2=CC(=O)C3={5}C({6}C={7}C({8}C=C3O2)O)O)O)O
Description:luteolin