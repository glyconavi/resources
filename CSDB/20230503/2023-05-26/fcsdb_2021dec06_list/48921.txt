RES
1b:o-dara-PEN-0:0|1:aldi
2n:n1
3b:b-dman-HEX-1:5
4s:acetyl
LIN
1:1o(1+1)2n
2:2n(13+1)3d
3:3o(6+1)4n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES CC[C@H](C)C[C@H](C)C[C@H](C){13}[C@@H](O)[C@@H](C)/C=C(C)/{9}[C@@H](O)[C@@H](C)/C=C(C)/{5}[C@@H](O)[C@@H](C)/C=C(C)/{1}C(O)=O
Description:2,4,6,8,10,12,14,16,18-nonamethyl-5,9,13-trihydroxy-2E,6E,10E-icosatrienoic acid