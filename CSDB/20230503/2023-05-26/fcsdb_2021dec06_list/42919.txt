RES
1b:b-dglc-HEX-1:5|6:a
2n:n1
LIN
1:1o(1+3)2n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O{3}C1CC[C@@]2(C)[C@](CC[C@]([C@@](CC[C@]3(C)[C@@]4([H])C[C@@](C)({30}C(O)=O)CC3)(C)C4=C5)(C)[C@]2([H])C5=O)([H])C1(C)C
Description:glycyrrhetinic acid