RES
1n:n1
2n:n2
3s:phosphate
4n:n3
5b:a-dglc-HEX-1:5
6s:acetyl
7b:a-dman-HEX-1:5
8b:a-dman-HEX-1:5
9b:a-dman-HEX-1:5
10n:n4
11b:a-dman-HEX-1:5
12n:n5
LIN
1:1n(2+1)2n
2:1n(1+1)3n
3:3n(1+1)4n
4:4n(4+1)5d
5:5o(2+1)6n
6:5o(6+1)7d
7:7o(2+1)8d
8:8o(2+1)9d
9:8o(-1+1)10n
10:9o(2+1)11d
11:10n(2+1)12n
NON
NON1
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4C@@H](O)[3C@H](O)[2C@@H](N)[1CH2]O
Description:phytosphingosine
NON2
Parent:1
Linkage:n(2+1)n
SmallMolecule:SMILES [24CH3][23CH2][22CH2][21CH2][20CH2][19CH2][18CH2][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH](O)[1C](=O)O
Description:2-hydroxy-tetracosanoic acid
NON3
Parent:3
Linkage:n(1+1)n
SmallMolecule:SMILES [6C@H](O)1[5C@H](O)[4C@@H](O)[3C@H](O)[2C@H](O)[1C@H](O)1
Description:myo-inositol
NON4
Parent:8
Linkage:o(-1+1)n
HistoricalEntity:phospho-ethanolamine
NON5
Parent:10
Linkage:n(2+1)n
HistoricalEntity:protein