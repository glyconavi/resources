RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+21)2n
NON
NON1
Parent:1
Linkage:o(1+21)n
SmallMolecule:SMILES CC[C@@H]({20}[C@@H](CCCCCCC/C=C/C=C/CCC1=C(CC2={51}C(C=C(OC2=O)C)O){3}C(O)=C{1}C(O)=C1)O)C
Description:resorcinoside V aglycon