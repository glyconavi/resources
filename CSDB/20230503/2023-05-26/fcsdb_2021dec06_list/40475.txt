RES
1r:r1
REP
REP1:6o(6+1)2d=-1--1
RES
2b:a-xman-HEX-1:5
3b:a-xgal-HEX-1:5
4b:a-xman-HEX-1:5
5b:a-xgal-HEX-1:5
6b:a-xman-HEX-1:5
7b:b-xgal-HEX-1:5
8s:pyruvate
LIN
1:2o(2+1)3d
2:2o(6+1)4d
3:4o(2+1)5d
4:4o(6+1)6d
5:3o(3+1)7d
6:7o(6+2)8n
7:7o(4+2)8n