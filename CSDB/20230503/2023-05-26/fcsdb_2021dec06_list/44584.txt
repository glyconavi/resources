RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+11)2n
NON
NON1
Parent:1
Linkage:o(1+11)n
SmallMolecule:SMILES O=C1CC[C@@]2(C)C(CC[C@@]3(C)C2{11}[C@@H](O)CC4=C([C@H](C)C[C@H](O5){24}[C@@H](O){25}C(C)(O)C)[C@@H]5C[C@]34C)C1(C)C
Description:alisol F