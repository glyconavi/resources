RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+3)2n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O{3}[C@H]1{2}[C@H](O)C[C@@]2(C)[C@](CC[C@@]3([H])C2=CC[C@]4(C)[C@]3(C)C{16}[C@@H](O)[C@](C)([C@@H](C(C)C)COC5=O)[C@]45[H])([H])C1(C)C
Description:kolokoside D aglycon