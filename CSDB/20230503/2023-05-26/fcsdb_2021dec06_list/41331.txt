RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+6)2n
NON
NON1
Parent:1
Linkage:o(1+6)n
HistoricalEntity:branched (1-3,1-6)-b-D-glucan (ID: 41329,41330)