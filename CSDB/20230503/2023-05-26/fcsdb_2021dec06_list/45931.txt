RES
1b:a-dglc-HEX-1:5
2s:methyl
3n:n1
LIN
1:1o(4+1)2n
2:1o(1+3)3n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C=C1{3}[C@@H](O)CC[C@]2(C)/C4=C(CC[C@@H]12)/[C@]3({30}C(=O)O){15}[C@H](O)C[C@H]([C@H](C)CC/C=C(C)\C)[C@@]3(C)CC4
Description:ascosteroside B aglycon