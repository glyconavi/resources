RES
1n:n1
2s:methyl
3b:a-lman-HEX-1:5|6:d
4b:a-lman-HEX-1:5|6:d
LIN
1:1n(54+1)2n
2:1n(3+1)3d
3:3o(2+1)4d
NON
NON1
SmallMolecule:SMILES O{3}C1=C(C2=CC={54}C(O)C=C2)OC3=C({5}C(O)=C{7}C(O)=C3C/C=C(C)\C)C1=O
Description:des-O-methylanhydroicaritin