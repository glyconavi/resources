RES
1r:r1
REP
REP1:5o(4+1)2n=-1--1
RES
2n:n1
3b:b-dglc-HEX-1:5
4s:acetyl
5b:b-dglc-HEX-1:5
6s:acetyl
LIN
1:2n(17+1)3d
2:3o(6+1)4n
3:3o(2+1)5d
4:5o(6+1)6n
NON
NON1
HistoricalEntity:superclass: lipid residue