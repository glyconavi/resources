RES
1b:a-lman-HEX-1:5|6:d
2n:n1
LIN
1:1o(1+5)2n
NON
NON1
Parent:1
Linkage:o(1+5)n
SmallMolecule:SMILES CCCCC[C@H]1CC2=C({5}C(O)=C({7}C(O)=C2CO1)C)C
Description:colletobredin aglycon