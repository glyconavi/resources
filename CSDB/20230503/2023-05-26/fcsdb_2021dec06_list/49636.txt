RES
1n:n1
2b:b-dxyl-PEN-1:5
3s:acetyl
LIN
1:1n(6+1)2d
2:1n(9+1)3n
NON
NON1
SmallMolecule:SMILES CC(C)/C3=C/2[C@H]1C{9}[C@@H](O)/C({15}CO)=C\{6}[C@H](O)[C@]1(C)CC[C@@]2(C)CC3
Description:erinacine Q aglycon