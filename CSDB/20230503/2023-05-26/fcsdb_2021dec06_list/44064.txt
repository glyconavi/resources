RES
1b:x-dman-HEX-1:5
2b:a-dgal-HEX-1:5
3s:amino
4n:n1
LIN
1:1o(2+1)2d
2:1d(1+1)3n
3:1o(1+2)4n
NON
NON1
Parent:1
Linkage:o(1+2)n
SmallMolecule:SMILES N{2}c1ccccn1
Description:2-aminopyridine