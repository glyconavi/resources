RES
1b:b-dgal-HEX-1:4
2n:n1
3b:b-dgal-HEX-1:4
4b:b-dgal-HEX-1:4
LIN
1:1o(1+1)2n
2:1o(5+1)3d
3:3o(6+1)4d
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES O{1}CCCN
Description:propanolamine