RES
1b:b-dxyl-HEX-1:5|1:d|4:d
2n:n1
LIN
1:1h(1+3)2n
NON
NON1
Parent:1
Linkage:h(1+3)n
SmallMolecule:SMILES CCCCCCC(C/C(C)=C\C(/C=C(/C=C/{8}C(O)C(C)(C1=CC({3}C={2}C(O)O1)=O)C)C){27}CO)C
Description:fusapyrone aglycon