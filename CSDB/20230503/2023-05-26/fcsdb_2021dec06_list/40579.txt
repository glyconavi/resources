RES
1b:x-xman-HEX-1:5
2n:n1
3n:n2
LIN
1:1o(3+-1)2n
2:1o(1+-1)3n
NON
NON1
Parent:1
Linkage:o(3+-1)n
HistoricalEntity:superclass: any residue
NON2
Parent:1
Linkage:o(1+-1)n
HistoricalEntity:superclass: any residue