RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+21)2n
NON
NON1
Parent:1
Linkage:o(1+21)n
SmallMolecule:SMILES C/C(C)=C\CC[C@@H]({21}C(=O)O)[C@H]1CC[C@@]2(C)/C4=C(CC[C@]12C)/[C@@]3(C)CC{3}[C@@H](O)C(C)(C)[C@@H]3CC4
Description:fomitoside H, I aglycon