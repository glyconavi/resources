RES
1n:n1
2n:n2
3b:b-dman-HEX-1:5
4b:a-dman-HEX-1:5
5b:b-xgal-HEX-1:4
LIN
1:1n(-1+1)2n
2:2n(2+1)3d
3:3o(3+1)4d
4:3o(6+1)5d
NON
NON1
HistoricalEntity:superclass: lipid residue
NON2
Parent:1
Linkage:n(-1+1)n
SmallMolecule:SMILES [6C@H](O)1[5C@H](O)[4C@@H](O)[3C@H](O)[2C@H](O)[1C@H](O)1
Description:myo-inositol