RES
1n:n1
2b:o-dman-HEX-0:0|1:aldi
3s:acetyl
4n:n2
5n:n3
LIN
1:1n(1+1)2d
2:1n(3+1)3n
3:1n(5+1)4n
4:4n(5+1)5n
NON
NON1
SmallMolecule:SMILES CCCCC{5}C(O)C{3}C(O)C{1}C(O)=O
Description:3,5-dihydroxydecanoic acid
NON2
Parent:1
Linkage:n(5+1)n
SmallMolecule:SMILES CCCCC{5}C(O)C{3}C(O)C{1}C(O)=O
Description:3,5-dihydroxydecanoic acid
NON3
Parent:4
Linkage:n(5+1)n
SmallMolecule:SMILES CCCCC{5}C(O)C{3}C(O)C{1}C(O)=O
Description:3,5-dihydroxydecanoic acid