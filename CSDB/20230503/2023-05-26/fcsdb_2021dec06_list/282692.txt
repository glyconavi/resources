RES
1b:o-dgal-HEX-0:0|1:aldi
2b:a-dglc-HEX-1:5
3b:b-dgal-HEX-1:4
4b:a-dglc-HEX-1:5
LIN
1:1o(2+1)2d
2:1o(6+1)3d
3:3o(2+1)4d