RES
1r:r1
REP
REP1:3o(4+1)2n=-1--1
RES
2n:n1
3b:x-dglc-HEX-1:5
4s:acetyl
LIN
1:2n(8+1)3d
2:3o(6+1)4n
NON
NON1
SmallMolecule:SMILES C/C({8}CO)=C/CCC(C{1}C(O)=O)C
Description:citronellal