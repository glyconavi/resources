RES
1n:n1
2b:a-lman-HEX-1:5|6:d
3b:b-dman-HEX-1:5
LIN
1:1n(5+1)2d
2:1n(9+1)3d
NON
NON1
SmallMolecule:SMILES O{17}C(C(CC)C)C(C)/C=C(C)/{13}[C@@H](O)[C@@H](C)/C=C(C)/{9}[C@@H](O)[C@@H](C)/C=C(C)/{5}[C@@H](O)[C@@H](C)/C=C(C)/{1}C(O)=O
Description:phialotide A, B, C, D aglycon