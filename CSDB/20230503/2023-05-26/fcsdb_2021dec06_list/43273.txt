RES
1b:b-dglc-HEX-1:5
2s:methyl
3n:n1
LIN
1:1o(4+1)2n
2:1o(1+8)3n
NON
NON1
Parent:1
Linkage:o(1+8)n
SmallMolecule:SMILES O=C({8}CO)Nc1ccccc1O
Description:N-(2-hydroxyphenyl)-2-hydroxyacetamide