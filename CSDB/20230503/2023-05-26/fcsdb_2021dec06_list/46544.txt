RES
1b:a-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+7)2n
NON
NON1
Parent:1
Linkage:o(1+7)n
SmallMolecule:SMILES Cc1ccc(N(=O)=O)cc1{7}CO
Description:(2-methyl-5-nitrophenyl)methanol