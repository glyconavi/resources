RES
1b:o-dara-PEN-0:0|1:aldi
2n:n1
LIN
1:1o(1+7)2n
NON
NON1
Parent:1
Linkage:o(1+7)n
SmallMolecule:SMILES Cc1c(C)c({7}C(=O)O)c(O)cc1O
Description:2,4-dihydroxy-5,6-dimethylbenzoic acid