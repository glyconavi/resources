RES
1n:n1
2n:n2
3b:a-llyx-HEX-1:5|2:d|6:d
4s:n-dimethyl
5b:a-ltre-HEX-1:5|2:d|3:d|6:d
6b:a-ltre-HEX-1:5|2:d|3:d|6:d
LIN
1:1n(1+16)2n
2:2n(7+1)3d
3:3d(3+1)4n
4:3o(4+1)5d
5:5o(4+1)6d
NON
NON1
SmallMolecule:SMILES [1CH3]O
Description:methyl
NON2
Parent:1
Linkage:n(1+16)n
SmallMolecule:SMILES O{7}[C@H]1C[C@@](C)(O)[C@H]({16}C(O)=O)C2=CC3=C(C(C4=C(C=CC=C4O)C3=O)=O)C(O)=C12