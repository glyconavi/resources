RES
1n:n1
2n:n2
3b:b-dglc-HEX-1:5
4s:methyl
LIN
1:1n(1+1)2n
2:2n(8+1)3d
3:3o(4+1)4n
NON
NON1
SmallMolecule:SMILES [1CH3]O
Description:methyl
NON2
Parent:1
Linkage:n(1+1)n
SmallMolecule:SMILES O{1}C1=C{3}C(O)=C(C{8}CO)C=C1
Description:4-(2-hydroxyethyl)-resorcinol