RES
1n:n1
2b:a-lman-HEX-1:5|6:d
3s:methyl
LIN
1:1n(3+1)2d
2:1n(1+1)3n
NON
NON1
SmallMolecule:SMILES CCCCCCCC{3}[C@H](O)C{1}C(=O)O
Description:R-3-hydroxyundecanoic acid