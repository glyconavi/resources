RES
1b:b-dglc-HEX-1:5
2s:acetyl
3n:n1
4b:b-dglc-HEX-1:5
5s:acetyl
6s:acetyl
7s:acetyl
LIN
1:1o(6+1)2n
2:1o(1+15)3n
3:1o(4+1)4d
4:4o(2+1)5n
5:4o(3+1)6n
6:4o(4+1)7n
NON
NON1
Parent:1
Linkage:o(1+15)n
SmallMolecule:SMILES O{15}CCCCCCCCCCCCC{2}C(O){1}C(O)=O
Description:2,15-dihydroxypentadecanoic acid