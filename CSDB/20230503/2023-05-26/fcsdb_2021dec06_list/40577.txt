RES
1b:x-xgal-HEX-1:5
2n:n1
LIN
1:1o(1+-1)2n
NON
NON1
Parent:1
Linkage:o(1+-1)n
HistoricalEntity:superclass: any residue