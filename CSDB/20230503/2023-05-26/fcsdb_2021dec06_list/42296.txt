RES
1n:n1
2b:b-dglc-HEX-1:5
3a:a1
LIN
1:1n(1+1)2d
2:1n(2+1)3n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:4
RES
4n:n2
ALTSUBGRAPH2
LEAD-IN RES:5
RES
5n:n3
NON
NON1
SmallMolecule:SMILES CCCCCCCCCC(C){8}C(O)CC/C=C/{3}[C@@H](O){2}[C@@H](N){1}CO
Description:(2S,3R,E)-2-amino-9-methyloctadec-4-ene-1,3,8-triol
NON2
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2C@@H](O)[1C](=O)O
Description:(R)-2-hydroxy-hexadecanoic acid
NON3
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2C@@H](O)[1C](=O)O
Description:(R)-2-hydroxy-stearic acid