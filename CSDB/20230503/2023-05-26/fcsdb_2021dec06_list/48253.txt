RES
1b:b-dglc-HEX-1:5
2s:acetyl
3n:n1
4b:b-dglc-HEX-1:5
5s:acetyl
LIN
1:1o(6+1)2n
2:1o(1+-1)3n
3:1o(2+1)4d
4:4o(6+1)5n
NON
NON1
Parent:1
Linkage:o(1+-1)n
HistoricalEntity:superclass: lipid residue