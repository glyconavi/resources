RES
1b:b-dman-HEX-1:5
2s:acetyl
3n:n1
LIN
1:1o(6+1)2n
2:1o(1+21)3n
NON
NON1
Parent:1
Linkage:o(1+21)n
SmallMolecule:SMILES O=C1C(N({2}C(O)=C1C(C(CCCCCCCCCCCCC{21}CO)C)=O)C)C
Description:epicoccamide aglycon