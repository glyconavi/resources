RES
1n:n1
2b:b-dglc-HEX-1:5
3s:methyl
LIN
1:1n(1+1)2d
2:1n(8+1)3n
NON
NON1
SmallMolecule:SMILES O{1}c1cccc2ccc{8}c(O)c12
Description:1,8-dihydroxynaphtalene, 8-hydroxy-1-naphtol