RES
1n:n1
2b:a-dglc-HEX-1:5
3s:acetyl
4s:acetyl
LIN
1:1n(3+1)2d
2:1n(15+1)3n
3:1n(4+1)4n
NON
NON1
SmallMolecule:SMILES C/C2=C/[C@H]1O[C@@H]3{3}[C@H](O){4}[C@@H](O)[C@](C)([C@@]1({15}CO)CC2)[C@@]34CO4
Description:scirpenol