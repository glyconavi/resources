RES
1n:n1
2n:n2
3s:phosphate
4n:n3
5b:b-dman-HEX-1:5
LIN
1:1n(2+1)2n
2:1n(1+1)3n
3:3n(1+1)4n
4:4n(2+1)5d
NON
NON1
HistoricalEntity:any sphingosine, any sphingenine
NON2
Parent:1
Linkage:n(2+1)n
HistoricalEntity:superclass: lipid residue
NON3
Parent:3
Linkage:n(1+1)n
SmallMolecule:SMILES [6C@H](O)1[5C@H](O)[4C@@H](O)[3C@H](O)[2C@H](O)[1C@H](O)1
Description:myo-inositol