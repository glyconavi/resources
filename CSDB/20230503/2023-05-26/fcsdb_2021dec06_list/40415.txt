RES
1r:r1
REP
REP1:2o(3+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3n:n1
LIN
1:2o(4+1)3n
NON
NON1
Parent:2
Linkage:o(4+1)n
HistoricalEntity:superclass: any residue