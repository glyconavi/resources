RES
1b:b-dxyl-PEN-1:5
2n:n1
LIN
1:1o(1+6)2n
2:1o(2+7)2n
NON
NON1
Parent:1
Linkage:o(1+6)n|o(2+7)n
SmallMolecule:SMILES CC(C)/C3=C/2[C@H]1C/C=C(C=O)\{7}[C@@H](O){6}[C@H](O)[C@]1(C)CC[C@@]2(C)CC3
Description:erinacine B aglycon