RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+6)2n
NON
NON1
Parent:1
Linkage:o(1+6)n
SmallMolecule:SMILES C=C[C@H]1{6}[C@H](O)OC=C2C1=CCOC2=O
Description:gentiopicroside aglycon