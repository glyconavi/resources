RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+1)2n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES C/C(=C\{1}CO)/CC/C=C(\C)/CO
Description:8-hydroxygeraniol