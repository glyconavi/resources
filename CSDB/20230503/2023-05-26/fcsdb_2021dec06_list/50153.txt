RES
1n:n1
2b:b-dglc-HEX-1:5
3n:n2
LIN
1:1n(1+1)2d
2:1n(2+1)3n
NON
NON1
HistoricalEntity:2S,3S,4R,4E,8E-2-amino-9-methyl-4,8-octadecadiene-1,3,4-triol
NON2
Parent:1
Linkage:n(2+1)n
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH](O)[1C](=O)O
Description:2-hydroxy-hexadecanoic acid