RES
1n:n1
2n:n2
3b:b-dglc-HEX-1:5
LIN
1:1n(1+2)2n
2:2n(1+1)3d
NON
NON1
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2C@@H](O)[1C](=O)O
Description:(R)-2-hydroxy-stearic acid
NON2
Parent:1
Linkage:n(1+2)n
HistoricalEntity:2S,3R,4E-2-amino-9-methyl-octadeca-4-en-1,3,9-triol