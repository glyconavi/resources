RES
1r:r1
REP
REP1:2o(6+1)2d=-1--1
RES
2b:a-dman-HEX-1:5
3a:a1
LIN
1:2o(2+1)3n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:4
RES
4b:a-dman-HEX-1:5
5b:a-dman-HEX-1:5
6b:a-dman-HEX-1:5
7s:phosphate
8b:a-dman-HEX-1:5
9b:a-dman-HEX-1:5
LIN
1:4o(2+1)5d
2:5o(3+1)6d
3:5o(6+1)7n
4:7n(1+1)8o
5:8o(3+1)9d
ALTSUBGRAPH2
LEAD-IN RES:10
RES
10b:a-dman-HEX-1:5
11b:a-dman-HEX-1:5
LIN
1:10o(2+1)11d
ALTSUBGRAPH3
LEAD-IN RES:12
RES
12b:a-dman-HEX-1:5