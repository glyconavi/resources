RES
1n:n1
2s:methyl
3b:b-dglc-HEX-1:5
4s:methyl
LIN
1:1n(54+1)2n
2:1n(7+1)3d
3:3o(4+1)4n
NON
NON1
SmallMolecule:SMILES C1=C{54}C(={53}C(C=C1C2=CC(=O)C3={5}C({6}C={7}C({8}C=C3O2)O)O)O)O
Description:luteolin