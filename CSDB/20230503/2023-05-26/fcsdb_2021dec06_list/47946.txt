RES
1b:b-dery-PEN-1:4|3:d
2n:n1
LIN
1:1o(1+9)2n
NON
NON1
Parent:1
Linkage:o(1+9)n
SmallMolecule:SMILES Nc1ncnc2[nH]{9}cnc12
Description:adenine