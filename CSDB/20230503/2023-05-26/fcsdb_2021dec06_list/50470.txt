RES
1b:a-lman-HEX-1:5|6:d
2n:n1
LIN
1:1o(1+4)2n
NON
NON1
Parent:1
Linkage:o(1+4)n
SmallMolecule:SMILES C/C(CC/C=C(C)/CC/C=C(C)\{65}CO)=C\CC1=C{1}C(O)=C(C)C={4}C1O
Description:4,15'-dihydroxyfarnesyltoluquinol