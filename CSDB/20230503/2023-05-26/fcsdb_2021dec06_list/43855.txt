RES
1b:a-dman-HEX-1:5
2n:n1
3b:a-dman-HEX-1:5
4b:a-dman-HEX-1:5
LIN
1:1o(1+3)2n
2:1o(2+1)3d
3:3o(2+1)4d
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES [4CH3][3C@@H](O)[2C@H](N)[1C](=O)O
Description:(L)-threonine