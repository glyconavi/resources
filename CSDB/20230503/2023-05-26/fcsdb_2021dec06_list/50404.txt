RES
1b:b-dman-HEX-1:5
2s:methyl
3n:n1
LIN
1:1o(4+1)2n
2:1o(1+11)3n
NON
NON1
Parent:1
Linkage:o(1+11)n
SmallMolecule:SMILES C/C(CCO)=C/C(N(O)CCC[C@@H]1NC([C@H](CCCN(O)C(C[C@](C)(O)C{11}CO)=O)NC1=O)=O)=O