RES
1b:o-dara-PEN-0:0|1:aldi
2n:n1
LIN
1:1o(1+1)2n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2]/[13CH]=[12CH]\[11CH2]/[10CH]=[9CH]\[8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:linoleic acid