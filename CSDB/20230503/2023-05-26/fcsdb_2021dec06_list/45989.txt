RES
1b:b-dglc-HEX-1:5
2s:acetyl
3b:b-dglc-HEX-1:5
4s:acetyl
5n:n1
6b:b-dglc-HEX-1:5
7s:acetyl
8b:b-dglc-HEX-1:5
9s:acetyl
LIN
1:1o(6+1)2n
2:1o(2+1)3d
3:3o(6+1)4n
4:3o(4+1)5n
5:5n(18+1)6d
6:6o(6+1)7n
7:6o(2+1)8d
8:8o(6+1)9n
NON
NON1
Parent:3
Linkage:o(4+1)n
SmallMolecule:SMILES [18CH2](O)[17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2]/[10CH]=[9CH]\[8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:18-hydroxy-cis-9-octadecenoic acid