RES
1b:o-lery-TET-0:0|1:aldi
2n:n1
3b:b-dman-HEX-1:5
4s:acetyl
5s:acetyl
6n:n2
7n:n3
LIN
1:1o(1+1)2n
2:1o(4+1)3d
3:3o(4+1)4n
4:3o(6+1)5n
5:3o(2+1)6n
6:3o(3+1)7n
NON
NON1
Parent:1
Linkage:o(1+1)n
HistoricalEntity:superclass: lipid residue
NON2
Parent:3
Linkage:o(2+1)n
HistoricalEntity:superclass: lipid residue
NON3
Parent:3
Linkage:o(3+1)n
HistoricalEntity:superclass: lipid residue