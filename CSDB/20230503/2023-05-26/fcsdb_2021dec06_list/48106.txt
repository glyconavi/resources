RES
1b:b-dalt-HEX-1:5
2n:n1
LIN
1:1o(1+19)2n
NON
NON1
Parent:1
Linkage:o(1+19)n
SmallMolecule:SMILES C[C@]1(C=C)CCC2[C@@](CCC([C@]3(C){19}CO)=O)(C3C[C@@H](O)C2=C1)C
Description:3-oxo-isopimara-8(14),15-diene-7α,19-diol