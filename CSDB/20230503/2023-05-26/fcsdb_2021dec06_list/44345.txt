RES
1b:b-dglc-HEX-1:5
2s:acetyl
3n:n1
4b:b-dglc-HEX-1:5
5s:acetyl
LIN
1:1o(6+1)2n
2:1o(1+13)3n
3:1o(2+1)4d
4:4o(6+1)5n
NON
NON1
Parent:1
Linkage:o(1+13)n
SmallMolecule:SMILES [22CH3][21CH2][20CH2][19CH2][18CH2][17CH2][16CH2][15CH2][14CH2][13C@H](O)[12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:(S)-13-hydroxy-docosanoic acid