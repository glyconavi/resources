RES
1b:x-dglc-HEX-1:5|6:a
2n:n1
LIN
1:1o(6+9)2n
NON
NON1
Parent:1
Linkage:o(6+9)n
SmallMolecule:SMILES C1=CC=C(C=C1)CC{9}CO
Description:3-phenyl-1-propanol