RES
1r:r1
REP
REP1:3o(3+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4n:n1
LIN
1:2o(3+1)3d
2:3o(6+1)4n
NON
NON1
Parent:3
Linkage:o(6+1)n
SmallMolecule:SMILES OC{1}C(O)OC(CO)CO
Description:2-(1,2-dihydroxyethoxy)propane-1,3-diol