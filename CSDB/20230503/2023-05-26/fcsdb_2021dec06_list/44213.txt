RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+4)2n
NON
NON1
Parent:1
Linkage:o(1+4)n
SmallMolecule:SMILES C[C@H]1CCC{13}[C@@H](CCC/C=C/C2=C{4}C(O)=C{2}C(O)=C2C(O1)=O)O
Description:α-zearalenol