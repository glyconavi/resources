RES
1r:r1
REP
REP1:10o(6+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3b:a-dgal-HEX-1:5
4b:b-dglc-HEX-1:5
5b:b-dglc-HEX-1:5
6b:a-dman-HEX-1:5
7b:a-dgal-HEX-1:5
8b:b-dglc-HEX-1:5
9b:a-dgal-HEX-1:5
10b:a-dgal-HEX-1:5
11b:a-lgal-HEX-1:5|6:d
LIN
1:2o(6+1)3d
2:3o(6+1)4d
3:4o(3+1)5d
4:5o(3+1)6d
5:6o(2+1)7d
6:6o(4+1)8d
7:8o(6+1)9d
8:9o(6+1)10d
9:9o(4+1)11d