RES
1n:n1
2b:a-dglc-HEX-1:5
3n:n2
4b:b-dglc-HEX-1:5
5s:acetyl
6s:acetyl
LIN
1:1n(1+1)2d
2:1n(2+1)3n
3:2o(4+1)4d
4:3n(4+1)5n
5:3n(2+1)6n
NON
NON1
SmallMolecule:SMILES CCCCCCCCCCCCCCCC{2}C(O){1}CO
Description:octadecane-1,2-diol
NON2
Parent:1
Linkage:n(2+1)n
SmallMolecule:SMILES O=C(O)C{4}C(O)C{2}C(O){1}C(=O)O
Description:2,4-dihydroxyhexanedioic acid