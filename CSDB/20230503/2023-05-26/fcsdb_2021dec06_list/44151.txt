RES
1b:x-dman-HEX-1:5
2s:amino
3n:n1
4b:a-dman-HEX-1:5
5b:a-dgal-HEX-1:4
LIN
1:1d(1+1)2n
2:1o(1+2)3n
3:1o(2+1)4d
4:4o(2+1)5d
NON
NON1
Parent:1
Linkage:o(1+2)n
SmallMolecule:SMILES N{2}c1ccccn1
Description:2-aminopyridine