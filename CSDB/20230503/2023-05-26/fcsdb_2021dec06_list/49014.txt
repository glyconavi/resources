RES
1b:a-dman-HEX-1:5
2b:a-dman-HEX-1:5
3b:b-dgal-HEX-1:4
4b:b-dgal-HEX-1:4
5b:b-dgal-HEX-1:4
6b:b-dgal-HEX-1:4
7b:b-dgal-HEX-1:4
8b:b-dgal-HEX-1:4
9b:b-dgal-HEX-1:4
10b:b-dgal-HEX-1:4
LIN
1:1o(2+1)2d
2:1o(6+1)3d
3:3o(5+1)4d
4:4o(5+1)5d
5:5o(6+1)6d
6:6o(5+1)7d
7:7o(5+1)8d
8:8o(5+1)9d
9:9o(5+1)10d