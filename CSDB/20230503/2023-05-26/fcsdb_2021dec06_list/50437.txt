RES
1b:b-dman-HEX-1:5
2s:acetyl
3n:n1
LIN
1:1o(6+1)2n
2:1o(1+21)3n
NON
NON1
Parent:1
Linkage:o(1+21)n
SmallMolecule:SMILES CC1C(/C(C(N1C)=O)={6}C(O)\C(CCCCCCCCCCCCC{21}CO)C)=O
Description:epicoccamide A, B, C aglycon