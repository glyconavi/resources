RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4s:methyl
5s:methyl
LIN
1:1n(8+1)2d
2:1n(57+1)3d
3:2o(4+1)4n
4:3o(4+1)5n
NON
NON1
SmallMolecule:SMILES C/C=C/C=C/C(CC1=C(CC2=C3O[C@H]([C@@H](C(C3={5}C(C4=C2C={8}C(O)C={6}C4O)O)=O)C)C){58}C(O)={57}C(O){56}C(O)=C1)=O
Description:indigotide E aglycon