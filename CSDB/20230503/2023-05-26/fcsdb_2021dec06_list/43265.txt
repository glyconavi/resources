RES
1b:a-drib-PEN-1:4
2n:n1
LIN
1:1o(1+53)2n
NON
NON1
Parent:1
Linkage:o(1+53)n
SmallMolecule:SMILES O{53}C1=CC(C)=C(OC2=C{2}C(O)=CC(C)=C2){4}C(O)=C1
Description:4-(3-hydroxy-5-methylphenoxy)-5-methylbenzene-1,3-diol