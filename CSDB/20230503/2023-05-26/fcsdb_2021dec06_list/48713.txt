RES
1b:b-dglc-HEX-1:5
2b:b-dxyl-PEN-1:5
3n:n1
LIN
1:1o(6+1)2d
2:1o(1+4)3n
NON
NON1
Parent:1
Linkage:o(1+4)n
SmallMolecule:SMILES CC(C1=CC={4}C(O)C=C1)=O
Description:piceol