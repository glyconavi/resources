RES
1n:n1
2n:n2
LIN
1:1n(1+9)2n
NON
NON1
SmallMolecule:SMILES C[S+](CCC[NH3+])C[C@H]1O{1}[C@@H](O)[C@H](O)[C@@H]1O
NON2
Parent:1
Linkage:n(1+9)n
SmallMolecule:SMILES Nc1ncnc2[nH]{9}cnc12
Description:adenine