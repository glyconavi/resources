RES
1b:b-dglc-HEX-1:5
2s:methyl
3n:n1
LIN
1:1o(4+1)2n
2:1o(1+7)3n
NON
NON1
Parent:1
Linkage:o(1+7)n
SmallMolecule:SMILES O=C1C(C2=CC={54}C(O)C=C2)=COC3=C1C=C{7}C(O)={8}C3
Description:daidzein