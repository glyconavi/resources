RES
1b:a-drib-PEN-1:4
2n:n1
LIN
1:1o(1+6)2n
NON
NON1
Parent:1
Linkage:o(1+6)n
SmallMolecule:SMILES C[C@@H]2Cc1c(O){6}c(O)cc(O)c1C(=O)O2
Description:R-(−)-3,4-dihydro-3-methyl-5,8-dihydroxy-1H-2-benzopyran1-one