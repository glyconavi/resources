RES
1n:n1
2s:methyl
3b:b-dglc-HEX-1:5
4s:methyl
LIN
1:1n(6+1)2n
2:1n(53+1)3d
3:3o(4+1)4n
NON
NON1
SmallMolecule:SMILES O=C1C=C(OC2=C1C={6}C(O)C=C2)C3=CC=C{53}C(O)=C3
Description:6,3'-dihydroxyflavone