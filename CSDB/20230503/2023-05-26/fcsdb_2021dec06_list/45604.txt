RES
1b:b-dgal-HEX-1:5
2n:n1
LIN
1:1o(1+1)2n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES C=C[C@@]3(C)CC[C@@H]2[C@](C)(CC[C@@H]1C(C)(C)CC{1}[C@H](O)[C@]12C)O3
Description:1α-hydroxy-ent-13-epi-manoyl oxide