RES
1b:b-dglc-HEX-1:5
2b:b-dglc-HEX-1:5
3n:n1
LIN
1:1o(2+1)2d
2:1o(1+12)3n
NON
NON1
Parent:1
Linkage:o(1+12)n
SmallMolecule:SMILES C{17}C(O)CCCC{12}C(O)C/C=C/CCCCCCC{1}C(=O)O
Description:17-hydroxy-ricinoleic acid