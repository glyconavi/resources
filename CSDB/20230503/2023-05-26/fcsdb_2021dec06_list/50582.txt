RES
1b:b-dglc-HEX-1:5
2s:methyl
3n:n1
LIN
1:1o(4+1)2n
2:1o(1+2)3n
NON
NON1
Parent:1
Linkage:o(1+2)n
SmallMolecule:SMILES O{2}C1=CC=CC=C1C{8}CO
Description:2-(2-hydroxyphenyl)-ethanol