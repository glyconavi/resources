RES
1b:b-dglc-HEX-1:5
2s:methyl
3n:n1
LIN
1:1o(4+1)2n
2:1o(1+6)3n
NON
NON1
Parent:1
Linkage:o(1+6)n
SmallMolecule:SMILES O=c1oc(CO)cc2c{6}c(O)cc(O)c12
Description:6,8-dihydroxy-3-hydroxymethylisocoumarin