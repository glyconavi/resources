RES
1b:b-dman-HEX-1:5
2a:a1
3s:acetyl
4s:acetyl
5n:n1
6n:n2
LIN
1:1o(-1+-1)2n
2:1o(4+1)3n
3:1o(6+1)4n
4:1o(2+1)5n
5:1o(3+1)6n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:7
RES
7b:o-drib-PEN-0:0|1:aldi
ALTSUBGRAPH2
LEAD-IN RES:8
RES
8b:o-dman-HEX-0:0|1:aldi
ALTSUBGRAPH3
LEAD-IN RES:9
RES
9b:o-dara-PEN-0:0|1:aldi
NON
NON1
Parent:1
Linkage:o(2+1)n
HistoricalEntity:superclass: lipid residue
NON2
Parent:1
Linkage:o(3+1)n
HistoricalEntity:superclass: lipid residue