RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+8)2n
NON
NON1
Parent:1
Linkage:o(1+8)n
SmallMolecule:SMILES C/C=C/C=C/C=C/[C@@H]1CC2=C({8}C(O)=C{6}C(O)=C2C(O1)=O)C
Description:(S)-3-[(1E,3E,5E)-hepta-1,3,5-trienyl]-3,4-dihydro-6,8-dihydroxy-5-methyl-1H-2-benzopyran-1-one