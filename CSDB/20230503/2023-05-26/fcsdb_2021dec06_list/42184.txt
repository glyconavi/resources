RES
1n:n1
2n:n2
3b:b-dglc-HEX-1:5
4s:phosphate
5s:amino
6n:n3
LIN
1:1n(-1+1)2n
2:1n(6+1)3d
3:1n(1+1)4n
4:3d(2+1)5n
5:4n(1+1)6n
NON
NON1
HistoricalEntity:superclass: any inositol
NON2
Parent:1
Linkage:n(-1+1)n
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:palmitic acid
NON3
Parent:4
Linkage:n(1+1)n
HistoricalEntity:superclass: lipid residue