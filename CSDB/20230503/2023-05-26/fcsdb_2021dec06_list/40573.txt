RES
1n:n1
2b:a-xman-HEX-x:x
3s:phosphate
4b:a-xman-HEX-x:x
5n:n2
LIN
1:1n(6+1)2d
2:1n(1+1)3n
3:2o(3+1)4d
4:3n(1+1)5n
NON
NON1
SmallMolecule:SMILES [6C@H](O)1[5C@H](O)[4C@@H](O)[3C@H](O)[2C@H](O)[1C@H](O)1
Description:myo-inositol
NON2
Parent:3
Linkage:n(1+1)n
HistoricalEntity:superclass: ceramide