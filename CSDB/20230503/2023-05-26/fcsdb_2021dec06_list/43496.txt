RES
1b:o-dman-HEX-0:0|1:aldi
2n:n1
3s:acetyl
LIN
1:1o(4+1)2n
2:2n(4+1)3n
NON
NON1
Parent:1
Linkage:o(4+1)n
SmallMolecule:SMILES C/C(C)=C\CCC(C)C(=O)C{4}C(O)/C(C)=C/{1}CO
Description:cosmosporaside B aglycon