RES
1b:a-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+2)2n
NON
NON1
Parent:1
Linkage:o(1+2)n
SmallMolecule:SMILES C{23}C(N){22}C(O)CCCCCCCCCCCCCCCCCC{3}C(O){2}C(C)O
Description:23-amino-tetracosane-2,3,22-triol