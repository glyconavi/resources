RES
1r:r1
REP
REP1:3o(3+1)2d=-1--1
RES
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4b:b-dglc-HEX-1:5
5a:a1
LIN
1:2o(3+1)3d
2:3o(6+1)4d
3:4o(6+1)5n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:6
RES
6s:glycolyl
ALTSUBGRAPH2
LEAD-IN RES:7
RES
7b:b-dglc-HEX-1:5
8s:glycolyl
LIN
1:7o(6+1)8n