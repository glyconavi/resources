RES
1b:o-dman-HEX-0:0|1:aldi
2n:n1
3b:b-dman-HEX-1:5
LIN
1:1o(1+1)2n
2:2n(13+1)3d
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES CC[C@H](C)C[C@H](C)/C=C(C)/{13}[C@@H](O)[C@@H](C)/C=C(C)/{9}[C@@H](O)[C@@H](C)/C=C(C)/{5}[C@@H](O)[C@@H](C)/C=C(C)/{1}C(=O)O
Description:rogerson aglycon