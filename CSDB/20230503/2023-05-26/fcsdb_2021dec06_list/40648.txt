RES
1b:b-dglc-HEX-1:5
2s:n-acetyl
3s:amino
4n:n1
5b:b-dglc-HEX-1:5
6s:n-acetyl
7n:n2
LIN
1:1d(2+1)2n
2:1d(1+1)3n
3:1o(1+4)4n
4:1o(4+1)5d
5:5d(2+1)6n
6:5o(4+-1)7n
NON
NON1
Parent:1
Linkage:o(1+4)n
SmallMolecule:SMILES [4C](=O)(N)[3CH2][2CH](N)[1C](=O)O
Description:asparagine
NON2
Parent:5
Linkage:o(4+-1)n
HistoricalEntity:outer core (ID 40642, ID 40647)