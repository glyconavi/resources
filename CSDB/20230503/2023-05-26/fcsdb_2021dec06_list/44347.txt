RES
1n:n1
2n:n2
3b:b-dglc-HEX-1:5
4b:b-dglc-HEX-1:5
LIN
1:1n(1+1)2n
2:2n(13+1)3d
3:3o(2+1)4d
NON
NON1
SmallMolecule:SMILES [1CH3]O
Description:methyl
NON2
Parent:1
Linkage:n(1+1)n
SmallMolecule:SMILES [22CH3][21CH2][20CH2][19CH2][18CH2][17CH2][16CH2][15CH2][14CH2][13C@H](O)[12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:(S)-13-hydroxy-docosanoic acid