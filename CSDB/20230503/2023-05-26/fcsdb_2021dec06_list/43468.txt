RES
1b:a-dall-HEX-1:5|6:d
2n:n1
LIN
1:1o(1+54)2n
NON
NON1
Parent:1
Linkage:o(1+54)n
SmallMolecule:SMILES O=C1CC(OC2=C1C=CC=C2)C3=CC={54}C(C=C3)O
Description:4'-hydroxyflavanone