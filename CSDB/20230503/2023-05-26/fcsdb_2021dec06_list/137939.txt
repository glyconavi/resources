RES
1b:a-dman-HEX-1:5
2n:n1
3b:a-dman-HEX-1:5
4b:b-dxyl-PEN-1:5
LIN
1:1o(1+1)2n
2:1o(3+1)3d
3:3o(2+1)4d
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES O=C(Nc1ccc(C{1}CO)cc1)C(F)(F)F
Description:2-(4-trifluoroacetamido-phenyl)ethanol