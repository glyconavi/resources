RES
1b:a-dglc-HEX-1:5
2s:acetyl
3n:n1
4n:n2
LIN
1:1o(3+1)2n
2:1o(1+9)3n
3:1o(6+2)4n
NON
NON1
Parent:1
Linkage:o(1+9)n
SmallMolecule:SMILES COC[C@]1(CC[C@]2([H])/C1=C\[C@]3({12}[C@@H](O)CC([C@](COC(C)=O)([H])C)=C3{9}[C@H]({8}[C@@H]([C@@H]2C)O)O)C)[H]
Description:fusicoccin A aglycon
NON2
Parent:1
Linkage:o(6+2)n
SmallMolecule:SMILES C=C{2}C(C)(C)O
Description:2-methylbut-3-en-2-ol