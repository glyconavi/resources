RES
1n:n1
2b:b-dglc-HEX-1:5
3s:methyl
4s:methyl
LIN
1:1n(4+1)2d
2:1n(54+1)3n
3:1n(7+1)4n
NON
NON1
SmallMolecule:SMILES O=C1/C(CC2=C1{4}C(O)=C{6}C(O)={7}C2O)=C/C3=CC={54}C(C=C3)O
Description:(Z)-6-hydroxyaurone