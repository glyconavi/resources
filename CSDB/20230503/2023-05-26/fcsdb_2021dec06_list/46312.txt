RES
1b:a-drib-PEN-1:4
2n:n1
LIN
1:1o(1+3)2n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES COC1=C{3}C(O)=CC(C(C2=C3{8}C(O)=CC({11}CO)=C2)=O)=C1C3=O
Description:questinol