RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+1)2n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES C/C(=C/C=C/C=C(C)/C=C/C=C(C)/C=C/C1=C(C)/CCCC1(C)C)/C=C/C=C(C)\C=C\C=C(C)/{1}C(=O)O
Description:neurosporaxanthin