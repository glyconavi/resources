RES
1b:b-dglc-HEX-1:5
2b:b-dglc-HEX-1:5
3n:n1
LIN
1:1o(6+1)2d
2:1o(1+1)3n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES [6CH]1=[5CH][4C]([N+]([O-])=O)=[3CH][2CH]=[1C]1(O)
Description:@paranitrolhenol~