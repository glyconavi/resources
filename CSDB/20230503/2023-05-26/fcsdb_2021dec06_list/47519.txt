RES
1b:b-dglc-HEX-1:5
2s:acetyl
3n:n1
4b:b-dglc-HEX-1:5
5s:acetyl
6s:acetyl
7s:acetyl
LIN
1:1o(6+1)2n
2:1o(1+16)3n
3:1o(4+1)4d
4:4o(2+1)5n
5:4o(3+1)6n
6:4o(6+1)7n
NON
NON1
Parent:1
Linkage:o(1+16)n
SmallMolecule:SMILES [16CH2](O)[15CH](O)[14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH](O)[1C](=O)O
Description:2,15,16-trihydroxy-hexadecanoic acid