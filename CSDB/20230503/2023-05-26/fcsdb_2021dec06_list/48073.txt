RES
1b:b-dalt-HEX-1:5
2n:n1
LIN
1:1o(1+19)2n
NON
NON1
Parent:1
Linkage:o(1+19)n
SmallMolecule:SMILES C[C@]1(C=C)CCC([C@@](C{3}[C@H]({2}[C@H](O)[C@]2(C){19}CO)O)(C2CC3=O)C)=C3C1
Description:virescenoside M aglycon