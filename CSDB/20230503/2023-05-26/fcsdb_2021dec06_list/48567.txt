RES
1b:x-dlyx-HEX-1:5|2:d|6:d
2s:n-dimethyl
3n:n1
4b:x-dara-HEX-1:5|2:d|6:d
5n:n2
LIN
1:1d(3+1)2n
2:1o(1+5)3n
3:1o(4+1)4d
4:4o(4+1)5n
NON
NON1
Parent:1
Linkage:o(1+5)n
SmallMolecule:SMILES CCOC(C(C=C(C=C1C(C2{5}C(O)=CC=CC23)=O){15}C(O)=O)=C1C3=O)=O
Description:juglanthraquinone A
NON2
Parent:4
Linkage:o(4+1)n
SmallMolecule:SMILES C[C@H]1C(=O)CC{1}[CH](O1)O
Description:cinerulose A