RES
1b:a-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+13)2n
NON
NON1
Parent:1
Linkage:o(1+13)n
SmallMolecule:SMILES CC1=C(C)C(C)=C(CCC[C@@]2({13}CO)C)C2=C1
Description:sporuloside aglycon