RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+61)2n
NON
NON1
Parent:1
Linkage:o(1+61)n
SmallMolecule:SMILES COC1=CC(C(C2=C3{58}C(O)=C({61}CO){57}C(O)=C2C/C=C(C)\C)=O)=C({51}C(O)=C1C4={3}C(C=C5C(C6=C(C(C5=C4OC)=O){8}C(O)=C({11}CO){6}C(O)=C6C/C=C(C)\C)=O)O)C3=O
Description:colleflaccinoside aglycon