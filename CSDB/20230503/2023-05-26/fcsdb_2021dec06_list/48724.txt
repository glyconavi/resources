RES
1r:r1
REP
REP1:5o(3+1)2d=-1--1
RES
2b:a-dman-HEX-1:5
3b:b-dxyl-PEN-1:5
4b:a-dman-HEX-1:5
5b:a-dman-HEX-1:5
6b:b-dglc-HEX-1:5|6:a
7b:b-dxyl-PEN-1:5
LIN
1:2o(2+1)3d
2:2o(3+1)4d
3:4o(3+1)5d
4:5o(2+1)6d
5:5o(4+1)7d