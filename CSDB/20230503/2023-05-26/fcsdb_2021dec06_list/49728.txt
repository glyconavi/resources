RES
1n:n1
2s:methyl
3n:n2
4n:n3
LIN
1:1n(4+1)2n
2:1n(3+1)3n
3:1n(3+2)3n
4:1n(1+17)4n
NON
NON1
SmallMolecule:SMILES C[C@H]1O{1}[C@@H](O)[C@@H](O){3}C(O)(O){4}[C@@H]1O
Description:6-deoxy-3-hydroxy-arabino-hexose
NON2
Parent:1
Linkage:n(3+1)n|n(3+2)n
SmallMolecule:SMILES O=C(O)CCCCC{2}[C@H](O){1}C(=O)O
Description:2S-hydroxysuberic acid, 2S-hydroxyoctanedioic acid
NON3
Parent:1
Linkage:n(1+17)n
SmallMolecule:SMILES C[C@@H]1CC[C@@H]2[C@@H]1C[C@]3({17}CO)[C@@H]4C[C@@]2(C=O)[C@@]3({13}C(O)=O)C(C(C)C)=C4
Description:sordaricin