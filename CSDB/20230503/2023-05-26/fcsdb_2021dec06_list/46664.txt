RES
1b:b-dglc-HEX-1:5
2s:methyl
3n:n1
LIN
1:1o(4+1)2n
2:1o(1+7)3n
NON
NON1
Parent:1
Linkage:o(1+7)n
SmallMolecule:SMILES C/C(C)=C\CC1=C2C(C(CC(C3=CC={54}C(O)C=C3)O2)=O)=C(OC)C={7}C1O
Description:isoxanthohumol