RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+15)2n
NON
NON1
Parent:1
Linkage:o(1+15)n
SmallMolecule:SMILES COC1={15}C(C(N2C3=CC=CC=C3C4=C2C1=NC=C4)=O)O
Description:nigakinone