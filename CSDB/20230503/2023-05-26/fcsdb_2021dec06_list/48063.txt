RES
1b:b-dalt-HEX-1:5
2n:n1
LIN
1:1o(1+19)2n
NON
NON1
Parent:1
Linkage:o(1+19)n
SmallMolecule:SMILES C[C@@]1(CCC2[C@@](C)(CCC([C@@]3({19}CO)C)=O)C3CC=C2C1)C=C
Description:3-oxo-isopimara-7(8),15-diene-19-ol