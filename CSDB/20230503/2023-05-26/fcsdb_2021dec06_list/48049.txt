RES
1b:b-dman-HEX-1:5|6:d
2s:methyl
3n:n1
LIN
1:1o(4+1)2n
2:1o(1+17)3n
NON
NON1
Parent:1
Linkage:o(1+17)n
SmallMolecule:SMILES C[C@@H]1CC[C@@H]2[C@@H]1C[C@]3({17}CO)[C@@H]4C[C@@]2(C=O)[C@@]3({13}C(O)=O)C(C(C)C)=C4
Description:sordaricin