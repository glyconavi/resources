RES
1b:b-dglc-HEX-1:5
2s:acetyl
3s:n-acetyl
4s:acetyl
5s:acetyl
6s:acetyl
7n:n1
LIN
1:1o(2+1)2n
2:1d(1+1)3n
3:1o(3+1)4n
4:1o(4+1)5n
5:1o(6+1)6n
6:1o(1+2)7n
NON
NON1
Parent:1
Linkage:o(1+2)n
SmallMolecule:SMILES N{2}[C@H]({1}C(O)=O)C{4}[C@@H](N){5}[C@H](O)C{7}[C@H](N){8}C(O)=O
Description:2,4,7-triamino-5-hydroxy-octane-1,8-dioic acid