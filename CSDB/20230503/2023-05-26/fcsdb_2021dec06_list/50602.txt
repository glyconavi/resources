RES
1b:b-dxyl-PEN-1:5
2n:n1
LIN
1:1o(1+6)2n
NON
NON1
Parent:1
Linkage:o(1+6)n
SmallMolecule:SMILES CC(C)/C3=C/2\C1=C\C=C({15}C(=O)O)/C{6}[C@H](O)[C@]1(C)CC[C@@]2(C)CC3
Description:erinacine H aglycon