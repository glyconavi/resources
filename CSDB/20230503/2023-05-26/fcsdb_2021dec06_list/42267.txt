RES
1n:n1
2b:b-dglc-HEX-1:5
3n:n2
LIN
1:1n(1+1)2d
2:1n(2+1)3n
NON
NON1
SmallMolecule:SMILES CCCCCCCCC{9}C(C)(O){8}C(O)CC{5}[C@H](O){4}[C@@H](O){3}[C@@H](O){2}[C@@H](N){1}CO
Description:(2S,3S,4R,5S)-2-amino-9-methyloctadecane-1,3,4,5,8,9-hexaol
NON2
Parent:1
Linkage:n(2+1)n
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2C@@H](O)[1C](=O)O
Description:(R)-2-hydroxy-hexadecanoic acid