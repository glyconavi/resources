RES
1b:b-dalt-HEX-1:5
2n:n1
LIN
1:1o(1+19)2n
NON
NON1
Parent:1
Linkage:o(1+19)n
SmallMolecule:SMILES C[C@@]1(CCC(C2=C1)[C@@](C)(CCC([C@@]3({19}CO)C)O)C3C[C@H]2O)C=C
Description:sandaracopimara-8(14),15-diene-3β,7α,19-triol