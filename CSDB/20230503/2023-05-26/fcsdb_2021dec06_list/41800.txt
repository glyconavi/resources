RES
1n:n1
2a:a1
3s:phosphate
4n:n2
5b:a-xman-HEX-1:5
LIN
1:1n(2+1)2n
2:1n(1+1)3n
3:3n(1+1)4n
4:4n(2+1)5d
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:6
RES
6n:n3
ALTSUBGRAPH2
LEAD-IN RES:7
RES
7n:n4
ALTSUBGRAPH3
LEAD-IN RES:8
RES
8n:n5
NON
NON1
SmallMolecule:SMILES [18CH3][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4C@@H](O)[3C@H](O)[2C@@H](N)[1CH2]O
Description:phytosphingosine
NON2
Parent:3
Linkage:n(1+1)n
SmallMolecule:SMILES [6C@H](O)1[5C@H](O)[4C@@H](O)[3C@H](O)[2C@H](O)[1C@H](O)1
Description:myo-inositol
NON3
SmallMolecule:SMILES [24CH3][23CH2][22CH2][21CH2][20CH2][19CH2][18CH2][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH](O)[1C](=O)O
Description:2-hydroxy-tetracosanoic acid
NON4
SmallMolecule:SMILES [22CH3][21CH2][20CH2][19CH2][18CH2][17CH2][16CH2][15CH2][14CH2][13CH2][12CH2][11CH2][10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH](O)[1C](=O)O
Description:2-hydroxy-docosanoic acid
NON5
HistoricalEntity:superclass: lipid residue