RES
1b:b-drib-HEX-1:5|2:d|6:d
2n:n1
LIN
1:1o(1+3)2n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C[C@]12CC[C@H]3[C@@H](CC{5}[C@@]4(O)[C@]3(C=O)CC{3}[C@H](O)C4){14}[C@@]1(O)CC[C@@H]2C5=CC(OC5)=O
Description:strophanthidin