RES
1b:b-dxyl-PEN-1:5
2n:n1
LIN
1:1o(1+8)2n
NON
NON1
Parent:1
Linkage:o(1+8)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3C](O)[2CH]=[1C]1[7CH2][8CH2]O
Description:3,4,8-trihydroxy-ethylbenzene