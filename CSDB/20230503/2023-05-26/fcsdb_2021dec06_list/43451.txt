RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+21)2n
NON
NON1
Parent:1
Linkage:o(1+21)n
SmallMolecule:SMILES C/C(C)=C\CC{20}[C@](C)(O)[C@H]1CC[C@]4(C)[C@@H]1{12}[C@H](O)C[C@@H]3[C@@]2(C)CCC(=O)OC(C)(C)[C@@H]2{6}[C@@H](O)C[C@]34C
Description:3a-oxa-3a-homo-6α,12β,20S-dihydroxydammar-3-one