RES
1n:n1
2b:b-dgal-HEX-1:4
3b:b-dgal-HEX-1:4
4b:b-dgal-HEX-1:4
5b:b-dgal-HEX-1:4
LIN
1:1n(2+1)2d
2:1n(13+1)3d
3:2o(5+1)4d
4:3o(5+1)5d
NON
NON1
SmallMolecule:SMILES O{13}[C@H]1C=C(C)[C@@](C[C@@H](C({2}CO)=C)CC2)([H])C2(C)C1
Description:HS-toxin C aglycon