RES
1b:b-dglc-HEX-1:5
2s:methyl
3n:n1
LIN
1:1o(4+1)2n
2:1o(1+54)3n
NON
NON1
Parent:1
Linkage:o(1+54)n
SmallMolecule:SMILES C/C(C)=C\CC1={52}C(O)C(C(/C=C/C2=CC={54}C(C=C2)O)=O)=C(OC)C={4}C1O
Description:xanthohumol