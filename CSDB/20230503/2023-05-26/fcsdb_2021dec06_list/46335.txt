RES
1b:o-dman-HEX-0:0|1:aldi
2n:n1
3s:methyl
4s:methyl
LIN
1:1o(6+7)2n
2:2n(2+1)3n
3:2n(4+1)4n
NON
NON1
Parent:1
Linkage:o(6+7)n
SmallMolecule:SMILES [6C]([8CH3])1=[5CH][4C](O)=[3CH][2C](O)=[1C]1[7C](=O)O
Description:2,4-dihydroxy-6-methylbenzoic acid