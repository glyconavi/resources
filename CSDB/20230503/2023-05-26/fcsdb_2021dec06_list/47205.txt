RES
1b:b-dglc-HEX-1:5
2s:amino
3n:n1
LIN
1:1d(1+1)2n
2:1o(1+1)3n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES O=Cc1c{1}[nH]c2ccccc12
Description:indole-3-carbaldehyde