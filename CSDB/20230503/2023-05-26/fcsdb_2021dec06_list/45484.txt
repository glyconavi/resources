RES
1b:b-dglc-HEX-1:5
2b:a-lman-HEX-1:5|6:d
3n:n1
LIN
1:1o(4+1)2d
2:1o(1+18)3n
NON
NON1
Parent:1
Linkage:o(1+18)n
SmallMolecule:SMILES C=C1{21}C(=O)O[C@@H](CCCCCCCCCCCCC{18}[C@H](C)O)[C@@H]1C(=O)O
Description:protoconstipatic acid