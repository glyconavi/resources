RES
1r:r1
REP
REP1:2o(3+1)2d=-1--1
RES
2b:a-dglc-HEX-1:5
3b:b-dgal-HEX-1:5
4s:methyl
LIN
1:2o(6+1)3d
2:3o(6+1)4n