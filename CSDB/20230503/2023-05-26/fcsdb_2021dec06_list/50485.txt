RES
1b:a-lman-HEX-1:5|6:d
2n:n1
LIN
1:1o(1+3)2n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O{3}[C@@H]1[C@@H](C2=CC={54}C(O)C=C2)OC3=C{7}C(O)=C{5}C(O)=C3C1=O
Description:aromadendrin