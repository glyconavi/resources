RES
1b:x-dman-HEX-1:5
2n:n1
3n:n2
4n:n3
LIN
1:1o(2+-1)2n
2:1o(1+-1)3n
3:1o(6+-1)4n
NON
NON1
Parent:1
Linkage:o(2+-1)n
HistoricalEntity:superclass: any residue
NON2
Parent:1
Linkage:o(1+-1)n
HistoricalEntity:superclass: any residue
NON3
Parent:1
Linkage:o(6+-1)n
HistoricalEntity:superclass: any residue