RES
1b:b-dglc-HEX-1:5
2s:methyl
3n:n1
LIN
1:1o(4+1)2n
2:1o(1+7)3n
NON
NON1
Parent:1
Linkage:o(1+7)n
SmallMolecule:SMILES CC(C1=NC=CC2=C1NC3=C2C=C{7}C(O)=C3)=O
Description:1-(7-hydroxy-9H-pyrido[3,4-b]indol-1-yl)ethan-1-one