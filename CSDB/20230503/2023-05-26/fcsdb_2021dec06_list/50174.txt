RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+28)2n
NON
NON1
Parent:1
Linkage:o(1+28)n
SmallMolecule:SMILES O{3}[C@H]1CC[C@]2(C)[C@@]3([H])CC=C4[C@]5([H])CC(C)(C)CC[C@@]({28}C(O)=O)5CC[C@](C)4[C@@](C)3CC[C@]([H])2C1(C)C
Description:oleanolic acid