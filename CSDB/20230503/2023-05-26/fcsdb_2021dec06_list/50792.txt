RES
1b:b-drib-PEN-1:4
2n:n1
LIN
1:1o(1+9)2n
NON
NON1
Parent:1
Linkage:o(1+9)n
SmallMolecule:SMILES OC1=NC=NC2=C1N={9}CN2
Description:6-hydroxypurine