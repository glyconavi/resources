RES
1n:n1
2s:phosphate
3n:n2
4a:a1
5b:a-dman-HEX-1:5
6b:a-dman-HEX-1:5
7b:b-dgal-HEX-1:5
LIN
1:1n(1+1)2n
2:2n(1+1)3n
3:3n(-1+-1)4n
4:4n(3+1)5d
5:5o(3+1)6d
6:5o(4+1)7d
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:8
LEAD-OUT RES:8+5
RES
8b:a-dman-HEX-1:5
ALTSUBGRAPH2
LEAD-IN RES:9
LEAD-OUT RES:9+5
RES
9b:a-dman-HEX-1:5
NON
NON1
HistoricalEntity:superclass: ceramide
NON2
Parent:2
Linkage:n(1+1)n
SmallMolecule:SMILES [6C@H](O)1[5C@H](O)[4C@@H](O)[3C@H](O)[2C@H](O)[1C@H](O)1
Description:myo-inositol