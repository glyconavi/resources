RES
1b:b-dalt-HEX-1:5|6:d
2s:methyl
3n:n1
LIN
1:1o(4+1)2n
2:1o(1+17)3n
NON
NON1
Parent:1
Linkage:o(1+17)n
SmallMolecule:SMILES C[C@@H]1CC[C@@H]2[C@@H]1C[C@]3({17}CO)[C@H]4C[C@@](C=O)2[C@]({13}[C@](O)=O)3C5(C(C)C5)C4=O
Description:sordarin E aglycon