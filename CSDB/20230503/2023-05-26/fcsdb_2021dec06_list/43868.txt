RES
1n:n1
2b:b-dglc-HEX-1:5
3s:acetyl
LIN
1:1n(21+1)2d
2:1n(3+1)3n
NON
NON1
SmallMolecule:SMILES C[C@]([C@]1(C)CC2)(CC[C@@H]1[C@@H](CCC(C(C)C)=C){21}C(O)=O)C3=C2[C@]4(C)[C@](CC3)([H])C(C)(C){3}[C@H](O)CC4
Description:fomitoside K aglycon