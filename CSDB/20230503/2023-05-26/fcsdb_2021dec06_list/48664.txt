RES
1b:b-dglc-HEX-1:5
2s:methyl
3n:n1
LIN
1:1o(4+1)2n
2:1o(1+3)3n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O=C1{3}C(O)=C(OC2=C1C=CC=C2)C3=CC=CC=C3
Description:flavonol