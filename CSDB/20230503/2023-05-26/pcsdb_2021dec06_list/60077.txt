RES
1n:n1
2b:b-dglc-HEX-1:5|6:a
3s:acetyl
4n:n2
LIN
1:1n(3+1)2d
2:1n(22+1)3n
3:1n(21+1)4n
NON
NON1
SmallMolecule:SMILES O{22}[C@@H]1[C@@]({16}[C@@H](O)C2)({28}CO)[C@](CC(C)(C){21}[C@H]1O)([H])C3=CC[C@@]([C@](CC{3}[C@@H]4O)(C)[C@]5([H])[C@]4(C){23}CO)([H])[C@](CC5)(C)[C@@]32C
Description:gymnemagenin
NON2
Parent:1
Linkage:n(21+1)n
SmallMolecule:SMILES [4CH3]/[3CH]=[2C]([5CH3])/[1C](=O)O
Description:tiglic acid