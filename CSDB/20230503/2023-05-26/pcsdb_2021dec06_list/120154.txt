RES
1n:n1
2b:b-dglc-HEX-1:5|6:a
3b:b-dxyl-PEN-1:5
4b:a-lman-HEX-1:5|6:d
5s:methyl
6b:a-lman-HEX-1:5|6:d
7b:b-dxyl-PEN-1:5
LIN
1:1n(3+1)2d
2:1n(28+1)3d
3:3o(3+1)4d
4:2o(6+1)5n
5:3o(2+1)6d
6:6o(4+1)7d
NON
NON1
SmallMolecule:SMILES CC1(C)[3C@@H](O)CC[C@]2(C)[C@@]3([H])CC=C4[C@]5([H])CC(C)(C)CC[C@@]([28C](O)=O)5[16C@H](O)C[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:echinocystic acid