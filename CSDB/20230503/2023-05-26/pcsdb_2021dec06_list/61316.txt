RES
1n:n1
2b:a-lara-PEN-1:5
3b:b-dglc-HEX-1:5
4b:a-lman-HEX-1:5|6:d
5b:b-dglc-HEX-1:5
6b:a-lman-HEX-1:5|6:d
7b:b-drib-PEN-1:5
8b:b-dglc-HEX-1:5
9b:b-dglc-HEX-1:5
10b:b-dglc-HEX-1:5
11n:n2
12s:methyl
13b:a-lman-HEX-1:5|6:d
14b:b-dglc-HEX-1:5
LIN
1:1n(3+1)2d
2:1n(28+1)3d
3:2o(2+1)4d
4:3o(6+1)5d
5:5o(4+1)6d
6:4o(3+1)7d
7:7o(4+1)8d
8:8o(4+1)9d
9:9o(3+1)10d
10:9o(2+9)11n
11:11n(4+1)12n
12:10o(6+1)13d
13:13o(2+1)14d
NON
NON1
SmallMolecule:SMILES [1CH2]1[2CH2][3C@H](O)[4C]([23CH3])([24CH3])[5C@@H]2[6CH2][7CH2][8C@]([26CH3])3[14C@@]([27CH3])4[15CH2][16CH2][17C@]([28C](=O)O)5[22CH2][21CH2][20C]([29CH3])([30CH3])[19CH2][18C@H]5/[13C]4=[12CH]/[11CH2][9C@@H]3[10C@]([25CH3])21
Description:oleanolic/oleanic acid
NON2
Parent:9
Linkage:o(2+9)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3C](O)[2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:caffeic acid