RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+19)2n
NON
NON1
Parent:1
Linkage:o(1+19)n
SmallMolecule:SMILES C=C[C@@H]1[C@H](CC2=NC=CC3=C2NC4=CC=CC=C34)C({23}C(O)=O)=CO{19}[C@H]1O
Description:lyalosidic acid aglycon