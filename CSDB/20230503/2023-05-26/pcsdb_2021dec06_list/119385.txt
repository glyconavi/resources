RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+9)2n
NON
NON1
Parent:1
Linkage:o(1+9)n
SmallMolecule:SMILES O=C1[C@@H](/C(C)={9}C/O)CC[C@H](C)C1
Description:2R,5S-2-((E)-1-hydroxyprop-1-en-2-yl)-5-methylcyclohexanone