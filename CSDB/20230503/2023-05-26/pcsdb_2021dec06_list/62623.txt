RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+1)2n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES N#C/C=C({1}CO)\{4}CO
Description:4-hydroxy-3-(hydroxymethyl)but-2-enenitrile