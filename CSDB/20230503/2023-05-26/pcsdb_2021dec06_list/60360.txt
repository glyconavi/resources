RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+15)2n
NON
NON1
Parent:1
Linkage:o(1+15)n
SmallMolecule:SMILES [H][C@@]12C(C(C=C1{15}CO)=O)=C(C{8}[C@@H]([C@@H]3[C@@H]2OC([C@H]3C)=O)O)C
Description:11α-methyl-lactucin