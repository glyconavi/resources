RES
1b:b-dglc-HEX-1:5
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4n:n1
LIN
1:1o(3+1)2d
2:1o(2+1)3d
3:1o(1+13)4n
NON
NON1
Parent:1
Linkage:o(1+13)n
SmallMolecule:SMILES C[C@@]1([19C](O)=O)CCC[C@]2(C)[C@H]1CC[C@]3(C4)[C@@]2([H])CC[13C@@](O)(C4=C)C3
Description:steviol