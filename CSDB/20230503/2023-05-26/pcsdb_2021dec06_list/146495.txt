RES
1b:b-dglc-HEX-1:5
2n:n1
3b:b-dgro-TET-1:4
4s:hydroxymethyl
5n:n2
LIN
1:1o(1+1)2n
2:1o(2+1)3d
3:3h(3+1)4n
4:3o(5+1)5n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES O={11}C(O)C1=CO{1}[C@H](O)[C@@H]2[C@H]1C{7}[C@H](O)C2=C
Description:gardoside aglycon
NON2
Parent:3
Linkage:o(5+1)n
SmallMolecule:SMILES O={1}C(O)/C(C)=C/CCC(C)C{8}CO
Description:6,7-dihydrofoliamenthic acid