RES
1b:b-dglc-HEX-1:5
2b:b-dglc-HEX-1:5
3n:n1
LIN
1:1o(6+1)2d
2:1o(1+3)3n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES CC1(C){3}[C@@H](O)CC[C@@]2([H])[C@]3(C)CC[C@]4(C)[C@@H]([C@H](C){22}[C@H](O){23}[C@@H](O){24}[C@@H](O){25}C(C)(O)C)CC[C@](C)4[C@@]([H])3CC=C12
Description:cucurbit-5-en-3β,22S,23R,24R,25-pentol