RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+7)2n
NON
NON1
Parent:1
Linkage:o(1+7)n
SmallMolecule:SMILES C{53}C({52}[C@H](O)CC1={7}C(O)C=C2C(C=CC(O2)=O)=C1)(O)C
Description:(R)-peucedanol