RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+8)2n
NON
NON1
Parent:1
Linkage:o(1+8)n
SmallMolecule:SMILES O=C1C(O)C(C2=CC={54}C(O)C=C2)OC3=C1{5}C(O)=C{7}C(O)={8}C3O
Description:5,7,8,4'-tetrahydroxy-dihydroflavanol