RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+3)2n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O{3}[C@H](C1)CC[C@@]2(C)C1=CC[C@]3([H])[C@@]2([H])CC[C@@]4(C)[C@]3([H])CC[C@]4([C@@H](C)CC[C@@H](CC)C(C)C)[H]
Description:β-sitosterol