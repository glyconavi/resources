RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+21)2n
NON
NON1
Parent:1
Linkage:o(1+21)n
SmallMolecule:SMILES C=C[C@@H]1[C@@H]2C[C@H]3c4[nH]c5ccccc5c(=O)c4CN3C(=O)C2=CO{21}[C@H]1O
Description:pumiloside aglycon