RES
1b:b-dglc-HEX-1:5|6:a
2b:a-lman-HEX-1:5|6:d
3b:b-dxyl-PEN-1:5
4s:methyl
5n:n1
LIN
1:1o(3+1)2d
2:1o(4+1)3d
3:1o(6+1)4n
4:1o(1+3)5n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES [1CH2]1[2CH2][3C@H](O)[4C]([23CH3])([24CH3])[5C@@H]2[6CH2][7CH2][8C@]([26CH3])3[14C@@]([27CH3])4[15CH2][16CH2][17C@]([28C](=O)O)5[22CH2][21CH2][20C]([29CH3])([30CH3])[19CH2][18C@H]5/[13C]4=[12CH]/[11CH2][9C@@H]3[10C@]([25CH3])21
Description:oleanolic/oleanic acid