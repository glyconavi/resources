RES
1n:n1
2a:a1
3s:acetyl
4b:b-dglc-HEX-1:5|6:a
5b:b-dglc-HEX-1:5
6b:b-dglc-HEX-1:5
LIN
1:1n(21+1)2n
2:1n(22+1)3n
3:1n(3+1)4d
4:4o(4+1)5d
5:4o(2+1)6d
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:7
RES
7n:n2
ALTSUBGRAPH2
LEAD-IN RES:8
RES
8n:n3
NON
NON1
SmallMolecule:SMILES C[C@]12CC{3}[C@H](O)[C@](C)({24}CO)C1CC[C@]3(C)C2CC=C4[C@@]3(C)C{16}[C@@H](O)[C@]5({28}CO)[C@H]4CC(C)(C){21}[C@@H](O){22}[C@@H]5O
Description:protoaescigenin
NON2
SmallMolecule:SMILES [4CH3]/[3CH]=[2C]([5CH3])/[1C](=O)O
Description:tiglic acid
NON3
SmallMolecule:SMILES [4CH3]/[3CH]=[2C]([5CH3])\[1C](=O)O
Description:angelic acid