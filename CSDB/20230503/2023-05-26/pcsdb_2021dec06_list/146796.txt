RES
1b:b-dglc-HEX-1:5
2s:acetyl
3n:n1
LIN
1:1o(2+1)2n
2:1o(1+8)3n
NON
NON1
Parent:1
Linkage:o(1+8)n
SmallMolecule:SMILES O{54}C1=CC=C(C=C1)[C@@H]2{3}[C@@H](O)CC3=C(O2){8}C(O)={7}C(O)C={5}C3O
Description:2R,3S-5,7,8,4'-tetrahydroxydihydroflavanol