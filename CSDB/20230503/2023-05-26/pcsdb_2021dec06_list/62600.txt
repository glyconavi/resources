RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+2)2n
NON
NON1
Parent:1
Linkage:o(1+2)n
SmallMolecule:SMILES N#CCC1={2}C(O)C={4}C(O)C=C1
Description:2-(2,4-dihydroxyphenyl)acetonitrile