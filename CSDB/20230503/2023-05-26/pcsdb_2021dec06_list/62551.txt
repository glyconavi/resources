RES
1n:n1
2b:b-dglc-HEX-1:5
3s:methyl
4n:n2
LIN
1:1n(1+1)2d
2:1n(7+1)3n
3:1n(11+9)4n
NON
NON1
SmallMolecule:SMILES O={7}C(O)C[C@@H]1C({11}C(=O)O)=CO{1}[C@@H](O)/C1=C/{10}CO
Description:10-hydroxyoleoside aglycon
NON2
Parent:1
Linkage:n(11+9)n
SmallMolecule:SMILES O={9}C(O)/C=C\C1=CC=CC=C1
Description:cis-cinnamic acid