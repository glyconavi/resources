RES
1n:n1
2b:b-dglc-HEX-1:5
3b:a-lara-PEN-1:5
4b:b-dglc-HEX-1:5
5b:a-lman-HEX-1:5|6:d
6b:b-dxyl-PEN-1:5
7b:b-dgro-TET-1:4
8s:hydroxymethyl
LIN
1:1n(3+1)2d
2:1n(28+1)3d
3:2o(3+1)4d
4:3o(2+1)5d
5:5o(4+1)6d
6:6o(3+1)7d
7:7h(3+1)8n
NON
NON1
SmallMolecule:SMILES C[C@]1({24}CO){3}[C@@H](O){2}[C@@H](O)C[C@]2(C)[C@@]3([H])CC=C4[C@]5([H])CC(C)(C)CC[C@@]({28}C(O)=O)5{16}[C@H](O)C[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:olean-12-en-2β,3β,16α,24-tetrol-28-oic acid