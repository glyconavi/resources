RES
1b:b-dgal-HEX-1:5
2n:n1
3b:b-dglc-HEX-1:5
4b:b-dglc-HEX-1:5
LIN
1:1o(1+3)2n
2:1o(4+1)3d
3:3o(2+1)4d
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C[C@H]1CC[C@@]2([C@H]([C@H]3[C@@H](O2)C[C@@H]4[C@@]3(CC[C@H]5[C@H]4CC[C@@H]6[C@@]5(CC{3}[C@@H](C6)O)C)C)C)OC1