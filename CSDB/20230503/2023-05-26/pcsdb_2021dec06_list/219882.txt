RES
1b:b-dara-HEX-2:5|1:aldi|2:keto
2b:a-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4b:b-dglc-HEX-1:5
5b:b-dglc-HEX-1:5
LIN
1:1o(2+1)2d
2:2o(2+1)3d
3:2o(3+1)4d
4:4o(3+1)5d