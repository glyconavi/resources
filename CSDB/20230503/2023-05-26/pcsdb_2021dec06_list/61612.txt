RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+3)2n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C[C@@H](C1C{24}[C@H](O)[C@](C)(C)O1)[C@H]5{16}[C@@H](O)C[C@@]6(C)C3CCC2[C@@](C)(C){3}[C@@H](O)CC[C@@]24C[C@@]34CC[C@]56C
Description:22R,25-epoxy-9,19-cyclolanosta-3β,16β,24S-triol