RES
1b:b-dglc-HEX-1:5
2n:n1
3n:n2
LIN
1:1o(6+7)2n
2:1o(1+3)3n
NON
NON1
Parent:1
Linkage:o(6+7)n
SmallMolecule:SMILES [6CH]1=[5C](O)[4C](O)=[3C](O)[2CH]=[1C]1[7C](=O)O
Description:3,4,5-trihydroxybenzoic acid
NON2
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O[3C]1=C(C2=CC=[54C](O)C=C2)OC3=C([5C](O)=[6CH][7C](O)=[8CH]3)C1=O
Description:3,5,7,4'-tetrahydroxyflavone