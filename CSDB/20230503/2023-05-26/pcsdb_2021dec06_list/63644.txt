RES
1n:n1
2b:b-dglc-HEX-1:5
3a:a1
4s:sulfate
LIN
1:1n(28+1)2d
2:1n(3+1)3n
3:3n(2+1)4n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:5
LEAD-OUT RES:5+4
RES
5b:b-dglc-HEX-1:5
ALTSUBGRAPH2
LEAD-IN RES:6
LEAD-OUT RES:6+4
RES
6b:b-dglc-HEX-1:5|6:d
NON
NON1
SmallMolecule:SMILES O{3}[C@H]1CC[C@]2(C)[C@@]3([H])CC=C4[C@]5([H])[C@@H](C)[C@H](C)CC[C@@]({28}C(O)=O)5CC[C@]({27}C(O)=O)4[C@@](C)3CCC2C1(C)C
Description:quinovic acid