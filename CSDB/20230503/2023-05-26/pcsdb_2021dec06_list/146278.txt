RES
1b:b-dglc-HEX-1:5
2n:n1
3b:b-dgro-TET-1:4
4s:hydroxymethyl
LIN
1:1o(1+2)2n
2:1o(6+1)3d
3:3h(3+1)4n
NON
NON1
Parent:1
Linkage:o(1+2)n
SmallMolecule:SMILES C{2}[C@H](O)CCC1=CC=C(O)C=C1
Description:(S)-4-hydroxyphenyl-2-butanol