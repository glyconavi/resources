RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+13)2n
NON
NON1
Parent:1
Linkage:o(1+13)n
SmallMolecule:SMILES O={13}C(O)CCCC1=CNC2=C1C=CC=C2
Description:hormodin