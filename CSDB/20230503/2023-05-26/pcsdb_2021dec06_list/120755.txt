RES
1b:b-dglc-HEX-1:5
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4n:n1
LIN
1:1o(3+1)2d
2:1o(2+1)3d
3:1o(1+3)4n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES CC1(C)CC[C@]2([28C](O)=O)CC[C@@]3(C)[C@]4(C)CC[C@@]5([H])[C@](C)([23CH2]O)[3C@@H](O)CC[C@]5(C)[C@@]4([H])CC=C3[C@]2([H])C1
Description:hederagenin