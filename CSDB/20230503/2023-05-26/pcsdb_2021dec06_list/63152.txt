RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4b:b-dglc-HEX-1:5
5b:b-dglc-HEX-1:5
6b:b-dglc-HEX-1:5
LIN
1:1n(3+1)2d
2:1n(28+1)3d
3:3o(3+1)4d
4:3o(6+1)5d
5:5o(2+1)6d
NON
NON1
SmallMolecule:SMILES C[C@]12CC{3}[C@H](O)[C@@](C)({23}C(O)=O)[C@@H]1CC[C@]3(C)[C@@H]2CC=C4[C@@]3(C)CC[C@]5({28}C(O)=O)[C@H]4CC(C)(C)CC5
Description:gypsogenic acid