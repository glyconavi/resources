RES
1n:n1
2b:a-lman-HEX-1:5|6:d
3n:n2
4b:b-dglc-HEX-1:5
5s:acetyl
LIN
1:1n(6+1)2d
2:1n(11+7)3n
3:1n(1+1)4d
4:4o(6+1)5n
NON
NON1
SmallMolecule:SMILES O[1C@H]1[9C@H]2([5C@@H]([4CH]=[3CH]O1)[6C@@H]([7C@H]3[8C@@]2(O3)[10CH2]O)O)
Description:catalpol aglycon
NON2
Parent:1
Linkage:n(11+7)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3CH][2CH]=[1C]1[7C](=O)O
Description:4-hydroxybenzoic acid