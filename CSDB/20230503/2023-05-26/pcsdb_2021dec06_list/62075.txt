RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+2)2n
NON
NON1
Parent:1
Linkage:o(1+2)n
SmallMolecule:SMILES C[C@]1(C){2}[C@H](O)C2(C){6}[C@H](O)CC1{7}C2O
Description:(1R,2S,4R,5S)-2,6,7-trihydroxyfenchane