RES
1n:n1
2b:b-dglc-HEX-1:5|6:d
3n:n2
4b:b-dglc-HEX-1:5
5b:b-dglc-HEX-1:5
6b:b-dglc-HEX-1:5
7b:b-dgal-HEX-1:5|6:d
8b:a-lman-HEX-1:5|6:d
9b:b-dxyl-PEN-1:5
10b:b-dglc-HEX-1:5
11b:a-lara-PEN-1:4
LIN
1:1n(6+1)2d
2:1n(1+21)3n
3:3n(28+1)4d
4:3n(3+1)5d
5:5o(2+1)6d
6:5o(6+1)7d
7:4o(2+1)8d
8:7o(2+1)9d
9:8o(3+1)10d
10:8o(4+1)11d
NON
NON1
SmallMolecule:SMILES [8CH2]=[7CH][6C@@]([8CH3])(O)[5CH2][4CH2]/[3CH]=[2C]([7CH3])/[1C](O)=O
Description:(S)-menthiafolic acid
NON2
Parent:1
Linkage:n(1+21)n
SmallMolecule:SMILES C[C@]12CC{3}[C@H](O)C(C)(C)[C@@H]1CC[C@]3(C)[C@@H]2CC=C4[C@@]3(C)C{16}[C@@H](O)[C@]5({28}C(O)=O)[C@H]4CC(C)(C){21}[C@@H](O)C5
Description:acacic acid