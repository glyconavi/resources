RES
1b:b-dglc-HEX-1:5|6:a
2b:b-dglc-HEX-1:5
3n:n1
4b:b-dgal-HEX-1:5
5b:a-lman-HEX-1:5|6:d
6b:a-lman-HEX-1:5|6:d
7b:a-lman-HEX-1:5|6:d
LIN
1:1o(2+1)2d
2:1o(1+3)3n
3:1o(4+1)4d
4:4o(2+1)5d
5:5o(2+1)6d
6:6o(2+1)7d
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C[C@@]12C(CC[C@]3(C)[C@]2([H])CC[C@@]4(OC5)[C@@]3(C)C{16}[C@@H](O)[C@]65C4CC(C)(C)CC6)C(C)(C){3}[C@@H](O)CC1
Description:protoprimulagenin A