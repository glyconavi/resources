RES
1b:b-dglc-HEX-1:5|6:a
2n:n1
3b:b-dgal-HEX-1:5
4b:a-lman-HEX-1:5|6:d
LIN
1:1o(1+3)2n
2:1o(2+1)3d
3:3o(2+1)4d
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES CC1(C){3}[C@@H](O)CC[C@]2(C)[C@@]3([H])CC=C4[C@]5([H])C[C@](C)({29}C(O)=O)C{22}[C@@H](O)[C@@](C)5CC[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:olean-12-en-3β,22β-diol-29-oic acid