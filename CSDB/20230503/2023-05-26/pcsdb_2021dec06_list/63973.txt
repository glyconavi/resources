RES
1b:b-dgal-HEX-1:5
2n:n1
LIN
1:1o(1+28)2n
NON
NON1
Parent:1
Linkage:o(1+28)n
SmallMolecule:SMILES CC1(C)CC[C@]2({28}C(=O)O)CC[C@]3(C)C(=CC[C@@H]4[C@@]5(C)C{2}[C@@H](O){3}[C@@H](O)[C@@](C)({23}CO)[C@@H]5CC[C@]43C)[C@@H]2{19}[C@@H]1O
Description:2α,3α,19α,23-tetrahydroxyolean-12-ene-28-oic acid