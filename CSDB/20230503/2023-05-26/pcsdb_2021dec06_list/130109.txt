RES
1b:b-dglc-HEX-1:5
2n:n1
3b:b-dglc-HEX-1:5
4b:a-lman-HEX-1:5|6:d
LIN
1:1o(1+3)2n
2:1o(6+1)3d
3:3o(6+1)4d
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C[C@@H]1CC[C@]2(O[26CH2]1)[C@@H](C)[C@H]3[C@H](C[C@@H]4[C@]3(C)CC[C@H]5[C@H]4CC=C6[C@]5(C)CC[3CH](O)C6)O2
Description:diosgenin