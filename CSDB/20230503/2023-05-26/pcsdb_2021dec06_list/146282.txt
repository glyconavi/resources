RES
1n:n1
2s:methyl
3s:methyl
4s:methyl
5b:b-dglc-HEX-1:5
6n:n2
LIN
1:1n(1+1)2n
2:1n(2+1)3n
3:1n(3+1)4n
4:1n(5+1)5d
5:5o(6+7)6n
NON
NON1
SmallMolecule:SMILES {1}OC1={2}C(O){3}C(O)=C{5}C(O)=C1
Description:5-hydroxypyrogallol
NON2
Parent:5
Linkage:o(6+7)n
SmallMolecule:SMILES COC1=CC({7}C(O)=O)=CC={4}C1O
Description:vanillic acid