RES
1n:n1
2b:x-lman-HEX-1:5|6:d
3s:methyl
4s:methyl
5s:methyl
6s:methyl
LIN
1:1n(6+1)2d
2:1n(3+1)3n
3:1n(7+1)4n
4:1n(54+1)5n
5:1n(5+1)6n
NON
NON1
SmallMolecule:SMILES O=C(C1=C(C={7}C(O){6}C(O)={5}C1O)O2){3}C(O)=C2C3=CC={54}C(O)C=C3
Description:6-hydroxykaempferol