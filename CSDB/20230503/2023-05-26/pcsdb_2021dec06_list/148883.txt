RES
1b:b-dglc-HEX-1:5|6:a
2n:n1
LIN
1:1o(1+3)2n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C[C@]1({24}CO){3}[C@@H](O)CC[C@]2(C)[C@@]3([H])CC=C4[C@]5([H])CC(C)(C){21}[C@H](O)C[C@@](C)5CC[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:yunganogenin C