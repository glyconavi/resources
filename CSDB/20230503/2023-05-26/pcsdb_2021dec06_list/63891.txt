RES
1n:n1
2b:b-dglc-HEX-1:5
3s:acetyl
4s:acetyl
5n:n2
LIN
1:1n(5+1)2d
2:1n(2+1)3n
3:1n(9+1)4n
4:1n(11+7)5n
NON
NON1
SmallMolecule:SMILES C=C1C2C(CC{5}[C@@H]1O){9}[C@@H](O){10}[C@H](O)C1=C(C)C(=O)C[C@@]1({15}C(C)(C)O){2}[C@H]2O
Description:2,9-deacetyltaxacustone
NON2
Parent:1
Linkage:n(11+7)n
SmallMolecule:SMILES [6CH]1=[5CH][4CH]=[3CH][2CH]=[1C]1[7C](=O)O
Description:benzoic acid