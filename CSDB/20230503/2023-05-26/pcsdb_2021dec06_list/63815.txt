RES
1n:n1
2b:b-dglc-HEX-1:5
3b:a-lara-PEN-1:5
4b:a-lgal-HEX-1:5|6:d
5b:b-dglc-HEX-1:5
6b:b-dxyl-PEN-1:5
LIN
1:1n(23+1)2d
2:1n(3+1)3d
3:3o(2+1)4d
4:3o(3+1)5d
5:5o(2+1)6d
NON
NON1
SmallMolecule:SMILES CC(C)=C{23}[C@@H](O)C{20}[C@](C)(O)[C@H]1C(=O)C[C@]2({30}CO)[C@@H]1CC[C@@H]1[C@@]3(C)CC{3}[C@H](O)C(C)(C)C3CC[C@]12C
Description:3β,20S,23S,30-tetrahydroxydammar-24-en-16-one