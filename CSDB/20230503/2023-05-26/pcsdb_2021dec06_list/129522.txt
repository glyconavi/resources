RES
1n:n1
2s:methyl
3s:methyl
4b:b-dglc-HEX-1:5
5b:a-lara-PEN-1:5
6b:a-lman-HEX-1:5|6:d
LIN
1:1n(3+1)2n
2:1n(4+1)3n
3:1n(8+1)4d
4:4o(2+1)5d
5:4o(3+1)6d
NON
NON1
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3C](O)[2CH]=[1C]1[7CH2][8CH2]O
Description:3,4,8-trihydroxy-ethylbenzene