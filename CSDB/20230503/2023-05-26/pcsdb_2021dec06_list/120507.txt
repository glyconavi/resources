RES
1b:b-drib-HEX-1:5|2:d|6:d
2s:methyl
3n:n1
LIN
1:1o(3+1)2n
2:1o(1+3)3n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O{14}[C@@]12[C@H](CC{5}[C@@]3(O)[C@]4(C=O)CC{3}[C@H](O)C3)[C@@H]4CC[C@@]15CO[C@@]6(COC(C6)=O)[C@H]5CC2
Description:20R-18,20-epoxystrophanthidin