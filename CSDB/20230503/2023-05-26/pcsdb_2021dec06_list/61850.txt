RES
1b:b-dglc-HEX-1:5
2s:acetyl
3s:acetyl
4s:acetyl
5n:n1
6n:n2
7s:acetyl
8s:acetyl
9n:n3
10s:acetyl
11s:acetyl
LIN
1:1o(2+1)2n
2:1o(4+1)3n
3:1o(3+1)4n
4:1o(6+9)5n
5:1o(1+1)6n
6:6n(11+1)7n
7:5n(4+1)8n
8:6n(7+8)9n
9:9n(3+1)10n
10:9n(4+1)11n
NON
NON1
Parent:1
Linkage:o(6+9)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3CH][2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:p-coumaric acid
NON2
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES COC(=O)/C1=C/O{1}[C@@H](O)/C(=C/{10}CO)C1C{7}C(=O)O
Description:10-hydroxy-oleuropein aglycon core
NON3
Parent:6
Linkage:n(7+8)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3C](O)[2CH]=[1C]1[7CH2][8CH2]O
Description:3,4,8-trihydroxy-ethylbenzene