RES
1n:n1
2s:acetyl
3s:acetyl
4b:b-dglc-HEX-1:5
5s:acetyl
6s:acetyl
7s:acetyl
8s:acetyl
LIN
1:1n(3+1)2n
2:1n(19+1)3n
3:1n(28+1)4d
4:4o(2+1)5n
5:4o(3+1)6n
6:4o(4+1)7n
7:4o(6+1)8n
NON
NON1
SmallMolecule:SMILES C[C@@H]1CC[C@@]2(CC[C@@]3(C([C@@H]2{19}[C@@]1(O)C)=CC[C@@H]4[C@]5(CC{3}[C@@H]([C@@]({23}C(O)=O)([C@@H]5CC[C@]43C)C)O)C)C){28}C(O)=O
Description:rotundioic acid