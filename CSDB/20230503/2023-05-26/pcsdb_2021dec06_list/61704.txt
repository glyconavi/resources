RES
1r:r1
REP
REP1:2n(7+9)2n=-1--1
RES
2n:n1
3b:b-dglc-HEX-1:5
LIN
1:2n(1+1)3d
NON
NON1
SmallMolecule:SMILES C{9}[C@@H](O)C1{1}[C@H](O)O/C=C({11}C(=O)O)\C1C{7}C(=O)O
Description:gonocaryoside E core aglycon