RES
1b:b-dglc-HEX-1:5
2n:n1
3b:b-dglc-HEX-1:5
4b:a-lman-HEX-1:5|6:d
LIN
1:1o(1+28)2n
2:1o(6+1)3d
3:3o(4+1)4d
NON
NON1
Parent:1
Linkage:o(1+28)n
SmallMolecule:SMILES C[C@@]1({23}CO){3}[C@H](O)CC[C@]2(C)[C@@]3([H]){11}[C@H](O)C[C@]4([H])[C@@]5([H])[C@H](C(C)=C)CC[C@@]({28}C(O)=O)5CC[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:lup-20(29)-en-3α,11α,23-triol-28-oic acid