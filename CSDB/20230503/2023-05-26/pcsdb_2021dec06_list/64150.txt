RES
1b:b-dglc-HEX-1:5
2n:n1
3b:b-dglc-HEX-1:5
4b:a-lman-HEX-1:5|6:d
LIN
1:1o(1+28)2n
2:1o(6+1)3d
3:3o(4+1)4d
NON
NON1
Parent:1
Linkage:o(1+28)n
SmallMolecule:SMILES C=C(C)[C@@H]1CC[C@]2({28}C(=O)O)CC[C@]3(C)[C@H](C{11}[C@@H](O)[C@@H]4[C@@]5(C)CC{3}[C@@H](O)[C@@](C)(C=O)[C@@H]5CC[C@]43C)[C@@H]12
Description:3α,11α-dihydroxylup-23-al-20(29)-en-28-oic acid