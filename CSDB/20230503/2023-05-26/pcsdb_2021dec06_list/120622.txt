RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+7)2n
NON
NON1
Parent:1
Linkage:o(1+7)n
SmallMolecule:SMILES CC([C@@H](CC1)CC[C@H]1{7}CO)=C
Description:1,4-trans-dihydroperillyl alcohol