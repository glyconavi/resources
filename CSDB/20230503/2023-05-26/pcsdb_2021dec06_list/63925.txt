RES
1n:n1
2s:methyl
3b:b-dglc-HEX-1:5
4b:b-dxyl-PEN-1:5
LIN
1:1n(6+1)2n
2:1n(7+1)3d
3:3o(6+1)4d
NON
NON1
SmallMolecule:SMILES O=c1c(-c2cc{54}c(O)cc2)coc2c{7}c(O){6}c(O)cc12
Description:6-hydroxydaidzein