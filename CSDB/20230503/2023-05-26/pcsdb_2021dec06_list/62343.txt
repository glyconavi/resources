RES
1n:n1
2s:methyl
3b:b-dglc-HEX-1:5|1:d
4b:x-xman-HEX-1:5|6:d
LIN
1:1n(7+1)2n
2:1n(6+1)3d
3:3o(2+1)4d
NON
NON1
SmallMolecule:SMILES O=C(C1=[5C](O)[6CH]=[7C](O)[8CH]=C1O2)[3CH]=C2C3=CC=[54C](O)C=C3
Description:5,7,4'-trihydroxyflavone