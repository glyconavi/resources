RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5|6:a
4b:a-lman-HEX-1:5|6:d
LIN
1:1n(28+1)2d
2:1n(3+1)3d
3:3o(3+1)4d
NON
NON1
SmallMolecule:SMILES CC5(C)CC[C@]4({28}C(=O)O)CC[C@]3(C)/C(=C\C[C@@H]2C1(C)C{2}[C@H](O){3}[C@H](O)[C@](C)(C)[C@H]1CC[C@]23C)[C@@H]4C5
Description:olean-12-en-2β,3β-diol-28-oic acid