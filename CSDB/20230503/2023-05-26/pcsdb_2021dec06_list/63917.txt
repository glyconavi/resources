RES
1b:b-dglc-HEX-1:5
2n:n1
3b:b-dgro-TET-1:4
4s:hydroxymethyl
LIN
1:1o(1+1)2n
2:1o(6+1)3d
3:3h(3+1)4n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES C/C(C)=C\{1}CO
Description:isopentenol