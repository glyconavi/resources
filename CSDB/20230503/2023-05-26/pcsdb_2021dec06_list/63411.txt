RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+7)2n
NON
NON1
Parent:1
Linkage:o(1+7)n
SmallMolecule:SMILES C/C1=C/CC(C(C)(O){7}CO)CC1=O
Description:5-(1,2-dihydroxypropan-2-yl)-2-methylcyclohex-2-en-1-one