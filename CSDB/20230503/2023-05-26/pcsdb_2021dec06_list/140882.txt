RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4s:acetyl
5b:b-dglc-HEX-1:5
6b:b-dglc-HEX-1:5
LIN
1:1n(3+1)2d
2:1n(26+1)3d
3:3o(6+1)4n
4:3o(2+1)5d
5:5o(4+1)6d
NON
NON1
SmallMolecule:SMILES C/C(C{22}[C@@H](O)[C@@H](C)[C@H]1CC[C@@]2([H])[C@]3([H])CC=C4C{3}[C@@H](O)C{1}[C@H](O)[C@]4(C)[C@@]3([H])CC[C@]12C)=C(C)\{26}C(O)=O
Description:1α,3β,22R-trihydroxyergosta-5,24E-dien-26-oic acid