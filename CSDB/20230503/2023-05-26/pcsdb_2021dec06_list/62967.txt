RES
1n:n1
2s:acetyl
3b:b-dglc-HEX-1:5
4s:acetyl
5s:acetyl
6s:acetyl
7s:acetyl
LIN
1:1n(4+1)2n
2:1n(2+1)3d
3:3o(2+1)4n
4:3o(3+1)5n
5:3o(6+1)6n
6:3o(4+1)7n
NON
NON1
SmallMolecule:SMILES CC(=O)C=C=C1C(C)(C)C{4}[C@H](O)C{2}[C@@]1(C)O
Description:icariside B1 aglycone