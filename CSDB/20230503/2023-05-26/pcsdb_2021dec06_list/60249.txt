RES
1n:n1
2b:b-dgal-HEX-1:5|6:d
3b:b-dglc-HEX-1:5
4b:b-dxyl-PEN-1:5
5b:a-lman-HEX-1:5|6:d
6s:acetyl
LIN
1:1n(24+1)2d
2:1n(1+1)3d
3:3o(3+1)4d
4:3o(2+1)5d
5:5o(4+1)6n
NON
NON1
SmallMolecule:SMILES [H][C@]1(O[C@@]2(OC[C@@H](C){24}[C@H](O){23}[C@@H]2O)[C@H]3C)C[C@@]4([H])[C@]5([H])CC=C6C{3}[C@@H](O)C{1}[C@@H](O)[C@]6(C)[C@@]5([H])C{12}[C@@H](O)[C@]4(C)[C@]13[H]
Description:23S,24S,25R-spirost-5-en-1β,3β,12β,23,24-pentol