RES
1b:b-dglc-HEX-1:5|6:d
2s:acetyl
3s:acetyl
4s:acetyl
5n:n1
LIN
1:1o(2+1)2n
2:1o(3+1)3n
3:1o(4+1)4n
4:1o(1+3)5n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O{3}[C@H]1CC[C@]2(C)[C@@]3([H])CC=C4[C@]5([H])[C@@H](C)[C@H](C)CC[C@@]({28}C(O)=O)5CC[C@]({27}C(O)=O)4[C@@](C)3CCC2C1(C)C
Description:quinovic acid