RES
1n:n1
2b:a-lara-PEN-1:5
3b:b-dglc-HEX-1:5
4b:a-lara-PEN-1:5
5b:b-dglc-HEX-1:5
6b:a-lman-HEX-1:5|6:d
7b:b-dglc-HEX-1:5
8b:b-dxyl-PEN-1:5
LIN
1:1n(23+1)2d
2:1n(3+1)3d
3:1n(28+1)4d
4:3o(2+1)5d
5:4o(2+1)6d
6:5o(2+1)7d
7:6o(4+1)8d
NON
NON1
SmallMolecule:SMILES C[C@@]1({23}C(O)=O){3}[C@@H](O){2}[C@@H](O)C[C@]2(C)[C@@]3([H])CC=C4[C@]5([H])CC(C)(C)CC[C@@]({28}C(O)=O)5{16}[C@H](O)C[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:16α-hydroxymedicagenic acid