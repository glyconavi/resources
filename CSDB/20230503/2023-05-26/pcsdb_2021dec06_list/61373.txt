RES
1n:n1
2b:b-dxyl-PEN-1:5
3b:b-dglc-HEX-1:5|6:a
4b:a-lara-PEN-1:5
5b:a-lman-HEX-1:5|6:d
LIN
1:1n(22+1)2d
2:1n(3+1)3d
3:3o(2+1)4d
4:4o(2+1)5d
NON
NON1
SmallMolecule:SMILES C[C@]1({24}CO){3}[C@@H](O)CC[C@]2(C)[C@@]3([H])CC=C4[C@]5([H])C[C@](C)({29}CO){21}[C@@H](O){22}[C@@H](O)[C@@](C)5CC[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:kudzusapogenol A