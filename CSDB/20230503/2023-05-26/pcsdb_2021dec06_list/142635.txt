RES
1b:b-dglc-HEX-1:5
2n:n1
3b:a-lara-PEN-1:5
4b:a-lara-PEN-1:5
LIN
1:1o(1+3)2n
2:1o(4+1)3d
3:3o(2+1)4d
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C[C@]1(C2=CC[C@]3([C@@]1(C)CC[C@@]4([C@]3(C)CC{3}[C@@H](C4(C)C)O)[H])[H])C{16}[C@@H]([C@@]5([C@]2(CC(C)(CC5)C)[H]){28}CO)O
Description:longispinogenin