RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4n:n2
LIN
1:1n(1+1)2d
2:1n(51+1)3d
3:3o(4+9)4n
NON
NON1
SmallMolecule:SMILES C=C[C@@H]1[C@@H]2CCOC(C3=CO{1}[C@@H](O)[C@H](C=C)[C@@H]3CCOC(C2=CO{51}[C@H]1O)=O)=O
Description:lisianthioside aglycon
NON2
Parent:3
Linkage:o(4+9)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3CH][2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:p-coumaric acid