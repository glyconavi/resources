RES
1b:b-dara-HEX-2:5|1:aldi|2:keto
2b:a-dglc-HEX-1:5
3n:n1
LIN
1:1o(2+1)2d
2:2o(6+9)3n
NON
NON1
Parent:2
Linkage:o(6+9)n
SmallMolecule:SMILES [6CH]1=[5C](O[11CH3])[4C](O)=[3C](O[10CH3])[2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:sinapic/sinapinic acid