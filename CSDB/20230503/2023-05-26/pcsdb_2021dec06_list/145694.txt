RES
1b:b-dglc-HEX-1:5
2b:b-dglc-HEX-1:5
3n:n1
4b:a-lara-PEN-1:5
5b:b-dxyl-PEN-1:5
LIN
1:1o(2+1)2d
2:1o(1+3)3n
3:1o(6+1)4d
4:4o(2+1)5d
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES CC1(C)[3C@@H](O)CC[C@]2(C)[C@@]3([H])CC=C4[C@]5([H])CC(C)(C)CC[C@@]([28C](O)=O)5[16C@H](O)C[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:echinocystic acid