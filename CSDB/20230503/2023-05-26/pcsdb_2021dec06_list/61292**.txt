RES
1b:b-dglc-HEX-1:5|6:a
2n:n1
3n:n2
LIN
1:1o(3+3)2n
2:1o(1+3)3n
NON
NON1
Parent:1
Linkage:o(3+3)n
SmallMolecule:SMILES O=C(O)CO{3}C(O)C(O)C(=O)O
Description:3-(carboxymethoxy)-2,3-dihydroxypropanoic acid
NON2
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES [1CH2]1[2CH2][3C@H](O)[4C]([23CH3])([24CH3])[5C@@H]2[6CH2][7CH2][8C@]([26CH3])3[14C@@]([27CH3])4[15CH2][16CH2][17C@]([28C](=O)O)5[22CH2][21CH2][20C]([29CH3])([30CH3])[19CH2][18C@H]5/[13C]4=[12CH]/[11CH2][9C@@H]3[10C@]([25CH3])21
Description:oleanolic/oleanic acid