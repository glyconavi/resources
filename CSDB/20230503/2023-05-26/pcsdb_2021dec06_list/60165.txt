RES
1r:r1
REP
REP1:5o(2+1)2n=-1--1
RES
2n:n1
3b:b-dgal-HEX-1:5|6:d
4b:b-dglc-HEX-1:5
5b:b-dglc-HEX-1:5|6:d
6n:n2
7b:b-dgal-HEX-1:5|6:d
8b:b-dglc-HEX-1:5
9b:b-dglc-HEX-1:5|6:d
LIN
1:2n(11+1)3d
2:3o(2+1)4d
3:4o(2+1)5d
4:4o(3+1)6n
5:6n(11+1)7d
6:7o(2+1)8d
7:8o(2+1)9d
NON
NON1
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11C@H](O)[10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:jalapinolic acid
NON2
Parent:4
Linkage:o(3+1)n
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11C@H](O)[10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:jalapinolic acid