RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+6)2n
NON
NON1
Parent:1
Linkage:o(1+6)n
SmallMolecule:SMILES O=C1C(C)=C(CO)C(C)(C)C={6}C1O
Description:6-hydroxy-3-(hydroxymethyl)-2,4,4-trimethyl-2,5-cyclohexadien-1-one