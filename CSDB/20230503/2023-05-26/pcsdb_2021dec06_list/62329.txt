RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+4)2n
NON
NON1
Parent:1
Linkage:o(1+4)n
SmallMolecule:SMILES O{8}C1=C(C2=O)C(C[C@@](C3=CC={4}C(O)C(OC)=C3)([H])O2)=CC=C1
Description:3R-thunberginol I