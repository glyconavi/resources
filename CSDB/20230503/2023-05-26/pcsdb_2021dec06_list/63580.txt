RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4b:a-lman-HEX-1:5|6:d
5b:b-dglc-HEX-1:5
LIN
1:1n(28+1)2d
2:1n(3+1)3d
3:3o(4+1)4d
4:3o(6+1)5d
NON
NON1
SmallMolecule:SMILES C[C@@]1({23}C(O)=O){3}[C@H](O)CC[C@]2(C)[C@@]3([H])CC[C@]4([H])[C@@]5([H])[C@H](C(C)=C)CC[C@@]({28}C(O)=O)5CC[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:lup-20(29)-en-3α-ol-23,28-dioic acid