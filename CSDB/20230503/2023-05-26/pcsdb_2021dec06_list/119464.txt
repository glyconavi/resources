RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+3)2n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O{3}[C@H]1CC(C)(C)C(CCC(C)=O)=C(C)C1
Description:3-hydroxy-dihydro-β-ionone