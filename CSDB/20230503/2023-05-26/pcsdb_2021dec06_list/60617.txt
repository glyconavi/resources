RES
1n:n1
2b:b-dglc-HEX-1:5
3s:methyl
4b:b-dglc-HEX-1:5
5s:sulfate
LIN
1:1n(28+1)2d
2:1n(27+1)3n
3:1n(3+1)4d
4:4o(2+1)5n
NON
NON1
SmallMolecule:SMILES O{3}[C@H]1CC[C@]2(C)[C@@]3([H])CC=C4[C@]5([H])[C@@H](C)[C@H](C)CC[C@@]({28}C(O)=O)5CC[C@]({27}C(O)=O)4[C@@](C)3CCC2C1(C)C
Description:quinovic acid