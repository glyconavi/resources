RES
1n:n1
2s:methyl
3b:b-dglc-HEX-1:5|6:a
4s:methyl
LIN
1:1n(6+1)2n
2:1n(7+1)3d
3:3o(6+1)4n
NON
NON1
SmallMolecule:SMILES O=c1cc(-c2cccc{52}c2O)oc2c{7}c(O){6}c(O){5}c(O)c12
Description:5,6,7,2'-tetrahydroxyflavone