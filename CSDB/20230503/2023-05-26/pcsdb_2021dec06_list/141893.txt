RES
1b:a-dgal-HEX-1:5
2b:a-dgal-HEX-1:5
3n:n1
LIN
1:1o(6+1)2d
2:1o(1+3)3n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES [6C@H](O)1[5C@H](O)[4C@@H](O)[3C@H](O)[2C@H](O)[1C@H](O)1
Description:myo-inositol