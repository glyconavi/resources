RES
1n:n1
2b:b-dglc-HEX-1:5
3b:a-lman-HEX-1:5|6:d
4b:b-dglc-HEX-1:5
LIN
1:1n(7+1)2d
2:1n(3+1)3d
3:3o(2+1)4d
NON
NON1
SmallMolecule:SMILES C/C(C)=C\CC1=C2C(C({3}C(O)=C(C3=CC=C(OC)C=C3)O2)=O)={5}C(O)C={7}C1O
Description:anhydroicaritin