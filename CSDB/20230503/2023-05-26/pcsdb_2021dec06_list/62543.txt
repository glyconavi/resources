RES
1n:n1
2b:b-dglc-HEX-1:5
3b:a-lara-PEN-1:5
4b:b-dglc-HEX-1:5
5b:a-lman-HEX-1:5|6:d
6b:b-dglc-HEX-1:5
7b:b-dxyl-PEN-1:5
8b:b-dgro-TET-1:4
9s:hydroxymethyl
LIN
1:1n(3+1)2d
2:1n(28+1)3d
3:2o(6+1)4d
4:3o(2+1)5d
5:4o(6+1)6d
6:5o(4+1)7d
7:7o(3+1)8d
8:8h(3+1)9n
NON
NON1
SmallMolecule:SMILES [H][C@]12[C@@]({23}CO)({24}CO){3}[C@@H](O){2}[C@@H](O)C[C@]1(C)[C@@]3([H])CC=C4[C@]5([H])CC(C)(C)CC[C@@]({28}C(O)=O)5{21}[C@H](O)C[C@](C)4[C@@](C)3CC2
Description:platycodigenin