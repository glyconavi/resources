RES
1b:b-dgal-HEX-1:5|6:d
2b:b-dglc-HEX-1:5
3s:methyl
4n:n1
LIN
1:1o(4+1)2d
2:1o(3+1)3n
3:1o(1+3)4n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O=C1OCC([C@H]2{16}[C@@H](O)C{14}[C@]3(O)[C@]4([H])CC[C@]5([H])C{3}[C@@H](O)CC[C@]5(C)[C@@]4([H])CC[C@]23C)=C1
Description:gitoxigenin