RES
1b:b-dgal-HEX-1:5
2n:n1
3b:b-dglc-HEX-1:5
4b:a-lman-HEX-1:5|6:d
LIN
1:1o(1+3)2n
2:1o(6+1)3d
3:3o(6+1)4d
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O[3C]1=C(C2=C[53C](O)=[54C](O)C=C2)OC3=C([5C](O)=[6CH][7C](O)=[8CH]3)C1=O
Description:3,5,7,3',4'-pentahydroxyflavone