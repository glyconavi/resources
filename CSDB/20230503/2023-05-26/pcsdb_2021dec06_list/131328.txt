RES
1b:x-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+7)2n
NON
NON1
Parent:1
Linkage:o(1+7)n
SmallMolecule:SMILES O=C1C(C2=CC={54}C(O)C=C2)=COC3=C1{5}C(O)={6}C{7}C(O)={8}C3
Description:genistein