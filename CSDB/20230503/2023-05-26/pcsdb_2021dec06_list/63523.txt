RES
1b:b-dglc-HEX-1:5
2a:a1
3n:n1
LIN
1:1o(-1+-1)2n
2:1o(1+3)3n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:4
RES
4b:a-lman-HEX-1:5|6:d
ALTSUBGRAPH2
LEAD-IN RES:5
RES
5n:n2
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O[3C]1=C(C2=C[53C](O)=[54C](O)C=C2)OC3=C([5C](O)=[6CH][7C](O)=[8CH]3)C1=O
Description:3,5,7,3',4'-pentahydroxyflavone
NON2
SmallMolecule:SMILES [6CH]1=[5C](O)[4C](O)=[3C](O)[2CH]=[1C]1[7C](=O)O
Description:3,4,5-trihydroxybenzoic acid