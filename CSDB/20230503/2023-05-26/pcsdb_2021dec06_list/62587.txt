RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+1)2n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES N#C[C@@H]1OC1({1}CO){5}CO
Description:(S)-3,3-bis(hydroxymethyl)oxirane-2-carbonitrile