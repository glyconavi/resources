RES
1b:b-dman-HEX-1:5
2b:b-dgal-HEX-1:4
3b:b-dgal-HEX-1:4
4n:n1
LIN
1:1o(4+1)2d
2:1o(2+1)3d
3:1o(1+3)4n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES CC1(C){3}[C@@H](O)CC[C@]2(C)[C@@]3([H])CC=C4[C@]5([H])CC(C)(C)C{22}[C@H](O)[C@@]({28}CO)5CC[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:olean-12-en-3β,22α,28-triol