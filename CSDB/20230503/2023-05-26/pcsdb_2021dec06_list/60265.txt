RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+153)2n
NON
NON1
Parent:1
Linkage:o(1+153)n
SmallMolecule:SMILES O{6}C1=C(OC)C=C2CC({152}CO)C({153}CO)C(C3=CC(OC)={54}C(O)C(OC)=C3)C2=C1OC
Description:lyoniresinol