RES
1b:b-dglc-HEX-1:5
2b:b-dglc-HEX-1:5
3b:a-lman-HEX-1:5|6:d
4n:n1
LIN
1:1o(3+1)2d
2:1o(2+1)3d
3:1o(1+3)4n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES [H][C@]12C[C@@]3([H])[C@]4([H])CC=C5C{3}[C@@H](O)CC[C@]5(C)[C@@]4([H])CC[C@]3(C){17}[C@@]1(O)[C@H](C)[C@]6(OC[C@@H](C){24}[C@H](O)C6)O2
Description:25R-spirost-5-en-3β,17,24R-triol