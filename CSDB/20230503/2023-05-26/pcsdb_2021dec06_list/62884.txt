RES
1b:a-lara-PEN-1:5
2b:a-lman-HEX-1:5|6:d
3n:n1
LIN
1:1o(2+1)2d
2:1o(1+3)3n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O{3}[C@H]1CC[C@]2(C)[C@@]3([H])CC=C4[C@]5([H])[C@@H](C)[C@H](C)CC[C@@]({28}C(O)=O)5CC[C@](C)4[C@@](C)3CCC2C1(C)C
Description:ursolic acid