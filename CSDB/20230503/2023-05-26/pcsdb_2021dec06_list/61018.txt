RES
1b:b-dglc-HEX-1:5
2s:n-acetyl
3b:b-dglc-HEX-1:5
4s:n-acetyl
5b:b-xman-HEX-1:5
6b:a-xman-HEX-1:5
7b:a-xman-HEX-1:5
8b:a-xman-HEX-1:5
9a:a1
10a:a2
LIN
1:1d(2+1)2n
2:1o(4+1)3d
3:3d(2+1)4n
4:3o(4+1)5d
5:5o(3+1)6d
6:5o(6+1)7d
7:7o(3+1)8d
8:6o(2+1)9n
9:7o(6+1)10n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:11
RES
11b:a-xman-HEX-1:5
ALTSUBGRAPH2
LEAD-IN RES:12
RES
12b:a-xman-HEX-1:5
13b:a-xman-HEX-1:5
LIN
1:12o(2+1)13d
ALT2
ALTSUBGRAPH1
LEAD-IN RES:14
RES
14b:a-xman-HEX-1:5
15b:a-xman-HEX-1:5
LIN
1:14o(2+1)15d
ALTSUBGRAPH2
LEAD-IN RES:16
RES
16b:a-xman-HEX-1:5