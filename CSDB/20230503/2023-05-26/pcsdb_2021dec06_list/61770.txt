RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+1)2n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES CC1C{8}[C@](C)(O)C2{1}[C@H](O)O/C=C\{5}[C@@]12O
Description:reptoside aglycon