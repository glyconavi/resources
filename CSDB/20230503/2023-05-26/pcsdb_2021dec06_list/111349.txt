RES
1b:b-dglc-HEX-1:5
2b:b-dxyl-PEN-1:5
3n:n1
LIN
1:1o(6+1)2d
2:1o(1+1)3n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES C/C(CC/C=C(C)\C)=C\{1}CO
Description:geraniol