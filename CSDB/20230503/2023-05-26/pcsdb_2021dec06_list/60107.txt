RES
1b:b-dgal-HEX-1:5|6:d
2n:n1
3b:b-dglc-HEX-1:5
4b:b-dglc-HEX-1:5
LIN
1:1o(1+3)2n
2:1o(3+1)3d
3:3o(2+1)4d
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C[C@@]1({23}CO){3}[C@@H](O)CC[C@]2(C)[C@@]3([H])C=C[C@]45[C@]6([H])CC(C)(C)CC[C@@](CO5)6{16}[C@@H](O)C[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:saikogenin F