RES
1b:b-dgal-HEX-1:5
2b:b-dglc-HEX-1:5
3b:a-lman-HEX-1:5|6:d
4n:n1
LIN
1:1o(4+1)2d
2:1o(2+1)3d
3:1o(1+3)4n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C[C@@H]1[C@]2(OC[C@H](C)CC2)O[C@@]3([H])C[C@@]4([H])[C@]5([H])CC[C@@]6([H])C{3}[C@@H](O){2}[C@H](O)C[C@]6(C)[C@@]5([H])CC[C@]4(C)[C@]31[H]
Description:gitogenin