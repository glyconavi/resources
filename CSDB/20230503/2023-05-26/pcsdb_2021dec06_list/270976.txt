RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+2)2n
NON
NON1
Parent:1
Linkage:o(1+2)n
SmallMolecule:SMILES O{2}[C@H]1C[C@]2([H])C(C)(C)O[C@@]1(C)CC2
Description:1S,2S,4R-2-hydroxy-1,8-cineole