RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+1)2n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES C=C{6}C(O)(C)CC/C=C(C)/{1}CO
Description:(2E)-2,6-dimethyl-6-hydroxyocta-2,7-dienol