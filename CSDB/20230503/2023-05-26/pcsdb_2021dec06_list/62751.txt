RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+52)2n
NON
NON1
Parent:1
Linkage:o(1+52)n
SmallMolecule:SMILES COC1=C{5}C(O)=C2C(OC(C3=CC=CC={52}C3O)=CC2=O)=C1
Description:echioidinin