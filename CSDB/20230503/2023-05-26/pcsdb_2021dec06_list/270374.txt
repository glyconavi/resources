RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4s:methyl
5n:n2
LIN
1:1n(26+1)2d
2:1n(2+1)3d
3:1n(22+1)4n
4:1n(3+7)5n
NON
NON1
SmallMolecule:SMILES [H][C@]1(O{22}C(CC[C@@H](C){26}CO)(O)[C@H]2C)C[C@@]3([H])[C@]4([H])C{6}[C@@H](O){5}[C@@]5(O)C{3}[C@@H](O){2}[C@H](O)C[C@]5(C)[C@@]4([H])CC[C@]3(C)[C@]12[H]
Description:25R-furostane-2α,3β,5α,6β,22,26-hexol
NON2
Parent:1
Linkage:n(3+7)n
SmallMolecule:SMILES [6CH]1=[5CH][4CH]=[3CH][2CH]=[1C]1[7C](=O)O
Description:benzoic acid