RES
1b:b-dglc-HEX-1:5
2b:a-lman-HEX-1:5|6:d
3n:n1
LIN
1:1o(2+1)2d
2:1o(1+3)3n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C=C1CC[C@]2([H])[C@@](CC{3}[C@H](O)C2(C)C)(C)[C@H]1CC{13}[C@@](C)(C=C)O
Description:3β-hydroxy-13-epimanool