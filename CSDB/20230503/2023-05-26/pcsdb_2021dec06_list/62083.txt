RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+2)2n
NON
NON1
Parent:1
Linkage:o(1+2)n
SmallMolecule:SMILES CC1(C)OC2(C){6}[C@@H](O)CC1C{2}[C@H]2O
Description:2,6-diexohydroxy-1,8-cineole