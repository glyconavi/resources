RES
1b:b-dglc-HEX-1:5
2s:n-acetyl
3n:n1
4b:x-xgal-HEX-1:5|6:d
5b:x-xxyl-PEN-1:5
LIN
1:1d(2+1)2n
2:1o(1+3)3n
3:1o(6+1)4d
4:4o(2+1)5d
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES CC1(C){3}[C@@H](O)CC[C@]2(C)[C@@]3([H])CC=C4[C@]5([H])CC(C)(C)[C@](O6)([H])C[C@@](C6=O)5{16}[C@H](O)C[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:acacic acid 21,28-lactone