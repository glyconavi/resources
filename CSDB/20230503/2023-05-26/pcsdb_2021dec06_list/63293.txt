RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dgal-HEX-1:5|6:d
4b:a-lman-HEX-1:5|6:d
5b:b-dxyl-PEN-1:5
LIN
1:1n(3+1)2d
2:1n(28+1)3d
3:3o(2+1)4d
4:4o(4+1)5d
NON
NON1
SmallMolecule:SMILES C[C@@]12CC[C@@H]3[C@@](C{2}[C@H](O){3}[C@H](O)[C@]3({23}C(O)=O)C)(C)[C@H]1CC=C4[C@@]2({27}CO)CC[C@]5({28}C(O)=O)[C@H]4CC(C)(C)CC5
Description:presenegenin