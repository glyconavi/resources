RES
1n:n1
2b:b-dgal-HEX-1:5
3b:b-dgro-TET-1:4
4b:b-dall-HEX-1:5|6:d
5s:hydroxymethyl
LIN
1:1n(12+1)2d
2:1n(3+1)3d
3:3o(5+1)4d
4:3h(3+1)5n
NON
NON1
SmallMolecule:SMILES [H][C@]1(O[C@@]2(OC[C@H](C)CC2)[C@H]3C)C[C@@]4([H])[C@]5([H])CC=C6C{3}[C@@H](O)CC[C@]6(C)[C@@]5([H])C{12}[C@@H](O)[C@]4(C)[C@]13[H]
Description:heloniogenin