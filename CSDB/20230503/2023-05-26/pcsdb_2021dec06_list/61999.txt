RES
1b:b-dglc-HEX-1:5
2a:a1
LIN
1:1o(1+1)2n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:3
RES
3n:n1
ALTSUBGRAPH2
LEAD-IN RES:4
RES
4n:n2
ALTSUBGRAPH3
LEAD-IN RES:5
RES
5n:n3
NON
NON1
SmallMolecule:SMILES COC(=O)/C1=C/O{1}[C@@H](O)C2[C@@H]({10}CO)C{6}[C@H](O)C12
Description:10-hydroxy-(5αh)-6-epidihydrocornin aglycon
NON2
SmallMolecule:SMILES COC(=O)/C1=C/O{1}[C@@H](O)C2[C@@H](C)CC(=O){5}[C@]12O
Description:hastatoside aglycon
NON3
SmallMolecule:SMILES COC(=O)/C1=C/O{1}[C@@H](O)C2[C@@H]({10}CO)CC(=O){5}[C@]12O
Description:10-hydroxyhastatoside aglycon