RES
1n:n1
2b:b-dxyl-PEN-1:5
3s:acetyl
4n:n2
5n:n3
6n:n4
LIN
1:1n(7+1)2d
2:1n(4+1)3n
3:1n(2+7)4n
4:1n(13+9)5n
5:5n(7+1)6n
NON
NON1
SmallMolecule:SMILES CC1=C2{10}[C@@H](O)C(=O)[C@@]3(C)C({1}[C@H](O){2}[C@](O)(C{13}[C@@H]1O)C2(C)C){4}[C@]1(O)CO[C@@H]1C{7}[C@@H]3O
Description:2,4,10-trideacetyltaxol A
NON2
Parent:1
Linkage:n(2+7)n
SmallMolecule:SMILES [6CH]1=[5CH][4CH]=[3CH][2CH]=[1C]1[7C](=O)O
Description:benzoic acid
NON3
Parent:1
Linkage:n(13+9)n
SmallMolecule:SMILES N{7}[C@@H](c1ccccc1)[C@@H](O){9}C(=O)O
Description:(2R,3S)-3-phenylisoserine
NON4
Parent:5
Linkage:n(7+1)n
SmallMolecule:SMILES [6CH3][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:hexanoic acid