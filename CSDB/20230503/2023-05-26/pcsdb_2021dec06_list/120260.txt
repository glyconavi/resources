RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+6)2n
NON
NON1
Parent:1
Linkage:o(1+6)n
SmallMolecule:SMILES O{12}[C@H]1[C@@]([C@]({20}C(CC/C=C(C)\C)(C)O)([H])CC2)([H])[C@]2(C)[C@@](C3)(C)[C@@](C1)([H])[C@](CC4)(C)[C@@](C(C)(C){3}[C@H]4O)([H]){6}[C@H]3O
Description:protopanaxatriol