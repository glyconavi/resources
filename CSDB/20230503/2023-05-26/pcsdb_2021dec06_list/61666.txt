RES
1n:n1
2n:n2
3b:b-dglc-HEX-1:5
LIN
1:1n(8+7)2n
2:2n(1+1)3d
NON
NON1
SmallMolecule:SMILES O{8}CCC1=CC={4}C(O)C=C1
Description:tyrosol
NON2
Parent:1
Linkage:n(8+7)n
HistoricalEntity:oleuropein aglycon core =SMILES O{1}[C@@H]1/C([C@@H](C{7}C(O)=O)C(C(OC)=O)=CO1)=C/C