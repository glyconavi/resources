RES
1b:b-dgal-HEX-1:5|6:d
2s:methyl
3n:n1
4b:b-dglc-HEX-1:5
5b:b-dglc-HEX-1:5
LIN
1:1o(3+1)2n
2:1o(1+21)3n
3:1o(2+1)4d
4:4o(6+1)5d
NON
NON1
Parent:1
Linkage:o(1+21)n
SmallMolecule:SMILES C{20}[C@H](O)[C@H]1CC[C@@]2([H])[C@]3([H])CC=C4C{3}[C@@H](O)CC[C@]4(C)[C@@]3([H])CC[C@]12C
Description:pregn-5-en-3β,20α-diol