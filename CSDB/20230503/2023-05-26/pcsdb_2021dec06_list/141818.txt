RES
1b:a-xgal-HEX-1:5|6:a
2n:n1
3b:a-xgal-HEX-1:5|6:a
4b:a-xgal-HEX-1:5|6:a
5b:a-xgal-HEX-x:x|6:d
6b:b-dglc-HEX-1:5
LIN
1:1o(1+-1)2n
2:1o(4+1)3d
3:3o(4+1)4d
4:4o(-1+1)5d
5:4o(-1+1)6d
NON
NON1
Parent:1
Linkage:o(1+-1)n
HistoricalEntity:superclass: any residue