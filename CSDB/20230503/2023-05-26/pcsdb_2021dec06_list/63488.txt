RES
1n:n1
2n:n2
3n:n3
4b:b-dglc-HEX-1:5
LIN
1:1n(1+1)2n
2:2n(5+9)3n
3:3n(4+1)4d
NON
NON1
SmallMolecule:SMILES [1CH3]O
Description:methyl
NON2
Parent:1
Linkage:n(1+1)n
SmallMolecule:SMILES [5C](=O)(O)[4CH2][3C]([6CH3])(O)[2CH2][1C](=O)(O)
Description:(R)-3-hydroxy-3-methylglutaric acid
NON3
Parent:2
Linkage:n(5+9)n
SmallMolecule:SMILES COC1=CC(/C=C/{9}CO)=C{3}C(O)={4}C1O
Description:coniferyl alcohol