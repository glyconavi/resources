RES
1n:n1
2b:b-dglc-HEX-1:5
3s:methyl
LIN
1:1n(7+1)2d
2:1n(6+1)3n
NON
NON1
SmallMolecule:SMILES O=c1c(-c2cc{54}c(O)cc2)coc2c{7}c(O){6}c(O)cc12
Description:6-hydroxydaidzein