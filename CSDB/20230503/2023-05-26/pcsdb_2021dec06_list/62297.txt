RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+11)2n
NON
NON1
Parent:1
Linkage:o(1+11)n
SmallMolecule:SMILES CCCCCCCCCC1CC/C=C\{11}C1O
Description:6-nonylcyclohex-2-en-1-ol