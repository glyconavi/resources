RES
1b:b-lgal-HEX-1:5|6:d
2s:sulfate
3n:n1
LIN
1:1o(4+1)2n
2:1o(1+1)3n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES [H][C@]1(O[C@@]2(OC[C@H](C)CC2)[C@H]3C)C[C@@]4([H])[C@]5([H])CC[C@@]6([H])C{3}[C@@H](O)C{1}[C@@H](O)[C@]6(C)[C@@]5([H])CC[C@]4(C)[C@]13[H]
Description:25R-5α-spirostan-1β,3β-diol