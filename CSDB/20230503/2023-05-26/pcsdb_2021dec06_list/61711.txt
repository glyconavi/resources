RES
1n:n1
2b:b-dglc-HEX-1:5
3n:n2
LIN
1:1n(1+1)2d
2:1n(9+1)3n
NON
NON1
SmallMolecule:SMILES C{9}[C@H](O)C1{1}[C@H](O)O/C=C({11}C(=O)O)\C1C{7}C(=O)O
Description:gonocaryoside E core aglycon
NON2
Parent:1
Linkage:n(9+1)n
SmallMolecule:SMILES C{3}C(O){2}C(C)(O){1}C(=O)O
Description:2,3-dihydroxy-2-methylbutanoic acid