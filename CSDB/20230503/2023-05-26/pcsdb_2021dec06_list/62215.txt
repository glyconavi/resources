RES
1b:b-dgal-HEX-1:5
2n:n1
3b:a-lman-HEX-1:5|6:d
4b:b-dglc-HEX-1:5
LIN
1:1o(1+1)2n
2:1o(2+1)3d
3:3o(3+1)4d
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES [H][C@]1(O[C@@]2(OC[C@H](C)CC2)[C@H]3C)C[C@@]4([H])[C@]5([H])CC=C6C{3}[C@@H](O)C{1}[C@@H](O)[C@]6(C)[C@@]5([H])CC[C@]4(C)[C@]13[H]
Description:ruscogenin