RES
1n:n1
2b:b-lara-PEN-1:5
3b:b-dglc-HEX-1:5
4b:a-lman-HEX-1:5|6:d
LIN
1:1n(3+1)2d
2:1n(22+1)3d
3:3o(4+1)4d
NON
NON1
SmallMolecule:SMILES C{22}C(C)(O)[C@@H]1CC[C@@]2(C)C1{16}C(O)C[C@]5(C)C2CCC4[C@@]3(C)CC{3}[C@H](O)[C@](C)(C)C3CC[C@]45C
Description:3,16-dihydroxyhopane