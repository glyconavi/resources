RES
1b:b-dlyx-HEX-1:5|2:d|6:d
2b:b-dglc-HEX-1:5
3s:methyl
4n:n1
LIN
1:1o(4+1)2d
2:1o(3+1)3n
3:1o(1+3)4n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O{3}[C@H]1CC[C@@]2(C)[C@@](CC[C@]3([H])[C@]2([H])CC[C@@]4(C){14}[C@]3(O)C[C@H](OC(C)=O)[C@@H]4C(CO5)=CC5=O)([H])C1
Description:oleandrigenin