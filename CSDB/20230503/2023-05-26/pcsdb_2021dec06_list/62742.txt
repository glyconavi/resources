RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5|6:a
4b:b-dgal-HEX-1:5
5b:a-lman-HEX-1:5|6:d
LIN
1:1n(22+1)2d
2:1n(3+1)3d
3:3o(2+1)4d
4:4o(2+1)5d
NON
NON1
SmallMolecule:SMILES C[C@]12CC[3C@@H]([C@]([C@@H]1CC[C@@]3([C@@H]2CC=C4[C@]3(CC[C@@]5([C@H]4CC(C[22C@H]5O)(C)C)C)C)C)(C)[24CH2]O)O
Description:soyasapogenol B