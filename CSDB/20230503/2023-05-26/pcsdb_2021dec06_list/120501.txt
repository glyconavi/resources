RES
1b:b-dgal-HEX-1:5|6:d
2b:b-dglc-HEX-1:5
3s:methyl
4n:n1
LIN
1:1o(4+1)2d
2:1o(3+1)3n
3:1o(1+3)4n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C[C@]12CC[C@H]3[C@@H](CC[C@@]4([H])[C@]3(C=O)CC{3}[C@H](O)C4){14}[C@@]1(O)CC[C@@H]2C5=CC(OC5)=O
Description:cannogenin