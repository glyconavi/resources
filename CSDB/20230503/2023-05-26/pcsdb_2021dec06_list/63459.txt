RES
1n:n1
2b:b-dglc-HEX-1:5
3s:methyl
4s:methyl
LIN
1:1n(4+1)2d
2:1n(5+1)3n
3:1n(3+1)4n
NON
NON1
SmallMolecule:SMILES O{7}CC1=C{3}C(O)={4}C(O){5}C(O)=C1
Description:5-(hydroxymethyl)benzene-1,2,3-triol