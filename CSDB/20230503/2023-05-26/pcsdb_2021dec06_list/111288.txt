RES
1b:b-dglc-HEX-1:5|6:a
2n:n1
3b:b-dglc-HEX-1:5|6:a
4b:a-dglc-HEX-1:5
LIN
1:1o(1+3)2n
2:1o(2+1)3d
3:3o(4+1)4d
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES [1CH2]1[2CH2][3C@H](O)[4C]([23CH3])([24CH3])[5C@@H]2[6CH2][7CH2][8C@]([26CH3])3[14C@@]([27CH3])4[15CH2][16CH2][17C@]([28CH3])5[22CH2][21CH2][20C@]([29CH3])([30C](=O)O)[19CH2][18C@H]5/[13C]4=[12CH]/[11C](=O)[9C@@H]3[10C@]([25CH3])21
Description:enoxolone