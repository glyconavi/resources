RES
1b:b-dglc-HEX-1:5
2b:a-xgal-HEX-x:x|6:d
3s:n-acetyl
4b:b-dglc-HEX-1:5
5s:n-acetyl
6b:b-xman-HEX-1:5
7b:b-xxyl-PEN-1:5
8a:a1
9a:a2
LIN
1:1o(3+1)2d
2:1d(2+1)3n
3:1o(4+1)4d
4:4d(2+1)5n
5:4o(4+1)6d
6:6o(2+1)7d
7:6o(6+1)8n
8:6o(3+1)9n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:10
RES
10b:a-xman-HEX-1:5
11b:b-dglc-HEX-1:5
12s:n-acetyl
LIN
1:10o(2+1)11d
2:11d(2+1)12n
ALTSUBGRAPH2
LEAD-IN RES:13
RES
13b:a-xman-HEX-1:5
ALT2
ALTSUBGRAPH1
LEAD-IN RES:14
RES
14b:a-xman-HEX-1:5
ALTSUBGRAPH2
LEAD-IN RES:15
RES
15b:a-xman-HEX-1:5
16b:b-dglc-HEX-1:5
17s:n-acetyl
LIN
1:15o(2+1)16d
2:16d(2+1)17n