RES
1b:b-dxyl-PEN-1:5
2a:a1
3a:a2
4n:n1
LIN
1:1o(4+1)2n
2:1o(3+1)3n
3:1o(1+3)4n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:5
RES
5b:b-dglc-HEX-1:5
ALTSUBGRAPH2
LEAD-IN RES:6
RES
6b:a-lara-PEN-1:5
ALT2
ALTSUBGRAPH1
LEAD-IN RES:7
RES
7b:a-lara-PEN-1:5
ALTSUBGRAPH2
LEAD-IN RES:8
RES
8b:b-dglc-HEX-1:5
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C[C@]12C([C@@](C)({3}[C@H](CC2)O){23}CO)CC[C@@]3([C@@]1(CC[C@@]45OC[C@]6([C@H]5CC(C)(CC6)C){16}[C@H](O)C[C@]43C)[H])C
Description:anagalligenin B