RES
1n:n1
2b:b-dglc-HEX-1:5
3s:methyl
LIN
1:1n(8+1)2d
2:1n(53+1)3n
NON
NON1
SmallMolecule:SMILES O{53}c4cc3c{56}c(O){57}c(O)c(c1{8}c(O)ccc2ccc(=O)oc12)c3oc4=O
Description:gulsamanin aglycone