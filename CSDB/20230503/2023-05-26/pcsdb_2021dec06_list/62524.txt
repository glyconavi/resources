RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4n:n2
5b:b-dglc-HEX-1:5|6:d
6b:b-lara-PEN-1:5
7b:a-lman-HEX-1:5|6:d
8b:b-dxyl-PEN-1:5
9b:b-dglc-HEX-1:5
10b:a-lara-PEN-1:4
LIN
1:1n(28+1)2d
2:1n(3+1)3d
3:1n(21+1)4n
4:4n(6+1)5d
5:3o(6+1)6d
6:2o(2+1)7d
7:6o(2+1)8d
8:7o(3+1)9d
9:7o(4+1)10d
NON
NON1
SmallMolecule:SMILES C[C@]12CC{3}[C@H](O)C(C)(C)[C@@H]1CC[C@]3(C)[C@@H]2CC=C4[C@@]3(C)C{16}[C@@H](O)[C@]5({28}C(O)=O)[C@H]4CC(C)(C){21}[C@@H](O)C5
Description:acacic acid
NON2
Parent:1
Linkage:n(21+1)n
SmallMolecule:SMILES C=C{6}[C@@](C)(O)CC/C=C({9}CO)/{1}C(O)=O
Description:6S-hydroxy-2-hydroxymethyl-6-methyl-2E,7-octadienoic acid