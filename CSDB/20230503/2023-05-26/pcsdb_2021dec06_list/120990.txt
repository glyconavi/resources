RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+7)2n
NON
NON1
Parent:1
Linkage:o(1+7)n
SmallMolecule:SMILES C{7}C(O)(C)C(O)CCC({2}C(O)CO)=C
Description:3,7-dimethyl-1,2,6,7-tetrahydroxy-oct-3(10)-ene