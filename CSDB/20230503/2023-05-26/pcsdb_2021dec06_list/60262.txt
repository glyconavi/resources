RES
1b:x-dglc-HEX-1:5
2b:b-dglc-HEX-1:5
3b:a-lman-HEX-1:5|6:d
LIN
1:1o(6+1)2d
2:2o(4+1)3d