RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4n:n2
5n:n3
6b:b-dxyl-PEN-1:5
7n:n4
LIN
1:1n(5+1)2d
2:1n(3+1)3d
3:3o(6+9)4n
4:2o(6+1)5n
5:3o(2+1)6d
6:6o(2+9)7n
NON
NON1
SmallMolecule:SMILES O[3C]1=C(C2=CC=[54C](O)[53C](O)=C2)[O+]=C3[8CH]=[7C](O)[6CH]=[5C](O)C3=C1
Description:3,3',4',5,7-pentahydroxyflavylium
NON2
Parent:3
Linkage:o(6+9)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3C](O)[2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:caffeic acid
NON3
Parent:2
Linkage:o(6+1)n
SmallMolecule:SMILES [3C](=O)(O)[2CH2][1C](=O)(O)
Description:malonic acid
NON4
Parent:6
Linkage:o(2+9)n
SmallMolecule:SMILES [6CH]1=[5C](O[11CH3])[4C](O)=[3C](O[10CH3])[2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:sinapic/sinapinic acid