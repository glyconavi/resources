RES
1b:b-dglc-HEX-1:5
2s:sulfate
3s:sulfate
4n:n1
5n:n2
LIN
1:1o(3+1)2n
2:1o(4+1)3n
3:1o(2+1)4n
4:1o(1+7)5n
NON
NON1
Parent:1
Linkage:o(2+1)n
SmallMolecule:SMILES [4CH3][3CH]([5CH3])[2CH2][1C](=O)O
Description:isovaleric acid
NON2
Parent:1
Linkage:o(1+7)n
SmallMolecule:SMILES C=C4[C@@H]3CC[C@H]2[C@]1(C)C{7}[C@H](O)C[C@@]({19}C(=O)O)({20}C(=O)O)[C@H]1CC[C@]2(C3){15}[C@H]4O
Description:carboxyatractyligenin