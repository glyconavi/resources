RES
1b:b-dglc-HEX-1:5|6:a
2b:b-dglc-HEX-1:5|6:a
3n:n1
LIN
1:1o(2+1)2d
2:1o(1+3)3n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES CC1(C){3}[C@@H](O)CCC2(C)C3=CC=C4[C@]5([H])C[C@](C)({29}C(O)=O){21}[C@H](O)C[C@@](C)5CC[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:isomacedonic acid