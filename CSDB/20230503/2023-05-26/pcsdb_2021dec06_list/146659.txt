RES
1b:a-lman-HEX-1:5|6:d
2s:acetyl
3s:acetyl
4s:acetyl
5n:n1
LIN
1:1o(2+1)2n
2:1o(3+1)3n
3:1o(4+1)4n
4:1o(1+4)5n
NON
NON1
Parent:1
Linkage:o(1+4)n
SmallMolecule:SMILES N#CC1=CC={4}C(O)C=C1
Description:4-hydroxy-benzyl-nitrile