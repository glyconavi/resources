RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-drib-HEX-1:5|2:d|6:d
4b:a-lman-HEX-1:5|6:d
5b:b-dglc-HEX-1:5
6s:methyl
LIN
1:1n(21+1)2d
2:1n(3+1)3d
3:2o(6+1)4d
4:3o(4+1)5d
5:3o(3+1)6n
NON
NON1
SmallMolecule:SMILES O{14}[C@@]12[C@]3([H])CC=C4C{3}[C@@H](O)CC[C@]4(C)[C@@]3([H])CC[C@]1(C)[C@@H]({20}[C@@](C)O)CC2
Description:calogenin