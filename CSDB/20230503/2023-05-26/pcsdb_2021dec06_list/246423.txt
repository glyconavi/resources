RES
1b:b-dglc-HEX-1:5
2n:n1
3n:n2
4b:b-dglc-HEX-1:5
LIN
1:1o(1+8)2n
2:1o(3+9)3n
3:3n(4+1)4d
NON
NON1
Parent:1
Linkage:o(1+8)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3C](O)[2CH]=[1C]1[7CH2][8CH2]O
Description:3,4,8-trihydroxy-ethylbenzene
NON2
Parent:1
Linkage:o(3+9)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3C](O)[2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:caffeic acid