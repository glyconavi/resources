RES
1n:n1
2s:methyl
3b:b-dgal-HEX-1:5|6:d
4b:a-lman-HEX-1:5|6:d
5b:a-lman-HEX-1:5|6:d
6b:a-lman-HEX-1:5|6:d
7b:a-lman-HEX-1:5|6:d
LIN
1:1n(1+1)2n
2:1n(11+1)3d
3:3o(2+1)4d
4:4o(4+1)5d
5:5o(3+1)6d
6:5o(4+1)7d
NON
NON1
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11C@H](O)[10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:jalapinolic acid