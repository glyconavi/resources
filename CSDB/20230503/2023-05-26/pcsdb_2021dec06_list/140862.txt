RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+5)2n
NON
NON1
Parent:1
Linkage:o(1+5)n
SmallMolecule:SMILES C/C1=C(CC{53}C(C)O)/C(C)(C)C{5}C(O)C1
Description:5-hydroxy-2-(3-hydroxybutyl)-1,3,3-trimethylcyclohexene