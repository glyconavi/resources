RES
1b:x-lman-HEX-1:5|6:d
2b:a-dgal-HEX-1:5|6:a
3b:b-dglc-HEX-1:5|6:a
4b:a-lman-HEX-1:5|6:d
5b:a-dgal-HEX-1:5|6:a
6b:b-dglc-HEX-1:5|6:a
7b:a-lman-HEX-1:5|6:d
8b:a-dgal-HEX-1:5|6:a
9b:b-dglc-HEX-1:5|6:a
LIN
1:1o(2+1)2d
2:2o(3+1)3d
3:2o(4+1)4d
4:4o(2+1)5d
5:5o(3+1)6d
6:5o(4+1)7d
7:7o(2+1)8d
8:8o(3+1)9d