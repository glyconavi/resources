RES
1b:b-dglc-HEX-1:5
2n:n1
3a:a1
LIN
1:1o(1+1)2n
2:2n(-1+-1)3n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:4
RES
4n:n2
ALTSUBGRAPH2
LEAD-IN RES:5
RES
5n:n3
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES O[1C@H]1[9C@H]2([5C@@H]([4CH]=[3CH]O1)[6C@@H]([7C@H]3[8C@@]2(O3)[10CH2]O)O)
Description:catalpol aglycon
NON2
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3C](O)[2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:caffeic acid
NON3
SmallMolecule:SMILES [6CH]1=[5CH][4CH]=[3CH][2CH]=[1C]1[7C](=O)O
Description:benzoic acid