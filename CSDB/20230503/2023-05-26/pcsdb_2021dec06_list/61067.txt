RES
1b:b-dglc-HEX-1:5
2s:n-acetyl
3b:b-dglc-HEX-1:5
4s:n-acetyl
5b:b-xman-HEX-1:5
6b:b-xxyl-PEN-1:5
7a:a1
8a:a2
LIN
1:1d(2+1)2n
2:1o(4+1)3d
3:3d(2+1)4n
4:3o(4+1)5d
5:5o(2+1)6d
6:5o(6+1)7n
7:5o(3+1)8n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:9
RES
9b:a-xman-HEX-1:5
ALTSUBGRAPH2
LEAD-IN RES:10
RES
10b:a-xman-HEX-1:5
11b:b-dglc-HEX-1:5
12s:n-acetyl
LIN
1:10o(2+1)11d
2:11d(2+1)12n
ALT2
ALTSUBGRAPH1
LEAD-IN RES:13
RES
13b:a-xman-HEX-1:5
14b:b-dglc-HEX-1:5
15s:n-acetyl
LIN
1:13o(2+1)14d
2:14d(2+1)15n
ALTSUBGRAPH2
LEAD-IN RES:16
RES
16b:a-xman-HEX-1:5