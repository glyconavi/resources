RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+21)2n
NON
NON1
Parent:1
Linkage:o(1+21)n
SmallMolecule:SMILES O[12C@H]1[C@@]([C@]([20C@@](CC/C=C(C)\C)(C)O)([H])CC2)([H])[C@]2(C)[C@@](C3)(C)[C@@](C1)([H])[C@](CC4)(C)[C@@](C(C)(C)[3C@H]4O)([H])[6C@H]3O
Description:20S-protopanaxatriol