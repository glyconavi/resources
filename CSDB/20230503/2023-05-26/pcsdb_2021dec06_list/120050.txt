RES
1b:b-dglc-HEX-1:5|6:a
2n:n1
3b:b-dgal-HEX-1:5
4b:a-lman-HEX-1:5|6:d
LIN
1:1o(1+3)2n
2:1o(2+1)3d
3:3o(2+1)4d
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C[C@]12CC{3}[C@H](O)C(C)(C)[C@@H]1CC[C@]3(C)[C@@H]2CC=C4[C@@]3(C)CC[C@]5(C)[C@H]4CC(C)(C)C{22}[C@H]5O
Description:sophoradiol