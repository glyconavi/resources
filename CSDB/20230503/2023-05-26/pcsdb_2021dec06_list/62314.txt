RES
1b:a-lara-PEN-1:5
2b:a-lara-PEN-1:5
3n:n1
LIN
1:1o(2+1)2d
2:1o(1+3)3n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C/C(C)=C\CC1=C2C(C({3}C(O)=C(C3=CC=C(OC)C=C3)O2)=O)={5}C(O)C={7}C1O
Description:anhydroicaritin