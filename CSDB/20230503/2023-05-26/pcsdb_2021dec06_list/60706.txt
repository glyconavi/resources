RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
LIN
1:1n(23+1)2d
2:1n(28+1)3d
NON
NON1
SmallMolecule:SMILES C[C@@]1({23}CO){3}[C@@H](O)CC[C@]2(C)[C@@]3([H])CC=C4[C@]5([H])CC(C)(C)CC[C@@]({28}CO)5{16}[C@@H](O)C[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:23-hydroxylongispinogenin