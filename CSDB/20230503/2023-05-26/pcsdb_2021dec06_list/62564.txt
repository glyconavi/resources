RES
1b:b-dglc-HEX-1:5
2n:n1
3n:n2
LIN
1:1o(6+1)2n
2:1o(1+1)3n
NON
NON1
Parent:1
Linkage:o(6+1)n
SmallMolecule:SMILES C=C(C{4}CO){1}C(=O)O
Description:4-hydroxy-2-methylenebutanoic acid
NON2
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES C=C(C{4}CO){1}C(=O)O
Description:4-hydroxy-2-methylenebutanoic acid