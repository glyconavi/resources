RES
1b:b-dglc-HEX-1:5|6:a
2n:n1
3b:b-dgal-HEX-1:5
4b:a-lman-HEX-1:5|6:d
LIN
1:1o(1+3)2n
2:1o(2+1)3d
3:3o(2+1)4d
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C[C@@]({24}CO)([C@](CC[C@@]([C@](CC[C@@]12C)3C)4C)5[H]){3}[C@@H](O)CC[C@]5(C)[C@@]4([H])CC=C3[C@]2([H])CC(C)(C)CC1=O
Description:soyasapogenol E