RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+54)2n
NON
NON1
Parent:1
Linkage:o(1+54)n
SmallMolecule:SMILES O{8}C1=C(C2=O)C(C[C@](C3=CC={54}C(O)C=C3)([H])O2)=CC=C1
Description:3S-hydrangenol