RES
1b:b-dglc-HEX-1:5
2b:b-dglc-HEX-1:5
3b:a-lara-PEN-1:4
4n:n1
LIN
1:1o(6+1)2d
2:1o(2+1)3d
3:1o(1+24)4n
NON
NON1
Parent:1
Linkage:o(1+24)n
SmallMolecule:SMILES C[C@]1({24}CO)CCC[C@]2(C)[C@@]3([H])CC[C@]4([H])[C@@]5(C(O6)=O)CC[C@H](C6(C)C)[C@@]([H])5CC[C@](C)4[C@@](C)3CCC12
Description:hopan-24-ol-28,22-olide