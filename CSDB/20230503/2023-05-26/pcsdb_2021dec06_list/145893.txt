RES
1b:b-dgal-HEX-1:5
2n:n1
3b:b-dglc-HEX-1:5
4b:b-dglc-HEX-1:5
5b:b-dxyl-PEN-1:5
6n:n2
LIN
1:1o(1+3)2n
2:1o(4+1)3d
3:3o(2+1)4d
4:3o(3+1)5d
5:5o(3+7)6n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES [H][C@]1(O[C@@]2(OCC(C)CC2)[C@H]3C)C[C@@]4([H])[C@]5([H])C{6}[C@@H](O)[C@@]6([H])C{3}[C@@H](O){2}[C@H](O)C[C@]6(C)[C@@]5([H])CC[C@]4(C)[C@]13[H]
Description:5α-spirostan-2α,3β,6β-triol
NON2
Parent:5
Linkage:o(3+7)n
SmallMolecule:SMILES [6CH]1=[5CH][4CH]=[3CH][2CH]=[1C]1[7C](=O)O
Description:benzoic acid