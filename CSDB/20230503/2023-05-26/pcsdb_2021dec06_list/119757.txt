RES
1n:n1
2s:methyl
3b:b-dglc-HEX-1:5
4n:n2
5b:a-lman-HEX-1:5|6:d
LIN
1:1n(2+1)2n
2:1n(4+1)3d
3:3o(6+7)4n
4:4n(4+1)5d
NON
NON1
SmallMolecule:SMILES O{4}C1=CC={1}C(O){2}C(O)=C1
Description:2,4-dihydroxyphenol
NON2
Parent:3
Linkage:o(6+7)n
SmallMolecule:SMILES O{4}C(C(OC)=CC({7}C(O)=O)=C1)=C1OC
Description:syringic acid