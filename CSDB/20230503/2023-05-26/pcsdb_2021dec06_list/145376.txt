RES
1b:b-dglc-HEX-1:5
2n:n1
3b:b-dglc-HEX-1:5
4b:b-dglc-HEX-1:5
LIN
1:1o(1+11)2n
2:1o(6+1)3d
3:3o(6+1)4d
NON
NON1
Parent:1
Linkage:o(1+11)n
SmallMolecule:SMILES COC1=C2C(=CC(=C1)CC{10}CO)C=C(O2)C3=CC4=C(C=C3)OCO4
Description:egonol