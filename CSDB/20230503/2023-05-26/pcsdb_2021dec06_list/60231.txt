RES
1b:b-dglc-HEX-1:5|6:a
2n:n1
LIN
1:1o(1+3)2n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES CC1(C){3}[C@@H](O)CC[C@]2(C)[C@@]3([H])CC=C4[C@]5([H])CC(C)(C)C{22}[C@H](O)[C@@]({28}C(O)=O)5CC[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:22α-hydroxyoleanolic acid