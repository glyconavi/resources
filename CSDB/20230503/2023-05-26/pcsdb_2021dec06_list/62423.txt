RES
1b:b-dglc-HEX-1:5|6:a
2n:n1
LIN
1:1o(1+7)2n
NON
NON1
Parent:1
Linkage:o(1+7)n
SmallMolecule:SMILES O=C1C=C(C2=CC=CC=C2)OC3=C1{5}C(O)={6}C(O){7}C(O)=C3
Description:baicalein