RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dgal-HEX-1:5
4b:b-dglc-HEX-1:5
LIN
1:1n(26+1)2d
2:1n(3+1)3d
3:3o(2+1)4d
NON
NON1
SmallMolecule:SMILES [H][C@]1(OC(CC[C@H](C){26}CO)=C2C)C[C@@]3([H])[C@]4([H])CC[C@]5([H])C{3}[C@@H](O)CC[C@]5(C)[C@@]4([H])CC[C@]3(C)[C@]12[H]
Description:(3β,5β,25S)-furost-20(22)-en-3,26-diol