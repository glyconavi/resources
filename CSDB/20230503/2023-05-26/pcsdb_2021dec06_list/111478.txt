RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+3)2n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O[3C]1=C(C2=CC=[54C](O)[53C](O)=C2)[O+]=C3[8CH]=[7C](O)[6CH]=[5C](O)C3=C1
Description:3,3',4',5,7-pentahydroxyflavylium