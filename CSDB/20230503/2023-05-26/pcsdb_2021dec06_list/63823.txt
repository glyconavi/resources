RES
1b:b-dgal-HEX-1:5
2n:n1
3n:n2
4b:b-dgal-HEX-1:5
5n:n3
LIN
1:1o(1+3)2n
2:1o(6+59)3n
3:3n(9+6)4d
4:4o(1+3)5n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O[3C]1=C(C2=C[53C](O)=[54C](O)C=C2)OC3=C([5C](O)=[6CH][7C](O)=[8CH]3)C1=O
Description:3,5,7,3',4'-pentahydroxyflavone
NON2
Parent:1
Linkage:o(6+59)n
SmallMolecule:SMILES O={59}C(O)[C@@H]1[C@@H]({9}C(=O)O)[C@@H](c2cc{54}c(O){53}c(O)c2)[C@@H]1c1cc{4}c(O){3}c(O)c1
Description:3,3',4,4'-tetrahydroxy-μ-truxinic acid
NON3
Parent:4
Linkage:o(1+3)n
SmallMolecule:SMILES O[3C]1=C(C2=C[53C](O)=[54C](O)C=C2)OC3=C([5C](O)=[6CH][7C](O)=[8CH]3)C1=O
Description:3,5,7,3',4'-pentahydroxyflavone