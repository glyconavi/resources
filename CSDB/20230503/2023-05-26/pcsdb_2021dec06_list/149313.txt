RES
1b:b-dgal-HEX-1:5
2b:a-lman-HEX-1:5|6:d
3b:a-lman-HEX-1:5|6:d
4n:n1
LIN
1:1o(6+1)2d
2:1o(2+1)3d
3:1o(1+3)4n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O[3C]1=C(C2=CC=[54C](O)C=C2)OC3=C([5C](O)=[6CH][7C](O)=[8CH]3)C1=O
Description:3,5,7,4'-tetrahydroxyflavone