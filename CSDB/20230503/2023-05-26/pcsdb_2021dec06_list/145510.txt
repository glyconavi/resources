RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dxyl-PEN-1:5
4b:a-lman-HEX-1:5|6:d
5s:acetyl
LIN
1:1n(6+1)2d
2:1n(3+1)3d
3:3o(2+1)4d
4:3o(3+1)5n
NON
NON1
SmallMolecule:SMILES CC1(C){3}[C@@H](O)CC[C@]2(C3)C43CC[C@]5(C)[C@@H]([C@](C)(O6)CC[C@H]6{25}C(C)(O)C){16}[C@@H](O)C[C@](C)5[C@]4([H])C{6}[C@H](O)[C@@]12[H]
Description:astramembrangenin