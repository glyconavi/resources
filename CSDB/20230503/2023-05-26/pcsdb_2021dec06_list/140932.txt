RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5|1:d
LIN
1:1n(8+1)2d
2:1n(11+1)3d
NON
NON1
SmallMolecule:SMILES Cc3c{1}c(O)c2C(=O)c1{8}c(O)cccc1{10}Cc2c3
Description:1,8-dihydroxy-11-methylanthron