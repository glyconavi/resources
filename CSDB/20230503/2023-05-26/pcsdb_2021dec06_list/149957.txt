RES
1n:n1
2b:x-dglc-HEX-1:5|6:a
3b:x-dglc-HEX-1:5|6:a
LIN
1:1n(7+1)2d
2:1n(54+1)3d
NON
NON1
SmallMolecule:SMILES O=C(C1=[5C](O)[6CH]=[7C](O)[8CH]=C1O2)[3CH]=C2C3=CC=[54C](O)C=C3
Description:5,7,4'-trihydroxyflavone