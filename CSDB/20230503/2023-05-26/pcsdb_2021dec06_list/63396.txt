RES
1n:n1
2b:b-dglc-HEX-1:5
3n:n2
LIN
1:1n(1+1)2d
2:1n(11+7)3n
NON
NON1
SmallMolecule:SMILES O={11}C(C1=CO{1}[C@@H](O)[C@@]2([H])[C@]1([H])CC=C2{10}CO)O
Description:geniposidic acid aglycon
NON2
Parent:1
Linkage:n(11+7)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3CH][2CH]=[1C]1[7C](=O)O
Description:4-hydroxybenzoic acid