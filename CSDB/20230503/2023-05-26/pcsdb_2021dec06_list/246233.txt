RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+3)2n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C[C@@]12CC[C@@]3(C(O)=O)CCC(C)(C)C[C@H]3C1=CC[C@H]4[C@@]2(C)C{6}[C@@H](O)[C@H]5[C@]4(C)C{2}[C@H](O){3}[C@H](O)[C@]5({23}CO)C
Description:protobassic acid