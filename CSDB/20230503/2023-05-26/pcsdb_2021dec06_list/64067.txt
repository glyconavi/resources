RES
1b:b-dglc-HEX-1:5
2s:acetyl
3s:acetyl
4s:acetyl
5s:acetyl
6n:n1
LIN
1:1o(2+1)2n
2:1o(3+1)3n
3:1o(6+1)4n
4:1o(4+1)5n
5:1o(1+1)6n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES CCC1=C(C(=O){1}NC1=O)C
Description:2-ethyl-3-methylmaleimide