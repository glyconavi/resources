RES
1b:b-drib-PEN-1:4
2n:n1
LIN
1:1o(1+7)2n
NON
NON1
Parent:1
Linkage:o(1+7)n
SmallMolecule:SMILES N#CC1=CN=C2{7}NC=NC(N)=C21
Description:4-aminopyrrolo[2,3-d]pyrimidine-5-carbonitrile