RES
1b:b-dglc-HEX-1:5
2n:n1
3b:a-lara-PEN-1:5
4b:b-dxyl-PEN-1:5
LIN
1:1o(1+3)2n
2:1o(6+1)3d
3:3o(2+1)4d
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES CC1(C){3}[C@@H](O)CC[C@]2(C)[C@@]3([H])CC=C4[C@]5([H])CC(C)(C)[C@](O6)([H])C[C@@](C6=O)5{16}[C@H](O)C[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:acacic acid 21,28-lactone