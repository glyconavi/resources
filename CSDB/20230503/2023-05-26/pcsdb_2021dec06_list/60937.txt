RES
1b:o-dgro-TRI-0:0|1:aldi
2b:b-dgal-HEX-1:5
3n:n1
4n:n2
LIN
1:1o(1+1)2d
2:1o(3+1)3n
3:1o(2+1)4n
NON
NON1
Parent:1
Linkage:o(3+1)n
HistoricalEntity:16:3, 18:2, 18:3 fatty acids
NON2
Parent:1
Linkage:o(2+1)n
HistoricalEntity:16:3, 18:2, 18:3 fatty acids