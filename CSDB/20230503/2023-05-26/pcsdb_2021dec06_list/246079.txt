RES
1b:b-dglc-HEX-1:5
2b:b-dglc-HEX-1:5
3b:b-dxyl-PEN-1:5
4n:n1
LIN
1:1o(3+1)2d
2:1o(2+1)3d
3:1o(1+1)4n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES CC/C=C\C{1}CO
Description:aoba alcohol