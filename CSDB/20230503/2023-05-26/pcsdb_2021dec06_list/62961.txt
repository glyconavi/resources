RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+6)2n
NON
NON1
Parent:1
Linkage:o(1+6)n
SmallMolecule:SMILES C=C{6}[C@](C)(O)C/C=C/{2}C(C)(C)O
Description:(3E,6R)-2,6-dimethyl-3,7-octadiene-2,6-diol