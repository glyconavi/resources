RES
1b:a-lara-PEN-1:5
2b:b-dglc-HEX-1:5
3n:n1
LIN
1:1o(3+1)2d
2:1o(1+6)3n
NON
NON1
Parent:1
Linkage:o(1+6)n
SmallMolecule:SMILES [8CH2]=[7CH][6C@@]([8CH3])(O)[5CH2][4CH2]/[3CH]=[2C]([7CH3])/[1C](O)=O
Description:(S)-menthiafolic acid