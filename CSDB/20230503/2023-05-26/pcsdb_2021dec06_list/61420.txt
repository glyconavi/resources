RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4b:b-dglc-HEX-1:5
5b:b-dglc-HEX-1:5
LIN
1:1n(19+1)2d
2:1n(13+1)3d
3:3o(2+1)4d
4:3o(3+1)5d
NON
NON1
SmallMolecule:SMILES C[C@@]1([19C](O)=O)CCC[C@]2(C)[C@H]1CC[C@]3(C4)[C@@]2([H])CC[13C@@](O)(C4=C)C3
Description:steviol