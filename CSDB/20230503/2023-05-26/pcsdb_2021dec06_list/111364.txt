RES
1r:r1
REP
REP1:5o(4+1)2d=-1--1
RES
2b:a-dglc-HEX-x:x
3b:a-dglc-HEX-x:x
4b:a-dglc-HEX-x:x
5b:a-dglc-HEX-x:x
6b:a-dglc-HEX-x:x
7b:a-dglc-HEX-x:x
8b:a-dglc-HEX-x:x
LIN
1:2o(4+1)3d
2:3o(4+1)4d
3:4o(4+1)5d
4:4o(6+1)6d
5:6o(4+1)7d
6:7o(4+1)8d