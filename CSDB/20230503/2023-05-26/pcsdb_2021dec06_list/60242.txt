RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4b:b-dxyl-PEN-1:5
LIN
1:1n(21+1)2d
2:1n(6+1)3d
3:3o(2+1)4d
NON
NON1
SmallMolecule:SMILES CC1(C){3}[C@@H](O)CC[C@]2(C)[C@@]3([H])C{12}[C@@H](O)[C@]4([H])[C@@H]({20}[C@@](C)(O)C/C=C/{25}C(C)(O)C)CC[C@](C)4[C@@](C)3C{6}[C@H](O)C12
Description:dammar-23-en-3β,6α,12β,20S,25-pentol