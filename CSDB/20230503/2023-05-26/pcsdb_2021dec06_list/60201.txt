RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+54)2n
NON
NON1
Parent:1
Linkage:o(1+54)n
SmallMolecule:SMILES COC1=CC(C2C3COC(C4=CC(OC)={104}C(O)C(OC)=C4)C3CO2)=CC(OC)={54}C1O
Description:syringaresinol