RES
1b:b-dgal-HEX-1:5|6:d
2s:methyl
3n:n1
4b:b-dglc-HEX-1:5
5b:b-dglc-HEX-1:5
LIN
1:1o(3+1)2n
2:1o(1+3)3n
3:1o(4+1)4d
4:4o(6+1)5d
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O{3}[C@H]1CC[C@@]2(C)[C@](CC[C@]3([H])[C@]2([H])CC[C@@]4(C){14}[C@]3(O)CC[C@@H]4C(CO5)=CC5=O)([H])C1
Description:uzarigenin