RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+1)2n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES O{1}[C@H]1/C=C\{4}[C@@H](O){3}[C@H](O){2}[C@@H]1O
Description:conduritol A