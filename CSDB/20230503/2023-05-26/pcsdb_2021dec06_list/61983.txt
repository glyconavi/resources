RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+4)2n
NON
NON1
Parent:1
Linkage:o(1+4)n
SmallMolecule:SMILES O=C2c1{4}c(O)c{6}c(O)cc1O{2}C2(O)Cc3cc{54}c(O)cc3
Description:maesopsin