RES
1n:n1
2b:a-lman-HEX-1:5|6:d
3b:a-lman-HEX-1:5|6:d
LIN
1:1n(3+1)2d
2:1n(7+1)3d
NON
NON1
SmallMolecule:SMILES O[3C]1=C(C2=CC=[54C](O)C=C2)OC3=C([5C](O)=[6CH][7C](O)=[8CH]3)C1=O
Description:3,5,7,4'-tetrahydroxyflavone