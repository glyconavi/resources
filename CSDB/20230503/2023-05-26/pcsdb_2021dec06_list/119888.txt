RES
1n:n1
2b:b-dglc-HEX-1:5
3b:a-lman-HEX-1:5|6:d
4n:n2
LIN
1:1n(1+1)2d
2:1n(6+1)3d
3:3o(3+9)4n
NON
NON1
SmallMolecule:SMILES O[1C@H]1[9C@H]2([5C@@H]([4CH]=[3CH]O1)[6C@@H]([7C@H]3[8C@@]2(O3)[10CH2]O)O)
Description:catalpol aglycon
NON2
Parent:3
Linkage:o(3+9)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3CH][2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:p-coumaric acid