RES
1b:b-drib-PEN-1:4
2s:acetyl
3s:acetyl
4s:acetyl
5n:n1
LIN
1:1o(2+1)2n
2:1o(3+1)3n
3:1o(5+1)4n
4:1o(1+4)5n
NON
NON1
Parent:1
Linkage:o(1+4)n
SmallMolecule:SMILES NC(=O)c1c{4}[nH]ccc1=O
Description:4-oxonicotinamide