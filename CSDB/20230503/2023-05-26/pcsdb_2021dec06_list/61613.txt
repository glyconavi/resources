RES
1n:n1
2b:b-dglc-HEX-1:5
3s:acetyl
LIN
1:1n(3+1)2d
2:1n(24+1)3n
NON
NON1
SmallMolecule:SMILES C[C@@H](C1C{24}[C@H](O)[C@](C)(C)O1)[C@H]5{16}[C@@H](O)C[C@@]6(C)C3CCC2[C@@](C)(C){3}[C@@H](O)CC[C@@]24C[C@@]34CC[C@]56C
Description:22R,25-epoxy-9,19-cyclolanosta-3β,16β,24S-triol