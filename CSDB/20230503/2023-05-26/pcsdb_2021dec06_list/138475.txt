RES
1n:n1
2b:b-dglc-HEX-1:5
3b:a-lara-PEN-1:5
4b:b-dglc-HEX-1:5
5b:a-lman-HEX-1:5|6:d
6b:b-dxyl-PEN-1:5
LIN
1:1n(3+1)2d
2:1n(28+1)3d
3:2o(2+1)4d
4:3o(2+1)5d
5:5o(4+1)6d
NON
NON1
SmallMolecule:SMILES CC1(C)CC[C@]2({28}C(O)=O)CC[C@@]3(C)[C@]4(C)CC[C@@]5([H])[C@@]({23}C(O)=O)(C){3}[C@@H](O){2}[C@@H](O)C[C@]5(C)[C@@]4([H])CC=C3[C@]2([H])C1
Description:medicagenic acid