RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+7)2n
NON
NON1
Parent:1
Linkage:o(1+7)n
SmallMolecule:SMILES C/C(C)=C\Cc3cc(c2cc(=O)c1{5}c(O)c{7}c(O)cc1o2)cc{54}c3O
Description:3'-prenylapigenine