RES
1b:b-dgal-HEX-1:5
2b:b-dxyl-PEN-1:5
3b:a-lman-HEX-1:5|6:d
4n:n1
LIN
1:1o(3+1)2d
2:1o(2+1)3d
3:1o(1+3)4n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C[C@H]1[C@H]2[C@H](C[C@H]3[C@@H]4CC=C5C{3}[C@@H](O)CC[C@]5(C)[C@H]4CC[C@@]32C)O[C@]12NC[C@H](C)C{23}[C@@H]2O
Description:solaverol A