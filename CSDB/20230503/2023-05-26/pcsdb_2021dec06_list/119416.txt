RES
1b:x-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+9)2n
NON
NON1
Parent:1
Linkage:o(1+9)n
SmallMolecule:SMILES OC[C@H](OC1=C(C=C(C=C1)CC{9}CO)OC)[C@@H](C2=CC=C(C(OC)=C2)O)O
Description:2-methoxy-4-[(1R,2S)-1,3-dihydroxy-2-[2-methoxy-4-(3-hydroxypropyl)phenoxy]propyl]phenol