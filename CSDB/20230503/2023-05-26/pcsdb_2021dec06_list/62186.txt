RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5|6:a
4b:b-dgal-HEX-1:5
5b:a-lman-HEX-1:5|6:d
LIN
1:1n(28+1)2d
2:1n(3+1)3d
3:3o(2+1)4d
4:4o(2+1)5d
NON
NON1
SmallMolecule:SMILES CC1(C)CC[C@]2({28}C(O)=O)CC[C@@]3(C)[C@]4(C)CC[C@@]5([H])[C@](C)({23}CO){3}[C@@H](O)CC[C@]5(C)[C@@]4([H])CC=C3[C@]2([H])C1
Description:hederagenin