RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dgal-HEX-1:5|6:d
4b:b-dglc-HEX-1:5
5b:a-lman-HEX-1:5|6:d
6n:n2
7s:acetyl
8s:methyl
9s:methyl
10b:b-dxyl-PEN-1:5
11b:b-dgal-HEX-1:5
LIN
1:1n(3+1)2d
2:1n(28+1)3d
3:3o(3+1)4d
4:3o(2+1)5d
5:3o(4+9)6n
6:4o(6+1)7n
7:6n(3+1)8n
8:6n(4+1)9n
9:5o(4+1)10d
10:10o(4+1)11d
NON
NON1
SmallMolecule:SMILES C[C@@]12CC[C@@H]3[C@@](C{2}[C@H](O){3}[C@H](O)[C@]3({23}C(O)=O)C)(C)[C@H]1CC=C4[C@@]2({27}CO)CC[C@]5({28}C(O)=O)[C@H]4CC(C)(C)CC5
Description:presenegenin
NON2
Parent:3
Linkage:o(4+9)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3C](O)[2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:caffeic acid