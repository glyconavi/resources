RES
1n:n1
2b:x-dman-HEX-1:5
3b:a-dglc-HEX-1:5|6:a
4b:a-dglc-HEX-1:5
5s:n-acetyl
LIN
1:1n(-1+1)2d
2:1n(-1+1)3d
3:3o(4+1)4d
4:4d(2+1)5n
NON
NON1
HistoricalEntity:superclass: any inositol