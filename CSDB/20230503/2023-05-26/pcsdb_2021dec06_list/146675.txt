RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+7)2n
NON
NON1
Parent:1
Linkage:o(1+7)n
SmallMolecule:SMILES O=C(C1=[5C](O)[6CH]=[7C](O)[8CH]=C1O2)[3CH]=C2C3=CC=[54C](O)C=C3
Description:5,7,4'-trihydroxyflavone