RES
1b:x-dxyl-PEN-x:x
2b:b-dxyl-PEN-1:5
3b:a-lara-PEN-1:4
4n:n1
LIN
1:1o(4+1)2d
2:2o(3+1)3d
3:3o(5+9)4n
NON
NON1
Parent:3
Linkage:o(5+9)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3C](O[10CH3])[2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:ferulic acid