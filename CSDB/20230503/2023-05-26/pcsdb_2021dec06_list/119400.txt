RES
1n:n1
2b:b-dglc-HEX-1:5
3n:n2
4b:b-dglc-HEX-1:5
5b:a-lman-HEX-1:5|6:d
6b:b-dglc-HEX-1:5
7b:a-lman-HEX-1:5|6:d
8b:a-lara-PEN-1:5
9b:b-dglc-HEX-1:5
10n:n3
11b:b-dxyl-PEN-1:5
12b:b-dxyl-PEN-1:5
LIN
1:1n(3+1)2d
2:1n(21+1)3n
3:1n(28+1)4d
4:4o(6+1)5d
5:2o(2+1)6d
6:4o(2+1)7d
7:3n(6+1)8d
8:8o(3+1)9d
9:8o(4+1)10n
10:7o(4+1)11d
11:11o(3+1)12d
NON
NON1
SmallMolecule:SMILES C[C@]12C{2}[C@H](O){3}[C@H](O)[C@]({23}CO)(C)[C@@H]1CC[C@]3(C)[C@@H]2CC=C4[C@@]3(C)C{16}[C@@H](O)[C@]5({28}C(O)=O)[C@H]4CC(C)(C){21}[C@@H](O)C5
Description:2β,23-dihydroxy-acacic acid
NON2
Parent:1
Linkage:n(21+1)n
SmallMolecule:SMILES [8CH2]=[7CH][6C@@]([8CH3])(O)[5CH2][4CH2]/[3CH]=[2C]([7CH3])/[1C](O)=O
Description:(S)-menthiafolic acid
NON3
Parent:8
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH3][3CH2][2CH]([5CH3])[1C](=O)O
Description:anteisovaleric acid