RES
1n:n1
2b:b-dglc-HEX-1:5
3s:methyl
LIN
1:1n(11+1)2d
2:1n(16+1)3n
NON
NON1
SmallMolecule:SMILES C[C@@H]1C=C(OC)C([C@@]2(C)[C@H]1C[C@@H]3[C@]4(C)[C@@H]2{11}[C@@H](C(OC)C(C)[C@@H]4C{16}[C@H](O)O3)O)=O
Description:16α-neoquassin derivative