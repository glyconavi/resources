RES
1b:b-dglc-HEX-1:5
2b:b-dglc-HEX-1:5
3n:n1
LIN
1:1o(2+1)2d
2:1o(1+3)3n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C{25}[C@](C)(O)C6CC[C@@](C)(C4{16}[C@@H](O)C[C@@]5(C)C2C{6}[C@H](O)C1[C@@](C)(C){3}[C@@H](O)CC[C@@]13C[C@@]23CC[C@]45C)O6
Description:cyclogalagenin