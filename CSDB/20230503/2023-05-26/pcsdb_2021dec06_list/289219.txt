RES
1b:b-dglc-HEX-x:x|1:d
2n:n1
LIN
1:1h(1+6)2n
NON
NON1
Parent:1
Linkage:h(1+6)n
SmallMolecule:SMILES O=C(C1=[5C](O)[6CH]=[7C](O)[8CH]=C1O2)[3CH]=C2C3=CC=[54C](O)C=C3
Description:5,7,4'-trihydroxyflavone