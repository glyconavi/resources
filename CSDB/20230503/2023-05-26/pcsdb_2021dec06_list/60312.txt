RES
1r:r1
REP
REP1:4o(3+1)2n=-1--1
RES
2n:n1
3b:a-lman-HEX-1:5|6:d
4b:a-lman-HEX-1:5|6:d
5b:a-lman-HEX-1:5|6:d
6n:n2
7b:a-lman-HEX-1:5|6:d
8n:n3
LIN
1:2n(11+1)3d
2:3o(4+1)4d
3:4o(4+1)5d
4:5o(3+1)6n
5:5o(4+1)7d
6:7o(3+1)8n
NON
NON1
SmallMolecule:SMILES [16CH3][15CH2][14CH2][13CH2][12CH2][11C@H](O)[10CH2][9CH2][8CH2][7CH2][6CH2][5CH2][4CH2][3CH2][2CH2][1C](=O)O
Description:jalapinolic acid
NON2
Parent:5
Linkage:o(3+1)n
SmallMolecule:SMILES [3CH3][2CH]([4CH3])[1C](=O)O
Description:isobutyric acid
NON3
Parent:7
Linkage:o(3+1)n
SmallMolecule:SMILES [3CH3][2CH]([4CH3])[1C](=O)O
Description:isobutyric acid