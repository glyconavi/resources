RES
1n:n1
2s:methyl
3b:b-dglc-HEX-1:5|6:a
4s:methyl
5b:b-dgal-HEX-1:5
6b:a-lman-HEX-1:5|6:d
LIN
1:1n(29+1)2n
2:1n(3+1)3d
3:3o(6+1)4n
4:3o(2+1)5d
5:5o(2+1)6d
NON
NON1
SmallMolecule:SMILES C[C@]1({24}CO){3}[C@@H](O)CC[C@]2(C)[C@@]3([H])CC=C4[C@]5([H])C[C@](C)({29}C(O)=O)C{22}[C@@H](O)[C@@](C)5CC[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:oxytrogenin