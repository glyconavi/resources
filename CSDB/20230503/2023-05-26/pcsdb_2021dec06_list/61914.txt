RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+1)2n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES COC(=O)/C1=C/O{1}[C@@H](O)C2[C@@H]({10}CO)C{6}[C@H](O)C12
Description:10-hydroxy-(5αh)-6-epidihydrocornin aglycon