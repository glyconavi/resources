RES
1b:b-dglc-HEX-1:5
2n:n1
3b:b-dglc-HEX-1:5
4n:n2
LIN
1:1o(1+3)2n
2:1o(3+1)3d
3:3o(6+1)4n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O[3C]1=C(C2=CC=[54C](O)[53C](O)=C2)[O+]=C3[8CH]=[7C](O)[6CH]=[5C](O)C3=C1
Description:3,3',4',5,7-pentahydroxyflavylium
NON2
Parent:3
Linkage:o(6+1)n
SmallMolecule:SMILES [3C](=O)(O)[2CH2][1C](=O)(O)
Description:malonic acid