RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+1)2n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES C{7}C(O)(C)/C=C/C[C@H](C)C{1}CO
Description:(3S)-3,7-dimethyl-7-hydroxyoct-5-enol