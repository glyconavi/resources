RES
1b:b-dara-HEX-2:5|1:aldi|2:keto
2s:acetyl
3n:n1
4n:n2
5b:a-dglc-HEX-1:5
6s:acetyl
7s:acetyl
8s:acetyl
LIN
1:1o(1+1)2n
2:1o(6+9)3n
3:1o(3+9)4n
4:1o(2+1)5d
5:5o(2+1)6n
6:5o(4+1)7n
7:5o(6+1)8n
NON
NON1
Parent:1
Linkage:o(6+9)n
SmallMolecule:SMILES [6CH]1=[5C](O[11CH3])[4C](O)=[3C](O[10CH3])[2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:sinapic/sinapinic acid
NON2
Parent:1
Linkage:o(3+9)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3C](O[10CH3])[2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:ferulic acid