RES
1b:b-dglc-HEX-1:5
2b:a-lman-HEX-1:5|6:d
3n:n1
LIN
1:1o(2+1)2d
2:1o(1+3)3n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C[C@@H]1CC[C@]2(O[26CH2]1)[C@@H](C)[C@H]3[C@H](C[C@@H]4[C@]3(C)CC[C@H]5[C@H]4CC=C6[C@]5(C)CC[3CH](O)C6)O2
Description:diosgenin