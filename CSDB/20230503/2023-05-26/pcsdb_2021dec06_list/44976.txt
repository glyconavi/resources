RES
1r:r1
REP
REP1:2o(3+1)2d=-1--1
RES
2b:b-dgal-HEX-1:4
3a:a1
LIN
1:2o(6+1)3n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:4
RES
4b:a-lman-HEX-1:5|6:d
5b:b-dgal-HEX-1:4
LIN
1:4o(2+1)5d
ALTSUBGRAPH2
LEAD-IN RES:6
RES
6b:a-lman-HEX-1:5|6:d
7b:b-dgal-HEX-1:4
8b:b-dgal-HEX-1:4
LIN
1:6o(2+1)7d
2:6o(4+1)8d