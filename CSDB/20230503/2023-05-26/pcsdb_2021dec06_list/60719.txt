RES
1b:b-dglc-HEX-1:5
2s:acetyl
3s:acetyl
4s:acetyl
5n:n1
6n:n2
7b:b-dglc-HEX-1:5
LIN
1:1o(2+1)2n
2:1o(3+1)3n
3:1o(6+1)4n
4:1o(1+6)5n
5:1o(4+7)6n
6:6n(3+1)7d
NON
NON1
Parent:1
Linkage:o(1+6)n
SmallMolecule:SMILES C=C[C@H]1{6}[C@H](O)OC=C2{4}[C@@]1(O)CCOC2=O
Description:swertiamarine aglycon
NON2
Parent:1
Linkage:o(4+7)n
SmallMolecule:SMILES [6CH]1=[5CH][4CH]=[3C](O)[2C](O)=[1C]1[7C](=O)O
Description:2,3-dihydroxybenzoic acid