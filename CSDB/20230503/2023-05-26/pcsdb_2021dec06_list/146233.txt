RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dxyl-PEN-1:5
4b:a-lman-HEX-1:5|6:d
5b:b-dxyl-PEN-1:5
6b:a-lman-HEX-1:5|6:d
LIN
1:1n(3+1)2d
2:1n(23+1)3d
3:3o(2+1)4d
4:4o(4+1)5d
5:5o(3+1)6d
NON
NON1
SmallMolecule:SMILES C[C@@]12CC[C@@]3(C(O)=O)CCC(C)(C)C[C@H]3C1=CC[C@H]4[C@@]2(C)C{6}[C@@H](O)[C@H]5[C@]4(C)C{2}[C@H](O){3}[C@H](O)[C@]5({23}CO)C
Description:protobassic acid