RES
1b:b-dglc-HEX-1:5
2n:n1
3n:n2
4b:a-lman-HEX-1:5|6:d
5b:b-dglc-HEX-1:5
LIN
1:1o(4+9)2n
2:1o(1+8)3n
3:1o(3+1)4d
4:4o(2+1)5d
NON
NON1
Parent:1
Linkage:o(4+9)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3C](O)[2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:caffeic acid
NON2
Parent:1
Linkage:o(1+8)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3C](O)[2CH]=[1C]1[7CH2][8CH2]O
Description:3,4,8-trihydroxy-ethylbenzene