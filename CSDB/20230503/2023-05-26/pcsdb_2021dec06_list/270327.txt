RES
1b:b-dglc-HEX-1:5|6:a
2s:methyl
3n:n1
4b:b-dgal-HEX-1:5
5b:a-lman-HEX-1:5|6:d
LIN
1:1o(6+1)2n
2:1o(1+3)3n
3:1o(2+1)4d
4:4o(4+1)5d
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES CC1(C){3}[C@@H](O){2}[C@@H](O)C[C@]2(C)[C@@]3([H])CC=C4[C@]5([H])CC(C)(C)CC[C@@]({28}CO)5CC[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:olean-12-en-2β,3β,28-triol