RES
1b:b-dglc-HEX-1:5
2b:a-lman-HEX-1:5|6:d
3n:n1
4n:n2
LIN
1:1o(3+1)2d
2:1o(1+8)3n
3:1o(4+9)4n
NON
NON1
Parent:1
Linkage:o(1+8)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3C](O)[2CH]=[1C]1[7CH2][8CH2]O
Description:3,4,8-trihydroxy-ethylbenzene
NON2
Parent:1
Linkage:o(4+9)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3C](O[10CH3])[2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:ferulic acid