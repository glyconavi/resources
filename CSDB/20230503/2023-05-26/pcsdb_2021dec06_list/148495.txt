RES
1b:b-dara-HEX-2:5|1:aldi|2:keto
2n:n1
3n:n2
4b:a-dglc-HEX-1:5
5b:b-dglc-HEX-1:5
6s:acetyl
LIN
1:1o(3+7)2n
2:1o(1+9)3n
3:1o(2+1)4d
4:4o(2+1)5d
5:5o(6+1)6n
NON
NON1
Parent:1
Linkage:o(3+7)n
SmallMolecule:SMILES [6CH]1=[5CH][4CH]=[3CH][2CH]=[1C]1[7C](=O)O
Description:benzoic acid
NON2
Parent:1
Linkage:o(1+9)n
SmallMolecule:SMILES [6CH]1=[5CH][4CH]=[3CH][2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:cinnamic acid