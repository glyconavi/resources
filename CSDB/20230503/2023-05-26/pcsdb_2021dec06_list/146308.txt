RES
1n:n1
2s:methyl
3b:b-dglc-HEX-1:5
4b:a-lman-HEX-1:5|6:d
5b:b-dglc-HEX-1:5
6s:acetyl
LIN
1:1n(26+1)2n
2:1n(3+1)3d
3:3o(2+1)4d
4:3o(4+1)5d
5:5o(6+1)6n
NON
NON1
SmallMolecule:SMILES [H][C@]1(O[C@@]2(O{26}[C@@H](O)[C@H](C)CC2)[C@H]3C)C[C@@]4([H])[C@]5([H])CC=C6C{3}[C@@H](O)CC[C@]6(C)[C@@]5([H])CC[C@]4(C)[C@]13[H]
Description:25R-spirost-5-en-3β,26R-diol