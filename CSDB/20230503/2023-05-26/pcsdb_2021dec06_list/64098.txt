RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
LIN
1:1n(4+1)2d
2:1n(9+1)3d
NON
NON1
SmallMolecule:SMILES COC1={4}C(O)C(OC)=CC(/C=C/{9}CO)=C1
Description:sinapic alcohol