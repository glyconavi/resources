RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4b:b-dxyl-PEN-1:5
5b:a-lara-PEN-1:5
LIN
1:1n(6+1)2d
2:1n(24+1)3d
3:1n(3+1)4d
4:4o(2+1)5d
NON
NON1
SmallMolecule:SMILES C[C@H](CC{24}[C@H](O){25}C(C)(C)O)[C@H]1{16}[C@@H](O)C[C@@]2(C)[C@@H]3C{6}[C@H](O)[C@H]4C(C)(C){3}[C@@H](O)CC[C@@]45C[C@@]35CC[C@]12C
Description:cyclocanthogenin