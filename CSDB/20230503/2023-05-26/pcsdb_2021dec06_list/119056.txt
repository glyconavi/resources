RES
1b:b-dglc-HEX-1:5
2b:a-lara-PEN-1:4
3b:a-lman-HEX-1:5|6:d
4n:n1
LIN
1:1o(4+1)2d
2:1o(2+1)3d
3:1o(1+3)4n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C[C@@H]1CC[C@@]2([C@H]({17}[C@]3([C@@H](O2)C[C@@H]4[C@@]3(CC[C@H]5[C@H]4CC=C6[C@@]5(CC{3}[C@@H](C6)O)C)C)O)C)OC1
Description:pennogenin