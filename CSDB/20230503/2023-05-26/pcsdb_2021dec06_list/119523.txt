RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4b:a-lman-HEX-1:5|6:d
5b:a-lman-HEX-1:5|6:d
LIN
1:1n(26+1)2d
2:1n(3+1)3d
3:3o(2+1)4d
4:3o(4+1)5d
NON
NON1
SmallMolecule:SMILES C[C@H](CCC(=O)/C(=C/1\C(=O)C[C@@H]2[C@@]1(CC[C@H]3[C@H]2CC=C4[C@@]3(CC{3}[C@@H](C4)O)C)C)/C){26}CO
Description:17(20)-dehydrocryptogenin