RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4b:b-dglc-HEX-1:5
LIN
1:1n(21+1)2d
2:1n(3+1)3d
3:3o(2+1)4d
NON
NON1
SmallMolecule:SMILES O[3C@H]1CC[C@@]2(C)[C@](CC[C@]3(C)[C@]2([H])C[12C@@H](O)[C@@]4([H])[C@@]3(C)CC[C@@]4([20C@@](CC/C=C(C)/C)(C)O)[H])([H])C1(C)C
Description:20S-protopanaxadiol