RES
1b:b-dglc-HEX-1:5|6:a
2b:b-dglc-HEX-1:5
3n:n1
4b:b-dgal-HEX-1:5
5b:a-lman-HEX-1:5|6:d
LIN
1:1o(2+1)2d
2:1o(1+3)3n
3:1o(3+1)4d
4:4o(2+1)5d
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C[C@@]5(C)CC4/C3=C/CC2[C@@]1(C)CC{3}[C@H](O)[C@](C)(C)C1CC[C@@]2(C)[C@]3(C)C{16}[C@@H](O)[C@@]4({28}CO)CC5=O
Description:armillarigenin