RES
1b:b-dglc-HEX-1:5
2s:acetyl
3s:acetyl
4n:n1
5n:n2
6b:a-lman-HEX-1:5|6:d
7s:acetyl
8s:acetyl
9s:acetyl
LIN
1:1o(4+1)2n
2:1o(2+1)3n
3:1o(1+8)4n
4:1o(6+9)5n
5:1o(3+1)6d
6:6o(2+1)7n
7:6o(3+1)8n
8:6o(4+1)9n
NON
NON1
Parent:1
Linkage:o(1+8)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3C](O)[2CH]=[1C]1[7CH2][8CH2]O
Description:3,4,8-trihydroxy-ethylbenzene
NON2
Parent:1
Linkage:o(6+9)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3C](O)[2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:caffeic acid