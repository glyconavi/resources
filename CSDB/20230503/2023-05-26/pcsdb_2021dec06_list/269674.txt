RES
1n:n1
2b:b-dglc-HEX-1:5
3b:a-lman-HEX-1:5|6:d
4b:b-dglc-HEX-1:5
5b:b-dglc-HEX-1:5
6b:a-lman-HEX-1:5|6:d
7n:n2
8b:b-dglc-HEX-1:5
LIN
1:1n(3+1)2d
2:1n(21+1)3d
3:2o(2+1)4d
4:3o(3+1)5d
5:5o(6+1)6d
6:6o(2+16)7n
7:7n(3+1)8d
NON
NON1
SmallMolecule:SMILES C=C{3}[C@@](C)(O)CC/C=C(C)/CC/C=C(C)/CC/C=C({20}CO)/C
Description:6E,10E,14Z-20-hydroxy-3S-geranyllinalool
NON2
Parent:6
Linkage:o(2+16)n
SmallMolecule:SMILES C=C{3}[C@@](C)(O)CC/C=C(C)/CC/C=C(C)/C{13}[CH](O)/C=C(C)/{16}C(O)=O
Description:6E,10E,14E-13-hydroxy-3S-geranyllinalool-16-oic acid