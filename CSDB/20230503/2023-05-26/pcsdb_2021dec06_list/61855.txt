RES
1n:n1
2a:a1
LIN
1:1n(3+1)2n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:3
RES
3b:b-dgal-HEX-1:5
ALTSUBGRAPH2
LEAD-IN RES:4
RES
4b:a-lman-HEX-1:5|6:d
NON
NON1
SmallMolecule:SMILES O[3C]1=C(C2=C[53C](O)=[54C](O)C=C2)OC3=C([5C](O)=[6CH][7C](O)=[8CH]3)C1=O
Description:3,5,7,3',4'-pentahydroxyflavone