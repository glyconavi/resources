RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+1)2n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES N#C{1}C1(O)/C=C\CC1
Description:1-cyano-1-hydroxy-2-cyclopentene