RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+3)2n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES CC(C)[C@@]6([C@H]5OC(=O)[C@H]([C@@H]4CC[C@H]3/C/2=C/C[C@H]1C{3}[C@@H](O)CC[C@]1(C)C2=C\C[C@@]34C){21}[C@H]5O)O[C@H]6C
Description:vernoniol B1