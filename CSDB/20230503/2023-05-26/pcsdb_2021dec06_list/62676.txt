RES
1b:b-dglc-HEX-1:5
2n:n1
3b:b-dgro-TET-1:4
4s:hydroxymethyl
5n:n2
LIN
1:1o(1+8)2n
2:1o(2+1)3d
3:3h(3+1)4n
4:3o(5+7)5n
NON
NON1
Parent:1
Linkage:o(1+8)n
SmallMolecule:SMILES O{8}CCC1=CC={4}C(O)C=C1
Description:tyrosol
NON2
Parent:3
Linkage:o(5+7)n
SmallMolecule:SMILES COC1=CC({7}C(O)=O)=CC={4}C1O
Description:vanillic acid