RES
1b:b-dglc-HEX-1:5
2b:b-dxyl-PEN-1:5
3n:n1
LIN
1:1o(2+1)2d
2:1o(1+3)3n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C[C@]([C@@]1([H])CC[C@]23C)([C@@]2([H])CC[C@]4([H]){17}[C@@H](O)[C@](CC[C@]43C)(CO5)CC[C@]5([H])[C@H](C){26}CO)CC{3}[C@H](O)[C@]1({28}CO)C
Description:hosenkol B