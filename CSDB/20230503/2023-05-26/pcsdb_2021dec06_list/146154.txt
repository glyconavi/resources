RES
1n:n1
2b:b-dglc-HEX-1:5|6:a
3b:a-lara-PEN-1:5
4b:b-dgal-HEX-1:5
5b:b-dxyl-PEN-1:5
6s:acetyl
7s:acetyl
8s:acetyl
LIN
1:1n(3+1)2d
2:1n(22+1)3d
3:2o(2+1)4d
4:3o(3+1)5d
5:5o(2+1)6n
6:5o(3+1)7n
7:5o(4+1)8n
NON
NON1
SmallMolecule:SMILES C[C@]12CC[3C@@H]([C@]([C@@H]1CC[C@@]3([C@@H]2CC=C4[C@]3(CC[C@@]5([C@H]4CC([21C@H]([22C@H]5O)O)(C)C)C)C)C)(C)[24CH2]O)O
Description:soyasapogenol A