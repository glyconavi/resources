RES
1n:n1
2n:n2
3b:b-dglc-HEX-1:5
4s:acetyl
5s:acetyl
6s:acetyl
7s:acetyl
LIN
1:1n(1+1)2n
2:2n(5+1)3d
3:3o(2+1)4n
4:3o(3+1)5n
5:3o(6+1)6n
6:3o(4+1)7n
NON
NON1
SmallMolecule:SMILES [1CH3]O
Description:methyl
NON2
Parent:1
Linkage:n(1+1)n
SmallMolecule:SMILES [5CH2](O)[4CH]([6CH3])[3CH2][2CH2][1C](=O)O
Description:5-hydroxy-isohexanoic acid