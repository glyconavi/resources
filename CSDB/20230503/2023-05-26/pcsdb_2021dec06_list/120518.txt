RES
1b:b-dgal-HEX-1:5
2b:b-dglc-HEX-1:5
3n:n1
LIN
1:1o(2+1)2d
2:1o(1+3)3n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES [H][C@]1(O[C@@]2(OC[C@@H](C)CC2)[C@H]3C){15}[C@H](O)[C@@]4([H])[C@]5([H])CC[C@]6([H])C{3}[C@@H](O)CC[C@]6(C)[C@@]5([H])CC[C@]4(C)[C@]13[H]
Description:15-hydroxysarsasapogenin