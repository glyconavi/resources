RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+28)2n
NON
NON1
Parent:1
Linkage:o(1+28)n
SmallMolecule:SMILES C[C@@H]1CC[C@]2({28}C(O)=O)CC[C@]3(C)C([C@@H]2{19}[C@@]1(O)C)=CC[C@H]4[C@@]3(C)CC[C@@H]5[C@]4(C)CC{3}[C@H](O)[C@@]5({24}CO){23}C(O)=O
Description:24-oxorotundioic acid