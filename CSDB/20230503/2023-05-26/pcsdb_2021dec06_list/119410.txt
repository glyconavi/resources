RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+12)2n
NON
NON1
Parent:1
Linkage:o(1+12)n
SmallMolecule:SMILES CC(CCC1=C(C)C{3}[C@@H](O)C[C@]1(C){12}CO)=O
Description:icariside B4 aglycon