RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+54)2n
NON
NON1
Parent:1
Linkage:o(1+54)n
SmallMolecule:SMILES O{3}C1=C(C2=CC(OC)={54}C(O)C(OC)=C2)OC3=C({5}C(O)=C{7}C(O)=C3)C1=O
Description:syringetin