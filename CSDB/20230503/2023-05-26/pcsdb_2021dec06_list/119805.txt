RES
1n:n1
2b:a-lman-HEX-1:5|6:d
3b:b-dglc-HEX-1:5
4b:a-lara-PEN-1:5
5n:n2
LIN
1:1n(28+1)2d
2:1n(16+1)3d
3:3o(2+1)4d
4:4o(4+1)5n
NON
NON1
SmallMolecule:SMILES [1CH2]1[2C@H](O)[3C@H](O)[4C@]([23CH2](O))([24CH3])[5C@@H]2[6CH2][7CH2][8C@]([26CH3])3[14C@@]([27CH3])4[15CH2][16C@@H](O)[17C@]([28C](=O)O)5[22CH2][21CH2][20C]([29CH3])([30CH3])[19CH2][18C@H]5/[13C]4=[12CH]/[11CH2][9C@@H]3[10C@]([25CH3])21
Description:polygalacic acid
NON2
Parent:4
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH3][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxybutanoic acid