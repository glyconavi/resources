RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4b:a-lman-HEX-1:5|6:d
5b:a-lman-HEX-1:5|6:d
6b:a-lman-HEX-1:5|6:d
LIN
1:1n(12+1)2d
2:1n(1+1)3d
3:3o(2+1)4d
4:3o(3+1)5d
5:2o(2+1)6d
NON
NON1
SmallMolecule:SMILES C/C(CC/C=C(CC/C=C({12}CO)\C)\C)=C\{1}CO
Description:12-hydroxy-all-trans-farnesol