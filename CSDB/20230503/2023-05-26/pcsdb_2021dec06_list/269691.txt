RES
1n:n1
2b:b-dgal-HEX-1:5|6:d
3b:a-lman-HEX-1:5|6:d
LIN
1:1n(1+1)2d
2:1n(3+1)3d
NON
NON1
SmallMolecule:SMILES [H][C@]1(O[C@@]2(OC[C@@H](C)CC2)[C@H]3C)C[C@@]4([H])[C@]5([H])CC=C6C{3}[C@@H](O)C{1}[C@@H](O)[C@]6(C)[C@@]5([H])CC[C@]4(C)[C@]13[H]
Description:25S-ruscogenin