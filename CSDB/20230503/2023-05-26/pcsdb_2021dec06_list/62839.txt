RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+4)2n
NON
NON1
Parent:1
Linkage:o(1+4)n
SmallMolecule:SMILES O=C(O)/C=C/c2ccc1occc1{4}c2O
Description:(E)-3-(4-hydroxy-1-benzofuran-5-yl)prop-2-enoic acid