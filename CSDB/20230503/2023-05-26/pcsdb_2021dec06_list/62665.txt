RES
1b:b-dglc-HEX-1:5
2b:a-lman-HEX-1:5|6:d
3b:a-lara-PEN-1:5
4b:a-lman-HEX-1:5|6:d
5n:n1
LIN
1:1o(2+1)2d
2:1o(3+1)3d
3:1o(6+1)4d
4:1o(1+1)5n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES C/C(C)=C\CC/C(C)=C/CC/C(C)=C/{1}CO
Description:(E,E)-farnesol