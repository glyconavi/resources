RES
1b:b-dglc-HEX-1:5
2b:b-dglc-HEX-1:5
3n:n1
LIN
1:1o(2+1)2d
2:1o(1+2)3n
NON
NON1
Parent:1
Linkage:o(1+2)n
SmallMolecule:SMILES CC1(C)C2(C)CCC1C{2}C2O
Description:borneol