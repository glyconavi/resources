RES
1b:b-dglc-HEX-1:5|6:a
2s:methyl
3n:n1
4b:b-dglc-HEX-1:5
5b:a-lman-HEX-1:5|6:d
LIN
1:1o(6+1)2n
2:1o(1+3)3n
3:1o(2+1)4d
4:4o(2+1)5d
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C[C@]12CC[3C@@H]([C@]([C@@H]1CC[C@@]3([C@@H]2CC=C4[C@]3(CC[C@@]5([C@H]4CC(C[22C@H]5O)(C)C)C)C)C)(C)[24CH2]O)O
Description:soyasapogenol B