RES
1b:b-dglc-HEX-1:5
2b:b-dglc-HEX-1:5
3b:a-lman-HEX-1:5|6:d
4n:n1
LIN
1:1o(6+1)2d
2:1o(4+1)3d
3:1o(1+3)4n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES CC1(C){3}[C@@H](O)CC[C@]2(C)[C@@]3([H])C=C[C@]45[C@]6([H])CC(C)(C)CC[C@@](CO5)6{16}[C@@H](O)C[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:saikogenin E