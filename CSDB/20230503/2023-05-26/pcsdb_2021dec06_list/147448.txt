RES
1b:b-dglc-HEX-1:5
2r:r1
3n:n1
LIN
1:1o(4+1)2n
2:1o(1+3)3n
REP
REP1:4o(4+1)4d=0-4
RES
4b:b-dglc-HEX-1:5
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O{3}[C@H](C1)CC[C@@]2(C)C1=CC[C@]3([H])[C@@]2([H])CC[C@@]4(C)[C@]3([H])CC[C@]4([C@@H](C)CC[C@@H](CC)C(C)C)[H]
Description:β-sitosterol