RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+54)2n
NON
NON1
Parent:1
Linkage:o(1+54)n
SmallMolecule:SMILES O{54}C1=C(OC)C=C([C@@H]2[C@@]3([H])CO[C@H](C4=CC(OC)={104}C(O)C=C4)[C@@]3([H])CO2)C=C1
Description:(+)-pinoresinol