RES
1n:n1
2s:acetyl
3s:acetyl
4s:acetyl
5b:b-dglc-HEX-1:5
6s:acetyl
7s:acetyl
8b:a-lman-HEX-1:5|6:d
9b:a-lman-HEX-1:5|6:d
10s:acetyl
11s:acetyl
12s:acetyl
13s:acetyl
14s:acetyl
15b:b-dglc-HEX-1:5
16s:acetyl
17s:acetyl
18s:acetyl
19s:acetyl
LIN
1:1n(6+1)2n
2:1n(16+1)3n
3:1n(21+1)4n
4:1n(3+1)5d
5:5o(3+1)6n
6:5o(6+1)7n
7:5o(4+1)8d
8:5o(2+1)9d
9:9o(3+1)10n
10:9o(4+1)11n
11:8o(4+1)12n
12:8o(3+1)13n
13:8o(2+1)14n
14:9o(2+1)15d
15:15o(2+1)16n
16:15o(3+1)17n
17:15o(6+1)18n
18:15o(4+1)19n
NON
NON1
SmallMolecule:SMILES C{3}[C@H](O)[C@H]1{16}[C@@H](O)C[C@@]2([H])[C@]3([H])C{6}[C@@H](O){5}[C@]4(O)C{20}[C@@H](O)CC[C@]4(C)[C@@]3([H])CC[C@]12C
Description:5α-pregnan-3β,5α,6β,16β,20α-pentol