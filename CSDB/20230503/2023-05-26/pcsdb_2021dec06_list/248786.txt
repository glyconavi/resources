RES
1n:n1
2b:a-lara-PEN-1:5
3b:b-dglc-HEX-1:5
4b:b-dxyl-PEN-1:5
LIN
1:1n(3+1)2d
2:1n(28+1)3d
3:3o(6+1)4d
NON
NON1
SmallMolecule:SMILES CC1(C){3}[C@@H](O)CC[C@]2(C)[C@@]3([H])C=CC4=C[C@]({28}C(O)=O)(CC[C@H](C(C)=O)C)CC[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:α-ilexanolic acid