RES
1b:a-dglc-HEX-1:5
2n:n1
3b:b-dara-HEX-2:5|1:aldi|2:keto
4n:n2
LIN
1:1o(6+9)2n
2:1o(1+2)3d
3:3o(3+9)4n
NON
NON1
Parent:1
Linkage:o(6+9)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3C](O[10CH3])[2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:ferulic acid
NON2
Parent:3
Linkage:o(3+9)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3C](O[10CH3])[2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:ferulic acid