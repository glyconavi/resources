RES
1b:b-dglc-HEX-1:5
2b:a-lman-HEX-1:5|6:d
3n:n1
4b:a-lman-HEX-1:5|6:d
5b:a-lman-HEX-1:5|6:d
LIN
1:1o(2+1)2d
2:1o(1+3)3n
3:1o(4+1)4d
4:4o(4+1)5d
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C[C@@H]1CC[C@@]2([C@H]({17}[C@]3([C@@H](O2)C[C@@H]4[C@@]3(CC[C@H]5[C@H]4CC=C6[C@@]5(CC{3}[C@@H](C6)O)C)C)O)C)OC1
Description:pennogenin