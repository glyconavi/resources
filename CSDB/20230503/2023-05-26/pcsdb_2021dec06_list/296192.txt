RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4n:n2
5b:b-dglc-HEX-1:5
LIN
1:1n(5+1)2d
2:1n(3+1)3d
3:3o(6+9)4n
4:4n(3+1)5d
NON
NON1
SmallMolecule:SMILES O{3}C1=C(C2=CC={54}C(O)C=C2)[O+]=C3C={7}C(O)C={5}C(O)C3=C1
Description:pelargonidin
NON2
Parent:3
Linkage:o(6+9)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3C](O)[2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:caffeic acid