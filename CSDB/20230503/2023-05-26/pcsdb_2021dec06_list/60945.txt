RES
1b:x-dglc-HEX-1:5
2s:amino
3n:n1
LIN
1:1d(1+1)2n
2:1o(1+9)3n
NON
NON1
Parent:1
Linkage:o(1+9)n
SmallMolecule:SMILES O{13}C/C(C)=C/CNC1=C2{7}NC={9}[N+]([H])C2=NC=N1
Description:trans-zeatin (9-protonated)