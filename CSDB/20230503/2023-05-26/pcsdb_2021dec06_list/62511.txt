RES
1r:r1
REP
REP1:13o(4+1)2d=-1--1
RES
2b:b-dgal-HEX-1:5
3b:b-dgal-HEX-1:5
4b:b-dgal-HEX-1:5
5b:b-dgal-HEX-1:5
6b:a-dara-PEN-1:4
7b:b-dgal-HEX-1:5
8b:b-dgal-HEX-1:5
9b:a-dara-PEN-1:4
10b:b-dara-PEN-1:4
11b:b-dgal-HEX-1:5
12b:b-dara-PEN-1:4
13b:b-dgal-HEX-1:5
14b:b-dara-PEN-1:4
15b:a-dara-PEN-1:4
16b:a-dara-PEN-1:4
17b:b-dara-PEN-1:4
18b:a-dara-PEN-1:4
LIN
1:2o(4+1)3d
2:3o(3+1)4d
3:3o(4+1)5d
4:4o(3+1)6d
5:5o(3+1)7d
6:5o(4+1)8d
7:7o(3+1)9d
8:8o(3+1)10d
9:8o(4+1)11d
10:11o(3+1)12d
11:11o(4+1)13d
12:10o(3+1)14d
13:14o(3+1)15d
14:13o(3+1)16d
15:12o(5+1)17d
16:17o(5+1)18d