RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4b:b-dxyl-PEN-1:5
5b:b-dglc-HEX-1:5
LIN
1:1n(31+1)2d
2:1n(3+1)3d
3:3o(4+1)4d
4:4o(4+1)5d
NON
NON1
SmallMolecule:SMILES C[C@@]1(CC[C@]2({30}C(O)=O)CC[C@]3(C([C@@H]2C1)=CC[C@@H]4[C@]5(C{2}[C@H]({3}[C@H]([C@]([C@@H]5CC[C@@]34C)({23}O)C)O)O)C)C){28}C(OC)=O
Description:phytolaccagenin