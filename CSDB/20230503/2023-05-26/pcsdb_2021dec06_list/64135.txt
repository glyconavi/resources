RES
1n:n1
2b:a-dgal-HEX-1:5
3b:b-dglc-HEX-1:5
4n:n2
LIN
1:1n(6+1)2d
2:1n(1+1)3d
3:1n(8+9)4n
NON
NON1
SmallMolecule:SMILES O{1}[C@H]1[C@@]2([H]){5}[C@@]({6}[C@H](O)C{8}[C@]2(C)O)(O)C=CO1
Description:harpagide aglycon
NON2
Parent:1
Linkage:n(8+9)n
SmallMolecule:SMILES [6CH]1=[5CH][4CH]=[3CH][2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:cinnamic acid