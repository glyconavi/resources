RES
1b:b-dglc-HEX-1:5
2b:b-dglc-HEX-1:5
3n:n1
LIN
1:1o(6+1)2d
2:1o(1+1)3n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES O=C1C2=CO{1}[C@@H](O)[C@H](C=C)[C@]2([H])CCO1
Description:sweroside aglycon