RES
1n:n1
2s:methyl
3b:b-dglc-HEX-1:5
4b:b-dall-HEX-1:5
LIN
1:1n(7+1)2n
2:1n(9+1)3d
3:3o(6+1)4d
NON
NON1
SmallMolecule:SMILES C[C@@H]1Cc2cc3c{7}c(O)c{9}c(O)c3{10}c(O)c2C(=O)O1
Description:7,9,10-trixydroxy-3R-methyl-1H-3,4-dihydronaphtho-[2,3c]-pyran-1-one