RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+6)2n
NON
NON1
Parent:1
Linkage:o(1+6)n
SmallMolecule:SMILES C[C@H](CC{24}[C@@H](O){25}C(C)(C)O)C4{16}[C@@H](O)C[C@@]5(C)C2C{6}[C@H](O)C1[C@@](C)(C){3}[C@@H](O)CC[C@@]13C[C@@]23CC[C@]45C
Description:3β,6α,16β,24(S),25-pentahydroxycycloartane