RES
1n:n1
2b:b-dglc-HEX-1:5
3b:a-lara-PEN-1:5
4b:a-lman-HEX-1:5|6:d
5b:b-dxyl-PEN-1:5
6b:b-dgro-TET-1:4
7s:hydroxymethyl
LIN
1:1n(3+1)2d
2:1n(24+1)3d
3:3o(2+1)4d
4:4o(4+1)5d
5:5o(3+1)6d
6:6h(3+1)7n
NON
NON1
SmallMolecule:SMILES [H][C@]12[C@@]({23}C(O)=O)({24}CO){3}[C@@H](O){2}[C@@H](O)C[C@]1(C)[C@@]3([H])CC=C4[C@]5([H])CC(C)(C)CC[C@@](C(O)=O)5{16}[C@H](O)C[C@](C)4[C@@](C)3CC2
Description:olean-12-en-2β,3β,16α,24-tetrol-23,28-dioic acid