RES
1n:n1
2b:b-dglc-HEX-1:5|6:d
3n:n2
4b:b-dglc-HEX-1:5
5b:b-dglc-HEX-1:5
6b:b-dgal-HEX-1:5|6:d
7b:a-lman-HEX-1:5|6:d
8b:b-dxyl-PEN-1:5
9b:a-lara-PEN-1:4
10b:b-dglc-HEX-1:5
LIN
1:1n(6+1)2d
2:1n(1+21)3n
3:3n(3+1)4d
4:3n(28+1)5d
5:4o(6+1)6d
6:5o(2+1)7d
7:6o(2+1)8d
8:7o(4+1)9d
9:7o(3+1)10d
NON
NON1
SmallMolecule:SMILES C=C{6}[C@@](C)(O)CC/C=C({9}CO)/{1}C(O)=O
Description:6S-hydroxy-2-hydroxymethyl-6-methyl-2E,7-octadienoic acid
NON2
Parent:1
Linkage:n(1+21)n
SmallMolecule:SMILES C[C@]12CC{3}[C@H](O)C(C)(C)[C@@H]1CC[C@]3(C)[C@@H]2CC=C4[C@@]3(C)C{16}[C@@H](O)[C@]5({28}C(O)=O)[C@H]4CC(C)(C){21}[C@@H](O)C5
Description:acacic acid