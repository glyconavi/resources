RES
1b:b-dglc-HEX-1:5|6:a
2n:n1
3b:b-dgal-HEX-1:5
4b:a-lman-HEX-1:5|6:d
LIN
1:1o(1+3)2n
2:1o(2+1)3d
3:3o(2+1)4d
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C[C@]12CC[3C@@H]([C@]([C@@H]1CC[C@@]3([C@@H]2CC=C4[C@]3(CC[C@@]5([C@H]4CC([21C@H]([22C@H]5O)O)(C)C)C)C)C)(C)[24CH2]O)O
Description:soyasapogenol A