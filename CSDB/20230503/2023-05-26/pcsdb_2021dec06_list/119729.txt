RES
1n:n1
2b:b-dglc-HEX-1:5|6:a
3b:b-dxyl-PEN-1:5
4n:n2
LIN
1:1n(3+1)2d
2:1n(28+1)3d
3:3o(2+9)4n
NON
NON1
SmallMolecule:SMILES CC1(C)[3C@@H](O)CC[C@]2(C)[C@@]3([H])CC=C4[C@]5([H])CC(C)(C)CC[C@@]([28C](O)=O)5[16C@H](O)C[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:echinocystic acid
NON2
Parent:3
Linkage:o(2+9)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3CH][2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:p-coumaric acid