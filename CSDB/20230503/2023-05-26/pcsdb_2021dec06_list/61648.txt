RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+3)2n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C{9}[C@@]1(O)CC/C=C\C2C1{1}[C@@H](O)C{3}[C@]2(C)O
Description:dictamnoside E aglycon