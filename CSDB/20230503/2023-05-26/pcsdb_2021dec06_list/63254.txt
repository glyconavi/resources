RES
1n:n1
2b:b-dglc-HEX-1:5|6:a
3b:b-dgal-HEX-1:5|6:d
4b:b-dgal-HEX-1:5
5b:b-dglc-HEX-1:5
6s:acetyl
7b:a-lman-HEX-1:5|6:d
8b:b-dxyl-PEN-1:5
9b:a-lara-PEN-1:5
10b:a-lara-PEN-1:5
LIN
1:1n(3+1)2d
2:1n(28+1)3d
3:2o(2+1)4d
4:3o(3+1)5d
5:3o(4+1)6n
6:3o(2+1)7d
7:7o(4+1)8d
8:8o(3+1)9d
9:9o(2+1)10d
NON
NON1
SmallMolecule:SMILES C[C@@]1(C=O){3}[C@@H](O)CC[C@]2(C)[C@@]3([H])CC=C4[C@@]5([H])CC(C)(C)CC[C@@]({28}C(O)=O)5{16}[C@H](O)C[C@](C)4[C@@](C)3CCC12
Description:quillaic acid