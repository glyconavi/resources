RES
1n:n1
2b:b-dglc-HEX-1:5
3s:methyl
4s:methyl
LIN
1:1n(7+1)2d
2:1n(54+1)3n
3:1n(6+1)4n
NON
NON1
SmallMolecule:SMILES O=C1C(C2=CC={54}C(C=C2)O)=COC3=C1{5}C(O)={6}C(O){7}C(O)=C3
Description:6-hydroxy-genistein