RES
1b:b-dglc-HEX-1:5
2n:n1
3n:n2
4n:n3
LIN
1:1o(3+7)2n
2:1o(6+57)2n
3:1o(1+7)3n
4:1o(4+5)4n
NON
NON1
Parent:1
Linkage:o(3+7)n|o(6+57)n
SmallMolecule:SMILES O{3}C1={4}C(O){5}C(O)=C(C2={55}C(O){54}C(O)={53}C(O)C=C2{57}C(O)=O)C({7}C(O)=O)=C1
Description:hexahydroxydiphenic acid
NON2
Parent:1
Linkage:o(1+7)n
SmallMolecule:SMILES [6CH]1=[5C](O)[4C](O)=[3C](O)[2CH]=[1C]1[7C](=O)O
Description:3,4,5-trihydroxybenzoic acid
NON3
Parent:1
Linkage:o(4+5)n
SmallMolecule:SMILES O={5}C(O)/C=C(\{4}C(=O)O)[C@H]1c2c(c{53}c(O){54}c(O){55}c2O)C(=O)O[C@@H]1{1}C(=O)O
Description:repandusinic acid A aglycon