RES
1b:x-dglc-HEX-1:5
2s:amino
3n:n1
LIN
1:1d(1+1)2n
2:1o(1+7)3n
NON
NON1
Parent:1
Linkage:o(1+7)n
SmallMolecule:SMILES C/C(C)=C/CNC1=C2C(N=C{7}N2)=NC=N1
Description:N6-isopent-2-enyladenine