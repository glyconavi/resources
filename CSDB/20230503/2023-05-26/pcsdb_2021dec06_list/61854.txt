RES
1n:n1
2s:methyl
3n:n2
4n:n3
5b:b-dglc-HEX-1:5
6b:b-dglc-HEX-1:5
LIN
1:1n(11+1)2n
2:1n(1+7)3n
3:1n(7+7)4n
4:3n(1+1)5d
5:4n(1+1)6d
NON
NON1
SmallMolecule:SMILES O={11}C(C1=CO{1}[C@@H](O)[C@@]2([H])[C@]1([H])C{7}[C@H](O)[C@@H]2C)O
Description:loganic acid aglycon
NON2
Parent:1
Linkage:n(1+7)n
SmallMolecule:SMILES C=CC1{1}[C@H](O)O/C=C(C(=O)OC)\C1C{7}C(=O)O
Description:oleuropein aglycon core isomer
NON3
Parent:1
Linkage:n(7+7)n
SmallMolecule:SMILES C=CC1{1}[C@H](O)O/C=C(C(=O)OC)\C1C{7}C(=O)O
Description:oleuropein aglycon core isomer