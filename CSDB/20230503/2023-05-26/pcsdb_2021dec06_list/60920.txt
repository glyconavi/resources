RES
1n:n1
2b:a-lman-HEX-1:5|6:d
3b:b-dglc-HEX-1:5
4b:x-lman-HEX-1:5|6:d
LIN
1:1n(7+1)2d
2:1n(3+1)3d
3:3o(2+1)4d
NON
NON1
SmallMolecule:SMILES O{3}C1=C(C2=CC={54}C(O)C(OC)=C2)OC3=C({5}C(O)=C{7}C(O)=C3)C1=O
Description:isorhamnetin