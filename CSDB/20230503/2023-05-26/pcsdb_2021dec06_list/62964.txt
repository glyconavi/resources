RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+9)2n
NON
NON1
Parent:1
Linkage:o(1+9)n
SmallMolecule:SMILES C{9}[C@@H](O)/C=C/[C@H]1/C({13}CO)=C\C(=O)CC1(C)C
Description:apocynol A