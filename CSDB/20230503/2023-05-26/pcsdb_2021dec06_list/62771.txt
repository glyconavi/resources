RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+7)2n
NON
NON1
Parent:1
Linkage:o(1+7)n
SmallMolecule:SMILES C/C2=C/{7}[C@]1(O)C[C@@H](C)C(=O)[C@@H]1{4}[C@](C)(O)C23CC3
Description:ptaquilosin