RES
1n:n1
2b:o-xgro-TRI-0:0|1:aldi
3b:a-lara-PEN-1:5
4b:b-dglc-HEX-1:5
5b:a-dglc-HEX-1:5|6:a
6b:b-dglc-HEX-1:5
7b:a-lman-HEX-1:5|6:d
8s:methyl
LIN
1:1n(31+1)2d
2:1n(3+1)3d
3:3o(2+1)4d
4:2o(3+1)5d
5:3o(4+1)6d
6:6o(2+1)7d
7:5o(6+1)8n
NON
NON1
SmallMolecule:SMILES CC1(C){3}[C@@H](O)CC[C@]2(C)[C@@]3([H])CC=C4[C@]5([H])C[C@]({30}C(O)=O)(C)CC[C@@]({28}CO)5{16}[C@H](O)C[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:olean-12-en-3β,16α,28-triol-30-oic acid