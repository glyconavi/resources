RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dgal-HEX-1:5|6:d
4b:a-lman-HEX-1:5|6:d
5n:n2
6b:a-lman-HEX-1:5|6:d
7s:methyl
8b:b-dgro-TET-1:4
9b:b-dxyl-PEN-1:5
10b:b-dgal-HEX-1:5
11s:hydroxymethyl
LIN
1:1n(3+1)2d
2:1n(28+1)3d
3:3o(3+1)4d
4:3o(4+9)5n
5:3o(2+1)6d
6:5n(4+1)7n
7:6o(3+1)8d
8:6o(4+1)9d
9:9o(4+1)10d
10:8h(3+1)11n
NON
NON1
SmallMolecule:SMILES C[C@@]12CC[C@@H]3[C@@](C{2}[C@H](O){3}[C@H](O)[C@]3({23}C(O)=O)C)(C)[C@H]1CC=C4[C@@]2({27}CO)CC[C@]5({28}C(O)=O)[C@H]4CC(C)(C)CC5
Description:presenegenin
NON2
Parent:3
Linkage:o(4+9)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3CH][2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:p-coumaric acid