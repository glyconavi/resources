RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+31)2n
NON
NON1
Parent:1
Linkage:o(1+31)n
SmallMolecule:SMILES O{3}[C@H](C(C)(C1CC[C@@]([C@@]2(CC[C@]3(CC[C@]({29}C(O)=O)(C[C@]3(C2=C4)[H])C)C)C)5C)C)CC[C@]1(C)[C@@]5([H])C4=O
Description:18β-glycyrrhetic acid