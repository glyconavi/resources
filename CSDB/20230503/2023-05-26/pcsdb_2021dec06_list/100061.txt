RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
LIN
1:1n(3+1)2d
2:1n(55+1)3d
NON
NON1
SmallMolecule:SMILES O{3}C1=C(C2=CC(OC)={54}C(O){55}C(O)=C2)OC3=C({5}C(O)=C{7}C(O)=C3)C1=O
Description:laricytrin