RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4b:b-dglc-HEX-1:5
LIN
1:1n(21+1)2d
2:1n(6+1)3d
3:3o(2+1)4d
NON
NON1
SmallMolecule:SMILES O[12C@H]1[C@@]([C@]([20C@@](CC/C=C(C)\C)(C)O)([H])CC2)([H])[C@]2(C)[C@@](C3)(C)[C@@](C1)([H])[C@](CC4)(C)[C@@](C(C)(C)[3C@H]4O)([H])[6C@H]3O
Description:20S-protopanaxatriol