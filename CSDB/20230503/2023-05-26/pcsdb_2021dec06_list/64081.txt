RES
1b:a-lara-PEN-1:5
2n:n1
3b:a-lman-HEX-1:5|6:d
4s:acetyl
LIN
1:1o(1+1)2n
2:1o(2+1)3d
3:3o(4+1)4n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES [H][C@]1(O[C@](OCC2=C)({23}[C@@H](O)C2)[C@H]3C)C[C@@]4([H])[C@]5([H])CC=C6C{3}[C@@H](O)C{1}[C@@H](O)[C@]6(C)[C@@]5([H])CC[C@]4(C)[C@]13[H]
Description:spirosta-5,25(27)-diene-1β,3β,23S-triol