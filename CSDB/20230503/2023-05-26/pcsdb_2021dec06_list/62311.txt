RES
1b:b-dxyl-PEN-1:5
2b:a-lara-PEN-1:5
3n:n1
LIN
1:1o(2+1)2d
2:1o(1+3)3n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES CC1(C){3}[C@@H](O)CC[C@]2(C)[C@@]3([H])CC=C4[C@]5([H]){19}[C@](C)(O)[C@H](C)CC[C@@]({28}C(O)=O)5CC[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:pomolic acid