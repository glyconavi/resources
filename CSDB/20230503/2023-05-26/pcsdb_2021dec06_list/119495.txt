RES
1b:b-dgal-HEX-1:5
2n:n1
3b:b-dglc-HEX-1:5
4b:b-dglc-HEX-1:5
5b:b-dglc-HEX-1:5
LIN
1:1o(1+3)2n
2:1o(4+1)3d
3:3o(2+1)4d
4:3o(3+1)5d
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES [H][C@]1(O[C@@]2(OC[C@H](C)CC2)[C@H]3C)C[C@@]4([H])[C@]5([H])C{6}[C@@H](O)[C@@]6([H])C{3}[C@@H](O)CC[C@]6(C)[C@@]5([H])CC[C@]4(C)[C@]13[H]
Description:25R-5α-spirostan-3β,6β-diol