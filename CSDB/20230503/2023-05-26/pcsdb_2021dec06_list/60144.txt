RES
1n:n1
2b:a-lglc-HEX-1:5|6:d
3n:n2
4n:n3
5b:b-dglc-HEX-1:5
6b:b-dglc-HEX-1:5
7s:n-acetyl
8b:x-xgal-HEX-1:5|6:d
9b:a-lman-HEX-1:5|6:d
10b:x-xxyl-PEN-1:5
11b:a-lara-PEN-1:4
12b:b-dglc-HEX-1:5
LIN
1:1n(6+1)2d
2:1n(1+21)3n
3:2o(4+1)4n
4:3n(28+1)5d
5:3n(3+1)6d
6:6d(2+1)7n
7:6o(6+1)8d
8:5o(2+1)9d
9:8o(2+1)10d
10:9o(4+1)11d
11:9o(3+1)12d
NON
NON1
SmallMolecule:SMILES [8CH2]=[7CH][6C]([8CH3])(O)[5CH2][4CH2]/[3CH]=[2C]([7CH3])/[1C](O)=O
Description:menthiafolic acid
NON2
Parent:1
Linkage:n(1+21)n
SmallMolecule:SMILES C[C@]12CC{3}[C@H](O)C(C)(C)[C@@H]1CC[C@]3(C)[C@@H]2CC=C4[C@@]3(C)C{16}[C@@H](O)[C@]5({28}C(O)=O)[C@H]4CC(C)(C){21}[C@@H](O)C5
Description:acacic acid
NON3
Parent:2
Linkage:o(4+1)n
SmallMolecule:SMILES [8CH2]=[7CH][6C]([8CH3])(O)[5CH2][4CH2]/[3CH]=[2C]([7CH3])/[1C](O)=O
Description:menthiafolic acid