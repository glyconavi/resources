RES
1n:n1
2s:methyl
3b:b-dgal-HEX-1:5|6:d
4b:b-dglc-HEX-1:5
5s:acetyl
LIN
1:1n(11+1)2n
2:1n(3+1)3d
3:3o(3+1)4d
4:4o(2+1)5n
NON
NON1
SmallMolecule:SMILES C[C@@]1({23}CO){3}[C@@H](O)CC[C@]2(C)[C@@]3([H]){11}[C@H](O)C=C4[C@]5([H])CC(C)(C)CC[C@@]({28}CO)5{16}[C@@H](O)C[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:olean-12-en-3β,11α,16β,23,28-pentol