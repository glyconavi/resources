RES
1b:b-dglc-HEX-1:5|6:a
2b:b-dglc-HEX-1:5
3n:n1
LIN
1:1o(2+1)2d
2:1o(1+3)3n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C[C@]1({24}CO){3}[C@@H](O)CC[C@]2(C)[C@@]3([H])CC=C4[C@]5([H])C[C@](C)({29}CO){21}[C@@H](O){22}[C@@H](O)[C@@](C)5CC[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:kudzusapogenol A