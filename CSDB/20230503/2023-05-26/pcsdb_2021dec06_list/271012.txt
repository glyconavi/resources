RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5|6:a
4n:n2
LIN
1:1n(28+1)2d
2:1n(3+1)3d
3:3o(3+3)4n
NON
NON1
SmallMolecule:SMILES [1CH2]1[2CH2][3C@H](O)[4C]([23CH3])([24CH3])[5C@@H]2[6CH2][7CH2][8C@]([26CH3])3[14C@@]([27CH3])4[15CH2][16CH2][17C@]([28C](=O)O)5[22CH2][21CH2][20C]([29CH3])([30CH3])[19CH2][18C@H]5/[13C]4=[12CH]/[11CH2][9C@@H]3[10C@]([25CH3])21
Description:oleanolic/oleanic acid
NON2
Parent:3
Linkage:o(3+3)n
SmallMolecule:SMILES O=C(O)C(O){3}[CH](O)OCC(O)=O
Description:2,3-dihydroxy-3'-carboxymethoxy-propionic acid