RES
1b:b-dglc-HEX-1:5
2b:b-dglc-HEX-1:5|6:a
3n:n1
LIN
1:1o(2+1)2d
2:1o(1+3)3n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES CC1(C)CC[C@]2({28}C(O)=O)CC[C@@]3(C)[C@]4(C)CC[C@@]5([H])[C@](C)({23}CO){3}[C@@H](O)CC[C@]5(C)[C@@]4([H])CC=C3[C@]2([H])C1
Description:hederagenin