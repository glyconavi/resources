RES
1b:b-dglc-HEX-1:5
2b:a-xgal-HEX-x:x|6:d
3s:amino
4s:n-acetyl
5n:n1
6b:b-dglc-HEX-1:5
7s:n-acetyl
8b:b-xman-HEX-1:5
9b:b-xxyl-PEN-1:5
10a:a1
11a:a2
LIN
1:1o(3+1)2d
2:1d(1+1)3n
3:1d(2+1)4n
4:1o(1+4)5n
5:1o(4+1)6d
6:6d(2+1)7n
7:6o(4+1)8d
8:8o(2+1)9d
9:8o(6+1)10n
10:8o(3+1)11n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:12
RES
12b:a-xman-HEX-1:5
13b:b-dglc-HEX-1:5
14s:n-acetyl
LIN
1:12o(2+1)13d
2:13d(2+1)14n
ALTSUBGRAPH2
LEAD-IN RES:15
RES
15b:a-xman-HEX-1:5
ALT2
ALTSUBGRAPH1
LEAD-IN RES:16
RES
16b:a-xman-HEX-1:5
ALTSUBGRAPH2
LEAD-IN RES:17
RES
17b:a-xman-HEX-1:5
18b:b-dglc-HEX-1:5
19s:n-acetyl
LIN
1:17o(2+1)18d
2:18d(2+1)19n
NON
NON1
Parent:1
Linkage:o(1+4)n
SmallMolecule:SMILES [4C](=O)(N)[3CH2][2CH](N)[1C](=O)O
Description:asparagine