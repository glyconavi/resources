RES
1b:b-dlyx-HEX-1:5|2:d|6:d
2s:methyl
3n:n1
4b:b-dglc-HEX-1:5
5b:b-dglc-HEX-1:5
LIN
1:1o(3+1)2n
2:1o(1+3)3n
3:1o(4+1)4d
4:4o(6+1)5d
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O{3}[C@H]1CC[C@]2(C)[C@@]3([H])CC[C@]4(C)[C@@H](C(CO5)=CC5=O)CC[C@@]4([H]){8}[C@]3(O)CC[C@@]([H])2C1
Description:8β-hydroxydigitoxigenin