RES
1n:n1
2b:a-xara-PEN-1:5
3b:b-xglc-HEX-1:5
4b:b-xglc-HEX-1:5
5b:b-xglc-HEX-1:5
6b:a-xman-HEX-1:5|6:d
LIN
1:1n(3+1)2d
2:1n(28+1)3d
3:2o(2+1)4d
4:3o(6+1)5d
5:5o(4+1)6d
NON
NON1
SmallMolecule:SMILES CC(C)([C@](CC[C@@]([C@](CC[C@@](CC1)2{28}C(O)=O)3C)4C)5[H]){3}[C@@H](O)CC[C@]5(C)[C@@]4([H])CC=C3[C@]2([H])CC1=C
Description:30-norolean-12,20(29)-dien-3β-ol-28-oic acid