RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+8)2n
NON
NON1
Parent:1
Linkage:o(1+8)n
SmallMolecule:SMILES O{7}C(C1=CC={4}C(O)C(OC)=C1){8}C(O){9}CO
Description:guaiacylglycerol