RES
1n:n1
2b:b-dglc-HEX-1:5
3b:a-lara-PEN-1:5
4b:a-lman-HEX-1:5|6:d
5b:b-dglc-HEX-1:5
6b:a-lman-HEX-1:5|6:d
LIN
1:1n(28+1)2d
2:1n(3+1)3d
3:3o(2+1)4d
4:3o(3+1)5d
5:2o(2+1)6d
NON
NON1
SmallMolecule:SMILES C[C@]5(C)CC[C@]4({28}C(=O)O)CC[C@]3(C)/C(=C\CC2[C@@]1(C)CC{3}[C@H](O)[C@](C)(C)C1CC[C@]23C)C4{19}[C@@H]5O
Description:siaresinolic acid