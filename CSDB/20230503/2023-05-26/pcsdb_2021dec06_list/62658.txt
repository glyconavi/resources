RES
1b:a-lman-HEX-1:5|6:d
2n:n1
LIN
1:1o(1+2)2n
NON
NON1
Parent:1
Linkage:o(1+2)n
SmallMolecule:SMILES O=c1cc(-c2cc{54}c(O)cc2)oc2c{7}c(O)c(/C={2}C/O){5}c(O)c12
Description:6-trans-2-hydroxyvinyl-5,7,4'-trihydroxyflavone