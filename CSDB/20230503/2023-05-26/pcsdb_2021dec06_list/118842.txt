RES
1b:b-dglc-HEX-1:5
2b:a-lman-HEX-1:5|6:d
3n:n1
LIN
1:1o(2+1)2d
2:1o(1+4)3n
NON
NON1
Parent:1
Linkage:o(1+4)n
SmallMolecule:SMILES C[C@]12C(C)=CCCC1[C@@](C)(CC{11}C(C=C)(O)C)[C@H](C)C{4}[C@@H]2O
Description:α-vinyl-1,2,3,4,4a,7,8,8a-octahydro-α,1,2,4a,5-pentamethyl-1-naphthalenepropanol