RES
1n:n1
2n:n2
3n:n3
4b:b-dglc-HEX-1:5|6:a
5b:b-dgal-HEX-1:5
6b:b-dgal-HEX-1:5
7b:a-lman-HEX-1:5|6:d
8b:b-dxyl-PEN-1:5
LIN
1:1n(21+1)2n
2:1n(22+1)3n
3:1n(3+1)4d
4:4o(2+1)5d
5:4o(3+1)6d
6:6o(2+1)7d
7:7o(2+1)8d
NON
NON1
SmallMolecule:SMILES C[C@@]6(C)CC4[C@@]35CCC2[C@@]1(C)CC{3}[C@H](O)[C@](C)(C)C1CC[C@@]2(C)[C@]3(C)C{16}[C@@H](O)[C@@]4({28}[C@@H](O)O5){22}[C@@H](O){21}[C@@H]6O
Description:maesasaponin core aglycon
NON2
Parent:1
Linkage:n(21+1)n
SmallMolecule:SMILES [4CH3]/[3CH]=[2C]([5CH3])\[1C](=O)O
Description:angelic acid
NON3
Parent:1
Linkage:n(22+1)n
SmallMolecule:SMILES [4CH3]/[3CH]=[2C]([5CH3])\[1C](=O)O
Description:angelic acid