RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+4)2n
NON
NON1
Parent:1
Linkage:o(1+4)n
SmallMolecule:SMILES C[C@H]1C[C@H]2OC(=O)[C@@H](C)[C@H]2CC=C1CC{4}[C@@H](C)O
Description:11α,13-dihydro-4H-xanthalongin