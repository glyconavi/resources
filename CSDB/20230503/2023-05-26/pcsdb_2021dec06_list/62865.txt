RES
1n:n1
2b:b-dglc-HEX-1:5
3b:a-lman-HEX-1:5|6:d
4s:methyl
LIN
1:1n(53+1)2d
2:1n(55+1)3d
3:1n(7+1)4n
NON
NON1
SmallMolecule:SMILES O=c1cc(-c2c{53}c(O){54}c(O){55}c(O)c2)oc2c{7}c(O)c{5}c(O)c12
Description:tricetin