RES
1b:b-dglc-HEX-1:5|6:a
2b:a-lara-PEN-1:4
3b:b-dglc-HEX-1:5
4b:a-lara-PEN-1:5
5s:methyl
6n:n1
LIN
1:1o(4+1)2d
2:1o(2+1)3d
3:1o(3+1)4d
4:1o(6+1)5n
5:1o(1+3)6n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES CC1(C{22}[C@@H]([C@@]2({16}[C@@H]({15}[C@@H]([C@@]3(C([C@H]2C1)=CC[C@@H]4[C@]5(CC{3}[C@@H](C(C)([C@]5([H])CC[C@@]34C)C)O)C)C)O)O){28}CO)O)C
Description:proschiwalligenin PA3