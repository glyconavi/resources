RES
1n:n1
2b:b-dglc-HEX-1:5
3s:methyl
4b:b-dglc-HEX-1:5
5b:a-lman-HEX-1:5|6:d
6b:b-dglc-HEX-1:5
LIN
1:1n(26+1)2d
2:1n(22+1)3n
3:1n(3+1)4d
4:4o(2+1)5d
5:4o(3+1)6d
NON
NON1
SmallMolecule:SMILES C[C@H]1[C@H]2[C@H](C[C@@H]3[C@@]2(CC[C@H]4[C@H]3CC=C5[C@@]4(CC{3}[C@@H](C5)O)C)C)O{22}C1(CC[C@H](C){26}[CH2]O)O
Description:25S-furost-5-en-3β,22,26-triol