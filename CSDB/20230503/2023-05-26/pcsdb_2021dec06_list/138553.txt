RES
1b:a-lman-HEX-1:5|6:d
2b:a-lman-HEX-1:5|6:d
3b:a-lgal-HEX-1:5|6:d
4n:n1
LIN
1:1o(3+1)2d
2:1o(2+1)3d
3:1o(1+13)4n
NON
NON1
Parent:1
Linkage:o(1+13)n
SmallMolecule:SMILES CC1=CCC[C@@]2([H])[C@](C)(CC{13}[C@@](C=C)(O)C)[C@@H](C)C{6}[C@H](O)[C@]12C
Description:cleroda-3,14-dien-6S,13S-diol