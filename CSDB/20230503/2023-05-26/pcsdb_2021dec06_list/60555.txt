RES
1b:x-lara-PEN-1:4
2n:n1
LIN
1:1o(5+9)2n
NON
NON1
Parent:1
Linkage:o(5+9)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3C](O[10CH3])[2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:ferulic acid