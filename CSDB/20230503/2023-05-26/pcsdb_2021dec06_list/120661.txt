RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+5)2n
NON
NON1
Parent:1
Linkage:o(1+5)n
SmallMolecule:SMILES O=C1O{5}[C@@H](O)C(OC)=C1
Description:4-methoxyfuran-2(5H)-one-5β-ol