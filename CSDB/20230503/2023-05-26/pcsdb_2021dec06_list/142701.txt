RES
1n:n1
2s:methyl
3b:b-dglc-HEX-1:5
4b:a-lman-HEX-1:5|6:d
5b:a-lman-HEX-1:5|6:d
LIN
1:1n(22+1)2n
2:1n(3+1)3d
3:3o(2+1)4d
4:3o(4+1)5d
NON
NON1
SmallMolecule:SMILES C[C@H]1[C@@H]2[C@H](C[C@]3[C@@]2(CC[C@H]4[C@H]3CC=C5[C@@]4(CC{3}[C@@H](C5)O)C)C)O{22}[C@@]1(CC[C@H](C){26}[CH2]O)O
Description:25S-furost-5-en-3β,22α,26-triol