RES
1b:x-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+7)2n
NON
NON1
Parent:1
Linkage:o(1+7)n
SmallMolecule:SMILES O=C1C2=CC(OC)={7}C(O)C=C2OC=C1C3=CC={54}C(O)C=C3
Description:glycitein