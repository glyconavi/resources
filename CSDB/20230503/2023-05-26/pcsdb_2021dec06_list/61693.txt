RES
1b:b-dglc-HEX-1:5
2b:b-dxyl-PEN-1:5
3n:n1
LIN
1:1o(6+1)2d
2:1o(1+3)3n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O=C(C1=C{3}C(O)=C({15}CO){1}C(O)=C12)C3=CC=CC=C3C2=O
Description:lucidin