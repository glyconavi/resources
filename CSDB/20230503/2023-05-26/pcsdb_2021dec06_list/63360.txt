RES
1b:a-lara-PEN-1:5
2b:b-dglc-HEX-1:5
3n:n1
4b:a-lman-HEX-1:5|6:d
5b:b-dglc-HEX-1:5
6b:b-dglc-HEX-1:5
LIN
1:1o(4+1)2d
2:1o(1+3)3n
3:1o(2+1)4d
4:4o(3+1)5d
5:5o(4+1)6d
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES [1CH2]1[2CH2][3C@H](O)[4C]([23CH3])([24CH3])[5C@@H]2[6CH2][7CH2][8C@]([26CH3])3[14C@@]([27CH3])4[15CH2][16CH2][17C@]([28C](=O)O)5[22CH2][21CH2][20C]([29CH3])([30CH3])[19CH2][18C@H]5/[13C]4=[12CH]/[11CH2][9C@@H]3[10C@]([25CH3])21
Description:oleanolic/oleanic acid