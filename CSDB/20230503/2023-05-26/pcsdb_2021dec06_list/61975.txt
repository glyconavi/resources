RES
1b:b-dglc-HEX-1:5
2a:a1
LIN
1:1o(-1+-1)2n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:3
RES
3n:n1
ALTSUBGRAPH2
LEAD-IN RES:4
RES
4n:n2
ALTSUBGRAPH3
LEAD-IN RES:5
RES
5n:n3
NON
NON1
SmallMolecule:SMILES N#C{1}[C@@]1(O)/C=C\CC1
Description:tetraphyllin A aglycon
NON2
SmallMolecule:SMILES N#C{1}[C@]1(O)/C=C\CC1
Description:deidaclin aglycon
NON3
SmallMolecule:SMILES C{2}C(O)(C#N)C
Description:2-hydroxy-methyl-propiononitrile