RES
1b:a-lman-HEX-1:5|6:d
2n:n1
LIN
1:1o(1+3)2n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES COc1{53}c(O)cc(-c2oc3c{7}c(O)c{5}c(O)c3c(=O){3}c2O)c{55}c1O
Description:mearnsetin