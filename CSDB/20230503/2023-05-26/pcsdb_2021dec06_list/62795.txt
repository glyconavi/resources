RES
1n:n1
2b:a-lman-HEX-1:5|6:d
3n:n2
LIN
1:1n(3+1)2d
2:1n(55+7)3n
NON
NON1
SmallMolecule:SMILES Cc1c(-c2oc3c{5}c(O)c{7}c(O)c3c(=O){3}c2O)c{55}c(O){54}c(O){53}c1O
Description:2'-C-methylmyricetin
NON2
Parent:1
Linkage:n(55+7)n
SmallMolecule:SMILES [6CH]1=[5C](O)[4C](O)=[3C](O)[2CH]=[1C]1[7C](=O)O
Description:3,4,5-trihydroxybenzoic acid