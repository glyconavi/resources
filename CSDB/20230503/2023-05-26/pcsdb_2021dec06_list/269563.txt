RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+3)2n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C[C@]1(C2=CC=C3[C@@]1(C)CC[C@@]4([C@]3(C)CC{3}[C@@H](C4(C)C)O)[H])C{16}[C@@H]([C@]5({28}CO)[C@@]2([H])CC(C)(CC5)C)O
Description:saikogenin B