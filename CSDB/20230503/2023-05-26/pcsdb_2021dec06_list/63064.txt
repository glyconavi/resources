RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+4)2n
NON
NON1
Parent:1
Linkage:o(1+4)n
SmallMolecule:SMILES COc1cc([C@H]2OC[C@@H]3[C@H](c4cc{4}c(O)c(OC)c4)OC[C@H]23)cc{54}c1O
Description:(+)-epipinoresinol