RES
1n:n1
2b:b-dglc-HEX-1:5
3s:methyl
4n:n2
5b:b-dglc-HEX-1:5
LIN
1:1n(1+1)2d
2:1n(11+1)3n
3:1n(7+7)4n
4:4n(1+1)5d
NON
NON1
SmallMolecule:SMILES O={11}C(C1=CO{1}[C@@H](O)[C@@]2([H])[C@]1([H])C{7}[C@H](O)[C@@H]2C)O
Description:loganic acid aglycon
NON2
Parent:1
Linkage:n(7+7)n
SmallMolecule:SMILES C=CC1{1}[C@H](O)O/C=C(C(=O)OC)\C1C{7}C(=O)O
Description:oleuropein aglycon core isomer