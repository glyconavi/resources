RES
1n:n1
2s:methyl
3b:b-dglc-HEX-1:5
4b:a-lman-HEX-1:5|6:d
5n:n2
LIN
1:1n(4+1)2n
2:1n(8+1)3d
3:3o(3+1)4d
4:3o(4+9)5n
NON
NON1
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3C](O)[2CH]=[1C]1[7CH2][8CH2]O
Description:3,4,8-trihydroxy-ethylbenzene
NON2
Parent:3
Linkage:o(4+9)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3C](O[10CH3])[2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:ferulic acid