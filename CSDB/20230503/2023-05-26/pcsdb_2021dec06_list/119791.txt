RES
1n:n1
2b:b-dxyl-PEN-1:5
3b:b-dglc-HEX-1:5|6:a
4s:methyl
LIN
1:1n(28+1)2d
2:1n(3+1)3d
3:3o(6+1)4n
NON
NON1
SmallMolecule:SMILES [1CH2]1[2CH2][3C@H](O)[4C]([23CH3])([24CH3])[5C@@H]2[6CH2][7CH2][8C@]([26CH3])3[14C@@]([27CH3])4[15CH2][16CH2][17C@]([28C](=O)O)5[22CH2][21CH2][20C]([29CH3])([30CH3])[19CH2][18C@H]5/[13C]4=[12CH]/[11CH2][9C@@H]3[10C@]([25CH3])21
Description:oleanolic/oleanic acid