RES
1n:n1
2b:b-dglc-HEX-1:5
3s:methyl
4s:methyl
5s:methyl
LIN
1:1n(2+1)2d
2:1n(3+1)3n
3:1n(4+1)4n
4:1n(6+1)5n
NON
NON1
SmallMolecule:SMILES O{2}C1={3}C(O){4}C(O)=C(C2={5}C(O){6}C(O)=CC=C2CC3)C3=C1
Description:2,3,4,5,6-pentahydroxy-9,10-dihydrophenanthrene