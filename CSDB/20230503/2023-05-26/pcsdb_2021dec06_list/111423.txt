RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+1)2n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES O{1}C1=CC(CC=C)=CC2=C1OCO2
Description:2,3-methylenedioxy-5-allylphenol