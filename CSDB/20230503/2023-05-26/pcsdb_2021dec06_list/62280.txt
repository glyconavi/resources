RES
1b:b-dglc-HEX-1:5
2b:a-lara-PEN-1:4
3n:n1
LIN
1:1o(6+1)2d
2:1o(1+1)3n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES COC1=CC(CC=C)=CC={1}C1O
Description:eugenol