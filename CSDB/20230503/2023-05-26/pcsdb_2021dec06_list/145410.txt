RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4b:a-lman-HEX-1:5|6:d
5b:a-lman-HEX-1:5|6:d
LIN
1:1n(26+1)2d
2:1n(3+1)3d
3:3o(2+1)4d
4:3o(4+1)5d
NON
NON1
SmallMolecule:SMILES C[C@H]1[C@H]2[C@@H](O[C@]13CC[C@@](C)({26}CO)O3)C[C@@H]4[C@]2(C)CC[C@H]5[C@H]4CC=C6[C@]5(C)CC{3}[C@H](O)C6
Description:nuatigenin