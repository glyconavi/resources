RES
1n:n1
2s:methyl
3s:methyl
4s:methyl
5s:methyl
6s:methyl
7b:b-dglc-HEX-1:5
8a:a1
LIN
1:1n(5+1)2n
2:1n(7+1)3n
3:1n(4+1)4n
4:1n(3+1)5n
5:1n(2+1)6n
6:1n(1+1)7d
7:7o(6+1)8n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:9
RES
9b:b-dglc-HEX-1:5
ALTSUBGRAPH2
LEAD-IN RES:10
RES
10b:b-dxyl-PEN-1:5
NON
NON1
SmallMolecule:SMILES O=c2c1c{7}c(O)c{5}c(O)c1oc3{4}c(O){3}c(O){2}c(O){1}c(O)c23
Description:1,2,3,4,5,7-hexahydroxyxanthone