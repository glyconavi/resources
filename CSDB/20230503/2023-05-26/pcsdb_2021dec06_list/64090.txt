RES
1n:n1
2b:b-dxyl-PEN-1:5
3b:b-dglc-HEX-1:5
LIN
1:1n(3+1)2d
2:1n(16+1)3d
NON
NON1
SmallMolecule:SMILES C[C@H](CC{24}[C@@H](O){25}C(C)(C)O)C4{16}[C@@H](O)C[C@@]5(C)C2C{6}[C@H](O)C1[C@@](C)(C){3}[C@@H](O)CC[C@@]13C[C@@]23CC[C@]45C
Description:3β,6α,16β,24(S),25-pentahydroxycycloartane