RES
1n:n1
2n:n2
3b:b-dglc-HEX-1:5
4b:a-lman-HEX-1:5|6:d
5b:a-lara-PEN-1:5
LIN
1:1n(27+1)2n
2:1n(3+1)3d
3:3o(2+1)4d
4:3o(3+1)5d
NON
NON1
SmallMolecule:SMILES [H][C@]1(O[C@@]2(OC[C@@H]({27}CO)CC2)[C@H]3C)C[C@@]4([H])[C@]5([H])CC=C6C{3}[C@@H](O)CC[C@]6(C)[C@@]5([H])CC[C@]4(C)[C@]13[H]
Description:narthogenin
NON2
Parent:1
Linkage:n(27+1)n
SmallMolecule:SMILES [5C](=O)(O)[4CH2][3C]([6CH3])(O)[2CH2][1C](=O)(O)
Description:3-hydroxy-3-methylglutaric acid