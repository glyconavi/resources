RES
1b:b-dglc-HEX-1:5
2b:a-lman-HEX-1:5|6:d
3n:n1
LIN
1:1o(2+1)2d
2:1o(1+7)3n
NON
NON1
Parent:1
Linkage:o(1+7)n
SmallMolecule:SMILES O=C(C1=C(O/2)C={7}C(O)C={5}C1O)CC2=C3C={54}C(C(OC)=CC/3)O
Description:hesperetin