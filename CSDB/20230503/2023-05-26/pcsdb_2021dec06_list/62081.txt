RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+2)2n
NON
NON1
Parent:1
Linkage:o(1+2)n
SmallMolecule:SMILES CC1(C)OC2(C)CCC1(O)C{2}[C@H]2O
Description:(1R,2R,4R)-2,4-dihydroxy-1,8-cineole