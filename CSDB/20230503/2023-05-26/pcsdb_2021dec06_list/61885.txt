RES
1b:a-lara-PEN-1:5
2a:a1
LIN
1:1o(1+6)2n
ALT
ALT1
ALTSUBGRAPH1
LEAD-IN RES:3
RES
3n:n1
ALTSUBGRAPH2
LEAD-IN RES:4
RES
4n:n2
ALTSUBGRAPH3
LEAD-IN RES:5
RES
5n:n3
NON
NON1
SmallMolecule:SMILES C/C(=C\{15}CO)CC/C1=C(C=O)/C{6}[C@H](O)C2[C@@](C)({16}CO)CCC[C@]12C
Description:gaudichaudioside A aglycone
NON2
SmallMolecule:SMILES C/C(=C\{15}CO)CC/C1=C(CO)/C{6}[C@H](O)C2[C@@](C)({16}CO)CCC[C@]12C
Description:gaudichaudioside B aglycone
NON3
SmallMolecule:SMILES C/C(=C\{15}CO)CC/C1=C(CO)/C{6}[C@H](O)C2[C@@](C)({16}CO)C[C@H](O)C[C@]12C
Description:gaudichaudioside C aglycone