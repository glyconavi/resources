RES
1b:x-dgal-HEX-x:x
2n:n1
3b:x-dglc-HEX-x:x
4b:x-dglc-HEX-x:x
5b:x-dglc-HEX-x:x
LIN
1:1o(1+3)2n
2:1o(4+1)3d
3:3o(2+1)4d
4:3o(3+1)5d
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C[C@@]12[C@@]([H])(C[C@@]3([H])[C@]2([H])[C@H](C)[C@]4([H])N3C[C@@H](C)CC4)[C@@](CC[C@]5([H])[C@@]6(CC{3}[C@H](O)C5)C)([H])[C@]6([H])CC1
Description:demissidine