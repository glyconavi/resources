RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4b:a-lman-HEX-1:5|6:d
5b:b-dglc-HEX-1:5
LIN
1:1n(26+1)2d
2:1n(3+1)3d
3:3o(2+1)4d
4:3o(4+1)5d
NON
NON1
SmallMolecule:SMILES C[C@H]1[C@H]2[C@@H](O{22}C1(O)CC[C@H]({26}[CH2]O)C)C[C@@H]3[C@]2(C)CC[C@H]4[C@H]3CC[C@@H]5[C@]4(C)CC{3}[C@H](O)C5
Description:25R-5α-furostan-3β,22,26-triol