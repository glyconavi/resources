RES
1r:r1
REP
REP1:8o(4+5)2n=-1--1
RES
2n:n1
3b:a-lman-HEX-1:5|6:d
4b:b-dxyl-PEN-1:5
5b:a-lara-PEN-1:5
6n:n2
7b:b-dglc-HEX-1:5
8b:a-lara-PEN-1:5
LIN
1:2n(1+4)3d
2:3o(3+1)4d
3:3o(1+2)5d
4:5o(1+28)6n
5:6n(3+1)7d
6:7o(2+1)8d
NON
NON1
SmallMolecule:SMILES [5C](=O)(O)[4CH2][3C]([6CH3])(O)[2CH2][1C](=O)(O)
Description:3-hydroxy-3-methylglutaric acid
NON2
Parent:5
Linkage:o(1+28)n
SmallMolecule:SMILES [1CH2]1[2C@H](O)[3C@H](O)[4C@]([23CH2](O))([24CH3])[5C@@H]2[6CH2][7CH2][8C@]([26CH3])3[14C@@]([27CH3])4[15CH2][16C@@H](O)[17C@]([28C](=O)O)5[22CH2][21CH2][20C]([29CH3])([30CH3])[19CH2][18C@H]5/[13C]4=[12CH]/[11CH2][9C@@H]3[10C@]([25CH3])21
Description:polygalacic acid