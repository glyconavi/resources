RES
1n:n1
2b:b-dglc-HEX-1:5
3s:methyl
LIN
1:1n(1+1)2d
2:1n(11+1)3n
NON
NON1
SmallMolecule:SMILES O={11}C(C1=CO{1}[C@@H](O)[C@@]2([H])[C@]1([H])C{7}[C@H](O)[C@@H]2C)O
Description:loganic acid aglycon