RES
1b:x-dglc-HEX-1:5|6:d
2s:thio
3n:n1
LIN
1:1d(1+1)2n
2:1o(1+51)3n
NON
NON1
Parent:1
Linkage:o(1+51)n
SmallMolecule:SMILES {51}S/C(CC1=CN(OC)C2=C1C=CC=C2)=N\OS(=O)([O-])=O
Description:(Z)-1-mercapto-2-(1-methoxy-1H-indol-3-yl)ethylidene-amino sulfate