RES
1b:b-dglc-HEX-1:5
2n:n1
3b:b-dgro-TET-1:4
4s:hydroxymethyl
LIN
1:1o(1+9)2n
2:1o(2+1)3d
3:3h(3+1)4n
NON
NON1
Parent:1
Linkage:o(1+9)n
SmallMolecule:SMILES O=C1CC(C)([C@H](C(C)=C1)/C=C/{9}[C@H](C)O)C
Description:6R,9S-3-oxo-α-ionol