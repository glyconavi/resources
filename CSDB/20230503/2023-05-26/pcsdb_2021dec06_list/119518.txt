RES
1n:n1
2b:a-xara-PEN-1:5
3b:b-xglc-HEX-1:5
4b:b-xglc-HEX-1:5
5b:a-xman-HEX-1:5|6:d
6s:acetyl
LIN
1:1n(3+1)2d
2:1n(28+1)3d
3:3o(6+1)4d
4:4o(4+1)5d
5:4o(6+1)6n
NON
NON1
SmallMolecule:SMILES CC1(C){3}[C@@H](O)CC[C@]2(C)[C@@]3([H])CC=C4[C@]5([H])C[C@](C)({29}CO)CC[C@@]({28}C(O)=O)5CC[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:mesembryanthemoidigenic acid