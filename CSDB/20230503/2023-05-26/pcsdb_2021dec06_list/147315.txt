RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+9)2n
NON
NON1
Parent:1
Linkage:o(1+9)n
SmallMolecule:SMILES O=C1CC(C)([C@H](C(C)=C1)CC{9}[C@@H](C)O)C
Description:blumenol C