RES
1b:b-dglc-HEX-1:5
2b:a-lman-HEX-1:5|6:d
3n:n1
4b:b-dglc-HEX-1:5
5b:b-dglc-HEX-1:5
LIN
1:1o(4+1)2d
2:1o(1+3)3n
3:1o(2+1)4d
4:4o(3+1)5d
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C[C@H]1[C@H]2[C@@H](O[C@]13CC[C@@](C)({26}CO)O3)C[C@@H]4[C@]2(C)CC[C@H]5[C@H]4CC=C6[C@]5(C)CC{3}[C@H](O)C6
Description:nuatigenin