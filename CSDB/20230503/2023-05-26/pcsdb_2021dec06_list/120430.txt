RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
LIN
1:1n(3+1)2d
2:1n(26+1)3d
NON
NON1
SmallMolecule:SMILES C[C@]([C@@]1([H])CC[C@]23C)([C@@]2([H])CC[C@]4([H]){17}[C@@H](O)[C@](CC[C@]43C)(CO5)CC[C@]5([H])[C@H](C){26}CO)CC{3}[C@H](O)[C@]1({28}CO)C
Description:hosenkol B