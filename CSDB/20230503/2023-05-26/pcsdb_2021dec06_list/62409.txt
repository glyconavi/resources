RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+6)2n
NON
NON1
Parent:1
Linkage:o(1+6)n
SmallMolecule:SMILES C{11}C([C@@H]1CC[C@]2({14}CO){1}[C@@H](O)CCC([C@H]2{6}[C@@H]1O)=C)(O)C
Description:dictamnoside B aglycone