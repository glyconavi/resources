RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dgal-HEX-1:5|6:d
4b:b-dxyl-PEN-1:5
5b:a-lman-HEX-1:5|6:d
6n:n2
7n:n3
8b:b-dxyl-PEN-1:5
9b:a-lman-HEX-1:5|6:d
LIN
1:1n(3+1)2d
2:1n(28+1)3d
3:2o(3+1)4d
4:3o(2+1)5d
5:3o(4+1)6n
6:6n(3+1)7n
7:5o(4+1)8d
8:8o(3+1)9d
NON
NON1
SmallMolecule:SMILES [1CH2]1[2C@H](O)[3C@H](O)[4C@]([23CH2](O))([24CH3])[5C@@H]2[6CH2][7CH2][8C@]([26CH3])3[14C@@]([27CH3])4[15CH2][16C@@H](O)[17C@]([28C](=O)O)5[22CH2][21CH2][20C]([29CH3])([30CH3])[19CH2][18C@H]5/[13C]4=[12CH]/[11CH2][9C@@H]3[10C@]([25CH3])21
Description:polygalacic acid
NON2
Parent:3
Linkage:o(4+1)n
SmallMolecule:SMILES [4CH3][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxybutanoic acid
NON3
Parent:6
Linkage:n(3+1)n
SmallMolecule:SMILES [4CH3][3CH](O)[2CH2][1C](=O)O
Description:3-hydroxybutanoic acid