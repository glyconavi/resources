RES
1n:n1
2s:methyl
3s:methyl
4s:methyl
5b:a-lman-HEX-1:5|6:d
6s:methyl
7s:methyl
8s:methyl
LIN
1:1n(16+1)2n
2:1n(2+1)3n
3:1n(23+1)4n
4:1n(3+1)5d
5:5o(2+1)6n
6:5o(4+1)7n
7:5o(3+1)8n
NON
NON1
SmallMolecule:SMILES C[C@@]1({23}CO){3}[C@@H](O){2}[C@@H](O)C[C@]2(C)[C@@]3([H])CC=C4[C@]5([H])CC(C)(C)[C@](O6)([H])C[C@@](C6=O)5{16}[C@H](O)C[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:2β,23-dihydroxy-acacic acid 21,28-lactone