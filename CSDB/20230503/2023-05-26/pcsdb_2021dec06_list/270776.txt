RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+7)2n
NON
NON1
Parent:1
Linkage:o(1+7)n
SmallMolecule:SMILES O{7}C(C=C(OC1=O)C(C=C1)=C2)={6}C2O
Description:cichorigenin