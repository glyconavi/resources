RES
1n:n1
2b:b-dglc-HEX-1:5
3s:methyl
LIN
1:1n(3+1)2d
2:1n(28+1)3n
NON
NON1
SmallMolecule:SMILES CC1(C)[3C@@H](O)CC[C@]2(C)[C@@]3([H])CC=C4[C@]5([H])CC(C)(C)CC[C@@]([28C](O)=O)5[16C@H](O)C[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:echinocystic acid