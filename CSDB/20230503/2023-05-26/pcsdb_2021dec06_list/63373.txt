RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+4)2n
NON
NON1
Parent:1
Linkage:o(1+4)n
SmallMolecule:SMILES C/C1={4}C(O)/C(=O)CO1
Description:4-hydroxy-5-methylfuran-3(2H)-one