RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+6)2n
NON
NON1
Parent:1
Linkage:o(1+6)n
SmallMolecule:SMILES N#C/C=C1{6}[C@H](O){5}[C@H](O){4}[C@H](O)C=C\1
Description:(Z)-2-((4R,5R,6S)-4,5,6-trihydroxycyclohex-2-en-1-ylidene)acetonitrile