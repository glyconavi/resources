RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+3)2n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES CC[C@H](/C=C/[C@@H](C)[C@H]1CC[C@@H]2[C@@]1(CC[C@H]3[C@H]2CC=C4[C@@]3(CC{3}[C@@H](C4)O)C)C)C(C)C
Description:stigmasterol