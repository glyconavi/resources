RES
1n:n1
2s:methyl
3b:b-dglc-HEX-1:5
4b:b-dgal-HEX-1:5
LIN
1:1n(28+1)2n
2:1n(3+1)3d
3:3o(4+1)4d
NON
NON1
SmallMolecule:SMILES O{2}[C@@H]1{3}[C@H](O)[C@@](C)({23}CO)[C@@](CC[C@]2(C)[C@]3([H])CC=C4[C@@]2(C)CC[C@]5({28}C(O)=O)[C@@]4([H])CC(C)(C)CC5)([H])[C@]3(C)C1
Description:bayogenin