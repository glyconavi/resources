RES
1n:n1
2n:n2
3b:b-drib-HEX-1:5|2:d|6:d
4s:methyl
5b:b-drib-HEX-1:5|2:d|6:d
6s:methyl
7b:b-dara-HEX-1:5|2:d|6:d
8s:methyl
9b:b-dara-HEX-1:5|2:d|6:d
10s:methyl
11b:b-dara-HEX-1:5|2:d|6:d
12s:methyl
LIN
1:1n(12+7)2n
2:1n(3+1)3d
3:3o(3+1)4n
4:3o(4+1)5d
5:5o(3+1)6n
6:5o(4+1)7d
7:7o(3+1)8n
8:7o(4+1)9d
9:9o(3+1)10n
10:9o(4+1)11d
11:11o(3+1)12n
NON
NON1
SmallMolecule:SMILES CC({17}[C@]1(O)CC{14}[C@]2(O){8}[C@]3(O)CC=C4C{3}[C@@H](O)CC[C@]4(C)[C@@]3([H])C{12}[C@@H](O)[C@]12C)=O
Description:deacetylmetaplexigenin
NON2
Parent:1
Linkage:n(12+7)n
SmallMolecule:SMILES [6CH]1=[5CH][4CH]=[3CH][2CH]=[1C]1[7C](=O)O
Description:benzoic acid