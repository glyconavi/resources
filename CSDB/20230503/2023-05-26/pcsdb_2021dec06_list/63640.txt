RES
1b:b-drib-HEX-1:5|2:d|6:d
2n:n1
3b:b-dglc-HEX-1:5
4b:b-dglc-HEX-1:5
LIN
1:1o(1+3)2n
2:1o(4+1)3d
3:3o(6+1)4d
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O{3}[C@H]1CC[C@]2(C)[C@@]3([H])CC[C@]4(C)[C@@H](C(CO5)=CC5=O)CC{14}[C@]4(O)[C@]3([H])CC[C@@]([H])2C1
Description:digitoxigenin