RES
1b:b-dglc-HEX-1:5
2b:b-dglc-HEX-1:5
3n:n1
4b:a-lman-HEX-1:5|6:d
5b:b-dglc-HEX-1:5
LIN
1:1o(4+1)2d
2:1o(1+3)3n
3:1o(2+1)4d
4:4o(3+1)5d
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES [H][C@]1(O[C@@]2(OC[C@H]({27}CO)CC2)[C@H]3C)C[C@@]4([H])[C@]5([H])CC=C6C{3}[C@@H](O)CC[C@]6(C)[C@@]5([H])CC[C@]4(C)[C@]13[H]
Description:isonarthogenin