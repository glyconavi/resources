RES
1n:n1
2b:x-xman-HEX-1:5|6:d
3b:x-dglc-HEX-1:5
4b:x-xman-HEX-1:5|6:d
LIN
1:1n(7+1)2d
2:1n(3+1)3d
3:3o(2+1)4d
NON
NON1
SmallMolecule:SMILES O[3C]1=C(C2=C[53C](O)=[54C](O)C=C2)OC3=C([5C](O)=[6CH][7C](O)=[8CH]3)C1=O
Description:3,5,7,3',4'-pentahydroxyflavone