RES
1b:b-dglc-HEX-1:5
2b:a-lman-HEX-1:5|6:d
3b:a-lman-HEX-1:5|6:d
4n:n1
LIN
1:1o(4+1)2d
2:1o(2+1)3d
3:1o(1+3)4n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O[3C@H]1CC[C@]2(C)[C@@]3([H])CC[C@]4(C)[C@@]5([H])[C@H](C)[C@@]6([H])CC[C@H](C)CN6[C@]([H])5C[C@]([H])4[C@@]([H])3CC=C2C1
Description:solanidine