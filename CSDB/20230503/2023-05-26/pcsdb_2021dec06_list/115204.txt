RES
1b:b-dglc-HEX-1:5
2b:b-dglc-HEX-1:5
3b:a-dxyl-PEN-1:5
4b:b-dglc-HEX-1:5
5b:a-dxyl-PEN-1:5
6b:b-dglc-HEX-1:5
7b:b-dgal-HEX-1:5
8b:a-dxyl-PEN-1:5
9b:a-lgal-HEX-1:5|6:d
10s:acetyl
11s:acetyl
LIN
1:1o(4+1)2d
2:2o(6+1)3d
3:2o(4+1)4d
4:4o(6+1)5d
5:4o(4+1)6d
6:3o(2+1)7d
7:6o(6+1)8d
8:7o(2+1)9d
9:7o(6+1)10n
10:7o(4+1)11n