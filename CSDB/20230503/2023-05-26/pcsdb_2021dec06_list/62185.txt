RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4s:sulfate
5b:a-lman-HEX-1:5|6:d
6b:a-lman-HEX-1:5|6:d
LIN
1:1n(3+1)2d
2:1n(28+1)3d
3:3o(2+1)4n
4:2o(2+1)5d
5:5o(2+1)6d
NON
NON1
SmallMolecule:SMILES C/C5=C/C[C@]4({28}C(=O)O)CC[C@]3(C)C(CCC2[C@@]1(C)CC{3}[C@H](O)[C@](C)(C)C1CC[C@]23C)C4[C@H]5C
Description:3β-hydroxyurs-20-en-28-oic acid