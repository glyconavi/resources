RES
1n:n1
2b:b-dglc-HEX-1:5
3b:a-lman-HEX-1:5|6:d
4n:n2
LIN
1:1n(1+1)2d
2:1n(6+1)3d
3:3o(3+9)4n
NON
NON1
SmallMolecule:SMILES O{10}CC1=C{6}[C@@H](O)[C@H]2[C@@H]1{1}[C@H](O)OC=C2
Description:aucubin aglycon
NON2
Parent:3
Linkage:o(3+9)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3C](O[10CH3])[2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:ferulic acid