RES
1n:n1
2s:methyl
3s:methyl
4n:n2
LIN
1:1n(4+1)2n
2:1n(3+1)3n
3:1n(1+3)4n
NON
NON1
SmallMolecule:SMILES C[C@H]1O{1}[C@@H](O)C{3}[C@H](O){4}[C@@H]1N
Description:4-amino-digitoxose
NON2
Parent:1
Linkage:n(1+3)n
SmallMolecule:SMILES CC(=O)[C@H]3CC{14}[C@]4(O)[C@@H]2CC[C@H]1C{3}[C@@H](O)CC[C@]1(C)[C@H]2CC[C@]34C
Description:holacurtine aglycon