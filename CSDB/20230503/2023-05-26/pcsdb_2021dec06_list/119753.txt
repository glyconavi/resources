RES
1n:n1
2b:b-dglc-HEX-1:5
3s:acetyl
LIN
1:1n(2+1)2d
2:1n(3+1)3n
NON
NON1
SmallMolecule:SMILES [H][C@]1(O[C@@]2(OC[C@H](C)CC2)[C@H]3C)C[C@@]4([H])[C@]5([H])C{6}[C@@H](O){5}[C@@]6(O)C{3}[C@@H](O){2}[C@H](O)C[C@]6(C)[C@@]5([H])CC[C@]4(C)[C@]13[H]
Description:alliogenin