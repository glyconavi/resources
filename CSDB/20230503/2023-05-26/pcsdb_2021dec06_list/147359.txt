RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+3)2n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O{3}[C@H]1CC[C@]2(C)[C@@]3([H])CC[C@]4(C)[C@@H](C(CO5)=CC5=O)CC{14}[C@]4(O)[C@]3([H])CC[C@@]([H])2C1
Description:digitoxigenin