RES
1b:a-lara-PEN-1:5
2b:b-dglc-HEX-1:5
3n:n1
4b:a-lara-PEN-1:4
5n:n2
LIN
1:1o(3+1)2d
2:1o(1+3)3n
3:1o(2+1)4d
4:4o(5+1)5n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C/C(C)=C/[C@H]1C[C@](C)(O)[C@@H]2[C@H]3CC[C@@H]4[C@@]5(C)CC{3}[C@H](O)C(C)(C)[C@@H]5CC[C@@]4(C)[C@]36C[C@]2(OC6)O1
Description:jujubogenin
NON2
Parent:4
Linkage:o(5+1)n
SmallMolecule:SMILES [3C](=O)(O)[2CH2][1C](=O)(O)
Description:malonic acid