RES
1n:n1
2b:b-dglc-HEX-1:5
3s:methyl
LIN
1:1n(16+1)2d
2:1n(28+1)3n
NON
NON1
SmallMolecule:SMILES [1CH2]1[2C@H](O)[3C@H](O)[4C@]([23CH2](O))([24CH3])[5C@@H]2[6CH2][7CH2][8C@]([26CH3])3[14C@@]([27CH3])4[15CH2][16C@@H](O)[17C@]([28C](=O)O)5[22CH2][21CH2][20C]([29CH3])([30CH3])[19CH2][18C@H]5/[13C]4=[12CH]/[11CH2][9C@@H]3[10C@]([25CH3])21
Description:polygalacic acid