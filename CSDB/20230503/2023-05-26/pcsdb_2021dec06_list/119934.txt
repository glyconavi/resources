RES
1n:n1
2s:methyl
3b:b-dglc-HEX-1:5
4b:b-dglc-HEX-1:5|6:a
LIN
1:1n(54+1)2n
2:1n(55+1)3d
3:3o(2+1)4d
NON
NON1
SmallMolecule:SMILES CC1=NC=C(C({54}CO)={3}C1O){55}CO
Description:pyridoxine