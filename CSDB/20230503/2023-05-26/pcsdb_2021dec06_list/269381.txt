RES
1b:b-dgal-HEX-1:5|6:d
2b:b-dglc-HEX-1:5
3n:n1
LIN
1:1o(3+1)2d
2:1o(1+3)3n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C[C@@]1({23}CO){3}[C@@H](O)CC[C@]2(C)[C@@]3([H])C=CC4=C5CC(C)(C)CC[C@@]({28}CO)5{16}[C@H](O)C[C@](C)4[C@@](C)3CC[C@@]12[H]
Description:saikogenin D