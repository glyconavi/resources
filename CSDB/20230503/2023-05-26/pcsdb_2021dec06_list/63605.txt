RES
1n:n1
2b:b-dglc-HEX-1:5
3s:methyl
4b:b-dgal-HEX-1:5|6:d
5b:a-lman-HEX-1:5|6:d
6b:b-dxyl-PEN-1:5
LIN
1:1n(26+1)2d
2:1n(22+1)3n
3:1n(1+1)4d
4:4o(2+1)5d
5:4o(3+1)6d
NON
NON1
SmallMolecule:SMILES C=C({26}CO)CC{22}[C@@]5(O)OC4CC3C2CCC1C{3}[C@@H](O)C{1}[C@@H](O)[C@]1(C)C2CC[C@]3(C)C4[C@@H]5C
Description:5α-furosta-25(27)-en-1β,3β,22,26-tetrol