RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+9)2n
NON
NON1
Parent:1
Linkage:o(1+9)n
SmallMolecule:SMILES COC1={4}C(O)C(OC)=CC(/C=C/{9}CO)=C1
Description:sinapic alcohol