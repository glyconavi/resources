RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4b:b-dglc-HEX-1:5
LIN
1:1n(2+1)2d
2:1n(52+1)3d
3:1n(6+1)4d
NON
NON1
SmallMolecule:SMILES O=C(OCC1=CC=CC={52}C1O)C2={2}C(O)C=CC={6}C2O
Description:2,6-dihydroxybenzoic acid 2'-hydroxybenzyl ester