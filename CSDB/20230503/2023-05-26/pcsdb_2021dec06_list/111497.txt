RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4b:a-lman-HEX-1:5|6:d
5b:a-lman-HEX-1:5|6:d
6b:b-dglc-HEX-1:5
LIN
1:1n(3+1)2d
2:1n(21+1)3d
3:2o(4+1)4d
4:3o(6+1)5d
5:3o(2+1)6d
NON
NON1
SmallMolecule:SMILES C=C{3}C(C)(O)CC/C=C(C)/CC/C=C(C)/CC/C=C({20}CO)/C
Description:6E,10E,14Z-20-hydroxy-geranyllinalool