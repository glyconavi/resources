RES
1b:b-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+5)2n
NON
NON1
Parent:1
Linkage:o(1+5)n
SmallMolecule:SMILES [H][C@]1(O[C@@]2(OC[C@H](C)CC2)[C@H]3C)C[C@@]4([H])[C@]5([H])CC{5}[C@]6(O){4}[C@@H](O){3}[C@@H](O)C{1}[C@@H](O)[C@]6(C)[C@@]5([H])CC[C@]4(C)[C@]13[H]
Description:kitigenin