RES
1b:x-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+3)2n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O=C1C2=C(C(C=C{3}C(O)=C3)=C3O1)OC4=C2C=CC(O)=C4
Description:coumestrol