RES
1b:b-dxyl-HEX-1:5|2:d|6:d
2b:b-dglc-HEX-1:5
3n:n1
LIN
1:1o(4+1)2d
2:1o(1+3)3n
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES C[C@]34CCC2C(CCC1C{3}[C@@H](O)CC[C@@]12{18}CO){14}[C@@]3(O)CC[C@@H]4/C5=C/C(=O)OC5
Description:cannogenol