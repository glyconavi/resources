RES
1n:n1
2b:b-dglc-HEX-1:5
3b:b-dglc-HEX-1:5
4b:b-dxyl-PEN-1:5
5n:n2
LIN
1:1n(5+1)2d
2:1n(3+1)3d
3:3o(2+1)4d
4:4o(2+9)5n
NON
NON1
SmallMolecule:SMILES O[3C]1=C(C2=CC=[54C](O)[53C](O)=C2)[O+]=C3[8CH]=[7C](O)[6CH]=[5C](O)C3=C1
Description:3,3',4',5,7-pentahydroxyflavylium
NON2
Parent:4
Linkage:o(2+9)n
SmallMolecule:SMILES [6CH]1=[5C](O[11CH3])[4C](O)=[3C](O[10CH3])[2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:sinapic/sinapinic acid