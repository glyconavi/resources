RES
1n:n1
2s:acetyl
3b:b-dglc-HEX-1:5
4b:b-dglc-HEX-1:5|6:d
5s:acetyl
6s:acetyl
7s:acetyl
8s:acetyl
9s:acetyl
10s:acetyl
11s:acetyl
12s:acetyl
13s:thio
LIN
1:1n(1+1)2n
2:1n(3+1)3d
3:1n(2+1)4d
4:4o(4+1)5n
5:4o(6+1)6n
6:4o(3+1)7n
7:4o(2+1)8n
8:3o(3+1)9n
9:3o(6+1)10n
10:3o(4+1)11n
11:3o(2+1)12n
12:4d(1+1)13n
NON
NON1
SmallMolecule:SMILES O{2}C1={3}C(O)C2=CC=CC=C2{1}N1
Description:1H-indole-2,3-diol