RES
1n:n1
2s:acetyl
3b:b-dglc-HEX-1:5
4b:a-lman-HEX-1:5|6:d
5b:b-dxyl-PEN-1:5
LIN
1:1n(26+1)2n
2:1n(3+1)3d
3:3o(4+1)4d
4:4o(2+1)5d
NON
NON1
SmallMolecule:SMILES CC({26}CN)CCC([C@@H](C)[C@H]1{16}[C@H](O)C[C@@]2([H])[C@]3([H])CC[C@@]4([H])C{3}[C@@H](O)CC[C@]4(C)[C@@]3([H])CC[C@]12C)=O
Description:3β,16α-dihydroxy-26-amino-5α,25ξ-cholestan-22-one