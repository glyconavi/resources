RES
1b:a-lara-PEN-1:5
2b:a-lman-HEX-1:5|6:d
3s:sulfate
4n:n1
LIN
1:1o(2+1)2d
2:1o(4+1)3n
3:1o(1+1)4n
NON
NON1
Parent:1
Linkage:o(1+1)n
SmallMolecule:SMILES [H][C@]1(O[C@](OCC2=C)(CC2)[C@H]3C)C[C@@]4([H])[C@]5([H])CC=C6C{3}[C@@H](O)C{1}[C@@H](O)[C@]6(C)[C@@]5([H])CC[C@]4(C)[C@]13[H]
Description:neoruscogenin