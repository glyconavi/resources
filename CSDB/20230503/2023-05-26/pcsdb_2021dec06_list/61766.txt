RES
1n:n1
2b:b-dglc-HEX-1:5
3s:acetyl
4s:methyl
5n:n2
LIN
1:1n(1+1)2d
2:1n(8+1)3n
3:1n(11+1)4n
4:1n(6+9)5n
NON
NON1
SmallMolecule:SMILES C{8}[C@]2(O)C{6}[C@@H](O)C1/C{11}(C(=O)O)=C\O{1}[C@@H](O)C12
Description:shanzhiside aglycon
NON2
Parent:1
Linkage:n(6+9)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3CH][2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:p-coumaric acid