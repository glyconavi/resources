RES
1b:b-dglc-HEX-1:5
2n:n1
3n:n2
LIN
1:1o(4+9)2n
2:1o(1+7)3n
NON
NON1
Parent:1
Linkage:o(4+9)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3CH][2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:p-coumaric acid
NON2
Parent:1
Linkage:o(1+7)n
SmallMolecule:SMILES O=C(C1=[5C](O)[6CH]=[7C](O)[8CH]=C1O2)[3CH]=C2C3=CC=[54C](O)C=C3
Description:5,7,4'-trihydroxyflavone