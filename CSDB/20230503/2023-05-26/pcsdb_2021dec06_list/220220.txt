RES
1b:b-dglc-HEX-1:5
2n:n1
3b:b-dgro-TET-1:4
4s:hydroxymethyl
LIN
1:1o(1+54)2n
2:1o(2+1)3d
3:3h(3+1)4n
NON
NON1
Parent:1
Linkage:o(1+54)n
SmallMolecule:SMILES O=C(C(C=C{54}C(O)=C1)={52}C1O)/C=C/C2=CC={4}C(O)C=C2
Description:isoliquiritigenin