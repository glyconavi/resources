RES
1b:b-dglc-HEX-1:5
2n:n1
3n:n2
LIN
1:1o(6+9)2n
2:1o(1+3)3n
NON
NON1
Parent:1
Linkage:o(6+9)n
SmallMolecule:SMILES [6CH]1=[5CH][4C](O)=[3CH][2CH]=[1C]1/[7CH]=[8CH]/[9C](=O)O
Description:p-coumaric acid
NON2
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES O{3}[C@H]1CC[C@@]2(C)[C@](CC[C@]3([H])[C@]2([H])CC[C@@]4(C){14}[C@]3(O)CC[C@@H]4C(CO5)=CC5=O)([H])C1
Description:uzarigenin