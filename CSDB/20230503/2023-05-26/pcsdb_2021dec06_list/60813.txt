RES
1b:x-dglc-HEX-1:5
2n:n1
LIN
1:1o(1+23)2n
NON
NON1
Parent:1
Linkage:o(1+23)n
SmallMolecule:SMILES C[C@H]({22}[C@@H](O){23}[C@H](O)[C@H](C(C)C)C)[C@H]1CC[C@@H]2[C@]1(C)CC[C@H]3[C@H]2CC([C@@H]4[C@]3(C)C{2}[C@@H](O){3}[C@@H](O)C4)=O
Description:castasterone