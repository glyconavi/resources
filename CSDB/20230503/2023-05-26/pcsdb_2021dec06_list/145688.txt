RES
1b:a-lara-PEN-1:5
2n:n1
3b:a-lman-HEX-1:5|6:d
4b:b-dxyl-PEN-1:5
5b:a-lman-HEX-1:5|6:d
6b:b-dxyl-PEN-1:5
LIN
1:1o(1+3)2n
2:1o(2+1)3d
3:3o(3+1)4d
4:4o(3+1)5d
5:5o(4+1)6d
NON
NON1
Parent:1
Linkage:o(1+3)n
SmallMolecule:SMILES [1CH2]1[2CH2][3C@H](O)[4C]([23CH3])([24CH3])[5C@@H]2[6CH2][7CH2][8C@]([26CH3])3[14C@@]([27CH3])4[15CH2][16CH2][17C@]([28C](=O)O)5[22CH2][21CH2][20C]([29CH3])([30CH3])[19CH2][18C@H]5/[13C]4=[12CH]/[11CH2][9C@@H]3[10C@]([25CH3])21
Description:oleanolic/oleanic acid