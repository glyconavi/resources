RES
1b:b-dglc-HEX-1:5
2n:n1
3b:b-dglc-HEX-1:5
4b:a-lman-HEX-1:5|6:d
LIN
1:1o(1+28)2n
2:1o(6+1)3d
3:3o(4+1)4d
NON
NON1
Parent:1
Linkage:o(1+28)n
SmallMolecule:SMILES CC1(C)CC[C@]2([28C](O)=O)CC[C@@]3(C)[C@]4(C)CC[C@@]5([H])[C@](C)([23CH2]O)[3C@@H](O)CC[C@]5(C)[C@@]4([H])CC=C3[C@]2([H])C1
Description:hederagenin