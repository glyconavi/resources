RES
1n:n1
2b:b-dglc-HEX-1:5
3b:a-lara-PEN-1:5
4b:a-lman-HEX-1:5|6:d
5b:b-dglc-HEX-1:5
LIN
1:1n(23+1)2d
2:1n(1+1)3d
3:3o(2+1)4d
4:4o(3+1)5d
NON
NON1
SmallMolecule:SMILES [H][C@]1(O[C@](OCC2=C)({23}[C@@H](O)C2)[C@H]3C)C[C@@]4([H])[C@]5([H])CC=C6C{3}[C@@H](O)C{1}[C@@H](O)[C@]6(C)[C@@]5([H])CC[C@]4(C)[C@]13[H]
Description:spirosta-5,25(27)-diene-1β,3β,23S-triol