import os
import datetime
from openpyxl import load_workbook
import sys

# コマンドライン引数からExcelファイルのパスを取得
file_path = sys.argv[1]

# 実行日の日付を取得
current_date = datetime.date.today()

# 実行スクリプトのディレクトリパスを取得
script_dir = os.path.dirname(os.path.abspath(__file__))

# フォルダのパス
folder_path = os.path.join(script_dir, str(current_date))

# フォルダが存在しない場合は作成
if not os.path.exists(folder_path):
    os.makedirs(folder_path)

# Excelファイルの読み込み
workbook = load_workbook(file_path)

# すべてのシートに対して処理を行う
for sheet in workbook.sheetnames:
    # シートの選択
    worksheet = workbook[sheet]

    # シート名から使用できない文字を削除してフォルダ名を作成
    folder_name = ''.join(c for c in sheet if c.isalnum() or c in [' ', '_', '-'])

    # フォルダのパス（タブ名のフォルダを作成する）
    tab_folder_path = os.path.join(folder_path, folder_name)

    # タブ名のフォルダが存在しない場合は作成
    if not os.path.exists(tab_folder_path):
        os.makedirs(tab_folder_path)

    # A列とC列のセルのデータを取得
    cell_data = []
    for row in worksheet.iter_rows(min_row=1, values_only=True):
        file_name_parts = str(row[0]).split(" ")  # ファイル名を半角スペースで分割

        if len(file_name_parts) > 0:
            file_name = file_name_parts[0] + ".txt"  # ファイル名は分割後の1番目
            c_cell_data = row[2]  # C列のセルのデータ

            # ファイルパス（タブ名のフォルダ内に保存する）
            file_path = os.path.join(tab_folder_path, file_name)

            # ファイルにC列のデータを保存
            with open(file_path, "w") as file:
                file.write(str(c_cell_data))
