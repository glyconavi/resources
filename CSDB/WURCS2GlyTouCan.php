<?php
header('Content-Type: text/plain;charset=UTF-8');
ini_set('error_reporting', E_ALL);
date_default_timezone_set('Asia/Tokyo');
$timeHeader = date("Y-m-d_H-i-s");

$db = "pcsdb";
$gfc = "gfc291";

$dirpath = dirname(__FILE__);
$subdir = "20230503/2023-05-26/".$db."_2021dec06_list";

$ctpath = $dirpath.DIRECTORY_SEPARATOR.$subdir;
$res_dir = opendir($ctpath);

$outputfile = $timeHeader.'_'.$db.'_GlycoCT2WURCS_r3_'.$gfc.'.tsv';
$errfile = $timeHeader.'_'.$db.'_GlycoCT2WURCS_error_r3_'.$gfc.'.tsv';

$num = 1;
while ($file_name = readdir($res_dir)) {
    if (strstr($file_name, '.txt')) {
        echo $num."\n";
        $num++;

        $ct = file_get_contents($dirpath.DIRECTORY_SEPARATOR.$subdir.DIRECTORY_SEPARATOR.$file_name, "r");

        $ct = "\"".$ct."\"";

        $cmd = "java -jar /Users/yamada/git/gitlab/glycoinfo/GlycanFormatConverter-cli/target/GlycanFormatConverter-cli.jar -i GlycoCT -e WURCS -seq ".$ct." | grep WURCS=2.0 | grep -v DEBUG";

        $id = str_replace("CSDB_CT_", "", $file_name);
        $id = str_replace(".txt", "", $id);

        $output = shell_exec($cmd);

        if (is_null($output)) {
            $line = $file_name."\t".$id."\t\n";
            file_put_contents($errfile, $line, FILE_APPEND | LOCK_EX);
        } elseif (str_contains($output, 'WURCS=2.0/')) {
            $line = $file_name."\t".$id."\t".$output;
            file_put_contents($outputfile, $line, FILE_APPEND | LOCK_EX);
        } else {
            $line = $file_name."\t".$id."\t".$output."\n";
            file_put_contents($errfile, $line, FILE_APPEND | LOCK_EX);
        }
    }
}
?>
