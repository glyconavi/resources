<?php
header('Content-Type: text/plain;charset=UTF-8');
ini_set('error_reporting', E_ALL);
date_default_timezone_set('Asia/Tokyo');
$timeHeader = date("Y-m-d_H-i-s");

$db = "pcsdb";
$gfc = "gfc291";

$dirpath = dirname(__FILE__);
$subdir = "20230503/2023-05-26/".$db."_2021dec06_list";

$ctpath = $dirpath.DIRECTORY_SEPARATOR.$subdir;
//echo $ctpath."\n";
$res_dir = opendir($ctpath);
//echo $res_dir."\n";

$outputfile = $timeHeader.'_'.$db.'_GlycoCT2WURCS_r3_'.$gfc.'.tsv';
$errfile = $timeHeader.'_'.$db.'_GlycoCT2WURCS_error_r3_'.$gfc.'.tsv';

$num = 1;
while( $file_name = readdir( $res_dir ) ){
    if (strstr($file_name, '.txt')) {
        //echo $file_name."\n";
        echo $num."\n";
        $num = $num + 1;
        $ct = file_get_contents($dirpath.DIRECTORY_SEPARATOR.$subdir.DIRECTORY_SEPARATOR.$file_name,"r");
        //echo trim($ct)."\n";

        //$ct = "\"".str_replace("\n", "\\", $ct)."\n";
        
        $ct = "\"".$ct."\"";
        //echo $ct."\n";





        $cmd = "java -jar /Users/yamada/git/gitlab/glycoinfo/GlycanFormatConverter-cli/target/GlycanFormatConverter-cli.jar -i GlycoCT -e WURCS -seq ".$ct." | grep WURCS=2.0 | grep -v DEBUG";

        //echo $cmd."\n";

        //
        //CSDB_CT_736.txt
        $id = str_replace("CSDB_CT_", "", $file_name);
        $id = str_replace(".txt", "", $id);

        $output = shell_exec($cmd);


        if (is_null($output)) {
            $line = $file_name."\t".$id."\t\n";
            file_put_contents($errfile , $line, FILE_APPEND | LOCK_EX);
        }
        else if (str_contains($output, 'WURCS=2.0/')) {
            $line = $file_name."\t".$id."\t".$output;
            // ファイルにt追記モードで書き込む ロックモードを併用
            file_put_contents($outputfile , $line, FILE_APPEND | LOCK_EX);
        }
        else{
            $line = $file_name."\t".$id."\t".$output."\n";
            file_put_contents($errfile , $line, FILE_APPEND | LOCK_EX);
        }

        //echo $file_name."\t".$id."\t".$output;



    }
}




/*
$gtcfile = "";
if (count($argv) > 0){
  $gtcfile = $argv[1];
}

if(is_file($gtcfile)) {
    try {
        $gtcfiledata = file_get_contents($gtcfile);
        $gtcstr = str_replace(array("\r\n","\r","\n"), "\n", $gtcfiledata);
        $gtclines = explode("\n", $gtcstr);

        if(is_file($file)) {
            $lines = array();
            try {
                $filedata = file_get_contents($file);
                $str = str_replace(array("\r\n","\r","\n"), "\n", $filedata);
                $lines = explode("\n", $str);
            }
            catch (Exception $e) {
                //echo $e;
            }

            
        }
    }
    catch (Exception $e) {
        //echo $e;
    }
}
*/

?>
