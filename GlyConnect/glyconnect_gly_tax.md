# glyconnect_gly_tax

## Endpoint

https://glyconnect.expasy.org/sparql

## `result`

```sparql
# @temp-proxy true
PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
PREFIX faldo:<http://biohackathon.org/resource/faldo#>
PREFIX glyco:<http://purl.jp/bio/12/glyco/conjugate#>
PREFIX glycan:<http://purl.jp/bio/12/glyco/glycan#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
#Select all glycosylations sites of 'Beta-2-glycoprotein 1' (for all species);
SELECT DISTINCT ?gtc ?tax ?tax_id
WHERE {
#?protein glyco:glycosylated_at ?specificglycosite .
#?specificglycosite faldo:location ?glycosite .
#?protein rdfs:label ?protein_name .
#?glycosite faldo:reference ?isoform .
#?glycosite faldo:position ?position .
#?refconjugate glyco:has_protein_part ?protein .
?refconjugate glycan:is_from_source ?source .
?refconjugate glyco:has_saccharide_part ?saccharide .
?saccharide glycan:has_glycan ?glycan .
BIND(REPLACE(STR(?glycan), "http://identifiers.org/glytoucan/", "", "i") AS ?gtc)
?source glycan:has_taxon ?taxon .
  BIND(REPLACE(STR(?taxon), "http://identifiers.org/taxonomy/", "", "i") AS ?tax_id)
?taxon rdfs:label ?tax . 
#  VALUES ?taxon_name { "Homo sapiens" }
}
order by ?tax ?gtc
```

## `Output`

```javascript
({
  json({result}) {
    let obj = {};
    return result.results.bindings.map((row) => {
         return {
			"GlyTouCan": row.gtc.value , 
	        "Taxon_name": row.tax.value,
      }
    });
  },
  text({result}) {
    return result.results.bindings.map(row => row.up.value).join("\n");
  }
})
```
