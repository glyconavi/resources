# LipidBank

## 調査

* 脂質と糖鎖を繋ぐことの検討

### 糖に関連する脂質について

* http://lipidbank.jp/ から、GlycolipidとLipopolysaccharide が対象となりそう

* Glycolipid (696)
   * Glycosphingolipid (581)
   * Glycoglycerolipid and others (115)

 - Lipopolysaccharide (734)

### LIPID CLASSの調査

* GSG: Glycosphingolipid
* GGG: Glycoglycerolipid and others
* CLS: Lipopolysaccharide


### LipidBankのデータダウンロードサイト： https://lipidbank.jp/download/

* LipidBankのカテゴリ毎にzip圧縮されたcdxファイルがあるようだ。

    * cdx_GSG.zip	2022-04-01 19:19	579K	 
    * cdx_GGG.zip	2022-04-01 19:19	111K	 
    * cdx_CLS.zip	2022-04-01 19:19	2.3M	 

### cdxで記載されている糖鎖構造の調査

* Marvinで、LipidBankからダウンロードしたcdxファイル（GSG0071.cdx）を開こうとしたが、エラーで開くことができなかった。

### LipidMAPSのSDFに含まれるLIpidBankのIDを調査

1. LipidMAPSのSDFファイルをダウンロード（https://www.lipidmaps.org/data/structure/download.php）
    * https://www.lipidmaps.org/files/?file=LMSD&ext=sdf.zip

2. ダウンロードしたファイル名を変更（LipidMAPS_structures.sdf）

* LipidMAPSのSDFに登録されている GSG, GGG, CLS のエントリーは以下のように少なかった。

```
bash-3.2$ grep "GSG" LipidMAPS_structures.sdf | wc -l
      46
bash-3.2$ grep "GGG" LipidMAPS_structures.sdf | wc -l
      37
bash-3.2$ grep "CLS" LipidMAPS_structures.sdf | wc -l
     161
```


