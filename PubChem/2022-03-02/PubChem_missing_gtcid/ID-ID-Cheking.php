<?php
header('Content-Type: text/plain;charset=UTF-8');
ini_set('auto_detect_line_endings', 1);
ini_set('error_reporting', E_ALL);
date_default_timezone_set('Asia/Tokyo');
$timeHeader = date("Y-m-d_H-i-s");

$file = "";
if (count($argv) > 1){
  $file = $argv[1];
}

$gtcfile = "";
if (count($argv) > 2){
  $gtcfile = $argv[2];
}

if(is_file($gtcfile)) {
    $gtclines = array();
    try {
        $gtcfiledata = file_get_contents($gtcfile);
        $gtcstr = str_replace(array("\r\n","\r","\n"), "\n", $gtcfiledata);
        $gtclines = explode("\n", $gtcstr);

        if(is_file($file)) {
            $lines = array();
            try {
                $filedata = file_get_contents($file);
                $str = str_replace(array("\r\n","\r","\n"), "\n", $filedata);
                $lines = explode("\n", $str);
            }
            catch (Exception $e) {
                //echo $e;
            }

            foreach ($lines as $line) {
                $getgtc = false;
                foreach ($gtclines as $gtcline) {
                    try {
                        if (trim($gtcline) === trim($line)) {
                            echo $line, "\tGlyTouCan\t", $gtcline, PHP_EOL;
                            $getgtc = true;
                        }
                    }
                    catch (Exception $e) {
                        error_log($e);
                    }
                }
                if ($getgtc === false) {
                    echo $line, PHP_EOL;
                }
            }
        }
    }
    catch (Exception $e) {
        //echo $e;
    }
}
?>