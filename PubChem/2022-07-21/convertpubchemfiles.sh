#!/bin/bash

while read -r f; do

  # ファイル一つ毎の処理
  zcat $f | java -jar /home/glyconavi/git/gitlab/glycoinfo/molwurcs/target/MolWURCS.jar --in sdf --out wurcs --double-check | grep "WURCS=2.0" > $f.wurcs.txt

done < <(find /home/glyconavi/test_data/pubchem/pubchem/ -mindepth 1 -maxdepth 1)



