# GlyCosmos Glycans Sequence Resources

## Glycosmos Glycans Sequence RDF/ttl

### Tool

  * https://gitlab.com/glyconavi/glycosmosglycans

```
$ java -jar glycosmosglycans.jar -h
GlyCosmosGlycansSeqRDF
0.3.1
Fin.
```

### TTL
```
$ java -jar glycosmosglycans.jar -f GTC-glycosmos-glycans-20200731.tsv
```

input tsv file: GTC-glycosmos-glycans-20200731.tsv

output ttl file: GTC-glycosmos-glycans-20200731.tsv_2020-08-03_GlyCosmos-Glycans-0.3.1.ttl

### GZ

```
gzip GTC-glycosmos-glycans-20200731.tsv
gzip GTC-glycosmos-glycans-20200731.tsv_2020-08-03_GlyCosmos-Glycans-0.3.1.ttl
```


