## RDF Resources

### Disease
* http://glyconavi.org/disease/
  * DiseaseGroupGraph-2021-03-16.ttl

### MilkSugar
* http://glyconavi.org/milksugar/
  * MilkSugar-2021-03-09.ttl

### Others
* http://glyconavi.org/others/
  * Others-2021-03-18.ttl