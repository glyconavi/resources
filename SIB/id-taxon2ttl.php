<?php
ini_set("memory_limit", "1000M");
setlocale(LC_ALL,'ja_JP.UTF-8');
$tsvfile = "GlyTouCanIDTaxonID.tsv";

if (count($argv)>0){
    $tsvfile = $argv[1];
}

$row = 1;
// ファイルが存在しているかチェックする
if (($handle = fopen($tsvfile, "r")) !== FALSE) {

$ttl = "@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .".PHP_EOL;
$ttl .= "@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .".PHP_EOL;
$ttl .= "@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .".PHP_EOL;
$ttl .= "@prefix dcterms: <http://purl.org/dc/terms/> .".PHP_EOL;
$ttl .= "@prefix SIO: <http://semanticscience.org/resource/> .".PHP_EOL;
$ttl .= "@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .".PHP_EOL;
$ttl .= "@prefix glycan:   <http://purl.jp/bio/12/glyco/glycan#> .".PHP_EOL;
$ttl .= "@prefix gtc: <http://identifiers.org/glytoucan/> .".PHP_EOL;
$ttl .= "@prefix glytoucan:   <http://www.glytoucan.org/glyco/owl/glytoucan#> .".PHP_EOL;
$ttl .= "@prefix source:   <http://rdf.glycoinfo.org/source/> .".PHP_EOL;


// read tsv file
$count = -1;
while (($row = fgetcsv($handle, 0, "\t"))) {
        $count++;
        if ($count !== 0) {
            $GlyTouCanId = "";
            $taxon_id    = "";
            $taxon       = "";

            if (count($row) > 2) {
                // set data
                $GlyTouCanId = trim($row[0]);
                $taxon_id    = trim($row[1]);
                $taxon       = trim($row[2]);
                // ttl
                $ttl .= "gtc:".$GlyTouCanId.PHP_EOL;
                // root
                $ttl .= "\ta\tglycan:Saccharide ;".PHP_EOL;
                $ttl .= "\tglytoucan:has_primary_id\t\"".$GlyTouCanId."\" ;".PHP_EOL;
                $ttl .= "\tdcterms:identifier\t\"".$GlyTouCanId."\" ;".PHP_EOL;
                $ttl .= "\tglycan:is_from_source\tsource:".$taxon_id." .".PHP_EOL;
                // source
                $ttl .= "source:".$taxon_id."\ta glycan:Source ;".PHP_EOL;
                $ttl .= "\tdcterms:identifier\t\"".$taxon_id."\" ;".PHP_EOL;
                $ttl .= "\tskos:prefLabel\t\"".$taxon."\" .".PHP_EOL;

                echo $count.":\t".$GlyTouCanId."\n";
            }
        }
    }
    $file = $tsvfile.".ttl";
    $result = file_put_contents($file, $ttl, FILE_APPEND | LOCK_EX);
    
    if ( $result === 0 ) {
        echo "書き込み失敗\n";
    } else {
        echo "書き込み成功：" . $result . " Byteの文字列の書込みをおこないました\n";
    }

    fclose($handle);
}
?>

