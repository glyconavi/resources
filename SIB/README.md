# WURCS-RDF

## JPN task

1. Get WURCS and ID data in GlyTouCan (human, mouse)(2021-11-12 16:32(JST))

2. Get all ID and WURCS in GlyTouCan, and convert tsv to csv

```
./GlyTouCanIDWURCS.sh > 2021-11-12-17-52-GlyTouCan-WURCS.tsv
vi 2021-11-12-17-52-GlyTouCan-WURCS.csv
// :%s/\t/,/g
```

3. Convert WURCS to GlycoCT using GlycanFOrmatConverter-cli (Skip)

* https://github.com/glycoinfo/GlycanFormatConverter-cli

* Sample
```
$ java -jar target/GlycanFormatConverter-cli.jar -i WURCS -e GlycoCT -seq WURCS=2.0/5,9,8/[a2122h-1x_1-5][a2112h-1b_1-5][a2122h-1b_1-5_2*NCC/3=O][a1221m-1a_1-5][Aad21122h-2a_2-6_5*NCC/3=O]/1-2-3-2-4-5-3-2-5/a4-b1_b3-c1_b6-g1_c4-d1_d2-e1_d6-f2_g4-h1_h6-i2 | grep -v DEBUG 2> /dev/null
```

4. Convert tsv to ttl

* https://gitlab.com/glyconavi/glycosmosglycans

```
$ git clone https://gitlab.com/glyconavi/glycosmosglycans.git
$ cd glycosmosglycans
$ mvn clean compile assembly:single
$ java -jar ./target/glycosmosglycans.jar -f 2021-11-12-17-52-GlyTouCan-WURCS.tsv
```
5. Load triplestore

 * EP: https://glyconavi.org/sparql

 * graph: http://glyconavi.org/glytoucan-seq



```sql
select distinct ?e ?glycoct
from <http://glyconavi.org/glytoucan-seq>
where {
?e <http://purl.jp/bio/12/glyco/glycan#has_glycosequence> ?glycoct_seq .
?glycoct_seq <http://purl.jp/bio/12/glyco/glycan#in_carbohydrate_format> <http://purl.jp/bio/12/glyco/glycan#carbohydrate_format_glycoct> .
?glycoct_seq <http://purl.jp/bio/12/glyco/glycan#has_sequence> ?glycoct .
}
limit 100
```

6. get Taxon data from glycosmos

```
$ ./GlyTouCanIDTaxonID.sh > GlyTouCanIDTaxonID.tsv
```

7. Convert tsv to ttl for ID, taxon

```php
$ php id-taxon2ttl.php GlyTouCanIDTaxonID.tsv
```

```php
$ rapper -i turtle -o ntriples GlyTouCanIDTaxonID.tsv.ttl > GlyTouCanIDTaxonID.tsv.ttl.n3
rapper: Parsing URI file:///Users/yamada/git/gitlab/glyconavi/resources/SIB/GlyTouCanIDTaxonID.tsv.ttl with parser turtle
rapper: Serializing with serializer ntriples
rapper: Parsing returned 6324563 triples
```

8. Load triplestore

 * EP: https://glyconavi.org/sparql

 * graph: http://glyconavi.org/glycan-taxon

* isql load

```sql
log_enable(2,1);
SPARQL CLEAR GRAPH <http://glyconavi.org/glycan-taxon>;
```

```sql
DELETE FROM DB.DBA.LOAD_LIST WHERE ll_graph = 'http://glyconavi.org/glycan-taxon';
DELETE FROM DB.DBA.LOAD_LIST;

ld_dir ('/virtuoso/glycan', 'GlyTouCanIDTaxonID.tsv.ttl.gz', 'http://glyconavi.org/glycan-taxon');

rdf_loader_run();
VT_INC_INDEX_DB_DBA_RDF_OBJ();
select * from DB.DBA.LOAD_LIST ORDER BY ll_started DESC;
```

9. get JSON data

* JSON data of human and mouse

#### SPARQL Endpoint

* EP: https://sparql.glyconavi.org/sparql

#### SPARQL
```sql
PREFIX dcterms: <http://purl.org/dc/terms/>
PREFIX glycan: <http://purl.jp/bio/12/glyco/glycan#>
PREFIX glytoucan: <http://www.glytoucan.org/glyco/owl/glytoucan#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

SELECT distinct ?taxon_id (?name AS ?taxon) ?glytoucan ?glycoct ?wurcs
FROM <http://glyconavi.org/glytoucan-seq>
FROM <http://glyconavi.org/glycan-taxon>
WHERE{
# taxon
  ?saccharide  glytoucan:has_primary_id ?glytoucan .
  ?saccharide a ?type .
  ?saccharide glycan:is_from_source ?source.
  ?source a glycan:Source .
  ?source dcterms:identifier ?taxon_id .
  VALUES ?taxon_id {"9606" "10090"}
  ?source skos:prefLabel ?name .
# sequences
  ?saccharide glycan:has_glycosequence ?GlycoSequenceCT , ?GlycoSequenceW .
  ?GlycoSequenceCT glycan:has_sequence ?glycoct .
  ?GlycoSequenceCT glycan:in_carbohydrate_format glycan:carbohydrate_format_glycoct.
  ?GlycoSequenceW glycan:has_sequence ?wurcs .
  ?GlycoSequenceW glycan:in_carbohydrate_format glycan:carbohydrate_format_wurcs.
}
```

### Convert to RDF(ttl) format

* Get JSON file from SPARQL endoint using SPARQL.

* convert from JSON to tsv file.

* convert from tsv to ttl file using 

https://gitlab.com/glyconavi/glytoucan-human-mouse



