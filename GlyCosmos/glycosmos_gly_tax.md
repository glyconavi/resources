
# glycosmos_gly_tax

## Endpoint

https://ts.glycosmos.org/sparql

## `result`

```sparql
# @temp-proxy true
PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
PREFIX glycan:<http://purl.jp/bio/12/glyco/glycan#>
prefix dcterms:  <http://purl.org/dc/terms/>
prefix skos:    <http://www.w3.org/2004/02/skos/core#>
SELECT DISTINCT ?gtc ?tax ?tax_id
WHERE {
  ?saccharide a glycan:Saccharide .
  ?saccharide dcterms:identifier ?gtc .
  ?saccharide glycan:is_from_source ?source .
  ?source a glycan:Source .
  ?source skos:exactMatch ?ncbi .
?ncbi skos:prefLabel ?tax .
?ncbi dcterms:identifier ?tax_id .
#  VALUES ?taxon_name { "Homo sapiens" }
}
order by ?tax ?gtc
```

## `Output`

```javascript
({
  json({result}) {
    let obj = {};
    return result.results.bindings.map((row) => {
         return {
			"GlyTouCan": row.gtc.value , 
	        "Taxon_name": row.tax.value,
      }
    });
  },
  text({result}) {
    return result.results.bindings.map(row => row.up.value).join("\n");
  }
})
```
