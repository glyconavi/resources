# Uniprot

## Lectin: Keyword: keyword:"Lectin [KW-0430]" AND reviewed:yes

* source: https://www.uniprot.org/uniprot/?query=keyword%3A%22Lectin+%5BKW-0430%5D%22+AND+reviewed%3Ayes&sort=score

* graph name: http://glyconavi.org/Uniprot-Lectin-Reviewed-2020-06-05

* graph name: http://glyconavi.org/Uniprot-Lectin-Reviewed

* file: Uniprot-Lectin_KW-0430_AND_reviewed-yes.rdf.gz

## Glycoprotein: keyword:"Glycoprotein [KW-0325]" AND reviewed:yes

* source: https://www.uniprot.org/uniprot/?query=reviewed:yes%20keyword:%22Glycoprotein%20%5BKW-0325%5D%22

* graph name: http://glyconavi.org/Uniprot-Glycoprotein-Reviewed-2020-06-05

* graph name: http://glyconavi.org/Uniprot-Glycoprotein-Reviewed

* file: Uniprot-Glycoprotein_KW-0325_AND_reviewed-yes.rdf.gz

# Load Triple store

* mv Lectin data

```
$ scp Uniprot-Lectin_KW-0430_AND_reviewed-yes.rdf.gz glyconavi@glyconavi.org:/home/glyconavi/glycosite/virtuoso/data/uniprot 
```

* mv Glycoprotein data

```
$ scp Uniprot-Glycoprotein_KW-0325_AND_reviewed-yes.rdf.gz glyconavi@glyconavi.org:/home/glyconavi/glycosite/virtuoso/data/uniprot 
```

* 移動した圧縮ファイルの解凍、確認

```
$ ssh glyconavi@glyconavi.org
$ cd /home/glyconavi/glycosite/virtuoso/data/u/home/glyconavi/glycosite/virtuoso/data/uniprot
[glyconavi@sparqlist uniprot]$ ls -lh
合計 888K
-rw-r--r-- 1 glyconavi glyconavi 850K 10月 24 13:59 Uniprot-Glycoprotein_KW-0325_AND_reviewed-yes.tar.gz
-rw-r--r-- 1 glyconavi glyconavi  35K 10月 24 13:59 Uniprot-Lectin_KW-0430_AND_reviewed-yes.tar.gz
$ tar -zxvf Uniprot-Lectin_KW-0430_AND_reviewed-yes.tar.gz
$ tar -zxvf Uniprot-Glycoprotein_KW-0325_AND_reviewed-yes.tar.gz
[glyconavi@sparqlist uniprot]$ ls -lh
合計 2.4M
drwxr-xr-x 2 glyconavi glyconavi 1016K 10月 24 13:11 Uniprot-Glycoprotein_KW-0325_AND_reviewed-yes
-rw-r--r-- 1 glyconavi glyconavi  850K 10月 24 13:59 Uniprot-Glycoprotein_KW-0325_AND_reviewed-yes.tar.gz
drwxr-xr-x 2 glyconavi glyconavi   48K 10月 23 15:10 Uniprot-Lectin_KW-0430_AND_reviewed-yes
-rw-r--r-- 1 glyconavi glyconavi   35K 10月 24 13:59 Uniprot-Lectin_KW-0430_AND_reviewed-yes.tar.gz
```

* データを入れるグラフのトリプルとグラフの削除

  * Lectin
  
```
log_enable(2,1);
SPARQL CLEAR GRAPH <http://glyconavi.org/Uniprot-Lectin-Reviewed-2020-06-05>;
```

  * Glycoproteins

```
log_enable(2,1);
SPARQL CLEAR GRAPH <http://glyconavi.org/Uniprot-Glycoprotein-Reviewed-2020-06-05>;
```



* isqlによるロード

* Lectin

```
DELETE FROM DB.DBA.LOAD_LIST WHERE ll_graph = 'http://glyconavi.org/Uniprot-Lectin-Reviewed-2020-06-05';
DELETE FROM DB.DBA.LOAD_LIST;

ld_dir ('/virtuoso/uniprot', 'Uniprot-Lectin_KW-0430_AND_reviewed-yes.rdf.gz', 'http://glyconavi.org/Uniprot-Lectin-Reviewed-2020-06-05');

rdf_loader_run();
VT_INC_INDEX_DB_DBA_RDF_OBJ();
select * from DB.DBA.LOAD_LIST ORDER BY ll_started DESC;
```

* Glycoprotein

```
DELETE FROM DB.DBA.LOAD_LIST WHERE ll_graph = 'http://glyconavi.org/Uniprot-Glycoprotein-Reviewed-2020-06-05';
DELETE FROM DB.DBA.LOAD_LIST;

ld_dir ('/virtuoso/uniprot', 'Uniprot-Glycoprotein_KW-0325_AND_reviewed-yes.rdf.gz', 'http://glyconavi.org/Uniprot-Glycoprotein-Reviewed-2020-06-05');

rdf_loader_run();
VT_INC_INDEX_DB_DBA_RDF_OBJ();
select * from DB.DBA.LOAD_LIST ORDER BY ll_started DESC;
```









* prefix

```
@base <http://purl.uniprot.org/uniprot/> .
@prefix annotation: <http://purl.uniprot.org/annotation/> .
@prefix citation: <http://purl.uniprot.org/citations/> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix disease: <http://purl.uniprot.org/diseases/> .
@prefix ECO: <http://purl.obolibrary.org/obo/ECO_> .
@prefix enzyme: <http://purl.uniprot.org/enzyme/> .
@prefix faldo: <http://biohackathon.org/resource/faldo#> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix go: <http://purl.obolibrary.org/obo/GO_> .
@prefix isoform: <http://purl.uniprot.org/isoforms/> .
@prefix keyword: <http://purl.uniprot.org/keywords/> .
@prefix location: <http://purl.uniprot.org/locations/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix position: <http://purl.uniprot.org/position/> .
@prefix pubmed: <http://purl.uniprot.org/pubmed/> .
@prefix range: <http://purl.uniprot.org/range/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix taxon: <http://purl.uniprot.org/taxonomy/> .
@prefix tissue: <http://purl.uniprot.org/tissues/> .
@prefix up: <http://purl.uniprot.org/core/> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
```



* Predicate list

```
prefix up: <http://purl.uniprot.org/core/>
prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
prefix skos: <http://www.w3.org/2004/02/skos/core#>
prefix faldo: <http://biohackathon.org/resource/faldo#>
prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
prefix foaf: <http://xmlns.com/foaf/0.1/>
prefix owl: <http://www.w3.org/2002/07/owl#>

rdf:type
rdfs:label
rdfs:comment
rdfs:seeAlso
owl:sameAs
up:locator
up:catalyzedPhysiologicalReaction
dcterms:identifier
up:encodedBy
up:recommendedName
skos:prefLabel
up:fullName
up:annotation
up:organism
rdf:value
up:range
faldo:reference
up:enzyme
up:orientation
up:fragment
up:measuredActivity
up:measuredAffinity
up:precursor
up:orfName
up:sequenceDiscrepancy
up:locusName
up:place
up:group
up:institution
up:measuredValue
up:experiments
up:interaction
up:participant
up:xeno
up:cdAntigenName
up:allergenName
up:measuredError
up:encodedIn
skos:altLabel
skos:exactMatch
faldo:position
faldo:begin
faldo:end
skos:closeMatch
rdf:object
rdf:predicate
rdf:subject
dcterms:creator
foaf:primaryTopicOf
up:alternativeName
up:attribution
up:author
up:basedOn
up:catalyticActivity
up:catalyzedReaction
up:cellularComponent
up:chain
up:chainSequenceMapping
up:citation
up:classifiedWith
up:cofactor
up:component
up:conflictingSequence
up:context
up:created
up:database
up:date
up:disease
up:ecName
up:enzymeClass
up:evidence
up:existence
up:isolatedFrom
up:locatedIn
up:locatedOn
up:manual
up:mass
up:method
up:mnemonic
up:modification
up:modified
up:name
up:oldMnemonic
up:pages
up:potentialSequence
up:proteome
up:replaces
up:resolution
up:reviewed
up:scope
up:sequence
up:shortName
up:signatureSequenceMatch
up:source
up:submittedTo
up:substitution
up:title
up:topology
up:transcribedFrom
up:translatedFrom
up:translatedTo
up:version
up:volume
up:md5Checksum
skos:related
```



## SPARQL

* prefix

```
base <http://purl.uniprot.org/uniprot/>
prefix annotation: <http://purl.uniprot.org/annotation/>
prefix citation: <http://purl.uniprot.org/citations/>
prefix dcterms: <http://purl.org/dc/terms/>
prefix disease: <http://purl.uniprot.org/diseases/>
prefix ECO: <http://purl.obolibrary.org/obo/ECO_>
prefix enzyme: <http://purl.uniprot.org/enzyme/>
prefix faldo: <http://biohackathon.org/resource/faldo#>
prefix foaf: <http://xmlns.com/foaf/0.1/>
prefix go: <http://purl.obolibrary.org/obo/GO_>
prefix isoform: <http://purl.uniprot.org/isoforms/>
prefix keyword: <http://purl.uniprot.org/keywords/>
prefix location: <http://purl.uniprot.org/locations/>
prefix owl: <http://www.w3.org/2002/07/owl#>
prefix position: <http://purl.uniprot.org/position/>
prefix pubmed: <http://purl.uniprot.org/pubmed/>
prefix range: <http://purl.uniprot.org/range/>
prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
prefix skos: <http://www.w3.org/2004/02/skos/core#>
prefix taxon: <http://purl.uniprot.org/taxonomy/>
prefix tissue: <http://purl.uniprot.org/tissues/>
prefix up: <http://purl.uniprot.org/core/>
prefix xsd: <http://www.w3.org/2001/XMLSchema#>
```


