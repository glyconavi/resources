<?php

header('Content-Type: text/plain;charset=UTF-8');
ini_set('auto_detect_line_endings', 1);
date_default_timezone_set('Asia/Tokyo');
$fileHeader = date("Y-m-d_H-i-s");

$file = $argv[1];


$path = basename($file, ".txt");


if (mkdir($path)) {
  echo "OK\n";
} else {
  echo "エラー\n";
}

$list = file_get_contents($file);

$text = str_replace(array("\r\n","\r"), "\n", $list);


$array = explode("\n", $text);

$count = 1;
foreach ($array  as $id) {

  try {

  $context = stream_context_create(array(
      'http' => array('ignore_errors' => true, 'timeout' => 120)
  ));


  $url = "https://www.uniprot.org/uniprot/".$id.".ttl";
  $ttl = file_get_contents($url, false, $context);

  $filename = $path.DIRECTORY_SEPARATOR.$id.'.ttl';
  file_put_contents($filename,$ttl);

  echo $count."\t".$id."\n";

  $logfilename = $path.DIRECTORY_SEPARATOR.'log.txt';
  file_put_contents($filename,$count."\t".$id);

  
  }
  catch (Exception $ex){
    $filename = $path.DIRECTORY_SEPARATOR.$id.'.err.txt';
    file_put_contents($filename,$id."\n".$ex."\n");

   $logfilename = $path.DIRECTORY_SEPARATOR.'log.err.txt';
   file_put_contents($filename,$count."\t".$id);
  }
  $count++;
}


echo "Fin";

?>
