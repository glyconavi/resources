#!/usr/bin/env bash

EP=https://sparql.glyconavi.org/sparql
LIMIT=500

function query() {
  local offset=${1:-0}
  shift

  local sparql=$(cat <<EOS | sed -e 's/^\s\+//' | tr '\n' ' '
SELECT DISTINCT ?ID ?TrivialName
FROM <http://rdf.glycoinfo.org/can>
WHERE {
 ?s <http://purl.org/dc/terms/identifier> ?ID .
 ?s <http://semanticscience.org/resource/SIO_000008> ?triv .
 ?s <http://semanticscience.org/resource/SIO_000008>/<http://purl.jp/bio/12/glyco/glycan/has_glycan> ?str .
 ?triv <http://semanticscience.org/resource/SIO_000300> ?TrivialName .
}
LIMIT ${LIMIT}
OFFSET ${offset}
EOS
)

  curl -XPOST -H 'Accept: text/tab-separated-values' --data-urlencode "query=${sparql}" ${EP} 2>/dev/null | sed '1d'

  return $?
}

function query_recursive() {
  local offset=${1:-0}
  shift

  result=$(query ${offset})

  local status=$?
  if [ ${status} -ne 0 ]; then
    printf "\ncurl returned exit code %s\n" ${status} >&2
    exit ${status}
  fi

  if [ -n "${result}" ]; then
    printf "\rObtained %s entries..." $((offset + $(echo -e "${result}" | wc -l))) >&2
    echo -e "${result}"
    query_recursive $((offset + LIMIT))
  else
    printf "\n" >&2
  fi
}

query_recursive

