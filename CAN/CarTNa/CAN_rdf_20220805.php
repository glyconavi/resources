<?php
header('Content-Type: text/plain;charset=UTF-8');
//ini_set('auto_detect_line_endings', 1);

$tsvfile1 = "20151211_CarTNa-data-id.tsv.2.CAN.tsv";
$tsvfile2 = "20151211_CarTNa-data-trivialname.tsv.2.CAN.tsv";
$tsvfile3 = "20160113_CarTNa-data-IUPAC-WURCS.tsv.2.CAN.tsv";

date_default_timezone_set('Asia/Tokyo');
//$fileHeader = date("Y-m-d_H-i-s");
$fileHeader = date("Y-m-d");
$wfp = fopen(dirname(__FILE__).DIRECTORY_SEPARATOR."CAN_".$fileHeader."_RDF.ttl", "a");

fwrite($wfp, "@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> ."."\n");
fwrite($wfp, "@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> ."."\n");
fwrite($wfp, "@prefix void:  <http://rdfs.org/ns/void#> ."."\n");
fwrite($wfp, "@prefix pav:   <http://purl.org/pav/> ."."\n");
fwrite($wfp, "@prefix dcat:  <http://www.w3.org/ns/dcat#> ."."\n");
fwrite($wfp, "@prefix foaf: <http://xmlns.com/foaf/0.1/> ."."\n");
fwrite($wfp, "@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> ."."\n");
fwrite($wfp, "@prefix owl:   <http://www.w3.org/2002/07/owl#> ."."\n");
fwrite($wfp, "@prefix dcterms: <http://purl.org/dc/terms/> ."."\n");
fwrite($wfp, "@prefix skos: <http://www.w3.org/2004/02/skos/core#> ."."\n");
fwrite($wfp, "@prefix sio: <http://semanticscience.org/resource/> ."."\n");
fwrite($wfp, "@prefix glycan: <http://purl.jp/bio/12/glyco/glycan#> ."."\n");
fwrite($wfp, "@prefix faldo: <http://biohackathon.org/resource/faldo#> ."."\n");
fwrite($wfp, "@prefix can: <http://glyconavi.org/CAN/> ."."\n");

fwrite($wfp, "\n");

fwrite($wfp, "<http://glyconavi.org/CAN>
        a                           void:Dataset ;
        dcterms:description        \"GlycoNAVI-CAN: Carbohydrate Name -version ".$fileHeader."\"@en ;
        dcterms:license             <http://creativecommons.org/licenses/by/4.0/> ;
        dcterms:publisher           <http://glyconavi.org> ;
        dcterms:title              \"GlycoNAVI-CAN RDF\"@en ;
        pav:contributedBy           <https://orcid.org/0000-0001-9504-189X> ;
        pav:version                \"0.5\";
        dcterms:hasVersion         <http://glyconavi.org/CAN/0.5> ;
        dcat:landingPage            <http://glyconavi.org/CAN> .

<https://orcid.org/0000-0001-9504-189X>
        a                 foaf:Person ;
        foaf:family_name \"YAMADA\";
        foaf:givenname   \"ISSAKU\";
        foaf:mbox         <mailto:issaku@noguchi.or.jp> .
\n");

// Data-id
//$predicatelist = array();
$objectType = array();
$lines = array();
$lines = file ($tsvfile1);
$count = 0;
foreach ($lines as $line){
//	echo $line;

	$line = str_replace("\n", "", trim($line));

    if ($count == 0) {
	    $predicates = explode("\t", $line);
//        foreach ($predicates as $pre) {
//        	array_push($predicatelist, $pre);
//        }
    }
    if ($count == 1) {
	    $objectType = explode("\t", $line);
//        foreach ($objectType as $objtype) {
//        	array_push($objectType, $objtype);
//        }
    }
    if ($count > 1) {

    	$objects = explode("\t", $line);
    	if (count($objects) == 4) {

    		if ($predicates[0] == "Subject"){
    			$index = 0;
				fwrite($wfp, $objectType[$index].trim($objects[$index])."\n");
				fwrite($wfp, "a void:rootResource ;\n");
    		}
    		if ($predicates[1] == "dcterms:identifier"){
				$index = 1;
				fwrite($wfp, "\t".$predicates[$index]."\t\"".trim($objects[$index])."\" ;\n");
    		}
    		if ($predicates[2] == "rdfs:label"){
				$index = 2;
				fwrite($wfp, "\t".$predicates[$index]."\t\"".trim($objects[$index])."\" ;\n");
    		}
    		if ($predicates[3] == "sio:SIO_000008"){
				$index = 3;
				fwrite($wfp, "\t".$predicates[$index]."\t"."<http://glyconavi.org/CAN/".urlencode(trim($objects[0]))."/".urlencode(trim($objects[$index]))."> ;\n");
				fwrite($wfp, "\t".$predicates[$index]."\t"."<http://glyconavi.org/CAN/".urlencode(trim($objects[1]))."/trivialname> .\n\n");

    		}
		}
    }
    $count++;
}

// Data-trivialname
//$predicatelist = array();
$objectType = array();
$lines = array();
$lines = file ($tsvfile2);
$count = 0;
foreach ($lines as $line){
//	echo $line;

	$line = str_replace("\n", "", trim($line));

    if ($count == 0) {
	    $predicates = explode("\t", $line);
//        foreach ($predicates as $pre) {
//        	array_push($predicatelist, $pre);
//        }
    }
    if ($count == 1) {
	    $objectType = explode("\t", $line);
//        foreach ($objectType as $objtype) {
//        	array_push($objectType, $objtype);
//        }
    }
    if ($count > 1) {

    	$objects = explode("\t", $line);
    	if (count($objects) == 7) {

    		if ($predicates[0] == "Subject"){
    			$index = 0;
//				fwrite($wfp, $objectType[$index].urlencode(trim($objects[$index]))."\n");
				fwrite($wfp, "<http://glyconavi.org/CAN/".urlencode(trim($objects[$index]))."/trivialname>\n");
				
    		}
    		if ($predicates[1] == "sio:SIO_000300"){
				$index = 1;
				fwrite($wfp, "\t".$predicates[$index]."\t\"".trim($objects[$index])."\" ;\n");
    		}
    		if ($predicates[2] == "rdfs:label"){
				$index = 2;
				fwrite($wfp, "\t".$predicates[$index]."\t\""."trivial name of ".trim($objects[$index])."\" ;\n");
    		}
    		if ($predicates[3] == "a"){
				$index = 3;
				fwrite($wfp, "\t".$predicates[$index]."\t".$objectType[$index].trim($objects[$index])." ;\n");
    		}    		
    		if ($predicates[4] == "sio:CHEMINF_000106"){
				$index = 4;
				if (strlen($objects[$index]) > 0) {
					fwrite($wfp, "\t".$predicates[$index]."\t\"".trim($objects[$index])."\" ;\n");
    			}
    		}
    		if ($predicates[5] == "skos:altLabel"){
				$index = 5;
				if (strlen($objects[$index]) > 0) {
					fwrite($wfp, "\t".$predicates[$index]."\t\"".trim($objects[$index])."\" ;\n");
    			}
    		}
    		if ($predicates[6] == "rdfs:isDefinedBy"){
				$index = 6;
				fwrite($wfp, "\t".$predicates[$index]."\t<".trim($objects[$index])."> .\n\n");
    		}    		
		}
    }
    $count++;
}

// Data-IUPAC-WURCS
//$predicatelist = array();
$objectType = array();
$lines = array();
$lines = file ($tsvfile3);
$count = 0;
foreach ($lines as $line){
//	echo $line;

	$line = str_replace("\n", "", trim($line));

    if ($count == 0) {
	    $predicates = explode("\t", $line);
//        foreach ($predicates as $pre) {
//        	array_push($predicatelist, $pre);
//        }
    }
    if ($count == 1) {
	    $objectType = explode("\t", $line);
//        foreach ($objectType as $objtype) {
//        	array_push($objectType, $objtype);
//        }
    }
    if ($count > 1) {

    	$objects = explode("\t", $line);
    	if (count($objects) == 6) {

    		if ($predicates[0] == "Subject"){
    			$index = 0;

    			$molecule_type = "saccharide";
        $molecule_class = "Saccahride";
				$glycan_type = "glycosequence";
        $glycan_class = "Glycosequence";
				if (strlen(trim($objects[3])) > 0){
//					$glycan_type = "glycoconjugate_sequence";
					$molecule_type = "glycoconjugate";
          $molecule_class = "Glycoconjugate";
				}

				fwrite($wfp, "<http://glyconavi.org/CAN/".urlencode(trim($objects[$index]))."/".urlencode(trim($objects[1])).">\n");
				fwrite($wfp, "\trdfs:label\t\"".$molecule_type." ".trim($objects[1])."\" ;\n");
				fwrite($wfp, "\tglycan:has_glycan\t<http://glyconavi.org/CAN/".urlencode(trim($objects[$index]))."/".urlencode(trim($objects[1]))."/carbohydrate> ;\n");
				fwrite($wfp, "\tsio:has_component-part\t<http://glyconavi.org/CAN/".urlencode(trim($objects[$index]))."/".urlencode(trim($objects[1]))."/carbohydrate> ;\n");
				if (strlen(trim($objects[3])) > 0){
					fwrite($wfp, "\tglycan:has_aglycon <http://glyconavi.org/CAN/".urlencode(trim($objects[$index]))."/".urlencode(trim($objects[1]))."/aglycon> ;\n");
					fwrite($wfp, "\tsio:has_component-part <http://glyconavi.org/CAN/".urlencode(trim($objects[$index]))."/".urlencode(trim($objects[1]))."/aglycon> ;\n");
				}
				fwrite($wfp, "\ta glycan:".$molecule_class." .\n\n");


				if (strlen(trim($objects[3])) > 0){
					fwrite($wfp, "<http://glyconavi.org/CAN/".urlencode(trim($objects[$index]))."/".urlencode(trim($objects[1]))."/carbohydrate>\n");
					fwrite($wfp, "\tfaldo:location\t<http://glyconavi.org/CAN/".urlencode(trim($objects[$index]))."/".urlencode(trim($objects[1]))."/carbohydrate#loc> ;\n");
					fwrite($wfp, "\tsio:is-connect-to <http://glyconavi.org/CAN/".urlencode(trim($objects[$index]))."/".urlencode(trim($objects[1]))."/aglycon> .\n\n");

					fwrite($wfp, "<http://glyconavi.org/CAN/".urlencode(trim($objects[$index]))."/".urlencode(trim($objects[1]))."/aglycon>\n");
					fwrite($wfp, "\tfaldo:location\t<http://glyconavi.org/CAN/".urlencode(trim($objects[$index]))."/".urlencode(trim($objects[1]))."/aglycon#loc> ;\n");
					fwrite($wfp, "\tsio:is-connect-to <http://glyconavi.org/CAN/".urlencode(trim($objects[$index]))."/".urlencode(trim($objects[1]))."/carbohydrate> .\n\n");

					fwrite($wfp, "<http://glyconavi.org/CAN/".urlencode(trim($objects[$index]))."/".urlencode(trim($objects[1]))."/carbohydrate#loc>\n");
					fwrite($wfp, "\ta faldo:ExactPosition ;\n");
					fwrite($wfp, "\tfaldo:position 1 ;\n");
					fwrite($wfp, "\tsio:direct-connect-to\t<http://glyconavi.org/CAN/".urlencode(trim($objects[$index]))."/".urlencode(trim($objects[1]))."/aglycon#loc> .\n\n");
				}

				fwrite($wfp, "<http://glyconavi.org/CAN/".urlencode(trim($objects[$index]))."/".urlencode(trim($objects[1]))."/carbohydrate>\n");
				fwrite($wfp, "\ta glycan:Saccharide ;\n");


				if (strlen(trim($objects[2])) > 0){
					fwrite($wfp, "\tglycan:has_".$glycan_type." <http://glyconavi.org/CAN/".urlencode(trim($objects[$index]))."/".urlencode(trim($objects[1]))."/iupac_condensed> ;\n");
				}
//				if (strlen(trim($objects[3])) > 0){
//					fwrite($wfp, "\tglycan:has_aglycon <http://glyconavi.org/CAN/".trim($objects[$index])."/".urlencode(trim($objects[1]))."/aglycon> ;\n");
//				}
				if (strlen(trim($objects[4])) > 0){
					fwrite($wfp, "\tglycan:has_".$glycan_type." <http://glyconavi.org/CAN/".urlencode(trim($objects[$index]))."/".urlencode(trim($objects[1]))."/carbohydrate_format> ;\n");
				}
				if (strlen(trim($objects[5])) > 0){
					fwrite($wfp, "\tglycan:has_".$glycan_type." <http://glyconavi.org/CAN/".urlencode(trim($objects[$index]))."/".urlencode(trim($objects[1]))."/wurcs> ;\n");
				}
//				fwrite($wfp, "\trdfs:label \"".trim($objects[$1])."\" .\n\n");
				fwrite($wfp, "\trdfs:label\t\"saccharide ".trim($objects[1])."\" .\n\n");
				
    		}
    		if ($predicates[2] == "glycan:carbohydrate_format_iupac_condensed"){
				$index = 2;
				if (strlen(trim($objects[2])) > 0){
					fwrite($wfp, "<http://glyconavi.org/CAN/".urlencode(trim($objects[0]))."/".urlencode(trim($objects[1]))."/iupac_condensed>\n");
					fwrite($wfp, "\ta glycan:".$glycan_class." ;\n");
					fwrite($wfp, "\tglycan:in_carbohydrate_format glycan:carbohydrate_format_iupac_condensed ;\n");
					fwrite($wfp, "\tglycan:has_sequence\t\"".trim($objects[$index])."\" .\n\n");
    			}
    		}
    		if ($predicates[3] == "glycan:has_aglycon"){
				$index = 3;
				if (strlen(trim($objects[3])) > 0){
					fwrite($wfp, "<http://glyconavi.org/CAN/".urlencode(trim($objects[0]))."/".urlencode(trim($objects[1]))."/aglycon>\n");
					fwrite($wfp, "\ta glycan:Aglycon ;\n");

					fwrite($wfp, "\tsio:SIO_000300\t\"".trim($objects[$index])."\" ; \n");
					if (trim($objects[$index]) == "Cer") {
						fwrite($wfp, "\tskos:altLabel\t\"Ceramide\" ; \n");
					}
					if (trim($objects[$index]) == "Sph") {
						fwrite($wfp, "\tskos:altLabel\t\"Sphingosine\" ; \n");
					}
					if (trim($objects[$index]) == "IPC") {
						fwrite($wfp, "\tskos:altLabel\t\"Inositolphosphoceramide\" ; \n");
					}
					if (trim($objects[$index]) == "DAG") {
						fwrite($wfp, "\tskos:altLabel\t\"Diacylglycerol\" ; \n");
					}
					
					fwrite($wfp, "\trdfs:label\t\"".trim($objects[$index])." is an aglycon of ".trim($objects[1])."\" .\n\n");

//					fwrite($wfp, "\tsio:SIO_000008\t[ sio:SIO_000300\t\"".trim($objects[$index])."\" ; ");
//					fwrite($wfp, "\trdfs:label\t\"".trim($objects[$index])." is an aglycon of ".trim($objects[0])."\" ] .\n\n");

    			}
    		}
    		if ($predicates[4] == "glycan:carbohydrate_format"){
				$index = 4;
				if (strlen(trim($objects[4])) > 0){
					fwrite($wfp, "<http://glyconavi.org/CAN/".urlencode(trim($objects[0]))."/".urlencode(trim($objects[1]))."/carbohydrate_format>\n");
					fwrite($wfp, "\ta glycan:".$glycan_class." ;\n");
					fwrite($wfp, "\tglycan:in_carbohydrate_format glycan:carbohydrate_format ;\n");
					fwrite($wfp, "\tglycan:has_sequence\t\"".trim($objects[$index])."\" .\n\n");
    			}
    		}
    		if ($predicates[5] == "glycan:carbohydrate_format_wurcs"){
				$index = 5;
				if (strlen(trim($objects[5])) > 0){
					fwrite($wfp, "<http://glyconavi.org/CAN/".urlencode(trim($objects[0]))."/".urlencode(trim($objects[1]))."/wurcs>\n");
					fwrite($wfp, "\ta glycan:".$glycan_class." ;\n");
					fwrite($wfp, "\tglycan:in_carbohydrate_format glycan:carbohydrate_format_wurcs ;\n");
					fwrite($wfp, "\tglycan:has_sequence\t\"".trim($objects[$index])."\" .\n\n");
    			}
    		}

/*
    		if ($predicates[6] == "glycan:carbohydrate_format_glycoct_condensed"){
				$index = 6;
				if (strlen(trim($objects[6])) > 0){
					fwrite($wfp, "<http://glyconavi.org/CAN/".trim($objects[0])."/".urlencode(trim($objects[1]))."/glycoct>\n");
					fwrite($wfp, "\ta glycan:".$glycan_type." ;\n");
					fwrite($wfp, "\tglycan:in_carbohydrate_format glycan:carbohydrate_format_glycoct_condensed ;\n");
					fwrite($wfp, "\tglycan:has_sequence\t\"".trim($objects[$index])."\" .\n\n");
    			}
    		}
*/


		}




    }
    $count++;
}


fclose($wfp);
?>
