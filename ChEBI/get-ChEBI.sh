#!/usr/bin/env bash

EP=https://test.sparql.glyconavi.org/sparql
LIMIT=500

function query() {
  local offset=${1:-0}
  shift

  local sparql=$(cat <<EOS | sed -e 's/^\s\+//' | tr '\n' ' '
prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
prefix : <http://purl.obolibrary.org/obo/chebi.owl#>
prefix chebi1: <http://purl.obolibrary.org/obo/chebi#2>
prefix chebi2: <http://purl.obolibrary.org/obo/chebi#>
prefix chebi: <http://purl.obolibrary.org/obo/chebi/>
prefix chebi3: <http://purl.obolibrary.org/obo/chebi#3>
prefix owl: <http://www.w3.org/2002/07/owl#>
prefix oboInOwl: <http://www.geneontology.org/formats/oboInOwl#>
prefix chebi4: <http://purl.obolibrary.org/obo/chebi#1>
prefix xml: <http://www.w3.org/XML/1998/namespace>
prefix xsd: <http://www.w3.org/2001/XMLSchema#>
prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
prefix obo: <http://purl.obolibrary.org/obo/>
select 
?number str(?o) as ?db str(?inchikey) as ?InChIKey str(?inchi) as ?InChI
from <http://glyconavi.org/chebi.ttl>
where {
?chebi oboInOwl:hasDbXref ?o .
?chebi chebi:inchi ?inchi .
?chebi chebi:inchikey ?inchikey . 
BIND(REPLACE(STR(?chebi),"http://purl.obolibrary.org/obo/CHEBI_","") as ?num) .
BIND(concat("ChEBI:" ,?num) as ?number) .
}
LIMIT ${LIMIT}
OFFSET ${offset}
EOS
)

  curl -XPOST -H 'Accept: text/tab-separated-values' --data-urlencode "query=${sparql}" ${EP} 2>/dev/null | sed '1d'

  return $?
}

function query_recursive() {
  local offset=${1:-0}
  shift

  result=$(query ${offset})

  local status=$?
  if [ ${status} -ne 0 ]; then
    printf "\ncurl returned exit code %s\n" ${status} >&2
    exit ${status}
  fi

  if [ -n "${result}" ]; then
    printf "\rObtained %s entries..." $((offset + $(echo -e "${result}" | wc -l))) >&2
    echo -e "${result}"
    query_recursive $((offset + LIMIT))
  else
    printf "\n" >&2
  fi
}

query_recursive

