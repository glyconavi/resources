#!/bin/bash

files="./*"
for f in *.sdf.gz
  do
  gzcat $f | java -jar MolWURCS.jar -i sdf -o wurcs 1> ./result/${f}.std.out.log 2> ./result/${f}.std.err.log 
done
