# ChEBI


## ttl

```php
$ php ChEBI-DBs-InChIKey-InChI.csv2ttl.php ChEBI-DBs-InChIKey-InChI.csv
```

### result

* ChEBI-DBs-InChIKey-InChI.csv.ttl

### check

```
$ rapper -i turtle -o ntriples ChEBI-DBs-InChIKey-InChI.csv.ttl > ChEBI-DBs-InChIKey-InChI.csv.ttl.n3
rapper: Parsing URI file:///Users/yamada/git/gitlab/glyconavi/resources/ChEBI/ChEBI-DBs-InChIKey-InChI.csv.ttl with parser turtle
rapper: Serializing with serializer ntriples
rapper: Parsing returned 4588524 triples
```

### file copy

```
scp ChEBI-DBs-InChIKey-InChI.csv.ttl.gz glyconavi@test.glyconavi.org:/home/glyconavi/glycosite/virtuoso/data/chebi
```

### rdf load

```
log_enable(2,1);
SPARQL CLEAR GRAPH <http://glyconavi.org/chebi-db-inchi>;
```

```
DELETE FROM DB.DBA.LOAD_LIST WHERE ll_graph = 'http://glyconavi.org/chebi-db-inchi';
DELETE FROM DB.DBA.LOAD_LIST;

ld_dir ('/virtuoso/chebi', 'ChEBI-DBs-InChIKey-InChI.csv.ttl.gz', 'http://glyconavi.org/chebi-db-inchi');

rdf_loader_run();
VT_INC_INDEX_DB_DBA_RDF_OBJ();
select * from DB.DBA.LOAD_LIST ORDER BY ll_started DESC;

```

# GlyTouCan Partner

* GlyTouCan ID and External DB ID (GlycoNAVI)

* EP: https://sparql.glyconavi.org/sparql

```
SELECT distinct ?gtc ?type ?bd_id
FROM <http://glyconavi.org/chebi-db-inchi>
WHERE {
?s ?p ?o1, ?o.
?o a <http://identifiers.org/glyconavi/GN_GlyTouCan> .
?o <http://purl.org/dc/terms/identifier> ?gtc .
?o1 <http://purl.org/dc/terms/identifier> ?id .
?o1 a ?type .
FILTER (?gtc != ?id )

BIND (URI(CONCAT(str(?type),"_")) as ?db)
BIND (URI(CONCAT(str(?db),?id)) as ?dbId)
BIND( REPLACE( str(?dbId), "http://identifiers.org/glyconavi/" , "" ) AS ?bd_id )

}
```


