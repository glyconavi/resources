<?php
ini_set("memory_limit", "1000M");
setlocale(LC_ALL,'ja_JP.UTF-8');
$sdffile = "ChEBI_complete_3star.sdf";

if (count($argv)>0){
    $sdffile = $argv[1];
}

$row = 1;
// ファイルが存在しているかチェックする
if (($handle = fopen($sdffile, "r")) !== FALSE) {


$ttl = "@prefix base: <http://identifiers.org/CHEBI:> .".PHP_EOL;
$ttl .= "@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .".PHP_EOL;
$ttl .= "@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .".PHP_EOL;
$ttl .= "@prefix dcterms: <http://purl.org/dc/terms/> .".PHP_EOL;

$ttl .= "@prefix ChEBI: <http://identifiers.org/CHEBI/> .".PHP_EOL;
$ttl .= "@prefix GlyTouCan: <http://identifiers.org/glytoucan/> .".PHP_EOL;
$ttl .= "@prefix KNApSAcK: <http://identifiers.org/knapsack/> .".PHP_EOL;
$ttl .= "@prefix KEGG: <http://identifiers.org/kegg.compound/> .".PHP_EOL;
$ttl .= "@prefix CAS: <http://identifiers.org/cas/> .".PHP_EOL;
$ttl .= "@prefix LINCS: <http://identifiers.org/lincs.smallmolecule/> .".PHP_EOL;
$ttl .= "@prefix DrugBank: <http://identifiers.org/drugbank/> .".PHP_EOL;
$ttl .= "@prefix PDBeChem: <http://identifiers.org/pdb-ccd/> .".PHP_EOL;
$ttl .= "@prefix PMID: <http://identifiers.org/pubmed/> .".PHP_EOL;
$ttl .= "@prefix HMDB: <http://identifiers.org/hmdb/> .".PHP_EOL;
$ttl .= "@prefix Wikipedia: <http://identifiers.org/wikipedia.en/> .".PHP_EOL;
$ttl .= "@prefix LIPID_MAPS_instance: <http://identifiers.org/lipidmaps/> .".PHP_EOL;
$ttl .= "@prefix Chemspider: <http://identifiers.org/chemspider/> .".PHP_EOL;
$ttl .= "@prefix Pubchem: <http://identifiers.org/pubchem.compound/> .".PHP_EOL;
$ttl .= "@prefix PDB: <http://identifiers.org/pdb/> .".PHP_EOL;
$ttl .= "@prefix MetaCyc: <http://identifiers.org/metacyc.compound/> .".PHP_EOL;
$ttl .= "@prefix SIO: <http://semanticscience.org/resource/> .".PHP_EOL;
$ttl .= "@prefix InChIKey: <http://identifiers.org/inchikey/> .".PHP_EOL;
$ttl .= "@prefix InChI: <http://identifiers.org/inchi/> .".PHP_EOL;



$ttl .= "@prefix db: <http://identifiers.org/glyconavi/GN_> .".PHP_EOL;

// URL TEMPLATE
//$ChEBIURL = "http://identifiers.org/CHEBI:"; // https://identifiers.org/CHEBI:36927
// URI TEMPLATE
//$ChEBIURI = "http://identifiers.org/CHEBI/"; // https://identifiers.org/CHEBI:36927
//$GlyTouCanURI = "http://identifiers.org/glytoucan/"; // https://identifiers.org/glytoucan:G00054MO
//$KNApSAcKURI = "http://identifiers.org/knapsack/"; // https://identifiers.org/knapsack:C00000001
//$KEGGURI = "http://identifiers.org/kegg.compound/"; // https://identifiers.org/kegg.compound:C12345
//$CASURI = "http://identifiers.org/cas/";  // https://identifiers.org/cas:50-00-0
//$LINCSURI = "http://identifiers.org/lincs.smallmolecule/"; // https://identifiers.org/lincs.smallmolecule:LSM-6306
//$DrugBankURI = "http://identifiers.org/drugbank/"; // https://identifiers.org/drugbank:DB00001
//$PDBeChemURI = "http://identifiers.org/pdb-ccd/"; // https://identifiers.org/pdb-ccd:AB0
//$PMIDURI = "http://identifiers.org/pubmed/"; //   https://identifiers.org/pubmed:16333295
//$HMDBURI = "http://identifiers.org/hmdb/"; //   https://identifiers.org/hmdb:HMDB00001
//$WikipediaURI = "http://identifiers.org/wikipedia.en/"; // https://identifiers.org/wikipedia.en:SM_UB-81
//$LIPID_MAPS_instanceURI = "http://identifiers.org/lipidmaps/";  // https://identifiers.org/lipidmaps:LMPR0102010012
//$ChemspiderURI = "http://identifiers.org/chemspider/"; // https://identifiers.org/chemspider:56586
//$PubchemURI = "http://identifiers.org/pubchem.compound/";  // https://identifiers.org/pubchem.compound:100101
//$PDBURI = "http://identifiers.org/pdb/"; // https://identifiers.org/pdb:2gc4

// https://identifiers.org/metacyc.compound:CPD-10330

// <a RDF data>` dcterms:license <http://creativecommons.org/licenses/by-sa/4.0/> . 
// <http://creativecommons.org/licenses/by-sa/4.0/> a <http://purl.org/dc/terms/LicenseDocument> .
// ex:111 rdfs:seeAlso <http://identifiers.org/pfam/PF01590> . 
// pdb:2gc4 dcterms:identifier "2gc4" . 
// <a resource> dcterms:references pubmed:24495517 .
// pubmed:24495517 a bibo:Article .

    $footerHeadArray = array();
    try {
        $filedata = file_get_contents($sdffile);
        //echo $filedata.PHP_EOL;
        $sdfmols = explode('$$$$', $filedata);
        $num =0;
        foreach ( $sdfmols as $sdf ) {
            $moldata = explode("M  END", $sdf);
            $count = 0;
            foreach ( $moldata as $data ) {
                if ($count == 0){
                    $mol = $data;
                }
                $count++;
                $footerdata = explode("> <", $data);

                $footerHead = "";
                //$footerHeadArray = array();
                foreach ( $footerdata as $footer ) {
                    //echo "footer:".$footer.PHP_EOL;
                    $fdata = explode(">\n", $footer);
                    if (count($fdata)>1){
                        $footerHead = $fdata[0];
                        //$footerHead = trim(str_replace(" Database Links", "", $footerHead));
                        //  Registry Numbers
                        //$footerHead = trim(str_replace(" Registry Numbers", "", $footerHead));
                        //  citation Links
                        //$footerHead = trim(str_replace(" citation Links", "", $footerHead));
                        // ChEBI ID
                        //$footerHead = trim(str_replace("ChEBI ID", "ChEBI", $footerHead));
                        // LIPID MAPS instance
                        //$footerHead = trim(str_replace("LIPID MAPS instance", "LIPIDMAPS", $footerHead));
                        // PDBeChem
                        //$footerHead = trim(str_replace("PDBeChem", "PDBChem", $footerHead));
                        // " " -> "_"
                        //$footerHead = trim(str_replace(" ", "-", $footerHead));

                        //echo "footerHead:\t".$footerHead.PHP_EOL;
                        array_push($footerHeadArray,$footerHead);

                        $footerContents = $fdata[1];
                        $footerContent = explode("\n", $footerContents);
                        foreach ( $footerContent as $Content ) {
                            if ($Content !== ""){
                                //echo "\tContent:\t".$Content.PHP_EOL;
                            }
                        }

                        /*
ACToR Database Links
Agricola citation Links
ArrayExpress Database Links
ArrayExpressAtlas Database Links
BKMS-react Database Links
BPDB Database Links
BRAND Names
BRENDA Database Links
BRENDALigand Database Links
Beilstein Registry Numbers
BindingDB Database Links
BioModels Database Links
CAS Registry Numbers
COMe Database Links
CarotenoidsDatabase Database Links
ChEBI ID
ChEBI Name
ChEMBL Database Links
Charge
ChemIDplus Database Links
Chemspider Database Links
Chinese Abstracts citation Links
CiteXplore citation Links
CompTox Database Links
Definition
Drug Central Database Links
DrugBank Database Links
ECMDB Database Links
EnzymePortal Database Links
FAO/WHO standards Database Links
FooDB Database Links
Formulae
GeneOntology Database Links
GlyGen Database Links
GlyTouCan Database Links
Gmelin Registry Numbers
Golm Database Links
HMDB Database Links
IEDB Database Links
INN
IUPAC Names
InChI
InChIKey
IntAct Database Links
IntEnz Database Links
KEGG COMPOUND Database Links
KEGG DRUG Database Links
KEGG GLYCAN Database Links
KNApSAcK Database Links
LINCS Database Links
LIPID MAPS class Database Links
LIPID MAPS instance Database Links
Last Modified
Mass
MassBank Database Links
MetaCyc Database Links
MetaboLights Database Links
MolBase Database Links
Monoisotopic Mass
NMRShiftDB Database Links
NURSA Database Links
PDB Database Links
PDBeChem Database Links
PPDB Database Links
PPR Links
Patent Database Links
Pesticides Database Links
PubChem Database Links
PubMed Central citation Links
PubMed citation Links
Pubchem Database Links
RESID Database Links
Reactome Database Links
Reaxys Registry Numbers
Rhea Database Links
SABIO-RK Database Links
SMID Database Links
SMILES
Secondary ChEBI ID
Star
SureChEMBL Database Links
SwissLipids Database Links
Synonyms
UM-BBD compID Links
UniProt Database Links
VSDB Database Links
VirtualMetabolicHuman Database Links
WebElements Database Links
Wikipedia Database Links
YMDB Database Links
                        */

                    }
                }
            }
            $num++;
            if ($num > 100){
                //break;
            }
        }
    }
    catch (Exception $e) {
        echo $e.PHP_EOL;
    }

    $unique = array_unique($footerHeadArray);
    $unique = array_values($unique);
    sort($unique);
    echo "########".PHP_EOL;
    foreach ( $unique as $head ) {
        echo $head.PHP_EOL;
    }


    /*
    $file = $sdffile.".ttl";
    $result = file_put_contents($file, $ttl, FILE_APPEND | LOCK_EX);
    
    if ( $result === 0 ) {
        echo "書き込み失敗\n";
    } else {
        echo "書き込み成功：" . $result . " Byteの文字列の書込みをおこないました\n";
    }
    */
}
?>