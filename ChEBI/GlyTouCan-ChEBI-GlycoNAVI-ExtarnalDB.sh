#!/usr/bin/env bash

EP=https://test.sparql.glyconavi.org/sparql
LIMIT=500

function query() {
  local offset=${1:-0}
  shift

  local sparql=$(cat <<EOS | sed -e 's/^\s\+//' | tr '\n' ' '
SELECT distinct str(?gtc) AS ?gtc_id str(?bd_id) AS ?ex_id
FROM <http://glyconavi.org/chebi-db-inchi>
WHERE {
?s ?p ?o1, ?o.
?o a <http://identifiers.org/glyconavi/GN_GlyTouCan> .
?o <http://purl.org/dc/terms/identifier> ?gtc .
?o1 <http://purl.org/dc/terms/identifier> ?id .
?o1 a ?type .
FILTER (?gtc != ?id )
BIND (URI(CONCAT(str(?type),"_")) as ?db)
BIND (URI(CONCAT(str(?db),?id)) as ?dbId)
BIND( REPLACE( str(?dbId), "http://identifiers.org/glyconavi/" , "" ) AS ?bd_id )
}
LIMIT ${LIMIT}
OFFSET ${offset}
EOS
)

  curl -XPOST -H 'Accept: text/tab-separated-values' --data-urlencode "query=${sparql}" ${EP} 2>/dev/null | sed '1d'

  return $?
}

function query_recursive() {
  local offset=${1:-0}
  shift

  result=$(query ${offset})

  local status=$?
  if [ ${status} -ne 0 ]; then
    printf "\ncurl returned exit code %s\n" ${status} >&2
    exit ${status}
  fi

  if [ -n "${result}" ]; then
    printf "\rObtained %s entries..." $((offset + $(echo -e "${result}" | wc -l))) >&2
    echo -e "${result}"
    query_recursive $((offset + LIMIT))
  else
    printf "\n" >&2
  fi
}

query_recursive

