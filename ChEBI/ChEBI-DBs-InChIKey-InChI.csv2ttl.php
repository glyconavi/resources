<?php
ini_set("memory_limit", "1000M");
setlocale(LC_ALL,'ja_JP.UTF-8');
$csvfile = "ChEBI-DBs-InChIKey-InChI.csv";

if (count($argv)>0){
    $csvfile = $argv[1];
}

$row = 1;
// ファイルが存在しているかチェックする
if (($handle = fopen($csvfile, "r")) !== FALSE) {


$ttl = "@prefix base: <http://purl.obolibrary.org/obo/CHEBI_> .".PHP_EOL;
$ttl .= "@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .".PHP_EOL;
$ttl .= "@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .".PHP_EOL;
$ttl .= "@prefix dcterms: <http://purl.org/dc/terms/> .".PHP_EOL;

//$ttl .= "@prefix ChEBI: <http://identifiers.org/CHEBI/> .".PHP_EOL;
$ttl .= "@prefix GlyTouCan: <http://identifiers.org/glytoucan/> .".PHP_EOL;
$ttl .= "@prefix KNApSAcK: <http://identifiers.org/knapsack/> .".PHP_EOL;
$ttl .= "@prefix KEGG: <http://identifiers.org/kegg.compound/> .".PHP_EOL;
$ttl .= "@prefix CAS: <http://identifiers.org/cas/> .".PHP_EOL;
$ttl .= "@prefix LINCS: <http://identifiers.org/lincs.smallmolecule/> .".PHP_EOL;
$ttl .= "@prefix DrugBank: <http://identifiers.org/drugbank/> .".PHP_EOL;
$ttl .= "@prefix PDBeChem: <http://identifiers.org/pdb-ccd/> .".PHP_EOL;
$ttl .= "@prefix PMID: <http://identifiers.org/pubmed/> .".PHP_EOL;
$ttl .= "@prefix HMDB: <http://identifiers.org/hmdb/> .".PHP_EOL;
$ttl .= "@prefix Wikipedia: <http://identifiers.org/wikipedia.en/> .".PHP_EOL;
$ttl .= "@prefix LIPID_MAPS_instance: <http://identifiers.org/lipidmaps/> .".PHP_EOL;
$ttl .= "@prefix Chemspider: <http://identifiers.org/chemspider/> .".PHP_EOL;
$ttl .= "@prefix Pubchem: <http://identifiers.org/pubchem.compound/> .".PHP_EOL;
$ttl .= "@prefix PDB: <http://identifiers.org/pdb/> .".PHP_EOL;
$ttl .= "@prefix MetaCyc: <http://identifiers.org/metacyc.compound/> .".PHP_EOL;
$ttl .= "@prefix SIO: <http://semanticscience.org/resource/> .".PHP_EOL;
$ttl .= "@prefix InChIKey: <http://identifiers.org/inchikey/> .".PHP_EOL;
$ttl .= "@prefix InChI: <http://identifiers.org/inchi/> .".PHP_EOL;



$ttl .= "@prefix db: <http://identifiers.org/glyconavi/GN_> .".PHP_EOL;

// URL TEMPLATE
//$ChEBIURL = "http://identifiers.org/CHEBI:"; // https://identifiers.org/CHEBI:36927
// URI TEMPLATE
//$ChEBIURI = "http://identifiers.org/CHEBI/"; // https://identifiers.org/CHEBI:36927
//$GlyTouCanURI = "http://identifiers.org/glytoucan/"; // https://identifiers.org/glytoucan:G00054MO
//$KNApSAcKURI = "http://identifiers.org/knapsack/"; // https://identifiers.org/knapsack:C00000001
//$KEGGURI = "http://identifiers.org/kegg.compound/"; // https://identifiers.org/kegg.compound:C12345
//$CASURI = "http://identifiers.org/cas/";  // https://identifiers.org/cas:50-00-0
//$LINCSURI = "http://identifiers.org/lincs.smallmolecule/"; // https://identifiers.org/lincs.smallmolecule:LSM-6306
//$DrugBankURI = "http://identifiers.org/drugbank/"; // https://identifiers.org/drugbank:DB00001
//$PDBeChemURI = "http://identifiers.org/pdb-ccd/"; // https://identifiers.org/pdb-ccd:AB0
//$PMIDURI = "http://identifiers.org/pubmed/"; //   https://identifiers.org/pubmed:16333295
//$HMDBURI = "http://identifiers.org/hmdb/"; //   https://identifiers.org/hmdb:HMDB00001
//$WikipediaURI = "http://identifiers.org/wikipedia.en/"; // https://identifiers.org/wikipedia.en:SM_UB-81
//$LIPID_MAPS_instanceURI = "http://identifiers.org/lipidmaps/";  // https://identifiers.org/lipidmaps:LMPR0102010012
//$ChemspiderURI = "http://identifiers.org/chemspider/"; // https://identifiers.org/chemspider:56586
//$PubchemURI = "http://identifiers.org/pubchem.compound/";  // https://identifiers.org/pubchem.compound:100101
//$PDBURI = "http://identifiers.org/pdb/"; // https://identifiers.org/pdb:2gc4

// https://identifiers.org/metacyc.compound:CPD-10330

// <a RDF data>` dcterms:license <http://creativecommons.org/licenses/by-sa/4.0/> . 
// <http://creativecommons.org/licenses/by-sa/4.0/> a <http://purl.org/dc/terms/LicenseDocument> .
// ex:111 rdfs:seeAlso <http://identifiers.org/pfam/PF01590> . 
// pdb:2gc4 dcterms:identifier "2gc4" . 
// <a resource> dcterms:references pubmed:24495517 .
// pubmed:24495517 a bibo:Article .



while (($row = fgetcsv($handle))) {
        
        // $row[0]
        $ChEBIId = "";
        // $row[1]
        $GlyTouCanId = "";
        $KNApSAcKId = "";
        $KEGGId = "";
        $CASId = "";
        $LINCSId = "";
        $DrugBankId = "";
        $PDBeChemId = "";
        $PMIDId = "";
        $HMDBId = "";
        $WikipediaId = "";
        $LIPID_MAPS_instanceId = "";
        $ChemspiderId = "";
        $PubchemId = "";
        $PDBId = "";
        // $row[2]
        $InChIKey = "";
        // $row[3]
        $InChI = "";


        if (count($row) > 3) {
            if(strpos($row[0],"ChEBI") !== false){
                //  "ChEBI"[0] , "DB:"[1] , "InChIKEy"[2] , "InChI"[3] ,
                
                $ChEBIId = trim($row[0]);
                $ChEBIId = str_replace("ChEBI:", "", $ChEBIId);

                // ttl
                $ttl .= "base:".$ChEBIId.PHP_EOL;

                $ttl .= "\ta\tSIO:SIO_010072 ;".PHP_EOL; // organic molecule
                $ttl .= "\trdfs:label\t\"Organic Molecule CHEBI:".$Id."\" ;".PHP_EOL;




                // https://identifiers.org/inchi:InChI=1S/C2H6O/c1-2-3/h3H,2H2,1H3
                // https://identifiers.org/inchikey:RYYVLZVUVIJVGH-UHFFFAOYSA-N

                // URI
                // http://identifiers.org/inchi/urlencode(InChI=1S/C2H6O/c1-2-3/h3H,2H2,1H3)
                // http://identifiers.org/inchikey/RYYVLZVUVIJVGH-UHFFFAOYSA-N

                $InChIKey = trim($row[2]);
                $InChI = trim($row[3]);


                $ttl .= "\tSIO:SIO_000008\tInChI:".urlencode($InChI)." ;".PHP_EOL;
                $ttl .= "\tSIO:SIO_000008\tInChIKey:".$InChIKey." ;".PHP_EOL;


                
                $name = trim($row[1]);
                
                if(strpos($name,'GlyTouCan') !== false){
                    $GlyTouCanId = $name;
                    $ttl .= "\trdfs:seeAlso\t".$GlyTouCanId." ;".PHP_EOL;
                }
                else if(strpos($name,'KNApSAcK') !== false){
                    $KNApSAcKId = $name;
                    $ttl .= "\trdfs:seeAlso\t".$KNApSAcKId." ;".PHP_EOL;
                }
                else if(strpos($name,'KEGG') !== false){
                    if(strpos($name," ") === false){
                        $KEGGId = $name;
                        $ttl .= "\trdfs:seeAlso\t".$KEGGId." ;".PHP_EOL;
                    }
                }
                else if(strpos($name,'CAS') !== false){
                    $CASId = $name;
                    if(strpos($name, "/") === false){
                        $ttl .= "\trdfs:seeAlso\t".$CASId." ;".PHP_EOL;
                    }
                }
                else if(strpos($name,'LINCS') !== false){
                    $LINCSId = $name;
                    $ttl .= "\trdfs:seeAlso\t".$LINCSId." ;".PHP_EOL;
                }
                else if(strpos($name,'DrugBank') !== false){
                    $DrugBankId = $name;
                    $ttl .= "\trdfs:seeAlso\t".$DrugBankId." ;".PHP_EOL;
                }
                else if(strpos($name,'PDBeChem') !== false){
                    $PDBeChemId = $name;
                    $ttl .= "\trdfs:seeAlso\t".$PDBeChemId." ;".PHP_EOL;
                }
                else if(strpos($name,'PMID') !== false){
                    $PMIDId = $name;
                    $ttl .= "\trdfs:references\t".$PMIDId." ;".PHP_EOL;   
                }
                else if(strpos($name,'HMDB') !== false){
                    $HMDBId = $name;
                    $ttl .= "\trdfs:seeAlso\t".$HMDBId." ;".PHP_EOL;
                }
                else if(strpos($name,'Wikipedia') !== false){
                    $WikipediaId = $name;
                    //$ttl .= "\trdfs:seeAlso\t".$WikipediaId." ;".PHP_EOL;
                    $Id = str_replace("Wikipedia:", "", $WikipediaId);
                    $ttl .= "\trdfs:seeAlso\t"."Wikipedia:".urlencode($Id)." ;".PHP_EOL;
                }
                else if(strpos($name,'LIPID_MAPS_instance') !== false){
                    $LIPID_MAPS_instanceId = $name;
                    $ttl .= "\trdfs:seeAlso\t".$LIPID_MAPS_instanceId." ;".PHP_EOL;
                }
                else if(strpos($name,'Chemspider') !== false){
                    $ChemspiderId = $name;
                    $ttl .= "\trdfs:seeAlso\t".$ChemspiderId." ;".PHP_EOL;
                }
                else if(strpos($name,'Pubchem') !== false){
                    $PubchemId = $name;
                    $ttl .= "\trdfs:seeAlso\t".$PubchemId." ;".PHP_EOL;
                }
                else if(strpos($name,'PDB') !== false  && strpos($name,'BPDB') === false && strpos($name,'PPDB') === false && strpos($name,'PDBeChem') === false){
                    $PDBId = $name;
                    $ttl .= "\trdfs:seeAlso\t".$PDBId." ;".PHP_EOL;
                }



                // ttl
                $ttl .= "\tdcterms:identifier\t\"CHEBI:".$ChEBIId."\" .".PHP_EOL;
                //$ttl .= "\trdfs:seeAlso\tChEBI:".$ChEBIId." .".PHP_EOL;


                // class identifier
                if(strpos($name,'GlyTouCan') !== false){
                    $GlyTouCanId = $name;
                    $ttl .= $GlyTouCanId."\ta\tdb:GlyTouCan ;".PHP_EOL;
                    $Id = str_replace("GlyTouCan:", "", $GlyTouCanId);
                    $ttl .= "\trdfs:label\t\"GlyTouCan:".$Id."\" ;".PHP_EOL;
                    $ttl .= "\tdcterms:identifier\t\"".$Id."\" .".PHP_EOL;
                }
                else if(strpos($name,'KNApSAcK') !== false){
                    $KNApSAcKId = $name;
                    $ttl .= $KNApSAcKId."\ta\tdb:KNApSAcK ;".PHP_EOL;
                    $Id = str_replace("KNApSAcK:", "", $KNApSAcKId);
                    $ttl .= "\trdfs:label\t\"KNApSAcK:".$Id."\" ;".PHP_EOL;
                    $ttl .= "\tdcterms:identifier\t\"".$Id."\" .".PHP_EOL;
                }
                else if(strpos($name,'KEGG') !== false){
                    if(strpos($name, " ") === false){
                        $KEGGId = $name;
                        $Id = str_replace("KEGG:", "", $KEGGId);
                        $ttl .= "KEGG:".urlencode($Id)."\ta\tdb:KEGG ;".PHP_EOL;
                        $ttl .= "\trdfs:label\t\"KEGG:".$Id."\" ;".PHP_EOL;
                        $ttl .= "\tdcterms:identifier\t\"".$Id."\" .".PHP_EOL;
                    }
                }
                else if(strpos($name,'CAS') !== false){
                    if(strpos($name, "/") === false){
                        $CASId = $name;
                        $ttl .= $CASId."\ta\tdb:CAS ;".PHP_EOL;
                        $Id = str_replace("CAS:", "", $CASId);
                        $ttl .= "\trdfs:label\t\"CAS(Chemical Abstract Service):".$Id."\" ;".PHP_EOL;
                        $ttl .= "\tdcterms:identifier\t\"".$Id."\" .".PHP_EOL;
                    }
                }
                else if(strpos($name,'LINCS') !== false){
                    $LINCSId = $name;
                    $ttl .= $LINCSId."\ta\tdb:LINCS ;".PHP_EOL;
                    $Id = str_replace("LINCS:", "", $LINCSId);
                    $ttl .= "\trdfs:label\t\"LINCS:".$Id."\" ;".PHP_EOL;
                    $ttl .= "\tdcterms:identifier\t\"".$Id."\" .".PHP_EOL;
                }
                else if(strpos($name,'DrugBank') !== false){
                    $DrugBankId = $name;
                    $ttl .= $DrugBankId."\ta\tdb:DrugBank ;".PHP_EOL;
                    $Id = str_replace("DrugBank:", "", $DrugBankId);
                    $ttl .= "\trdfs:label\t\"DrugBank:".$Id."\" ;".PHP_EOL;
                    $ttl .= "\tdcterms:identifier\t\"".$Id."\" .".PHP_EOL;
                }
                else if(strpos($name,'PDBeChem') !== false){
                    $PDBeChemId = $name;
                    $ttl .= $PDBeChemId."\ta\tdb:PDBeChem ;".PHP_EOL;
                    $Id = str_replace("PDBeChem:", "", $PDBeChemId);
                    $ttl .= "\trdfs:label\t\"PDB(Protein Data Bank) Chemical Dictionary:".$Id."\" ;".PHP_EOL;
                    $ttl .= "\tdcterms:identifier\t\"".$Id."\" .".PHP_EOL;
                }
                else if(strpos($name,'PMID') !== false){
                    $PMIDId = $name;
                    $ttl .= $PMIDId."\ta\tdb:PubMed ;".PHP_EOL;
                    $Id = str_replace("PMID:", "", $PMIDId);
                    $ttl .= "\trdfs:label\t\"PubMed:".$Id."\" ;".PHP_EOL;
                    $ttl .= "\tdcterms:identifier\t\"".$Id."\" .".PHP_EOL;
                }
                else if(strpos($name,'HMDB') !== false){
                    $HMDBId = $name;
                    $ttl .= $HMDBId."\ta\tdb:HMDB ;".PHP_EOL;
                    $Id = str_replace("HMDB:", "", $HMDBId);
                    $ttl .= "\trdfs:label\t\"HMDB(Human Metabolome Database):".$Id."\" ;".PHP_EOL;
                    $ttl .= "\tdcterms:identifier\t\"".$Id."\" .".PHP_EOL;
                }
                else if(strpos($name,'Wikipedia') !== false){
                    $WikipediaId = $name;
                    $Id = str_replace("Wikipedia:", "", $WikipediaId);
                    $ttl .= "Wikipedia:".urlencode($Id)."\ta\tdb:Wikipedia ;".PHP_EOL;
                    $ttl .= "\trdfs:label\t\"Wikipedia:".$Id."\" ;".PHP_EOL;
                    $ttl .= "\tdcterms:identifier\t\"".$Id."\" .".PHP_EOL;
                }
                else if(strpos($name,'LIPID_MAPS_instance') !== false){
                    $LIPID_MAPS_instanceId = $name;
                    $ttl .= $LIPID_MAPS_instanceId."\ta\tdb:LIPID_MAPS ;".PHP_EOL;
                    $Id = str_replace("LIPID_MAPS_instance:", "", $LIPID_MAPS_instanceId);
                    $ttl .= "\trdfs:label\t\"LIPID MAPS:".$Id."\" ;".PHP_EOL;
                    $ttl .= "\tdcterms:identifier\t\"".$Id."\" .".PHP_EOL;
                }
                else if(strpos($name,'Chemspider') !== false){
                    $ChemspiderId = $name;
                    $ttl .= $ChemspiderId."\ta\tdb:Chemspider ;".PHP_EOL;
                    $Id = str_replace("Chemspider:", "", $ChemspiderId);
                    $ttl .= "\trdfs:label\t\"Chemspider:".$Id."\" ;".PHP_EOL;
                    $ttl .= "\tdcterms:identifier\t\"".$Id."\" .".PHP_EOL;
                }
                else if(strpos($name,'Pubchem') !== false){
                    $PubchemId = $name;
                    $ttl .= $PubchemId."\ta\tdb:Pubchem ;".PHP_EOL;
                    $Id = str_replace("Pubchem:", "", $PubchemId);
                    $ttl .= "\trdfs:label\t\"Pubchem:".$Id."\" ;".PHP_EOL;
                    $ttl .= "\tdcterms:identifier\t\"".$Id."\" .".PHP_EOL;
                }
                else if(strpos($name,'PDB') !== false && strpos($name,'BPDB') === false && strpos($name,'PPDB') === false && strpos($name,'PDBeChem') === false){
                    $PDBId = $name;
                    $ttl .= $PDBId."\ta\tdb:PDB ;".PHP_EOL;
                    $Id = str_replace("PDB:", "", $PDBId);
                    $ttl .= "\trdfs:label\t\"PDB(Protein Data Bank):".$Id."\" ;".PHP_EOL;
                    $ttl .= "\tdcterms:identifier\t\"".$Id."\" .".PHP_EOL;
                }


                $ttl .= "InChI:".urlencode($InChI)."\tSIO:SIO_000300\t\"".$InChI."\" .".PHP_EOL;
                $ttl .= "InChI:".urlencode($InChI)."\trdfs:label\t\"InChI(International Chemical Identifier)\" .".PHP_EOL;
                $ttl .= "InChI:".urlencode($InChI)."\trdf:type\tSIO:CHEMINF_000113 .".PHP_EOL;

                $ttl .= "InChIKey:".urlencode($InChIKey)."\tSIO:SIO_000300\t\"".$InChIKey."\" .".PHP_EOL;
                $ttl .= "InChI:".urlencode($InChI)."\trdfs:label\t\"InChI(International Chemical Identifier)Key\" .".PHP_EOL;
                $ttl .= "InChIKey:".urlencode($InChIKey)."\trdf:type\tSIO:CHEMINF_000059 .".PHP_EOL;

                echo $ChEBIId."\n";
            }
        }
    }
    $file = $csvfile.".ttl";
    $result = file_put_contents($file, $ttl, FILE_APPEND | LOCK_EX);
    
    if ( $result === 0 ) {
        echo "書き込み失敗\n";
    } else {
        echo "書き込み成功：" . $result . " Byteの文字列の書込みをおこないました\n";
    }

    fclose($handle);
}
?>

