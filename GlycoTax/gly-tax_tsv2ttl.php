<?php
ini_set("memory_limit", "1000M");
setlocale(LC_ALL,'ja_JP.UTF-8');
$path = "2021-12-28_GlyConnect_gly-tax_result.tsv";
$DB = "GlycoNAVI";
if (count($argv)>2){
    $path = $argv[1];
    $DB = $argv[2];
}


// ファイルが存在しているかチェックする
if (($handle = fopen($path, "r")) !== FALSE) {

// TSVファイルデータ読み込み
//$file = new SplFileObject($path);
//$file->setFlags(SplFileObject::READ_CSV);
//$file->setCsvControl("t");


$ttl = "@prefix gtc: <http://identifiers.org/glytoucan/> .".PHP_EOL;
$ttl .= "@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .".PHP_EOL;
$ttl .= "@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .".PHP_EOL;
$ttl .= "@prefix dcterms: <http://purl.org/dc/terms/> .".PHP_EOL;
$ttl .= "@prefix glycan: <http://purl.jp/bio/12/glyco/glycan#> .".PHP_EOL;
//$ttl .= "@prefix ChEBI: <http://identifiers.org/CHEBI/> .".PHP_EOL;
$ttl .= "@prefix sou: <http://rdf.glycoinfo.org/source/> .".PHP_EOL;
//$ttl .= "@prefix SIO: <http://semanticscience.org/resource/> .".PHP_EOL;
$ttl .= "@prefix tax: <http://glyconavi.org/glycan/tax/> .".PHP_EOL;
$ttl .= "@prefix gn: <http://glyconavi.org/owl/> .".PHP_EOL;

//foreach($file as $row) {
$count =0;
while ($row = fgetcsv($handle, 0, "\t")) {
    $count++;
    if($count > 1){
        $GlyTouCanId = "";
        $name = "";
        $taxId = "";

        //echo "COUNT:\t".count($row)."\n";

            if(count($row) === 3){
                //  "ChEBI"[0] , "DB:"[1] , "InChIKEy"[2] , "InChI"[3] ,
                
                $GlyTouCanId = trim($row[0]);
                $name  = trim($row[1]);
                $taxId = trim($row[2]);

                // ttl
                $ttl .= "tax:".$GlyTouCanId."_".$taxId.PHP_EOL;
                $ttl .= "\trdf:type\tgn:GlycoTax;".PHP_EOL;
                $ttl .= "\tdcterms:identifier\t\"".$GlyTouCanId."_".$taxId."\" ;".PHP_EOL;
                $ttl .= "\tgn:from\tgn:".$DB." ;".PHP_EOL;
                $ttl .= "\tglycan:has_resource_entry\tgtc:".$GlyTouCanId." ;".PHP_EOL;
                $ttl .= "\trdfs:label\t\"".$DB.":".$GlyTouCanId."_".$taxId."\" .".PHP_EOL;

                $ttl .= "gn:".$DB.PHP_EOL;
                $ttl .= "\trdfs:label\t\"".$DB."\" .".PHP_EOL;

                $ttl .= "gtc:".$GlyTouCanId.PHP_EOL;
                $ttl .= "\trdf:type\tglycan:Saccharide;".PHP_EOL;
                $ttl .= "\tglycan:is_from_source\tsou:".$taxId." ;".PHP_EOL;
                $ttl .= "\tdcterms:identifier\t\"".$GlyTouCanId."\" .".PHP_EOL;

                $ttl .= "sou:".$taxId.PHP_EOL;
                $ttl .= "\trdf:type\tglycan:Source;".PHP_EOL;
                $ttl .= "\tdcterms:identifier\t\"".$taxId."\" ;".PHP_EOL;
                $ttl .= "\trdfs:label\t\"".$name."\" .".PHP_EOL;


                echo $GlyTouCanId."\t".$name."\t".$taxId."\n";
            }
        }
        
    }
    $file = $path.".ttl";
    $result = file_put_contents($file, $ttl, FILE_APPEND | LOCK_EX);
    
    if ( $result === 0 ) {
        echo "書き込み失敗\n";
    } else {
        echo "書き込み成功：" . $result . " Byteの文字列の書込みをおこないました\n";
    }

    fclose($handle);
}
?>

