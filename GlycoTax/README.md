# Glycan and Taxonomy

# data source

* GlyCosmos: bcsdb, glycoepitope, glycome-db

https://sparqlist.glyconavi.org/glycosmos_gly_tax

* GlyConnect

https://sparqlist.glyconavi.org/glyconnect_gly_tax


# tsv to ttl

* GlyConnect
```php
php gly-tax_tsv2ttl.php ./2021-12-28_GlyConnect_gly-tax_result.tsv GlyConnect
```

* GlyCosmos
```php
php gly-tax_tsv2ttl.php ./2021-12-28_GlyCosmos_gly-tax_result.tsv GlyCosmos
```



* GlycoNAVI

TODO:



